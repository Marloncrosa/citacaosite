module.exports = {
    globDirectory: 'public/modules/site/',
    globPatterns: [
        "**/*.{css,woff,eot,svg,ttf,gif,js,txt,json,jpg,png,xml,ico,md,webmanifest,scss,woff2}",
        'offline'
    ],
    swSrc: 'Modules/Site/Assets/js/service-worker.js',
    swDest: 'public/modules/site/js/service-worker.js',
    globIgnores: [
        '../workbox-cli-config.js',
        'uploads/**',
        'public/uploads/**'
    ]
};

