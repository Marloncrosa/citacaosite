let mix = require('laravel-mix');
let webpack = require('webpack');

// Disable success notifications
mix.disableNotifications();

/* Allow multiple Laravel Mix applications*/
require('laravel-mix-merge-manifest');

mix.mergeManifest();

// Default config
mix.options({
    postCss: [
        require('autoprefixer')
    ],
    processCssUrls: false,
    autoprefixer: {
        enabled: true,
        options: {
            browsers: ['> 1%', 'last 2 versions', 'ff >= 50', 'Chrome >= 49', 'Safari >= 9', 'ie >= 15', 'edge >= 15', 'iOS >= 9', 'Android >= 4.4'],
        }
    }
});

mix.webpackConfig({
    watchOptions: {
        ignored: /node_modules/
    },
    plugins: [
        new webpack.IgnorePlugin(/^codemirror$/)
    ],
    resolve: {
        modules: [
            'node_modules'
        ],
        alias: {
            Vendor: path.resolve(__dirname, 'vendor'),

            Admin: path.resolve(__dirname, 'Modules/Admin/Assets/js'),
            AdminPlugin: path.resolve(__dirname, 'Modules/Admin/Assets/js/plugins'),

            Site: path.resolve(__dirname, 'Modules/Site/Assets/js'),
            SitePlugin: path.resolve(__dirname, 'Modules/Site/Assets/js/plugins'),

            'handlebars': 'handlebars/dist/handlebars.js',
            'CodeMirror': 'codemirror',
        }
    },
    module: {
        rules: [
            {
                test: /url\(https\:\/\/fonts.gstatic([^\)]+?\.(woff(2)?|ttf|eot|svg)[^"])/,
                loader: "file-loader?name=/public/fonts/[name].[ext]"
            }
        ],
        loaders: [
            {
                test: /jquery/,
                loader: 'expose-loader?$!expose?jQuery'
            },
            {
                test: /eonasdan-bootstrap-datetimepicker/,
                loader: 'imports-loader?define=>false,exports=>false,moment=moment'
            }
        ]
    }
});

mix
    .copy('node_modules/jquery/dist/jquery.min.js', 'public/modules/admin/js')
    .copy('Modules/Admin/Assets/manifest.json', 'public/modules/admin')
    .copy('Modules/Admin/Assets/js/myFiles/service-worker.js', 'public/modules/admin/js')

    .js('Modules/Admin/Assets/js/admin.js', 'public/modules/admin/js')
    .sourceMaps()

    .sass('Modules/Admin/Assets/scss/admin.scss', 'public/modules/admin/css', {
        includePaths: [
            'node_modules'
        ]
    })

    .copyDirectory('Modules/Admin/Assets/js/plugins/tinymce', './public/modules/admin/js/tinymce')

    .copyDirectory('Modules/Admin/Assets/metronic2/vendors/flaticon/fonts', './public/modules/admin/fonts')
    .copyDirectory('Modules/Admin/Assets/metronic2/vendors/line-awesome/fonts', './public/modules/admin/fonts')
    .copyDirectory('Modules/Admin/Assets/metronic2/vendors/metronic/fonts', './public/modules/admin/fonts')

    .copyDirectory('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/modules/admin/fonts')
    .copyDirectory('node_modules/socicon/font', 'public/modules/admin/fonts')
    .copyDirectory('node_modules/summernote/dist/font', 'public/modules/admin/fonts')

    .copyDirectory('Modules/Admin/Assets/fonts', './public/modules/admin/fonts')

    .copyDirectory('node_modules/jstree/src/themes/default', 'public/modules/admin/css/images/jstree')

    .copyDirectory('Modules/Admin/Assets/css/tinymce/skins/lightgray/fonts', './public/modules/admin/css/fonts')

    .copyDirectory('Modules/Admin/Assets/metronic2/app/media/img', './public/modules/admin/img/metronic')
    .copyDirectory('Modules/Admin/Assets/img', './public/modules/admin/img')
;

mix

    .js('Modules/Site/Assets/js/site.js', 'public/modules/site/js')
    .sass('Modules/Site/Assets/scss/site.scss', 'public/modules/site/css', {
        includePaths: [
            'node_modules'
        ]
    })
    .copy('Modules/Site/Assets/manifest.json', 'public/modules/site')
    // .copy('Modules/Site/Assets/js//service-worker.js', 'public/modules/site/js')

    .copyDirectory('node_modules/jquery/dist', 'public/modules/site/js')
    .copyDirectory('node_modules/@fortawesome/fontawesome-free/webfonts', 'public/modules/site/fonts')

    .copyDirectory('Modules/Site/Assets/fonts', 'public/modules/site/fonts')
    .copyDirectory('Modules/Site/Assets/icons', 'public/modules/site/icons')

    .copyDirectory('Modules/Site/Assets/images', 'public/modules/site/images')

// .setPublicPath('public/modules/site')
;

if (mix.inProduction()) {
    // mix
    //     .minify([
    //         './public/modules/admin/css/admin.css',
    //         './public/modules/admin/js/admin.js',
    //
    //         './public/modules/site/css/site.css',
    //         './public/modules/site/js/site.js',
    //     ])
    // .version([
    //     './public/modules/admin/css/admin.css',
    //     './public/modules/admin/js/admin.js',
    //     './public/modules/site/css/site.css',
    //     './public/modules/site/js/site.js'
    // ])
    ;
}
