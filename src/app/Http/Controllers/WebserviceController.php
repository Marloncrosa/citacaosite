<?php namespace App\Http\Controllers;

use Intervention\Image\Facades\Image;
use Modules\Admin\Entities\City;
use Modules\Admin\Entities\State;
use Modules\Admin\Entities\Country;

use Illuminate\Http\Request;
use DB;
use Illuminate\Pagination\Paginator;

class WebserviceController extends Controller
{
    public function index()
    {
    }

    private function GetBrowserAgentName($user_agent) {
        if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
        elseif (strpos($user_agent, 'Edge')) return 'Edge';
        elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
        elseif (strpos($user_agent, 'Safari')) return 'Safari';
        elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
        elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
        return 'Other';
    }

    public function states(Request $request)
    {
        $data = collect();

        try {
            $states = State::whereHas('country', function ($query) use ($request){
                $query->where('initials', $request->country_id);
            })
                ->orderBy('state', 'asc')
                ->get();

            $data->push([
                'id' => '',
                'state' => __('webservice.geral.state_found')
            ]);

            if ($states->count() > 0) {
                foreach ($states as $key => $value) {
                    $data->push([
                        'id' => $value->id,
                        'state' => $value->state
                    ]);
                }
            } else {
                throw new \Exception(__('webservice.geral.state_not_found'));
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        // Retorno
        return $data->toJson();
    }

    public function cities(Request $request)
    {
        $data = collect();

        try {
            $cities = City::where('state_id', '=', $request->state_id)
                ->orderBy('city', 'asc')
                ->get();

            $data->push([
                'id' => '',
                'city' => __('webservice.geral.city_found')
            ]);

            if ($cities->count() > 0) {
                foreach ($cities as $key => $value) {
                    $data->push([
                        'id' => $value->id,
                        'city' => $value->city
                    ]);
                }
            } else {
                throw new \Exception(__('webservice.geral.city_not_found'));
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }

        // Retorno
        return $data->toJson();
    }

    public function thumb($width = 0, $height = 0, $imagepath = null, $color = 'ffffff', $forcejpg = false)
    {
        header('Cache-Control: public, max-age=1928448000');

        // if(!is_null($imagepath)){
        if ($width == 0) $width = null;
        if ($height == 0) $height = null;

        //         dd( public_path('uploads').'/'.$imagepath );
        $imagepath = public_path('uploads') . '/' . $imagepath;

        $img = Image::cache(function ($image) use ($width, $height, $imagepath, $color) {
            if (!is_null($width) || !is_null($height)) {
                $image = $image->make($imagepath)->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $image = $image->make($imagepath);
            }
            $image = $image->resizeCanvas($width, $height, 'center', false, $color);
        }, 1, true);

        // return $img->response();

        try {

            $BrowserAgentName = strtolower($this->GetBrowserAgentName($_SERVER['HTTP_USER_AGENT']));
            if (!$forcejpg && $BrowserAgentName != 'safari' && $BrowserAgentName != 'ie') {
                return $img->response('webp', 60);
            } else {
                return $img->response(null, 60);
            }
        } catch (\Exception $e) {
            return $img->response(null, 60);
        }

    }

    public function thumbfit($width = 0, $height = 0, $imagepath = null, $color = 'ffffff', $forcejpg = false)
    {
        header('Cache-Control: public, max-age=1928448000');

        // if(!is_null($imagepath)){
        if ($width == 0) $width = null;
        if ($height == 0) $height = null;

        // dd( public_path('uploads').'/'.$imagepath );
        $imagepath = public_path('uploads') . '/' . $imagepath;

        $img = Image::cache(function ($image) use ($width, $height, $imagepath, $color) {
            if (!is_null($width) || !is_null($height)) {
                $image = $image->make($imagepath)->fit($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $image = $image->make($imagepath);
            }
            $image = $image->resizeCanvas($width, $height, 'center', false, $color);
        }, 43200, true);

        try {
            $BrowserAgentName = strtolower($this->GetBrowserAgentName($_SERVER['HTTP_USER_AGENT']));
            if (!$forcejpg && $BrowserAgentName != 'safari' && $BrowserAgentName != 'ie') {
                return $img->response('webp', 60);
            } else {
                return $img->response(null, 60);
            }
        } catch (\Exception $e) {
            return $img->response(null, 60);
        }

    }

    public function thumbpublic($width = 0, $height = 0, $imagepath = null, $color = 'ffffff', $forcejpg = false)
    {
        header('Cache-Control: public, max-age=1928448000');

        // if(!is_null($imagepath)){
        if ($width == 0) $width = null;
        if ($height == 0) $height = null;

        // dd( public_path('uploads').'/'.$imagepath );
        $imagepath = public_path() . '/' . $imagepath;

        $img = Image::cache(function ($image) use ($width, $height, $imagepath, $color) {
            if (!is_null($width) || !is_null($height)) {
                $image = $image->make($imagepath)->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $image = $image->make($imagepath);
            }
            $image = $image->resizeCanvas($width, $height, 'center', false, $color);
        }, 43200, true);

        try {
            $BrowserAgentName = strtolower($this->GetBrowserAgentName($_SERVER['HTTP_USER_AGENT']));
            if (!$forcejpg && $BrowserAgentName != 'safari' && $BrowserAgentName != 'ie') {
                return $img->response('webp', 60);
            } else {
                return $img->response(null, 60);
            }
        } catch (\Exception $e) {
            return $img->response(null, 60);
        }
    }

    public function thumbfitpublic($width = 0, $height = 0, $imagepath = null, $color = 'ffffff', $forcejpg = false)
    {
        header('Cache-Control: public, max-age=1928448000');

        // if(!is_null($imagepath)){
        if ($width == 0) $width = null;
        if ($height == 0) $height = null;

        // dd( public_path('uploads').'/'.$imagepath );
        $imagepath = public_path() . '/' . $imagepath;

        $img = Image::cache(function ($image) use ($width, $height, $imagepath, $color) {
            if (!is_null($width) || !is_null($height)) {
                $image = $image->make($imagepath)->fit($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $image = $image->make($imagepath);
            }
            $image = $image->resizeCanvas($width, $height, 'center', false, $color);
        }, 43200, true);

        try {
            $BrowserAgentName = strtolower($this->GetBrowserAgentName($_SERVER['HTTP_USER_AGENT']));
            if (!$forcejpg && $BrowserAgentName != 'safari' && $BrowserAgentName != 'ie') {
                return $img->response('webp', 60);
            } else {
                return $img->response(null, 60);
            }
        } catch (\Exception $e) {
            return $img->response(null, 60);
        }
    }

    public function thumburl($width = 0, $height = 0, $imagepath = null, $color = '333333', $forcejpg = false)
    {
        header('Cache-Control: public, max-age=1928448000');
        // if(!is_null($imagepath)){
        if ($width == 0) $width = null;
        if ($height == 0) $height = null;

        $img = Image::cache(function ($image) use ($width, $height, $imagepath, $color) {
            if (!is_null($width) || !is_null($height)) {
                if (!is_null($width) && !is_null($height)) {
                    // $image -> make($imagepath) -> resize($width, $height, function ($constraint) {
                    // 	$constraint->aspectRatio();
                    // });
                    // $image -> resizeCanvas($width, $height, 'center', false, $color);
                    // $image -> make($imagepath) -> resize($width, $height);
                    // $image -> make($imagepath) -> resizeCanvas($width, $height, 'center', true, 'ffffff');
                    // $image -> make($imagepath) -> resizeCanvas($width, $height, 'center', false, 'ffffff');
                    $image = $image->make($imagepath)->fit($width, $height, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    });
                } else {
                    // $image -> make($imagepath) -> resize($width, $height, function ($constraint) {
                    // 	$constraint->aspectRatio();
                    // });
                    // $image -> resizeCanvas($width, $height, 'center', false, $color);
                    $image = $image->make($imagepath)->fit($width, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
            } else {
                $image = $image->make($imagepath);
            }
            $image = $image->resizeCanvas($width, $height, 'center', false, $color);
        }, 43200, true);

        try {
            $BrowserAgentName = strtolower($this->GetBrowserAgentName($_SERVER['HTTP_USER_AGENT']));
            if (!$forcejpg && $BrowserAgentName != 'safari' && $BrowserAgentName != 'ie') {
                return $img->response('webp', 60);
            } else {
                return $img->response(null, 60);
            }
        } catch (\Exception $e) {
            return $img->response(null, 60);
        }

        // }
        // return false;
    }

    public function thumbwc($width = 0, $height = 0, $imagepath = null, $forcejpg = false)
    {
        header('Cache-Control: public, max-age=1928448000');

        // if(!is_null($imagepath)){
        if ($width == 0) $width = null;
        if ($height == 0) $height = null;

        // dd( public_path('uploads').'/'.$imagepath );
        $imagepath = public_path('uploads') . '/' . $imagepath;

        $img = Image::cache(function ($image) use ($width, $height, $imagepath) {
            if (!is_null($width) || !is_null($height)) {
                $image = $image->make($imagepath)->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $image = $image->make($imagepath);
            }
            $image = $image->resizeCanvas($width, $height, 'center', false);
        }, 43200, true);

        try {
            $BrowserAgentName = strtolower($this->GetBrowserAgentName($_SERVER['HTTP_USER_AGENT']));
            if (!$forcejpg && $BrowserAgentName != 'safari' && $BrowserAgentName != 'ie') {
                return $img->response('webp', 60);
            } else {
                return $img->response(null, 60);
            }
        } catch (\Exception $e) {
            return $img->response(null, 60);
        }
    }

    public function thumbfitwc($width = 0, $height = 0, $imagepath = null, $forcejpg = false)
    {
        header('Cache-Control: public, max-age=1928448000');

        // if(!is_null($imagepath)){
        if ($width == 0) $width = null;
        if ($height == 0) $height = null;

        // dd( public_path('uploads').'/'.$imagepath );
        $imagepath = public_path('uploads') . '/' . $imagepath;

        $img = Image::cache(function ($image) use ($width, $height, $imagepath) {
            if (!is_null($width) || !is_null($height)) {
                $image = $image->make($imagepath)->fit($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                });
            } else {
                $image = $image->make($imagepath);
            }
            $image = $image->resizeCanvas($width, $height, 'center', false);
        }, 43200, true);

        try {
            $BrowserAgentName = strtolower($this->GetBrowserAgentName($_SERVER['HTTP_USER_AGENT']));
            if (!$forcejpg && $BrowserAgentName != 'safari' && $BrowserAgentName != 'ie') {
                return $img->response('webp', 60);
            } else {
                return $img->response(null, 60);
            }
        } catch (\Exception $e) {
            return $img->response(null, 60);
        }
    }

}
