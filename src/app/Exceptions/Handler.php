<?php

    namespace App\Exceptions;

    use Exception;
    use Illuminate\Auth\AuthenticationException;
    use Modules\Admin\Exceptions\AuthenticationException as AuthenticationExceptionAdm;
    use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
    // use GrahamCampbell\Exceptions\NewExceptionHandler as ExceptionHandler;

    class Handler extends ExceptionHandler
    {
        /**
         * A list of the exception types that should not be reported.
         *
         * @var array
         */
        protected $dontReport = [
            \Modules\Admin\Exceptions\AuthenticationException::class,
            \Illuminate\Auth\AuthenticationException::class,
            \Illuminate\Auth\Access\AuthorizationException::class,
            \Symfony\Component\HttpKernel\Exception\HttpException::class,
            \Illuminate\Database\Eloquent\ModelNotFoundException::class,
            \Illuminate\Session\TokenMismatchException::class,
            \Illuminate\Validation\ValidationException::class,
        ];

        /**
         * Report or log an exception.
         *
         * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
         *
         * @param  \Exception  $exception
         * @return void
         */
        public function report(Exception $exception)
        {
            parent::report($exception);
        }

        /**
         * Render an exception into an HTTP response.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Exception  $exception
         * @return \Illuminate\Http\Response
         */
        public function render($request, Exception $exception)
        {
            $e = $this->prepareException($exception);

            if ($e instanceof HttpResponseException) {
                return $e->getResponse();
            } elseif ($e instanceof AuthenticationException) {
                return $this->unauthenticated($request, $e);
            } elseif ($e instanceof AuthenticationExceptionAdm) {
                return $this->unauthenticatedadm($request, $e);
            } elseif ($e instanceof ValidationException) {
                return $this->convertValidationExceptionToResponse($e, $request);
            }

            // return $this->prepareResponse($request, $e);
            return parent::render($request, $exception);
        }

        /**
         * Convert an authentication exception into an unauthenticated response.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Illuminate\Auth\AuthenticationException  $exception
         * @return \Illuminate\Http\Response
         */
        protected function unauthenticated($request, AuthenticationException $exception)
        {
            if ($request->expectsJson()) {
                return response()->json(['error' => 'Unauthenticated.'], 401);
            }

            return redirect()->guest(route('site.login'));
        }

        /**
         * Convert an authentication exception into an unauthenticated response.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Illuminate\Auth\AuthenticationException  $exception
         * @return \Illuminate\Http\Response
         */
        protected function unauthenticatedadm($request, AuthenticationExceptionAdm $exception)
        {
            if ($request->expectsJson()) {
                return response()->json(['error' => 'Unauthenticated.'], 401);
            }

            return redirect()->guest(url('admin/login'));
        }
    }
