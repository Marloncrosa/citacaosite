<?php

return [
    'geral' => [
        'state_found' => 'Selecione um estado',
        'state_not_found' => 'Estados não encontrado',
        'city_found' => 'Selecione uma cidade',
        'city_not_found' => 'Cidades não encontrados',
    ]
];
