@extends('errors::illustrated-layout')

@section('code', '503')
@section('title', 'Serviço indisponível')

@section('image')
    <div style="background-image: url('img/logo.png'); -webkit-background-size: 199px 67px;" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message', __($exception->getMessage() ?: __('Desculpa, estamos em manutenção. Tente novamente mais tarde.')))
