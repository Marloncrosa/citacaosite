@extends('errors::illustrated-layout')

@section('code', '500')
@section('title', 'Erro')

@section('image')
    <div style="background-image: url('img/logo.png'); -webkit-background-size: 199px 67px;" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message')
    Ooooooooooops, algo deu errado!
    <br><br>
    Já fomos alertados e lançaremos uma correção em breve
    <br>
    <a href="{!! url('/') !!}" class="button">
        Voltar
    </a>
@stop
