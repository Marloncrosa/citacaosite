@extends('errors::illustrated-layout')

@section('code', '419')
@section('title', 'Página Expirada')

@section('image')
    <div style="background-image: url('img/logo.png'); -webkit-background-size: 199px 67px;" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message')
    A página expirou devido a inatividade
    <br><br>
    Por favor, atualize e tente novamente.
@stop
