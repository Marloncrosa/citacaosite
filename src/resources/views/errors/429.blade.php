@extends('errors::illustrated-layout')

@section('code', '429')
@section('title', 'Erro')

@section('image')
    <div style="background-image: url('img/logo.png'); -webkit-background-size: 199px 67px;" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message')
    Muitas requisições foram feitas, aguarde um pouco e tente novamente.
@stop
