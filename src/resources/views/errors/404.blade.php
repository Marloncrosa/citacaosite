@extends('errors::illustrated-layout')

@section('code', '404')
@section('title', 'Página não encontrada')

@section('image')
    <div style="background-image: url('img/logo.png'); -webkit-background-size: 199px 67px;" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message')
    Desculpe, a página que está procurando não pode ser encontrada.
@stop

