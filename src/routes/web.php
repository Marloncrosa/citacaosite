<?php

// Route::group(['domain' => env('APP_COOKIELESS_URL'), 'middleware' => 'cookieless'], function ($router) {
Route::group(['middleware' => 'cookieless'], function ($router) {

    $router->get(
        'thumb/{width}/{height}/{imagepath}/{color?}/{forcejpg?}',
        [
            'as' => "thumb",
            'uses' => "WebserviceController@thumb"
        ]
    )
        ->where('width', '([0-9]+)')
        ->where('height', '([0-9]+)')
        ->where('imagepath', '.*(?:jpg|jpeg|gif|png)$');

    $router->get(
        'thumbwc/{width}/{height}/{imagepath}/{forcejpg?}',
        [
            'as' => "thumbwc",
            'uses' => "WebserviceController@thumbwc"
        ]
    )->where('width', '([0-9]+)')->where('height', '([0-9]+)')->where('imagepath', '.*(?:jpg|jpeg|gif|png)$');

    $router->get(
        'thumbfit/{width}/{height}/{imagepath}/{color?}/{forcejpg?}',
        [
            'as' => "thumbfit",
            'uses' => "WebserviceController@thumbfit"
        ]
    )->where('width', '([0-9]+)')->where('height', '([0-9]+)')->where('imagepath', '.*(?:jpg|jpeg|gif|png)$');

    $router->get(
        'thumbfitwc/{width}/{height}/{imagepath}/{forcejpg?}',
        [
            'as' => "thumbfitwc",
            'uses' => "WebserviceController@thumbfitwc"
        ]
    )->where('width', '([0-9]+)')->where('height', '([0-9]+)')->where('imagepath', '.*(?:jpg|jpeg|gif|png)$');

    $router->get(
        'thumburl/{width}/{height}/{imagepath}',
        [
            'as' => "thumburl",
            'uses' => "WebserviceController@thumburl"
        ]
    )->where('width', '([0-9]+)')->where('height', '([0-9]+)')->where('imagepath', '.*(?:jpg|jpeg|gif|png)$');
});

Route::group(['middleware' => 'cookieless'], function ($router) {
    $router->get(
        'thumbpublic/{width}/{height}/{imagepath}/{color?}/{forcejpg?}',
        [
            'as' => "thumbpublic",
            'uses' => "WebserviceController@thumbpublic"
        ]
    )->where('width', '([0-9]+)')->where('height', '([0-9]+)')->where('imagepath', '.*(?:jpg|jpeg|gif|png)$');

    $router->get(
        'thumbfitpublic/{width}/{height}/{imagepath}/{color?}/{forcejpg?}',
        [
            'as' => "thumbfitpublic",
            'uses' => "WebserviceController@thumbfitpublic"
        ]
    )->where('width', '([0-9]+)')->where('height', '([0-9]+)')->where('imagepath', '.*(?:jpg|jpeg|gif|png)$');
});

Route::group(['middleware' => 'web', 'prefix' => ''], function ($router) {
    $action = !is_null(Request::segment(2)) ? Request::segment(2) : 'index';

    $router->match(
        ['post', 'get'],
        'webservice/{funcao?}',
        [
            'as' => "webservice.geral",
            'uses' => "WebserviceController@{$action}"
        ]
    );

    //        $router->match(
    //            ['post','get'],
    //            'webservice/cidades',
    //            [
    //                'as' 	=> 	"webservice",
    //                'uses' 	=> 	"WebserviceController@cidades"
    //            ]
    //        );
    //
    //        $router->match(
    //            ['post','get'],
    //            'webservice/produto',
    //            [
    //                'as' 	=> 	"webservice",
    //                'uses' 	=> 	"WebserviceController@produto"
    //            ]
    //        );
});
