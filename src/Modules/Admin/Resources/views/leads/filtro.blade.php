<div class="row m--margin-bottom-20">
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
        <label for="leads_origin_id">{!! __('Origem') !!}:</label>
        <select name="leads_origin_id" class="form-control m-input" data-col-index="2">
            <option value="">{!! __('Todos') !!}</option>
            @foreach ($origins as $origin)
                <option value="{!! $origin->id !!}">{!! $origin->title !!}</option>
            @endforeach
        </select>
    </div>

    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
        <label>{!! __('Status') !!}:</label>
        <select name="status_id" class="form-control m-input" data-col-index="3">
            <option value="">{!! __('Todos') !!}</option>
            @foreach ($leads_status as $item)
                <option value="{!! $item->id !!}">{!! $item->title !!}</option>
            @endforeach
        </select>
    </div>

    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
        <label>{!! __('Operador') !!}:</label>
        <select name="operator_id" class="form-control m-input" data-col-index="5">
            <option value="">{!! __('Todos') !!}</option>
            @foreach ($operators as $operator)
                <option value="{!! $operator->id !!}">{!! $operator->name !!}</option>
            @endforeach
        </select>
    </div>

{{--    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">--}}
{{--        <label for="draft_id">{!! __('Contrato') !!}:</label>--}}
{{--        {!! Form::select(--}}
{{--              'draft_id',--}}
{{--              ['' => __('Selecionar')] + \Modules\Admin\Entities\Drafts::all()->pluck('title','id')->all(),--}}
{{--              null,--}}
{{--              ['class'=>'form-control m-select2', 'id' => 'draft_id'])--}}
{{--          !!}--}}
{{--    </div>--}}
</div>
