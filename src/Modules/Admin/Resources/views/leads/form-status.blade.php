@extends('admin::layouts.master-clear')

@section('conteudo')
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'method' => 'post', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-12">
                            <h3 class="alert" style="color: red">Por gentileza, aguardar o carregamento completo da página.</h3>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('leads_status_id', __('Status do Lead'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'leads_status_id',
                                      ['' => __('Selecionar')] + $relacionamentos['lead_status']->pluck('title','id')->all(),
                                      null,
                                      ['class'=>'form-control m-select2'])
                                  !!}
                            </div>
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
