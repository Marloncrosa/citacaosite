@extends('admin::layouts.master-clear')

@section('conteudo')
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'method' => 'post', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-12">
                            <h3 class="alert" style="color: red">Por gentileza, aguardar o carregamento completo da página.</h3>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('leads_origin_id', __('Lead Origem'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'leads_origin_id',
                                      ['' => __('Selecionar')] + $relacionamentos['lead_origin']->where('active', true)->pluck('title','id')->all(),
                                      null,
                                      ['class'=>'form-control m-select2'])
                                  !!}
                            </div>
                        </div>
                        {{--<div class="col-12 col-sm-6">
                            {!! Form::label('leads_status_id', __('Status do Lead'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'leads_status_id',
                                      ['' => __('Selecionar')] + $relacionamentos['lead_status']->pluck('title','id')->all(),
                                      null,
                                      ['class'=>'form-control m-select2'])
                                  !!}
                            </div>
                        </div>--}}
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('operator_id', __('Operador'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'operator_id',
                                      ['' => __('Selecionar')] + $relacionamentos['operator']->pluck('name','id')->all(),
                                      null,
                                      [
                                        'class'=>'form-control m-select2'
                                       ])
                                  !!}
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            {!! Form::label('client_id', __('Cliente'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'client_id',
                                      ['' => __('Selecionar')] +$relacionamentos['client']->pluck('name','id')->all(),
                                      null,
                                      [
                                        'class' => 'form-control m-select2',
                                        'disabled' => ((!empty($FormModel->operator_id))? 'true' : 'false')
                                      ])
                                  !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('name', __('Nome Completo'), [ 'class' => 'col-lg-6 col-form-label' ]) !!}
                            {!! Form::text('name', null, ['class' => 'form-control m-input', 'id' => 'name']) !!}
                        </div>

                        <div class="col-sm-6">
                            {!! Form::label('email', __('E-mail'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            {!! Form::email('email', null, ['class' => 'form-control m-input', 'id' => 'email']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-4">
                            {!! Form::label('birthdate', __('Data de Nascimento'), [ 'class' => 'col-lg-6 col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('birthdate', null, ['class' => 'form-control m-input mascara-data', 'id' => 'birthdate']) !!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            {!! Form::label('cpf', __('CPF'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            {!! Form::text('cpf', null, ['class' => 'form-control m-input mascara-cpf', 'id' => 'cpf']) !!}
                        </div>

                        <div class="col-sm-4">
                            {!! Form::label('rg', __('RG'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            {!! Form::text('rg', null, ['class' => 'form-control m-input', 'id' => 'rg']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('cellphone', __('Celular'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('cellphone', null, ['class' => 'form-control m-input mascara-telefone', 'id' => 'cellphone']) !!}
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            {!! Form::label('phone', __('Telefone'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('phone', null, ['class' => 'form-control m-input mascara-telefone', 'id' => 'phone']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-12">
                            {!! Form::label('profession', __('Profissão'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            {!! Form::text('profession', null, ['class' => 'form-control m-input', 'id' => 'profession']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-4">
                            {!! Form::label('country_id', __('País'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'country_id',
                                      ['' => __('Selecionar')] + $relacionamentos['country']->pluck('country','initials')->all(),
                                      (empty($FormModel->country_id))? 'BR' : $FormModel->country->initials,
                                      [
                                        'id'=>'country_id',
                                        'data-tags'=>'true',
                                        'class'=>'form-control m-select2',
                                        'data-value'=> (empty($FormModel->country_id))? 'BR' : $FormModel->country->initials
                                      ])
                                  !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            {!! Form::label('state_id', __('Estado'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'state_id',
                                      ['' => __('Selecionar')],
                                      null,
                                      [
                                        'id'=>'state_id',
                                        'data-tags'=>'true',
                                        'class'=>'form-control m-select2',
                                        'data-value' => (empty($FormModel->state_id))? '' : $FormModel->state_id
                                      ])
                                  !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            {!! Form::label('city_id', __('Cidade'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'city_id',
                                      ['' => __('Selecionar')],
                                      null,
                                      [
                                        'id'=>'city_id',
                                        'data-tags'=>'true',
                                        'class'=>'form-control m-select2',
                                        'data-value' => (empty($FormModel->city_id))? '' : $FormModel->city_id
                                      ])
                                  !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-3">
                            {!! Form::label('postal_code', __('CEP'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!!
                                    Form::text('postal_code', null, [
                                        'class' => 'form-control m-input mascara-cep cep-preenche',
                                        'id' => 'postal_code',
                                        'data-estado' => '#state_id',
                                        'data-cidade' => '#city_id',
                                        'data-endereco' => '#address',
                                        'data-bairro' => '#neighborhood',
                                        'data-numero' => '#number',
                                        'data-pais' => '#country_id',
                                    ])
                                !!}
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            {!! Form::label('address', __('Endereço'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('address', null, ['class' => 'form-control m-input', 'id' => 'address']) !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-3">
                            {!! Form::label('number', __('Número'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('number', null, ['class' => 'form-control m-input', 'id' => 'number']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('address_complement', __('Complemento'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('address_complement', null, ['class' => 'form-control m-input', 'id' => 'address_complement']) !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('neighborhood', __('Bairro'), [ 'class' => '' ]) !!}
                            <div class="">
                                {!! Form::text('neighborhood', null, ['class' => 'form-control m-input', 'id' => 'neighborhood']) !!}
                            </div>
                        </div>
                    </div>

                    @php
                        $questoes = Modules\Admin\Entities\LeadsQuestions::where('active', true)
                        ->where('draft_id', $FormModel->client_draft_interess)
                        ->get();
                    @endphp
                    @if(!empty($questoes))
                        @foreach ($questoes as $item)
                            <div class="form-group m-form__group row m-0">
                                <div class="col-sm-12">
                                    {!! Form::label($item->question, $item->question, [ 'class' => '' ]) !!}
                                    @php
                                        $value = '';
                                        if( $item->type_answer != 'text' ){
                                            $value = false;
                                        }
                                        if( isset($FormModel->answers['resposta'][$item->question]) ){
                                            $value = $FormModel->answers['resposta'][$item->question];
                                        }else if( isset($FormModel->answers[$item->question]) ) {
                                            $value = $FormModel->answers[$item->question];
                                        }
                                    @endphp
                                    @if ($item->type_answer == 'text')
                                        {!! Form::text('answers['. $item->question .']', $value, ['class' => 'form-control m-input', 'id' => $item->question]) !!}
                                    @else
                                        <br>
                                        {!! Form::statuscor('answers['. $item->question .']', $value) !!}
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('', __('Termo?'), ['class' => 'col-form-label']) !!}
                            <div class="row m-0">
                                {!! Form::statuscor('term', ((!empty($FormModel->term) && !is_null($FormModel->term))? $FormModel->term : null) ) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-lg-5">
                            {!! Form::label('observations', __('Obserações'), ['class' => 'col-form-label']) !!}
                            <div class="row m-0">
                                {!! Form::textarea('observations', null, ['class' => '', 'data-editor-completo' => '']) !!}
                            </div>
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
