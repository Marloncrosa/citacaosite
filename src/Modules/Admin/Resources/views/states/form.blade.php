@extends('admin::layouts.master-clear')

@section('conteudo')
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet">
                    {!! Form::model($FormModel, ['url' => $urlform, 'class' => 'm-form m-form--label-align-right form-geral', 'method' => 'post', 'files' => true]) !!}

                        {!! Form::hidden($campohidden, $valorhidden, ['required' => 'true']) !!}

                        <div class="form-group m-form__group row m-0">
                            <div class="col-12 col-sm-6">
                                {!! Form::label('state', 'Estado', ['class' => 'col-form-label']) !!}
                                {!! Form::text('state', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                            </div>
                            <div class="col-12 col-sm-6">
                                {!! Form::label('initials', 'Iniciais', ['class' => 'col-form-label']) !!}
                                {!! Form::text('initials', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                            </div>
                        </div>

                        {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
