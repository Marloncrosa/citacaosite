@extends('admin::layouts.master-clear')

@section('conteudo')
  <div class="m-content">
    <div class="row">
      <div class="col-lg-12">
        <div class="m-portlet">
          {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

            <div class="form-group m-form__group row m-0">
              <div class="col-12 col-sm-6">
                {!! Form::label('type', __('Type'), ['class' => 'col-form-label']) !!}
                {!! Form::select('type', [
                  '' => __('Select an option'),
                  'admin' => __('Admin'),
                  'franchise' => __('Franchise'),
                  'site' => __('Site'),
                  'plataform' => __('Plataform')
                ] , null , ['class' => 'form-control m-input', 'required' => 'true']) !!}
              </div>
              <div class="col-12 col-sm-6">
                {!! Form::label('version', __('Vesion'), ['class' => 'col-form-label']) !!}
                {!! Form::text('version', null, ['class' => 'form-control m-input']) !!}
              </div>
            </div>

            <div class="form-group m-form__group row m-0">
              {!! Form::label('when', __('When'), ['class' => 'col-6 col-12 col-lg-2 col-form-label']) !!}
              <div class="col-12 col-lg-5">
                {!! Form::text('when', ((empty($FormModel->date_time))? Carbon\Carbon::now()->format('d/m/Y H:i:s') : null ), ['class' => 'mascara-datahora form-control m-input', 'required' => 'true']) !!}
              </div>
            </div>

            <div class="form-group m-form__group row m-0">
              {!! Form::label('description', 'Description', ['class' => 'col-12 col-lg-2 col-form-label']) !!}
              <div class="col-12 col-lg-5">
                {!! Form::textarea('description', null, ['class' => '', 'data-editor-completo' => '']) !!}
              </div>
            </div>

            <div class="form-group m-form__group row m-0">
              {!! Form::label('', 'Active?', ['class' => 'col-12 col-lg-2 col-form-label']) !!}
              <div class="col-12 col-lg-5">
                {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
              </div>
            </div>

            {!! Form::botoesform($pagina, true, true, true, false) !!}

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@stop
