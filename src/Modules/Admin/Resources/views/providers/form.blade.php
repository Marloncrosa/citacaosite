@extends('admin::layouts.master-clear')

@section('conteudo')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet mb-0">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('fantasy_name', __('Nome da Empresa'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                        <div class="col-lg-6">
                            {!! Form::text('fantasy_name', null, ['class' => 'form-control m-input', 'id' => 'name']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('corporate_name', __('Razão Social'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                        <div class="col-lg-6">
                            {!! Form::text('corporate_name', null, ['class' => 'form-control m-input', 'id' => 'name']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('email', __('E-mail'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            {!! Form::email('email', null, ['class' => 'form-control m-input', 'id' => 'email']) !!}
                        </div>

                        <div class="col-sm-6">
                            {!! Form::label('password', 'Senha', ['class' => 'col-6 col-form-label']) !!}
                            {!! Form::password('password', array('class' => 'form-control m-input', 'required' => 'true')) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('document', __('CNPJ'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                        <div class="col-lg-6">
                            {!! Form::text('document', null, ['class' => 'form-control m-input mascara-documento', 'id' => 'document']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">

                        <div class="col-12 col-sm-6">
                            {!! Form::label('cellphone', __('Celular'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('cellphone', null, ['class' => 'form-control m-input mask-intl-phone', 'id' => 'cellphone']) !!}
                            </div>
                        </div>

                        <div class="col-12 col-sm-6">
                            {!! Form::label('phone', __('Telefone'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('phone', null, ['class' => 'form-control m-input mask-intl-phone', 'id' => 'cellphone']) !!}
                            </div>
                        </div>
                    </div>


                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-4">
                            {!! Form::label('country_id', __('País'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'country_id',
                                      ['' => __('Selecione')] + $relacionamentos['country']->pluck('country','initials')->all(),
                                      null,
                                      ['class'=>'form-control m-select2', 'id' => 'country_id'])
                                  !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            {!! Form::label('state_id', __('Estado'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'state_id',
                                      ['' => __('Selecione')],
                                      null,
                                      ['class'=>'form-control m-select2', 'id' => 'state_id'])
                                  !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            {!! Form::label('city_id', __('Cidade'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'city_id',
                                      ['' => __('Selecione')],
                                      null,
                                      ['class'=>'form-control m-select2', 'id' => 'city_id'])
                                  !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-3">
                            {!! Form::label('postal_code', __('CEP'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!!
                                    Form::text('postal_code', null, [
                                        'class' => 'form-control m-input mascara-cep cep-preenche',
                                        'id' => 'postal_code',
                                        'data-estado' => '#state_id',
                                        'data-cidade' => '#city_id',
                                        'data-endereco' => '#address',
                                        'data-bairro' => '#neighborhood',
                                        'data-numero' => '#number',
                                        'data-pais' => '#country_id',
                                    ])
                                !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('address', __('Endereço'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('address', null, ['class' => 'form-control m-input', 'id' => 'address']) !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-3">
                            {!! Form::label('number', __('Número'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('number', null, ['class' => 'form-control m-input', 'id' => 'number']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('address_complement', __('Complemento'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('address_complement', null, ['class' => 'form-control m-input', 'id' => 'address_complement']) !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('neighborhood', __('Bairro'), [ 'class' => '' ]) !!}
                            <div class="">
                                {!! Form::text('neighborhood', null, ['class' => 'form-control m-input', 'id' => 'neighborhood']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('observation', 'Observações', ['class' => 'col-12 col-lg-2 col-form-label']) !!}
                        <div class="col-12 col-lg-5">
                            {!! Form::textarea('observation', null, ['class' => '', 'data-editor-completo' => '']) !!}
                        </div>
                    </div>



                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('', __('Ativo?'), ['class' => 'col-lg-2 col-form-label']) !!}
                            {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                        </div>

                        <div class="col-sm-6">
                            {!! Form::label('', __('Liberado?'), ['class' => 'col-lg-2 col-form-label']) !!}
                            {!! Form::statuscor('released', ((!empty($FormModel->released) && !is_null($FormModel->released))? $FormModel->released : null) ) !!}
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

