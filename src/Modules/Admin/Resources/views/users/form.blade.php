@extends('admin::layouts.master-clear')

@section('conteudo')
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

    @if(!empty($FormModel))
      <div class="m-portlet m-portlet--full-height m-0">
        <div class="m-portlet__body p-0">
          <div class="m-card-profile">
            <div class="m-card-profile__title m--hide">
              {!! __('Perfil') !!}
            </div>
            <div class="m-card-profile__pic">
              <div class="m-card-profile__pic-wrapper">
                @if((!empty($FormModel->avatar_imagem) && file_exists(public_path('uploads/'.$FormModel->avatar_imagem))))
                  <img src="{{ route('thumbfit',[ 80, 80, $FormModel->avatar_imagem, 'ffffff', true]) }}"
                       alt="{!! $FormModel->nome !!}">
                @else
                  <img src="metronic/app/media/img/users/user4.jpg" alt=""/>
                @endif
              </div>
            </div>
            <div class="m-card-profile__details">
              <span class="m-card-profile__name">{!! $FormModel->nome !!}</span>
              <a href="" class="m-card-profile__email m-link">{!! $FormModel->email !!}</a>
            </div>
          </div>
        </div>
      </div>
    @endif

    <div class="m-portlet m-portlet--full-height m-portlet--tabs">
      <div class="m-portlet__head pt-0">
        <div class="m-portlet__head-tools m-0">
          <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
              role="tablist">
            <li class="nav-item m-tabs__item">
              <a class="nav-link m-tabs__link active" data-toggle="tab"
                 href="#atualizar_cadastro" role="tab">
                <i class="flaticon-share m--hide"></i>
                @if ($action != 'edit')
                  {!! __('Criar Um Novo perfil') !!}
                @else
                  {!! __('Editar Perfil') !!}
                @endif
              </a>
            </li>
            @if ($action == 'edit')
              <li class="nav-item m-tabs__item">
                <a class="nav-link m-tabs__link" data-toggle="tab"
                   href="#atualizar_senha" role="tab">
                  <i class="flaticon-share m--hide"></i>
                  Atualizar Senha
                </a>
              </li>
            @endif
          </ul>
        </div>
      </div>
      <div class="tab-content">
        <div class="tab-pane active" id="atualizar_cadastro">
          {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--fit m-form--label-align-right form-geral', 'files' => true]) !!}

          <div class="form-group m-form__group row">
            {!! Form::label('', __('Avatar'), [ 'class' => 'col-2 col-form-label' ]) !!}
            <div class="col-7">
              @if( !empty($FormModel->avatar) )
                <label for="avatar"
                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo float-left mr-2">
                  <span>

                    <span>{!! __('Selecione um avatar') !!}</span>
                    {!! Form::fileHTML('avatar', null, ['id' => 'avatar', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                  </span>
                </label>

                <a href="{{ route('thumb',[ 0, 0, $FormModel->avatar_imagem ]) }}"
                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                  {!! __('Show avatar') !!}
                </a>

                <a
                  data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel -> id}/campo/avatar/path/users/path1/{$FormModel->id}/path2/avatar") }}"
                  data-texto="Deseja remover a avatar?" data-excluir=""
                  data-remover=".itemavatar .deletfile, .itemavatar .viewfile" title="Excluir"
                  class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                  <i aria-hidden="true" class="fa fa-times"></i>
                </a>
              @else
                <label for="avatar"
                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                  <span>

                    <span>{!! __('Selecione um avatar') !!}</span>
                    {!! Form::fileHTML('avatar', null, ['id' => 'avatar', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                  </span>
                </label>
              @endif
            </div>
          </div>

          <div class="form-group m-form__group row">
            {!! Form::label('name', 'Nome', ['class' => 'col-2 col-form-label']) !!}
            <div class="col-7">
              {!! Form::text('name', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
            </div>
          </div>

          <div class="form-group m-form__group row">
            {!! Form::label('cellphone', 'Telefone', ['class' => 'col-2 col-form-label']) !!}
            <div class="col-7">
              {!! Form::text('cellphone', null, ['class' => 'form-control m-input mask-intl-phone']) !!}
            </div>
          </div>

          <div class="form-group m-form__group row">
            {!! Form::label('login', 'Login', ['class' => 'col-2 col-form-label']) !!}
            <div class="col-7">
              {!! Form::text('login', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
            </div>
          </div>

          <div class="form-group m-form__group row">
            {!! Form::label('email', 'E-mail', ['class' => 'col-2 col-form-label']) !!}
            <div class="col-7">
              {!! Form::email('email', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
            </div>
          </div>

          {{--@if ($action != 'edit')
            <div class="form-group m-form__group row">
              {!! Form::label('password', 'Nova Senha', ['class' => 'col-2 col-form-label']) !!}
              <div class="col-7">
                {!! Form::password('password', array('class' => 'form-control m-input', 'required' => 'true')) !!}
              </div>
            </div>

            <div class="form-group m-form__group row">
              {!! Form::label('password_confirmation', 'Confirmar Nova Senha', ['class' => 'col-2 col-form-label']) !!}
              <div class="col-7">
                {!! Form::password('password_confirmation', array('class' => 'form-control m-input', 'required' => 'true')) !!}
              </div>
            </div>
          @endif--}}

          <div class="form-group m-form__group row">
            {!! Form::label('', __('Ativo?'), ['class' => 'col-2 col-form-label']) !!}
            <div class="col-7">
              {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
            </div>
          </div>

          {!! Form::botoesform($pagina, $cancelar = true, $salvarecontinuar = false, $salvar = true, $limpar = false, $options = [  ]) !!}
          {!! Form::close() !!}
        </div>

        @if ($action == 'edit')
          <div class="tab-pane" id="atualizar_senha">
            {!! Form::model($FormModel, ['url' => route('admin.anyroute', [ 'slug' => $urlpass ]), 'class' => 'm-form m-form--fit m-form--label-align-right form-geral', 'autocomplete' => 'off']) !!}

            <div class="form-group m-form__group row">
              {!! Form::label('password', 'Nova Senha', ['class' => 'col-2 col-form-label']) !!}
              <div class="col-7">
                {!! Form::password('password', array('class' => 'form-control m-input', 'required' => 'true')) !!}
              </div>
            </div>

            <div class="form-group m-form__group row">
              {!! Form::label('password_confirmation', 'Confirmar Nova Senha', ['class' => 'col-2 col-form-label']) !!}
              <div class="col-7">
                {!! Form::password('password_confirmation', array('class' => 'form-control m-input', 'required' => 'true')) !!}
              </div>
            </div>


            {!! Form::botoesform($pagina, $cancelar = true, $salvarecontinuar = true, $salvar = true, $limpar = false, $options = [ 'texto-salvar' => ' Alterar senha ', 'texto-salvar-continuar' => ' Alterar senha e continuar editando ' ]) !!}
            {!! Form::close() !!}
          </div>
        @endif

      </div>
    </div>

  </div>
@stop

@push('line-js')
  <style>
    .internal-form .m-portlet {
      margin-bottom: 0 !important;
    }

    .m-portlet.m-portlet--full-height {
      position: relative;
    }

    .m-portlet .m-portlet__foot:not(.m-portlet__no-border) {
      position: fixed;
    }
  </style>
@endpush
