@extends('admin::layouts.master-clear')

@section('conteudo')
  <div class="m-content">
    <div class="row">
      <div class="col-lg-12">
        <div class="m-portlet">
          {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}


          <div class="form-group m-form__group row col m-0 baseClass">
            {!! Form::label('permissions', __('Permissions Allowed'), ['class' => 'col-2 col-form-label']) !!}
            <div class="clearfix"></div>
            <div class="col-12 form-group m-form__group">
              <div class="col-8 col-lg-4">
                <div class="input-group">
                  <input type="text" name="search" class="form-control m-input" placeholder="{!! __('Search for...') !!}">
                  <div class="input-group-append">
                    <button class="btn btn-secondary clear" type="reset">{!! __('Clear') !!}</button>
                    <button class="btn btn-secondary search" type="button">{!! __('Search') !!}</button>
                  </div>
                </div>

              </div>
              <br>
            </div>

            <div class="col-12">
              <div id="permissions" class="tree-demo" role="tree" aria-multiselectable="true" tabindex="0" aria-activedescendant="j3_3" aria-busy="false">
                <ul>
                  <li id="todas_permissioes" data-jstree='{"opened":true}' data-inputname="all_permissions" data-inputvalue="all">
                    {{--<input type="checkbox" name="permission_all"> --}}
                    {!! __('All permissions') !!}
                    <ul>
                      @foreach ($permissions as $key => $permission)
                        <li data-inputname="permission_allowed[]" data-inputvalue="{!! $permission->id !!}" data-inputchecked="{!! (!is_null($FormModel->permissions()->where('id', $permission->id)->first()))? true : false!!}">
                          {{--<input type="checkbox" name="permission_{!! $permission->id !!}" value="{!! $permission->id !!}">--}}
                          {!! $permission->name !!}
                        </li>
                      @endforeach
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          <div class="form-group m-form__group row col m-0  baseClass">
            {!! Form::label('permissions', __('Permissions Not Allowed'), ['class' => 'col-2 col-form-label']) !!}
            <div class="clearfix"></div>
            <div class="col-12 form-group m-form__group">
              <div class="col-8 col-lg-4">
                <div class="input-group">
                  <input type="text" name="search" class="form-control m-input" placeholder="{!! __('Search for...') !!}">
                  <div class="input-group-append">
                    <button class="btn btn-secondary clear" type="reset">{!! __('Clear') !!}</button>
                    <button class="btn btn-secondary search" type="button">{!! __('Search') !!}</button>
                  </div>
                </div>
              </div>
              <br>
            </div>

            <div class="col-12">
              <div id="permissions_not" class="tree-demo" role="tree" aria-multiselectable="true" tabindex="0" aria-activedescendant="j3_3" aria-busy="false">
                <ul>
                  <li id="todas_permissioes" data-jstree='{"opened":true}' data-inputname="all_permissions" data-inputvalue="all">
                    {{--<input type="checkbox" name="permission_all"> --}}
                    {!! __('All permissions') !!}
                    <ul>
                      @foreach ($permissions as $key => $permission)
                        <li data-inputname="permission_allowed[]" data-inputvalue="{!! $permission->id !!}" data-inputchecked="{!! (!is_null($FormModel->permissions()->where('id', $permission->id)->first()))? true : false!!}">
                          {{--<input type="checkbox" name="permission_{!! $permission->id !!}" value="{!! $permission->id !!}">--}}
                          {!! $permission->name !!}
                        </li>
                      @endforeach
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
          </div>

          {!! Form::botoesform($pagina, true, true, true, false) !!}

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@stop

@push('line-js')
  <script>
    $(function () {
      var tree = $('.tree-demo');

      tree.jstree({
        'plugins': ["html_data", "search", "checkbox", "realcheckboxes", "types"],
        'core': {
          "themes": {
            "responsive": false
          }
        },
        "types": {
          "default": {
            "icon": "far fa-list-alt m--font-primary"
          }
        }
      });

      tree.on('ready.jstree', function(e, data){
          $('#permissions li').each(function() {
            let element = $(this),
              inputchecked = (element.data('inputchecked'))? element.data('inputchecked') : false,
              inputname = (element.data('inputname'))? element.data('inputname') : false,
              inputvalue = (element.data('inputvalue'))? element.data('inputvalue') : false
            ;

            element.find('> .jstree-icon.jstree-checkbox').html('<input type="checkbox" class="jstree-checkbox">');

            input = element.find('input[type="checkbox"]:first');

            if(inputvalue){
              input.val(inputvalue);
            }
            if(inputname){
              input.attr('name', inputname);
            }
            if(inputchecked){
              input.prop('checked', true);
              tree.jstree('check_node', $(this));
            }

          });
      });

      tree.on('search.jstree', function (e, data) {
        if((data.res).length <= 0){
          parent.swal({
            text: '{!! __('Permission not found') !!}',
            icon: "error",
          });
        }
      });

      var to = setTimeout(function () {}, 100);
      var search = $('[name="search"]');

      search.on('keydown', function(e){
        if(e.keyCode == 13)
        {
          $(this).parent().find('button.search').trigger("click");
          return false;
        }
      });

      $('button.search').on('click', function () {
        clearTimeout(to);
        to = setTimeout(function () {
          var v = search.val();
          console.log($(this).parents('.baseClass').find('.tree-demo'))
          $(this).parents('.baseClass').find('.tree-demo').jstree(true).search(v, false, true);
        }, 250);
      });

      $('button.clear').on('click', function () {
        clearTimeout(to);
        $(this).parents('.baseClass').find('.tree-demo').jstree(true).clear_search();
        search.val('');
      });

    });
  </script>
@endpush
