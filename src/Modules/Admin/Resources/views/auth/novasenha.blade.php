@extends('admin::layouts.master-login' , ['bodyclass' => 'login'])

@section('conteudo-externo')

    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div
            class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin"
            id="m_login">

            <div
                class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content m-grid-item--center"
                style="background-image: url(img/background-workplace.jpg);background-size: cover;background-position: left;background-repeat: no-repeat;background-attachment: inherit;">
                <div class="m-grid__item">
                    {{--<h3 class="m-login__welcome">Área Exclusiva</h3>
                    <p class="m-login__msg">
                        Lorem ipsum dolor sit amet, coectetuer adipiscing
                        <br>elit sed diam nonummy et nibh euismod
                    </p>--}}
                </div>
            </div>

            <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
                <div class="m-stack m-stack--hor m-stack--desktop">
                    <div class="m-stack__item m-stack__item--fluid">
                        <div class="m-login__wrapper">
                            <div class="m-login__logo">
                                <a href="#">
                                    <img src="img/logos/logo.png" alt="{{ env('APP_SITE_TITLE', 'Site Padrão') }}"
                                         class="w-100">
                                </a>
                            </div>

                            <div class="m-login__signin">
                                <div class="m-login__head">
                                    <h3 class="m-login__title">Acesse o Admin</h3>
                                </div>

                                {!! Form::model(null, ['route' => 'admin.nova-senha.save.post', 'class' => 'm-login__form m-form', 'files' => false, 'name' => 'login', 'autocomplete' => 'on']) !!}

                                <input type="hidden" name="token" value="{{ $token }}">
                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="text" placeholder="E-mail"
                                           name="email" autocomplete="on">
                                </div>

                                <div class="form-group m-form__group">
                                    <input class="form-control m-input" type="password"
                                           placeholder="Nova Senha" name="password">
                                </div>

                                <div class="form-group m-form__group">
                                    <input class="form-control m-input m-login__form-input--last" type="password"
                                           placeholder="Confirmar Nova Senha" name="password_confirmation">
                                </div>

                                <div class="m-login__form-action">
                                    <button id="m_login_signin_submit"
                                            class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Acessar
                                    </button>
                                </div>

                                {{ Form::close() }}
                            </div>


                        </div>
                    </div>


                    <div class="m-stack__item m-stack__item--center">
                        <div class="m-login__account">
                            Sistema desenvolvido por
                            <a target="_blank"
                               href="{{ env('APP_DEV_LINK','http://GPRODATA.com.br') }}">{{ env('APP_DEV_NAME','GPRODATA') }}</a>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
@stop

@push('line-js')
    <script type="text/javascript">
        $(function () {

            $(document).off('form button', 'click');
            $('form button').off('click');

            // $('#m_login_signin_submit').off('click').on('click', function(e) {
            //     // e.preventDefault();
            //     var btn = $(this);
            //     var form = $(this).closest('form');
            //
            //     btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            // });
            // $('#m_login_forget_password_submit').off('click').on('click', function(e) {
            //     // e.preventDefault();
            //
            //     var btn = $(this);
            //     var form = $(this).closest('form');
            //
            //     btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
            // });
        });
    </script>
@endpush
