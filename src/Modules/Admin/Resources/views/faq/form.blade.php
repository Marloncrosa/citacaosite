@extends('admin::layouts.master-clear')

@section('conteudo')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet mb-0">
                    {!! Form::model($FormModel, ['url' => $urlform, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('title', 'Título', ['class' => 'col-form-label']) !!}
                            {!! Form::text('title', null, ['class' => 'slugify form-control m-input', 'required' => 'true', 'data-slugged' => 'input#slug']) !!}
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('slug', 'URL Amigável', ['class' => 'col-form-label']) !!}
                            {!! Form::text('slug', null, ['class' => 'form-control m-input']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('description', __('Descrição'), ['class' => 'col-12 col-lg-2 col-form-label']) !!}
                        <div class="col-12 col-lg-5">
                            {!! Form::textarea('description', null, ['class' => '', 'data-editor-completo' => '']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('likes', __('Likes'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                        <div class="col-lg-6">
                            {!! Form::number('likes', null, ['class' => 'form-control m-input', 'id' => 'initial_quantity']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('', __('Ativo?'), ['class' => 'col-lg-2 col-form-label']) !!}
                        <div class="col-lg-6">
                            {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

