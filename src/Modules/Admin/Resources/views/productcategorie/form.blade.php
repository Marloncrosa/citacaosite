@extends('admin::layouts.master-clear')

@section('conteudo')
    {!! Form::model($FormModel, ['url' => $urlform, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

    {!! Form::hidden($campohidden, $valorhidden, ['required' => 'true']) !!}

    <div class="form-group m-form__group row m-0">
        <div class="col-12 col-sm-6">
            {!! Form::label('title', 'Nome', ['class' => 'col-form-label']) !!}
            {!! Form::text('title', null, ['class' => 'slugify form-control m-input', 'required' => 'true', 'data-slugged' => 'input#slug']) !!}
        </div>
        <div class="col-12 col-sm-6">
            {!! Form::label('slug', 'URL Amigável', ['class' => 'col-form-label']) !!}
            {!! Form::text('slug', null, ['class' => 'form-control m-input']) !!}
        </div>
        <div class="col-12 col-sm-12">
            {!! Form::label('video_link', 'Link do vídeo', ['class' => 'col-form-label']) !!}
            {!! Form::text('video_link', null, ['class' => 'form-control m-input']) !!}
        </div>
    </div>

    <div class="form-group m-form__group row m-0">
        <div class="col-sm-4 itemimage">
            {!! Form::label('', 'Imagem de destaque', [ 'class' => 'col-form-label' ]) !!}
            <br>
            @if( !empty($FormModel->image) )
                <label for="image"
                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                    <div>
                        <div>{!! __('Selecionar') !!}</div>
                        {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                    </div>
                </label>
                <a href="{{ route('thumb',[ 0, 0, "products_categories/{$FormModel->id}/{$FormModel->image}" ]) }}"
                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                    visualizar </a>
                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image/path/products_categories/path1/{$FormModel->id}") }}"
                   data-texto="Deseja remover a image?" data-excluir=""
                   data-remover=".itemimage .deletfile, .itemimage .viewfile" title="Excluir"
                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                    <i aria-hidden="true" class="fa fa-times"></i>
                </a>
            @else
                <label for="image"
                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                    <div>
                        <div>{!! __('Selecionar') !!}</div>
                        {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                    </div>
                </label>
            @endif
        </div>

        <div class="col-sm-4">
            {!! Form::label('active', __('Ativo?'), ['class' => 'col-form-label']) !!}
            <br>
            {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
        </div>

        <div class="col-sm-4">
            {!! Form::label('featured', __('Destaque?'), ['class' => 'col-form-label']) !!}
            <br>
            {!! Form::statuscor('featured', ((!empty($FormModel->featured) && !is_null($FormModel->featured))? $FormModel->featured : null) ) !!}
        </div>
    </div>


    {!! Form::botoesform($pagina, true, true, true, false) !!}

    {!! Form::close() !!}
@stop
