@extends('admin::layouts.master-clear')

@section('conteudo')

  <div class="m-content">
    <div class="row">
      <div class="col-lg-12">
        <div class="m-portlet">
          {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

            <div class="form-group m-form__group row m-0">
              <div class="col-12 col-sm-6">
                {!! Form::label('title', __('Titulo'), ['class' => 'col-form-label']) !!}
                {!! Form::text('title', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
              </div>
            </div>

            <div class="form-group m-form__group row m-0">
              {!! Form::label('description', __('Descrição'), ['class' => 'col-12 col-lg-2 col-form-label']) !!}
              <div class="col-12 col-lg-5">
                {!! Form::textarea('description', null, ['class' => '', 'id' => 'description', 'required' => 'true', 'data-editor-completo' => '']) !!}
              </div>
            </div>

            <div class="form-group m-form__group row m-0">
              {!! Form::label('', __('Ativo?'), ['class' => 'col-lg-2 col-form-label']) !!}
              <div class="col-lg-6">
                {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
              </div>
            </div>

            {!! Form::botoesform($pagina, true, true, true, false) !!}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@stop
