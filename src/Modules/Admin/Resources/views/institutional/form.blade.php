@extends('admin::layouts.master-clear')

@section('conteudo')
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet mb-0">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('title', 'Título', ['class' => 'col-form-label']) !!}
                            {!! Form::text('title', null, ['class' => 'slugify form-control m-input', 'required' => 'true', 'data-slugged' => 'input#slug']) !!}
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('slug', 'URL Amigável', ['class' => 'col-form-label']) !!}
                            {!! Form::text('slug', null, ['class' => 'form-control m-input', 'disabled']) !!}
                        </div>
                    </div>

{{--                    <div class="form-group m-form__group row m-0">--}}
{{--                        {!! Form::label('subtitle', __('SubTítulo'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}--}}
{{--                        <div class="col-lg-6">--}}
{{--                            {!! Form::text('subtitle', null, ['class' => 'form-control m-input', 'id' => 'subtitle']) !!}--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('description1', 'Primeira Descrição', ['class' => 'col-12 col-lg-2 col-form-label']) !!}
                        <div class="col-12 col-lg-5">
                            {!! Form::textarea('description1', null, ['class' => '', 'data-editor-completo' => '']) !!}
                        </div>
                    </div>

{{--                    <div class="form-group m-form__group row m-0">--}}
{{--                        {!! Form::label('description2', 'Segunda Descrição', ['class' => 'col-12 col-lg-2 col-form-label']) !!}--}}
{{--                        <div class="col-12 col-lg-5">--}}
{{--                            {!! Form::textarea('description2', null, ['class' => '', 'data-editor-completo' => '']) !!}--}}
{{--                        </div>--}}
{{--                    </div>--}}

{{--                    <div class="form-group m-form__group row m-0">--}}
{{--                        {!! Form::label('', 'Imagem', [ 'class' => 'col-lg-2 col-form-label' ]) !!}--}}
{{--                        <div class="col-12 col-lg-6">--}}
{{--                            @if( !empty($FormModel->image) )--}}
{{--                                <label for="image" class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">--}}
{{--                                <span>--}}

{{--                                    <span>{!! __('Selecione uma imagem') !!}</span>--}}
{{--                                    {!! Form::fileHTML('image', null, ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}--}}
{{--                                </span>--}}
{{--                                </label>--}}
{{--                                <a href="{{ route('thumb',[ 0, 0, "institutional/{$FormModel->id}/{$FormModel->image}" ]) }}"--}}
{{--                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile"> visualizar image </a>--}}
{{--                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image/path/institutional/path1/{$FormModel->id}") }}"--}}
{{--                                   data-texto="Deseja remover a image?" data-excluir=""--}}
{{--                                   data-remover=".itemimage .deletfile, .itemimage .viewfile" title="Excluir"--}}
{{--                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">--}}
{{--                                    <i aria-hidden="true" class="fa fa-times"></i>--}}
{{--                                </a>--}}
{{--                            @else--}}
{{--                                <label for="image" class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">--}}
{{--                                <span>--}}

{{--                                    <span>{!! __('Selecione uma imagem') !!}</span>--}}
{{--                                    {!! Form::fileHTML('image', null, ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}--}}
{{--                                </span>--}}
{{--                                </label>--}}
{{--                            @endif--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
