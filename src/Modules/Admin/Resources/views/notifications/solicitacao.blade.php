@component('admin::mail.html.message')
{{-- Saudação --}}
@isset($saudacao)
# {{ $saudacao }}
@endisset

{{-- Linhas de Introdução --}}
@isset($introLines)
@if(is_array($introLines))
@foreach ($introLines as $line)
{{ $line }}

@endforeach
@else
  {{ $introLines }}
@endif
@endisset

{{-- Botão --}}
@isset($actionText)
<?php
	switch ($level) {
		case 'success':
			$color = 'green';
			break;
		case 'error':
			$color = 'red';
			break;
		default:
			$color = 'black';
	}
?>
@component('admin::mail.html.button', ['url' => $actionUrl, 'color' => $color])
{{ $actionText }}
@endcomponent
@endisset

{{-- Outras Linhas --}}
@isset($outroLines)
  @if(is_array($outroLines))
    @foreach ($outroLines as $line)
      {{ $line }}

    @endforeach
  @else
    {{ $outroLines }}
  @endif
@endisset

{{-- Agradecimento --}}
@if (! empty($agradecimento))
{{ $agradecimento }}
@else
{{__("Atenciosamente")}},
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('admin::mail.html.subcopy')
{{__("Se você está tendo problemas para clicar no botão")}} "{{ $actionText }}", {{__("copie e cole a url abaixo")}}
{{__("no seu navegador")}}: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent
