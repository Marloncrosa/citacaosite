@component('admin::mail.html.message')
{{-- Greeting --}}
# {!! $greeting !!}

{{-- Intro Lines --}}
@foreach ($introLines as $line)
{!! $line !!}

@endforeach

{{-- Action Button --}}
@isset($actionText)
<?php
	switch ($level) {
		case 'success':
			$color = 'green';
			break;
		case 'error':
			$color = 'red';
			break;
		default:
			$color = 'black';
	}
?>
@component('admin::mail.html.button', ['url' => $actionUrl, 'color' => $color])
{!! $actionText !!}
@endcomponent
@endisset

{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{!! $line !!}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{!! $salutation !!}
@else
{{__("Atenciosamente")}},
{!! config('app.name') !!}
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('admin::mail.html.subcopy')
{{__("Se você está tendo problemas para clicar no botão")}} "{!! $actionText !!}", {{__("copie e cole a url abaixo")}}
{{__("no seu navegador")}}: [{!! $actionUrl !!}]({!! $actionUrl !!})
@endcomponent
@endisset
@endcomponent
