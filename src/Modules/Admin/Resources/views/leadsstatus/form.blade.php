@extends('admin::layouts.master-clear')

@section('conteudo')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet mb-0">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('title', __('Título'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            {!! Form::text('title', null, ['class' => 'form-control m-input', 'id' => 'title', 'required' => 'true']) !!}
                        </div>

                        <div class="col-12 col-sm-6">
                            {!! Form::label('similars_id', __('Similar'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'similars_id',
                                      ['' => __('Selecionar')] + $relacionamentos['similars']->where('id','!=',$FormModel->id)->pluck('title','id')->all(),
                                      null,
                                      ['class'=>'form-control m-select2'])
                                  !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('', __('Ativo?'), ['class' => 'col-lg-2 col-form-label']) !!}
                            {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                        </div>

                        <div class="col-sm-6">
                            {!! Form::label('color', __('Cor'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            {!! Form::color('color', null, ['class' => 'form-control m-input']) !!}
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
