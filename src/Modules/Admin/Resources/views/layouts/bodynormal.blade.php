<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-aside-left--minimize m-brand--minimize m-footer--push m-aside--offcanvas-default">

<div class="m-grid m-grid--hor m-grid--root m-page">
    @include('admin::layouts.navegacao-user')

    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body  {{ (!isset($bodyclass) || $bodyclass != 'login')? 'small-12' : '' }}">
        @include('admin::layouts.menu-lateral')

        <div class="m-grid__item m-grid__item--fluid m-wrapper">

            @if (!isset($bodyclass) || $bodyclass != 'login')
                @include('admin::layouts.breadcrumb')
            @endif


            {{--<div class="m-content">
                @if (isset($page['subtitle']))
                    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30"
                         role="alert">
                        <div class="m-alert__icon">
                            <i class="flaticon-exclamation m--font-brand"></i>
                        </div>
                        <div class="m-alert__text">
                            {!! $page['subtitle'] !!}
                        </div>
                    </div>
                @endif
            </div>--}}

            @yield('conteudo-externo')

            @if (!isset($bodyclass) || $bodyclass != 'login')
                @yield('conteudo')
            @endif

        </div>
    </div>

</div>

@if (auth()->check())
    @include('admin::layouts.quick-sidebar')
@endif

@include('admin::layouts.alerts')

{{-- Scripts --}}
<div class="js">

    @include('admin::layouts.defaultjs')

    @stack('line-js-after-admin')

    @if(env('APP_ENV') == 'local')
        <script src="{!! mix('/modules/admin/js/admin.js') !!}" type="text/javascript"></script>
    @elseif(env('APP_ENV') == 'production')
        <script src="{!! mix('/modules/admin/js/admin.min.js') !!}" type="text/javascript"></script>
    @endif

    @stack('line-js')

    @if (isset($pagina) && isset($page_sluge) && !empty($page_sluge))
        <script type="text/javascript" charset="utf-8" async defer>
            $(function () {
                url = "{{ route('admin.anyroute', [ $page_sluge ]) }}";
                $selecionado = $('.m-menu__subnav a[href="' + url + '"]').parent();
                $selecionado.addClass('m-menu__item--active');
                $selecionado.parents('.m-menu__item--tabs ').addClass('m-menu__item--active  m-menu__item--active-tab')
                // $elpais = $selecionado.parents('.is-drilldown-submenu-parent')
                // $elpais.each(function () {
                //     $(this).find('> a').addClass('ativo')
                // })
                // $('ul > li > a[href="'+ url +'"]').parent().addClass('ativo')
            })
        </script>
    @endif
    <script type="text/javascript" charset="utf-8">
        $(function () {
            parent.$.sessionTimeout({
                title: "{!! __('Notificación') !!}",
                message: "{!! __('Su sesión está a punto de caducar') !!}",
                keepAliveUrl: "{!! route('admin.keepalive') !!}",
                keepAliveInterval: "60000",
                ajaxType: "GET",
                keepAliveButton: "{!! __('Mantente conectado') !!}",
                redirUrl: "{!! route('admin.logout') !!}",
                logoutUrl: "{!! route('admin.logout') !!}",
                logoutButton: "{!! __('Logout') !!}",
                warnAfter: '{!! (config('session.lifetime') - 10) * 60000 !!}',
                redirAfter: '{!! (config('session.lifetime')) * 60000 !!}',
                ignoreUserActivity: true,
                countdownMessage: "{!! __('Redirigiendo en') !!} {timer}",
                countdownSmart: true,
                countdownBar: true
            });

            // $('#loading').modal({
            //     backdrop: 'static',
            //     keyboard: false,
            // });

            $(document).on('submit', 'form', function(){
                parent.$('#loading').modal({
                    backdrop: 'static',
                    keyboard: false,
                });
            });

            // $.fancybox.open({
            //     src: '#loading',
            //     type: 'inline',
            //     gutter: 0,
            //     width: "100%",
            //     height: "100%",
            //     modal: true,
            //     hideScrollbar: true
            // });
        })
    </script>
</div>

</body>
