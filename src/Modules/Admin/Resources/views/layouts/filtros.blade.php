<form action="javascript:;" class="filter_form m-form m-form--fit m--margin-bottom-20">
    <div class="row m--margin-bottom-20">
        @includeIf("admin::layouts.filtro")
    </div>

    @includeIf("admin::".strtolower(camel_case($pagina)).".filtro")

    <div class="m-separator m-separator--md m-separator--dashed"></div>

    <div class="row">
        <div class="col-lg-12">
            <button class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn m-btn--icon" id="m_search">
                <span><i class="la la-search"></i><span>{!! __('Filtrar') !!}</span></span>
            </button>
            <button type="reset" class="btn btn-secondary m-btn m-btn--icon" id="m_reset">
                <span><i class="la la-close"></i><span>{!! __('Limpar') !!}</span></span>
            </button>
        </div>
    </div>
</form>

{{--
<form class=" m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
    <div class="row align-items-center">
        <div class="col-xl-8 order-2 order-xl-1">
            <div class="form-group m-form__group row align-items-center">


            </div>
        </div>
        <div class="col-xl-4 order-1 order-xl-2 m--align-right">
            <button type="submit" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                <span><i class="la la-find"></i><span>{!! __('Filter') !!}</span></span>
            </button>
            <div class="m-separator m-separator--dashed d-xl-none"></div>
        </div>
    </div>
</form>
--}}

@push('line-js')
    <script>
        $.fn.serializeFormJSON = function () {

            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
            return o;
        };

        $.extend(true, $.fn.dataTable.defaults, {
            'ajax': {
                'data': function (d) {
                    serialized = $('.filter_form').serializeFormJSON();
                    return $.extend({}, d, serialized);
                }
            }
        });

        $('.filter_form').on('submit', function () {
            table = LaravelDataTables.dataTableBuilder;
            $('[type="search"], .search').each(function () {
                table.search(this.value)
            });
            table.draw();
            return false;
        });

        $('[type="reset"]').on('click', function(){
           setTimeout(function(){
               $('.filter_form').trigger('submit');
           }, 500);
        });
    </script>
@endpush
