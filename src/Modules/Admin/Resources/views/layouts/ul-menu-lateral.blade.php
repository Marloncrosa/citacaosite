<ul class="m-menu__nav m-menu__nav--dropdown-submenu-arrow ">

    <li class="m-menu__item m-menu__item--submenu m-menu__item--submenu-fullheight" aria-haspopup="true"
        m-menu-submenu-toggle="click" m-menu-dropdown-toggle-class="m-aside-menu-overlay--on">
        <a href="javascript:;" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-customer"></i>
            <div class="m-menu__link-text">{!! __('Commercial') !!}</div>
            <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="m-menu__submenu ">
            <div class="m-menu__arrow"></div>
            <div class="m-menu__wrapper">
                <ul class="m-menu__subnav">
                    <li class="m-menu__item m-menu__item--parent m-menu__item--submenu-fullheight" aria-haspopup="true">
                        <div class="m-menu__link">
                            <div class="m-menu__link-text">{!! __('Site') !!}</div>
                        </div>
                    </li>

                    @canpermission ('Acesso ao Menu produtos')
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">{!! __('Produtos') !!}</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    @canpermission ('Listar produtos')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'clients' ]) !!}" class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Clientes') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @endcanpermission


                    @canpermission ('Acesso ao Menu produtos')
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">{!! __('Produtos') !!}</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    @canpermission ('Listar produtos')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'products' ]) !!}" class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Produtos') !!}</div>
                        </a>
                    </li>
                    @endcanpermission

                    @canpermission ('Listar produtos')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'product_categorie' ]) !!}"
                            class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Categorias do produto ') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @endcanpermission

                    @canpermission ('Acesso ao Menu Banners')
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">{!! __('Banners') !!}</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    @canpermission ('Listar Banners')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{{ route('admin.anyroute', [ 'slug' => 'banners' ]) }}" class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Todos os Banners') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @endcanpermission

                    @canpermission ('Acesso ao Menu Pages')
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">{!! __('Institucional') !!}</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    @canpermission ('Listar Pages')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'institutional' ]) !!}"
                            class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Gerenciamento') !!}</div>
                        </a>
                    </li>
                    @endcanpermission

                    @canpermission ('Listar Pages')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'logos' ]) !!}"
                            class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Logos') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @endcanpermission

                    @canpermission ('Acesso ao Menu News')
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">{!! __('News') !!}</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    @canpermission ('Listar Categorias do News')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{{ route('admin.anyroute', [ 'slug' => 'news_categories' ]) }}"
                            class="m-menu__link ">
                            <div class="m-menu__link-title">
                                <div class="m-menu__link-wrap">
                                    <div class="m-menu__link-text">{!! __('Gerenciador de Categorias') !!}</div>
                                </div>
                            </div>
                        </a>
                    </li>
                    @endcanpermission

                    @canpermission ('Listar News')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{{ route('admin.anyroute', [ 'slug' => 'news' ]) }}" class="m-menu__link ">
                            <div class="m-menu__link-title">
                                <div class="m-menu__link-wrap">
                                    <div class="m-menu__link-text">{!! __('Gerenciamento do Blog') !!}</div>
                                </div>
                            </div>
                        </a>
                    </li>
                    @endcanpermission
                    @endcanpermission


                    @canpermission ('Acesso ao Menu Portfólio')
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">{!! __('Portfólio') !!}</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    @canpermission ('Listar Portfólio')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'portfolio' ]) !!}" class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Portfólio') !!}</div>
                        </a>
                    </li>
                    @endcanpermission

                    @canpermission ('Listar Tipos Portfólio')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'portfolio_types' ]) !!}"
                            class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Tipos de Portfólio') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @endcanpermission

                    @canpermission ('Acesso ao Menu serviços')
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">{!! __('Serviços') !!}</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    @canpermission ('Listar serviços')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'service_characteristics' ]) !!}"
                            class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Características') !!}</div>
                        </a>
                    </li>
                    @endcanpermission

                    @canpermission ('Listar serviços')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'service' ]) !!}" class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Serviços') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @endcanpermission
                </ul>
            </div>
        </div>
    </li>

    <li class="m-menu__item m-menu__item--submenu m-menu__item--submenu-fullheight m-menu__item--bottom-2"
        aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-dropdown-toggle-class="m-aside-menu-overlay--on">
        <a
            href="javascript:;" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-globe"></i>
            <div class="m-menu__link-text">{!! __('Site') !!}</div>
            <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
            <div class="m-menu__arrow"></div>
            <div class="m-menu__wrapper">
                <ul class="m-menu__subnav">
                    <li class="m-menu__item m-menu__item--parent m-menu__item--submenu-fullheight" aria-haspopup="true">
                        <div class="m-menu__link">
                            <div class="m-menu__link-text">{!! __('Gestão de Dados') !!}</div>
                        </div>
                    </li>

                    @if(
                                         auth()->user()->canpermission('Acesso ao Menu leads') ||
                                         auth()->user()->canpermission('Acesso ao Menu meus leads') ||
                                         auth()->user()->canpermission('Acesso ao Menu clientes')
                                     )
                        <li class="m-menu__section ">
                            <h4 class="m-menu__section-text">{!! __('Leads') !!}</h4>
                            <i class="m-menu__section-icon flaticon-more-v2"></i>
                        </li>
                        @canpermission ('Acesso ao Menu meus leads')
                        <li class="m-menu__item " aria-haspopup="true">
                            <a
                                href="{!! route('admin.anyroute', [ 'slug' => 'operator_leads' ]) !!}"
                                class="m-menu__link ">
                                <div class="m-menu__link-text">{!! __('Meus Leads') !!}</div>
                            </a>
                        </li>
                        @endcanpermission
                        @canpermission ('Acesso ao Menu leads')
                        <li class="m-menu__item " aria-haspopup="true">
                            <a
                                href="{!! route('admin.anyroute', [ 'slug' => 'leads' ]) !!}" class="m-menu__link ">
                                <div class="m-menu__link-text">{!! __('Todos os Leads') !!}</div>
                            </a>
                        </li>
                        @endcanpermission
                    @endif

                    @canpermission ('Acesso ao Menu Contatos')
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">{!! __('Contatos') !!}</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    @canpermission ('Listar Contatos')

                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'contact' ]) !!}" class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Contatos') !!}</div>
                        </a>
                    </li>

                    @endcanpermission
                    @endcanpermission

                    @canpermission ('Acesso ao Menu FAQ')
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">{!! __('Perguntas Frequentes') !!}</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>
                    @canpermission ('Listar FAQ')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="{!! route('admin.anyroute', [ 'slug' => 'faq' ]) !!}" class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Perguntas Frequentes') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @canpermission ('Acesso ao Menu newsletter')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'newsletter_interests' ]) !!}"
                            class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Interesses da Newsletter') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @canpermission ('Acesso ao Menu newsletter')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'newsletter' ]) !!}"
                            class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Newsletter') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @canpermission ('Acesso ao Menu newsletter')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'testimony' ]) !!}"
                            class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Depoimentos') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @endcanpermission
                </ul>
            </div>
        </div>
    </li>

    <li class="m-menu__item m-menu__item--submenu m-menu__item--submenu-fullheight m-menu__item--bottom-1"
        aria-haspopup="true" m-menu-submenu-toggle="click" m-menu-dropdown-toggle-class="m-aside-menu-overlay--on">
        <a href="javascript:;" class="m-menu__link m-menu__toggle">
            <i class="m-menu__link-icon flaticon-settings"></i>
            <div class="m-menu__link-text">{!! __('Online Portal') !!}</div>
            <i class="m-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="m-menu__submenu ">
            <div class="m-menu__arrow"></div>
            <div class="m-menu__wrapper">
                <ul class="m-menu__subnav">
                    <li class="m-menu__item m-menu__item--parent m-menu__item--submenu-fullheight" aria-haspopup="true">
                        <div class="m-menu__link">
                            <div class="m-menu__link-text">{!! __('Configuration') !!}</div>
                        </div>
                    </li>

                    {{--@canpermission ('Acesso ao Menu contatos')
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">{!! __('Contatos') !!}</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>

                    @canpermission ('Acesso ao Menu assuntos contatos')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                                href="{!! route('admin.anyroute', [ 'slug' => 'contact_subjects' ]) !!}"
                                class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Contatos Assuntos') !!}</div>
                        </a>
                    </li>
                    @endcanpermission

                    @canpermission ('Acesso ao Menu newsletter')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                                href="{!! route('admin.anyroute', [ 'slug' => 'newsletter_interests' ]) !!}"
                                class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Interesses da Newsletter') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @endcanpermission--}}


                    @canpermission ('Acesso ao Menu usuários')
                    <li class="m-menu__section ">
                        <h4 class="m-menu__section-text">{!! __('Usuários') !!}</h4>
                        <i class="m-menu__section-icon flaticon-more-v2"></i>
                    </li>
                    @canpermission ('Listar usuários')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'users' ]) !!}" class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Gerenciamento de Usuários') !!}</div>
                        </a>
                    </li>
                    @endcanpermission

                    @canpermission ('Listar permissões')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'permissions' ]) !!}"
                            class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Permissões') !!}</div>
                        </a>
                    </li>
                    @endcanpermission

                    @canpermission ('Listar grupo de permissões')
                    <li class="m-menu__item " aria-haspopup="true">
                        <a
                            href="{!! route('admin.anyroute', [ 'slug' => 'groups_permission' ]) !!}"
                            class="m-menu__link ">
                            <div class="m-menu__link-text">{!! __('Grupos de Permissões') !!}</div>
                        </a>
                    </li>
                    @endcanpermission
                    @endcanpermission


                </ul>
            </div>
        </div>
    </li>

</ul>
