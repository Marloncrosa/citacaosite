<!DOCTYPE html>
<html class="{{ isset($bodyclass)? $bodyclass : 'login' }}" lang="pt-BR">
<head>
    <meta name="robots" content="noindex, nofollow">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>{{ config('app.name', Lang::get('titles.app')) }} - Admin</title>

    <meta name="description" content="">
    <meta name="author" content="Felipe Almeman">
    <meta name="generator" content="{!! env('APP_DEV_NAME') !!}">
    <meta name="keywords" content="">
    <meta name="theme-color" content="#ff5c5c">

    <meta name="Language" content="Português">
    <meta http-equiv="cache-control" content="public">
    <meta name="distribution" content="Global">
    <meta name="city" content="Londrina">
    <meta name="country" content="Brasil">
    <meta name="Copyright" content="{{ config('app.name', Lang::get('titles.app')) }}">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <base href="{{ Request::root() }}/modules/admin/">

    <meta name="viewport" content="width=device-width, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png?v=eMvWeK5T67">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png?v=eMvWeK5T67">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png?v=eMvWeK5T67">
    <link rel="icon" type="image/png" sizes="192x192" href="img/favicon/android-chrome-192x192.png?v=eMvWeK5T67">
    <link rel="icon" type="image/png" sizes="194x194" href="img/favicon/favicon-194x194.png?v=eMvWeK5T67">
    <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg?v=eMvWeK5T67" color="#5bbad5">
    <link rel="shortcut icon" href="img/favicon/favicon.ico?v=eMvWeK5T67">

    <link rel="manifest" href="manifest.json">

    <meta name="apple-mobile-web-app-title" content="{{ config('app.name', Lang::get('titles.app')) }}">
    <meta name="application-name" content="{{ config('app.name', Lang::get('titles.app')) }}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/mstile-144x144.png?v=oLBaN4wG3Y">
    <meta name="msapplication-config" content="img/favicon/browserconfig.xml?v=oLBaN4wG3Y">
    <meta name="theme-color" content="#ffffff">

    <meta property="og:type" content="website">
    <meta property="og:title" content="{{ config('app.name', Lang::get('titles.app')) }}">
    <meta property="og:site_name" content="{{ config('app.name', Lang::get('titles.app')) }}"/>
    <meta property="og:url" content="{{ env('APP_URL') }}">
    <meta property="og:image" content="{{ env('APP_URL') }}social.jpg">
    <meta property="og:description" content="">

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="{{ config('app.name', Lang::get('titles.app')) }}">
    <meta name="twitter:url" content="{{ env('APP_URL') }}">
    <meta name="twitter:image:src" content="{{ env('APP_URL') }}social.jpg">
    <meta name="twitter:description" content="">

    <meta itemprop="name" content="{{ config('app.name', Lang::get('titles.app')) }}">
    <meta itemprop="description" content="">
    <meta itemprop="image" content="{{ env('APP_URL') }}social.jpg">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function () {
                sessionStorage.fonts = true;
            }
        });
    </script>

    @if(env('APP_ENV') == 'local')
        <link href="{!! mix('/modules/admin/css/admin.css') !!}" rel="stylesheet">
    @elseif(env('APP_ENV') == 'production')
        <link href="{!! mix('/modules/admin/css/admin.min.css') !!}" rel="stylesheet">
    @endif

    <script src="js/jquery.min.js"></script>
    @stack('line-css')

    <script>
        window.app_key = 'KIGuKOoKl67uNVYXzU2EnuQeT3FHBwu48ujJVtfUhY';
    </script>

    <script>
        // API URL
        window.apiurl = '{!! route('webservice.geral', [ '' ]) !!}';
    </script>

    @if(app('env') == 'production')
        <script>
            document.onkeydown = function (e) {
                if (e.keyCode == 123) {
                    return false;
                }
                if (e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)) {
                    return false;
                }
                if (e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)) {
                    return false;
                }
                if (e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)) {
                    return false;
                }
                // if (e.ctrlKey && e.keyCode == 'C'.charCodeAt(0)) {
                //     return false;
                // }
                // if (e.ctrlKey && e.keyCode == 'V'.charCodeAt(0)) {
                //     return false;
                // }
            }
        </script>
    @endif
</head>
@include('admin::layouts.bodynormal')
</html>
