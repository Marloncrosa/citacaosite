<!-- {{--
<ul class="vertical menu expanded" data-drilldown>
  <li><a href="{{ url('banners') }}"> Banners </a></li>

  <li><a href="{{ url('institucional') }}"> Institutional </a></li>


  <li>
    <a href="javascript:void(0);"> Notícias </a>
    <ul class="submenu menu vertical" data-submenu="">
      <li class="js-drilldown-back"><a tabindex="0">Voltar</a></li>

      <li><a href="{{ url('posicoes_noticias') }}"> Posições </a></li>
      <li><a href="{{ url('editorias_noticias') }}"> Editorias </a></li>
      <li><a href="{{ url('autores_noticias') }}"> Autores </a></li>
      <li><a href="{{ url('categorias_noticias') }}"> Categorias </a></li>
      <li><a href="{{ url('noticias') }}"> Itens </a></li>
    </ul>
  </li>

  <li><a href="{{ url('empresas') }}"> Empresas </a></li>
  <li><a href="{{ url('videos') }}"> Vídeos </a></li>
  <li><a href="{{ url('revistas') }}"> Revistas</a></li>

  <li><a href="{{ url('publicidades') }}"> Publicidades </a></li>

  <li><a href="{{ url('newsletter') }}"> Leads </a></li>
  <li><a href="{{ url('contato') }}"> Contatos </a></li>
  --}}
{{--<li><a href="{{ url('configuracoes') }}"> Configurações </a></li>--}}{{--

</ul>
--}} -->


<button class="m-aside-left-close  m-aside-left-close--skin-dark" id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark">

    {{--<!-- BEGIN: Brand -->
    <div class="m-brand  m-brand--skin-dark ">
        <a href="{!! url('admin') !!}" class="m-brand__logo">
            <img src="img/logos/logo.png" alt="{{ env('APP_SITE_TITLE', 'Site Padrão') }}"/>
        </a>
    </div>
    <!-- END: Brand -->--}}

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
         data-menu-vertical="true" m-menu-scrollable="true" m-menu-dropdown-timeout="500">
        @include('admin::layouts.ul-menu-lateral')
    </div>

    <!-- END: Aside Menu -->
</div>
<div class="m-aside-menu-overlay"></div>
