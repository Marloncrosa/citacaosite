@extends('admin::layouts.master')

@section('conteudo')
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                @if (isset($page['subtitle']))
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                {!! $page['subtitle'] !!}
                            </h3>
                        </div>
                    </div>
                @endif
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        @if (!empty($button))
                            @foreach ($button as $key => $value)
                                <li class="m-portlet__nav-item">
                                    <a href="{{ route('admin.anyroute', [ 'slug' => $value['link'].'/' ]) }}"
                                       class="{{ $value['class'] }} btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air"
                                       target="{{ (isset($value['target']))? $value['target'] : '_parent' }}">
                                        <span><i class="{{ $value['icone'] }}"></i><span>{{ $value['title'] }}</span></span>
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                @include('admin::layouts.filtros')

                {!! $html->table([
                    'class' => 'table'
                ]) !!}
            </div>
        </div>
    </div>

    @php
        $script = $html->scripts();
        $script = str_replace('<script type="text/javascript">(function(window,$){', '', $script);
        $script = str_replace('})(window,jQuery);', '', $script);
        $script = str_replace('</script>', '', $script);
    @endphp

    <script>
        $(function(){
            window.LaravelDataTables=window.LaravelDataTables||{};

            setTimeout(function () {
                {!! $script !!}
            }, 500);
        });
    </script>
@stop

@push('line-js')
    <script>
        if ($.fn.dataTable != undefined) {
            $.fn.dataTable.ext.classes.sLengthSelect = 'custom-select custom-select-sm form-control form-control-sm';
            $.extend(true, $.fn.dataTable.defaults, {
                // dom: '<"top"i>rt<"bottom"flp><"clear">',
                pageLength: 10,
                // searchDelay: 500,
                responsive: true,
                select: true,
                processing: true,
                serverSide: true,
                pagingType: "full_numbers",
                lengthMenu: [
                    [10, 25, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, -1],
                    [10, 25, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, "Todos"]
                ],
                language: {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_",
                    "sLoadingRecords": '<strong>Carregando</strong> <br><div class="sk-cube-grid"><div class="sk-cube sk-cube1"></div><div class="sk-cube sk-cube2"></div><div class="sk-cube sk-cube3"></div><div class="sk-cube sk-cube4"></div><div class="sk-cube sk-cube5"></div><div class="sk-cube sk-cube6"></div><div class="sk-cube sk-cube7"></div><div class="sk-cube sk-cube8"></div><div class="sk-cube sk-cube9"></div></div>',
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sFirst": "<i class=\"la la-angle-double-left\"></i>",
                        "sPrevious": "<i class=\"la la-angle-left\"></i>",
                        "sNext": "<i class=\"la la-angle-right\"></i>",
                        "sLast": "<i class=\"la la-angle-double-right\"></i>"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    },

                    select: {
                        rows: {
                            _ : "{!! __('You have selected %d rows') !!}",
                            0 : "",
                            1 : "{!! __('Only 1 row selected') !!}"
                        }
                    },
                    processing: '<strong>Carregando</strong> <br><div class="sk-cube-grid"><div class="sk-cube sk-cube1"></div><div class="sk-cube sk-cube2"></div><div class="sk-cube sk-cube3"></div><div class="sk-cube sk-cube4"></div><div class="sk-cube sk-cube5"></div><div class="sk-cube sk-cube6"></div><div class="sk-cube sk-cube7"></div><div class="sk-cube sk-cube8"></div><div class="sk-cube sk-cube9"></div></div>'
                }/*,
                initComplete: function () {
                    if (!$(this).hasClass('nosearchHeader')) {
                        Linhanova = $('<tr role="filter row"></tr>').appendTo($(this).find('thead'));
                        this.api().columns().every(function (i) {
                            var tableData = this;
                            cabecalho = $(tableData.header());
                            ColunaNova = $('<th rowspan="1" colspan="1"></th>').appendTo(Linhanova);
                            if (cabecalho.text() != 'Ações' && tableData.settings()[0].aoColumns[i].bSearchable) {
                                var input = '<input type="text" class="form-control form-control-sm form-filter m-input" data-col-index="' + this.index() + '"  placeholder="Procurar ' + cabecalho.text() + '"/>';
                                ColunaNova.append('<div class="clearfix"></div>');
                                var elemento = $(input).appendTo(ColunaNova);
                                // elemento.on('change keyup', function () {
                                //     elemento = $(this);
                                //     clearTimeout(timeoutsearch);
                                //     timeoutsearch = setTimeout(function () {
                                //         tableData.search(elemento.val(), false, false, true).draw();
                                //     }, 1000);
                                // });
                            } else if (cabecalho.text() == 'Ações') {
                                var buscarButton = $('<button class="btn btn-brand m-btn btn-sm m-btn--icon"><span><i class="la la-search"></i><span>Buscar</span></span></button>'),
                                    limparButton = $('<button class="btn btn-secondary m-btn btn-sm m-btn--icon"><span><i class="la la-close"></i><span>Limpar</span></span></button>');

                                buscar = $(buscarButton).appendTo(ColunaNova);
                                limpar = $(limparButton).appendTo(ColunaNova);

                                buscar.on("click", function (e) {
                                    e.preventDefault();
                                    var n = {};
                                    Linhanova.find(".m-input").each(function () {
                                        var t = $(this).data("col-index");
                                        n[t] ? n[t] += "|" + $(this).val() : n[t] = $(this).val()
                                    });
                                    $.each(n, function (a, e) {
                                        tableData.column(a).search(e || "", false, false)
                                    });
                                    tableData.draw()
                                });

                                limpar.on("click", function (e) {
                                    e.preventDefault();
                                    Linhanova.find(".m-input").each(function (a) {
                                        $(this).val("");
                                        tableData.column($(this).data("col-index")).search("", false, false, true)
                                    });
                                    tableData.draw()
                                });
                            }
                        });
                    }
                },*/
            });
        }
    </script>
@endpush
