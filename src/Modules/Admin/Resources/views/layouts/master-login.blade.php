<!DOCTYPE html>
<html class="{{ isset($bodyclass)? $bodyclass : 'login' }}" lang="pt-BR">
    <head>
        <meta name="robots" content="noindex, nofollow">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>{{ config('app.name', Lang::get('titles.app')) }} - Admin</title>

        <meta name="description" content="">
        <meta name="author" content="Felipe Almeman">
        <meta name="generator" content="{!! env('APP_DEV_NAME') !!}">
        <meta name="keywords" content="">
        <meta name="theme-color" content="#ff5c5c">

        <meta name="Language" content="Português">
        <meta http-equiv="cache-control" content="public">
        <meta name="distribution" content="Global">
        <meta name="city" content="Londrina">
        <meta name="country" content="Brasil">
        <meta name="Copyright" content="{{ config('app.name', Lang::get('titles.app')) }}">

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <base href="{{ Request::root() }}/modules/admin/">

        <meta name="viewport" content="width=device-width, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">

        <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png?v=eMvWeK5T67">
        <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png?v=eMvWeK5T67">
        <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-touch-icon.png?v=eMvWeK5T67">
        <link rel="icon" type="image/png" sizes="192x192" href="img/favicon/android-chrome-192x192.png?v=eMvWeK5T67">
        <link rel="icon" type="image/png" sizes="194x194" href="img/favicon/favicon-194x194.png?v=eMvWeK5T67">
        <link rel="mask-icon" href="img/favicon/safari-pinned-tab.svg?v=eMvWeK5T67" color="#5bbad5">
        <link rel="shortcut icon" href="img/favicon/favicon.ico?v=eMvWeK5T67">

        <link rel="manifest" href="manifest.json">

        <meta name="apple-mobile-web-app-title" content="{{ config('app.name', Lang::get('titles.app')) }}">
        <meta name="application-name" content="{{ config('app.name', Lang::get('titles.app')) }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="img/favicon/mstile-144x144.png?v=oLBaN4wG3Y">
        <meta name="msapplication-config" content="img/favicon/browserconfig.xml?v=oLBaN4wG3Y">
        <meta name="theme-color" content="#ffffff">

        <meta property="og:type" content="website">
        <meta property="og:title" content="{{ config('app.name', Lang::get('titles.app')) }}">
        <meta property="og:site_name" content="{{ config('app.name', Lang::get('titles.app')) }}"/>
        <meta property="og:url" content="{{ env('APP_URL') }}">
        <meta property="og:image" content="{{ env('APP_URL') }}social.jpg">
        <meta property="og:description" content="">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="{{ config('app.name', Lang::get('titles.app')) }}">
        <meta name="twitter:url" content="{{ env('APP_URL') }}">
        <meta name="twitter:image:src" content="{{ env('APP_URL') }}social.jpg">
        <meta name="twitter:description" content="">

        <meta itemprop="name" content="{{ config('app.name', Lang::get('titles.app')) }}">
        <meta itemprop="description" content="">
        <meta itemprop="image" content="{{ env('APP_URL') }}social.jpg">

        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
            WebFont.load({
                google: {
                    "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
                },
                active: function () {
                    sessionStorage.fonts = true;
                }
            });
        </script>

        @if(env('APP_ENV') == 'local')
            <link href="{!! mix('/modules/admin/css/admin.css') !!}" rel="stylesheet">
        @elseif(env('APP_ENV') == 'production')
            <link href="{!! mix('/modules/admin/css/admin.min.css') !!}" rel="stylesheet">
        @endif

        <script src="js/jquery.min.js"></script>
        @stack('line-css')

    </head>
    <body
        class="body-{{ isset($bodyclass)? $bodyclass : '' }} m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

        @include('admin::layouts.alerts')

        @yield('conteudo-externo')


        {{-- Scripts --}}
        <div class="js">
            @include('admin::layouts.defaultjs')

            @stack('line-js-after-admin')

            @if(env('APP_ENV') == 'local')
                <script src="{!! mix('/modules/admin/js/admin.js') !!}" type="text/javascript"></script>
            @elseif(env('APP_ENV') == 'production')
                <script src="{!! mix('/modules/admin/js/admin.min.js') !!}" type="text/javascript"></script>
            @endif

            @stack('line-js')

            @include('admin::layouts.defaultjs-after')
        </div>

    </body>
</html>
