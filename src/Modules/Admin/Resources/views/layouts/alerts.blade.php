@push('line-js')
    @if (count($errors) > 0)
        @php
            $message = '';
        @endphp
        @foreach ($errors->all() as $key => $value)
            @php
                $message .= $value;
                $message .= '<br>';
            @endphp
        @endforeach
        <script>
            $(function () {
                var content = parent.document.createElement('div');
                content.innerHTML = "{!! $message !!}";

                parent.$('#loading').modal('hide');
                parent.mySwal({
                    content: content,
                    icon: "error",
                });
            })
        </script>
    @endif

    @if (isset($error) && $error)
        <script>
            $(function () {
                var content = parent.document.createElement('div');
                content.innerHTML = "{!! $error !!}";

                parent.$('#loading').modal('hide');
                parent.mySwal({
                    content: content,
                    icon: "error",
                });
            })
        </script>
    @endif

    @if (session()->has('error'))
        <script>
            $(function () {
                var content = parent.document.createElement('div');
                content.innerHTML = "{!! session('error') !!}";

                parent.$('#loading').modal('hide');
                parent.mySwal({
                    content: content,
                    icon: "error",
                });
            })
        </script>
    @endif

    @if (isset($success) && $success)
        <script>
            $(function () {
                var content = parent.document.createElement('div');
                content.innerHTML = "{!! $success !!}";

                parent.$('#loading').modal('hide');
                parent.mySwal({
                    content: content,
                    icon: "success",
                });
            })
        </script>
    @endif

    @if (session()->has('success'))
        <script>
            $(function () {
                var content = parent.document.createElement('div');
                content.innerHTML = "{!! session('success') !!}";

                parent.$('#loading').modal('hide');
                parent.mySwal({
                    content: content,
                    icon: "success",
                });
            })
        </script>
    @endif

    @if (isset($sucesso) && $sucesso)
        <script>
            $(function () {
                var content = parent.document.createElement('div');
                content.innerHTML = "{!! $sucesso !!}";

                parent.$('#loading').modal('hide');
                parent.mySwal({
                    content: content,
                    icon: "success",
                });
            })
        </script>
    @endif

    @if (session()->has('sucesso'))
        <script>
            $(function () {
                var content = parent.document.createElement('div');
                content.innerHTML = "{!! session('sucesso') !!}";

                parent.$('#loading').modal('hide');
                parent.mySwal({
                    content: content,
                    icon: "success",
                });
            })
        </script>
    @endif

    @if (isset($status) && $status)
        <script>
            $(function () {
                // aki2
                var content = parent.document.createElement('div');
                content.innerHTML = "{!! $status !!}";

                parent.$('#loading').modal('hide');
                parent.mySwal({
                    content: content,
                    icon: "info",
                });
            })
        </script>
    @endif

    @if (session('status'))
        <script>
            $(function () {
                // aki3
                var content = parent.document.createElement('div');
                content.innerHTML = "{!! session('status') !!}";

                parent.$('#loading').modal('hide');
                parent.mySwal({
                    content: content,
                    icon: "info",
                });
            })
        </script>
    @endif
@endpush

<div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="loadingTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content" style="background: none; -webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none;">
            <div class="modal-body">
                <div class="dataTables_processing text-center">
                    <strong>Carregando</strong>
                    <br>
                    <div class="sk-cube-grid">
                        <div class="sk-cube sk-cube1"></div>
                        <div class="sk-cube sk-cube2"></div>
                        <div class="sk-cube sk-cube3"></div>
                        <div class="sk-cube sk-cube4"></div>
                        <div class="sk-cube sk-cube5"></div>
                        <div class="sk-cube sk-cube6"></div>
                        <div class="sk-cube sk-cube7"></div>
                        <div class="sk-cube sk-cube8"></div>
                        <div class="sk-cube sk-cube9"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
