<body
    class="m-page--fluid m--skin-dark m-content--skin-light2 m-aside-left--skin-dark {{ (!isset($bodyclass) || $bodyclass != 'login')? '' : '' }}"
    style="overflow: hidden; padding: 0;">

    <div class="m-portlet m-scrollable m-scrollable--track" data-scrollable="true"
         style="margin-bottom: 50px; height: 100%; padding: 0;">
        <div class="m-portlet__head">

            <a href="javascript: parent.$('.sidebar__close').trigger('click');" id="sidebar_close" class="sidebar__close"><i
                    class="la la-close"></i></a>
            <a href="javascript: parent.$('.sidebar_update').trigger('click');" id="sidebar_update" class="sidebar__update"><i
                    class="la la-rotate-right"></i></a>

            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    @if (isset($page['title']))
                        @if (isset($page['title']))
                            <h3 class="m-portlet__head-text">{!! $page['title'] !!}</h3>
                        @endif
                    @endif
                </div>
            </div>
        </div>
        <div class="m-portlet__body p-0 pr-1">
            @yield('conteudo-externo')

            @if (!isset($bodyclass) || $bodyclass != 'login')
                @yield('conteudo')
            @endif
        </div>
    </div>

    @include('admin::layouts.alerts')

    {{-- Scripts --}}
    <div class="js">
        @include('admin::layouts.defaultjs')

        @stack('line-js-after-admin')

        @if(env('APP_ENV') == 'local')
            <script src="{!! mix('/modules/admin/js/admin.js') !!}" type="text/javascript"></script>
        @elseif(env('APP_ENV') == 'production')
            <script src="{!! mix('/modules/admin/js/admin.min.js') !!}" type="text/javascript"></script>
        @endif

        @stack('line-js')

        @include('admin::layouts.defaultjs-after')
    </div>

</body>
