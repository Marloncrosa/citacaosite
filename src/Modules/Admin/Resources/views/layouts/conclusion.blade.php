@extends('admin::layouts.master-clear')

@push('line-js')
    @if (!session()->has('reload'))
        <script>
            $(function () {
                if (parent.LaravelDataTables) {
                    parent.LaravelDataTables.dataTableBuilder.draw();
                }
                if (parent.Calendar) {
                    parent.Calendar.fullCalendar('refetchEvents')
                }
                parent.$('#loading').modal('hide');
                // setTimeout(function () {
                // parent.$('.sidebar__close').trigger('click');
                parent.$('.form-sidebar:first', window.parent.document).removeClass('sidebar--on').removeAttr('style');
                parent.$('.form-sidebar-overlay', window.parent.document).removeClass('open');
                parent.$('.sidebar__hide').css('left', '-35px');
                parent.$('.sidebar__hide i').toggleClass('la-angle-double-left').toggleClass('la-angle-double-right');
                // }, 500)
            })
        </script>
    @else
        <script>
            $(function () {
                if (parent.LaravelDataTables) {
                    parent.LaravelDataTables.dataTableBuilder.draw();
                }
                if (parent.Calendar) {
                    parent.Calendar.fullCalendar('refetchEvents')
                }
                parent.$('#loading').modal('hide');
                setTimeout(function () {
                    location.href = "{!! session('reload') !!}";
                }, 1000)
            })
        </script>
    @endif
@endpush
