<div class="form-sidebar-preload"><i class="la la-spinner"></i></div>
<div class="form-sidebar sidebar--tabbed sidebar--skin-light">
    <div class="sidebar__content">
        {{--<a href="javascript:;" id="sidebar_close" class="sidebar__close"><i class="la la-close"></i></a>--}}
        {{--<a href="javascript:;" id="sidebar_update" class="sidebar__update"><i class="la la-rotate-right"></i></a>--}}
        <a href="javascript:;" id="sidebar_hide" class="sidebar__hide closed"><i class="la la-angle-double-left"></i></a>
        <iframe src="javascript:;" frameborder="0" width="100%" height="100%" class=""></iframe>
    </div>
</div>
<div class="form-sidebar-overlay"></div>


<div id="m_scroll_top" class="m-scroll-top">
    <i class="la la-arrow-up"></i>
</div>
