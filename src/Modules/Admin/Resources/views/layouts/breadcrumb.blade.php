<div class="m-subheader ">
    <div class="d-flex align-items-center flex-column flex-lg-row">
        @if (isset($page['title']))
            <div class="callout large secondary">
                @if (isset($page['title']))
                    <h3 class="m-subheader__title m-subheader__title--separator">{!! $page['title'] !!}</h3>
                @endif
            </div>
        @endif
        <nav aria-label="Você está:" role="navigation" class="mr-auto display-inline-block h-auto">
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                <li class="m-nav__item m-nav__item--home h-auto ">
                    <a href="{{ route('admin.home') }}" class="m-nav__link m-nav__link--icon">
                        <i class="m-nav__link-icon la la-home text-center"></i>
                    </a>
                </li>
                <li class="m-nav__separator">-</li>
                @if (isset($page['ant-title-array']))
                    @foreach ($page['ant-title-array'] as $key => $value)
                        <li class="m-nav__item h-auto">
                            <a href="{{ (isset($value['url']))? route('admin.anyroute', [ 'slug' => $value['url'] ]) : '' }}{{ (isset($value['url2']))? $value['url2'] : '' }}"
                               class="m-nav__link {{ (isset($value['class']))? $value['class'] : '' }}">
                                <span class="m-nav__link-text">{{ $value['pagina'] }}</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                    @endforeach
                @endif
                @if (isset($page['ant-title']))
                    <li class="m-nav__item h-auto">
                        <a href="{{ (isset($page['extrat']['url']))? route('admin.anyroute', [ 'slug' => $page['extrat']['url'] ]) : '' }}{{ (isset($$page['extrat']['url2']))? $page['extrat']['url2'] : '' }}"
                           class="m-nav__link {{ (isset($page['extrat']['class']))? $page['extrat']['class'] : '' }}">
                            <span class="m-nav__link-text">{{ $page['extrat']['pagina'] }}</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                @endif
                @if(isset($pagina))
                    <li class="m-nav__item h-auto">
                        <a href="{{ isset($page['urlbread'])? url($page['urlbread']) : route('admin.anyroute', [ 'slug' => $pagina .'/index/' ]) }}"
                           class="m-nav__link {{ (isset($page['class']))? $page['class'] : '' }}">
                            <span class="m-nav__link-text">{{ $page['title'] }}</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                @endif
                @if (isset($page['ant-extrat-array']))
                    @foreach ($page['ant-extrat-array'] as $key => $value)
                        <li class="m-nav__item h-auto">
                            <a href="{{ (isset($value['url']))? route('admin.anyroute', [ 'slug' => $value['url'] ]) : '' }}{{ (isset($value['url2']))? $value['url2'] : '' }}"
                               class="m-nav__link {{ (isset($value['class']))? $value['class'] : '' }}">
                                <span class="m-nav__link-text">{{ $value['pagina'] }}</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                    @endforeach
                @endif
                @if (isset($page['ant-extrat']))
                    <li class="m-nav__item h-auto">
                        <a href="{{ (isset($page['extrat']['url']))? route('admin.anyroute', [ 'slug' => $page['extrat']['url'] ]) : '' }}{{ (isset($$page['extrat']['url2']))? $page['extrat']['url2'] : '' }}"
                           class="m-nav__link {{ (isset($page['extrat']['class']))? $page['extrat']['class'] : '' }}">
                            <span class="m-nav__link-text">{{ $page['extrat']['pagina'] }}</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                @endif
                @if (!empty($page['extrat']))
                    <li class="m-nav__item h-auto">
                        <a href="{{ route('admin.anyroute', [ 'slug' => $pagina .'/'. $action .'/'. $attr ]) }}" class="m-nav__link {{ (isset($page['class']))? $page['class'] : '' }}">
                            <span class="m-nav__link-text">{{ $page['extrat'] }}</span>
                        </a>
                    </li>
                    <li class="m-nav__separator">-</li>
                @endif
                @if (!empty($page['extrat-array']))
                    @foreach ($page['extrat-array'] as $key => $value)
                        <li class="m-nav__item h-auto">
                            <a href="{{ (isset($value['url']))? route('admin.anyroute', [ 'slug' => $value['url'] ]) : '' }}{{ (isset($value['url2']))? $value['url2'] : '' }}"
                               class="m-nav__link {{ (isset($value['class']))? $value['class'] : '' }}">
                                <span class="m-nav__link-text">{{ $value['pagina'] }}</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">-</li>
                    @endforeach
                @endif
            </ul>
        </nav>

    </div>
</div>
