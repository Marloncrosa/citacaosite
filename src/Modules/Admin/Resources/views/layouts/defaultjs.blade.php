<script type="text/javascript">
    window.WantToUpdate = '{!! __("Você tem certeza que deseja alterar?") !!}';
    window.WantToDelete = '{!! __('Você tem certeza que deseja remover?') !!}';
    window.SelectCity = '{!! __('Selecione uma cidade') !!}';

    window.swalInformation = '{!! __('Informação') !!}';
    window.dateFormat = '{!! __('dd/mm/yyyy') !!}';
    window.dateFormatMask = '{!! __('99/99/9999') !!}';
    window.dateFormatMoment = '{!! __('DD/MM/YYYY') !!}';

    window.dateToday = '{!! __("Hoje") !!}';
    window.dateYesterday = '{!! __("Ontem") !!}';
    window.dateLast7Days = '{!! __("Os últimos 7 días") !!}';
    window.dateLast30Days = '{!! __("Os últimos 30 días") !!}';
    window.dateThisMonth = '{!! __("Este mês") !!}';
    window.dateLastMonth = '{!! __("Mês pasado") !!}';


    window.datepickerDates = {
        days: ["{!! __('Domingo') !!}", "{!! __('Segunda-Feira') !!}", "{!! __('Terça-Feira') !!}", "{!! __('Quarta-Feira') !!}", "{!! __('Quinta-Feira') !!}", "{!! __('Sexta-Feira') !!}", "{!! __('Sábado') !!}", "{!! __('Domingo') !!}"],
        daysShort: ["{!! __('Dom') !!}", "{!! __('Seg') !!}", "{!! __('Ter') !!}", "{!! __('Qua') !!}", "{!! __('Qui') !!}", "{!! __('Sex') !!}", "{!! __('Sab') !!}", "{!! __('Dom') !!}"],
        daysMin: ["{!! __('Dom') !!}", "{!! __('Seg') !!}", "{!! __('Ter') !!}", "{!! __('Qua') !!}", "{!! __('Qui') !!}", "{!! __('Sex') !!}", "{!! __('Sa') !!}", "{!! __('Dom') !!}"],
        months: ["{!! __('Janeiro') !!}", "{!! __('Fevereiro') !!}", "{!! __('Março') !!}", "{!! __('Abril') !!}", "{!! __('Maio') !!}", "{!! __('Junho') !!}", "{!! __('Julho') !!}", "{!! __('Agosto') !!}", "{!! __('Setembro') !!}", "{!! __('Outubro') !!}", "{!! __('Novembro') !!}", "{!! __('Dezembro') !!}"],
        monthsShort: ["{!! __('Jan') !!}", "{!! __('Fev') !!}", "{!! __('Mar') !!}", "{!! __('Abr') !!}", "{!! __('Maio') !!}", "{!! __('Jun') !!}", "{!! __('Jul') !!}", "{!! __('Ago') !!}", "{!! __('Set') !!}", "{!! __('Out') !!}", "{!! __('Nov') !!}", "{!! __('Dez') !!}"],
        today: "{!! __('Hoje') !!}",
        clear: "{!! __('Limpar') !!}",
        titleFormat: "{!! __('MM yyyy') !!}",
        format: window.dateFormat
    };

    window.datetimepickerDates = {
        days: ["{!! __('Domingo') !!}", "{!! __('Segunda-Feira') !!}", "{!! __('Terça-Feira') !!}", "{!! __('Quarta-Feira') !!}", "{!! __('Quinta-Feira') !!}", "{!! __('Sexta-Feira') !!}", "{!! __('Sábado') !!}", "{!! __('Domingo') !!}"],
        daysShort: ["{!! __('Dom') !!}", "{!! __('Seg') !!}", "{!! __('Ter') !!}", "{!! __('Qua') !!}", "{!! __('Qui') !!}", "{!! __('Sex') !!}", "{!! __('Sab') !!}", "{!! __('Dom') !!}"],
        daysMin: ["{!! __('Dom') !!}", "{!! __('Seg') !!}", "{!! __('Ter') !!}", "{!! __('Qua') !!}", "{!! __('Qui') !!}", "{!! __('Sex') !!}", "{!! __('Sa') !!}", "{!! __('Dom') !!}"],
        months: ["{!! __('Janeiro') !!}", "{!! __('Fevereiro') !!}", "{!! __('Março') !!}", "{!! __('Abril') !!}", "{!! __('Maio') !!}", "{!! __('Junho') !!}", "{!! __('Julho') !!}", "{!! __('Agosto') !!}", "{!! __('Setembro') !!}", "{!! __('Outubro') !!}", "{!! __('Novembro') !!}", "{!! __('Dezembro') !!}"],
        monthsShort: ["{!! __('Jan') !!}", "{!! __('Fev') !!}", "{!! __('Mar') !!}", "{!! __('Abr') !!}", "{!! __('Maio') !!}", "{!! __('Jun') !!}", "{!! __('Jul') !!}", "{!! __('Ago') !!}", "{!! __('Set') !!}", "{!! __('Out') !!}", "{!! __('Nov') !!}", "{!! __('Dez') !!}"],
        today: "{!! __('Hoje') !!}",
        suffix: [],
        meridiem: [],
        format: window.dateFormat
    };

    window.daterangepickerOptions = {
        locale: {
            "format": [window.dateFormatMoment],
            "separator": " - ",
            "applyLabel": "{!! __('Aplicar') !!}",
            "cancelLabel": "{!! __('Cancelar') !!}",
            "fromLabel": "{!! __('De') !!}",
            "toLabel": "{!! __('Até') !!}",
            "customRangeLabel": "{!! __('Personalizado') !!}",
            "weekLabel": "W",
            "daysOfWeek": [
                "{!! __("Do") !!}",
                "{!! __("Lu") !!}",
                "{!! __("Ma") !!}",
                "{!! __("Mi") !!}",
                "{!! __("Ju") !!}",
                "{!! __("Vi") !!}",
                "{!! __("Sá") !!}"
            ],
            "monthNames": [
                "{!! __('Janeiro') !!}",
                "{!! __('Fevereiro') !!}",
                "{!! __('Março') !!}",
                "{!! __('Abril') !!}",
                "{!! __('Maio') !!}",
                "{!! __('Junho') !!}",
                "{!! __('Julho') !!}",
                "{!! __('Agosto') !!}",
                "{!! __('Setembro') !!}",
                "{!! __('Outubro') !!}",
                "{!! __('Novembro') !!}",
                "{!! __('Dezembro') !!}"
            ],
            "firstDay": 1
        }
    };
</script>
