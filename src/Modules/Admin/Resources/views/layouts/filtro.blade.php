<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
  <label>{!! __('Quando foi criado') !!}:</label>
    <div class="dataranger-picker">
        <input type='text' class="form-control m-input" name="created_at" placeholder="{!! __('Informe uma data') !!}" readonly />
    </div>
</div>
<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
  <label>{!! __('Procure') !!}:</label>
  <input type="search" class="form-control m-input" placeholder="{!! __('Procure por') !!}" data-col-index="1">
</div>
@if(Schema::hasColumn($model->getTable(), 'active'))
<div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
  <label>{!! __('Ativo?') !!}:</label>
  {!! Form::select('active', [
    '' => __('Selecione'),
    '1' => __('Ativo'),
    '0' => __('Inativo'),
  ] , null , ['class' => 'form-control m-select2']) !!}
</div>
@endif

@if(Schema::hasColumn($model->getTable(), 'deleted_at'))
  <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
    <label>{!! __('Registros removidos?') !!}</label>
    {!! Form::select('removed', [
      '0' => __('Não removidos'),
      '1' => __('Só removidos'),
      '2' => __('Todos os Registros'),
    ] , null , ['class' => 'form-control m-select2']) !!}
  </div>
@endif

