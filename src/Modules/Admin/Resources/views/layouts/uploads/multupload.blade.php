@extends('admin::layouts.master')

@section('conteudo')

	<span class="fileupload-process">
		<div class="total-progress progress" role="progressbar" tabindex="0" aria-valuenow="0" aria-valuemin="0" aria-valuetext="0 porcento" aria-valuemax="100" style="opacity: 0;">
			<span class="progress-meter" style="width: 0%">
				<p class="progress-meter-text"></p>
			</span>
		</div>
	</span>

	<table class="hover stack" class="files" id="table-preview" data-maxfilesize="{{ (isset($maxfilesize))? $maxfilesize : '100' }}" data-url="{{ $urlmult }}" data-campo="{{ (isset($campo))? $campo : 'file' }}">
		<thead>
			<tr>
				<th width="80">
					#
				</th>
				<th>
					Nome do arquivo
				</th>
				<th width="100">
					Tamanho
				</th>
				<th width="500">
					Progresso
				</th>
				<th width="300">
					Ações
				</th>
			</tr>
		</thead>
		<tbody id="previews">
			<tr id="template" class="file-row template-tr">
				<!-- This is used as the file preview template -->
				<td>
					<span class="preview"><img data-dz-thumbnail /></span>
				</td>
				<td>
					<p class="name" data-dz-name></p>
					<small class="error callout alert" style="opacity: 0;" data-closable>
						<span data-dz-errormessage></span>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</small>
					<p class="clearfix"></p>
				</td>
				<td>
					<p class="size" data-dz-size></p>
				</td>
				<td>
					<span class="fileupload-process">
						<div class="total-progress progress" role="progressbar" tabindex="0" aria-valuenow="0" aria-valuemin="0" aria-valuetext="0 porcento" aria-valuemax="100">
							<span class="progress-meter" style="width: 0%" data-dz-uploadprogress>
								<p class="progress-meter-text"></p>
							</span>
						</div>
					</span>
				</td>
				<td>
					{{--<button class="button primary start">
						<i class="fa fa-cloud-upload" aria-hidden="true"></i>
						<span>Enviar arquivo</span>
					</button>--}}
					<button data-dz-remove class="button alert delete">
						<i class="fa fa-trash" aria-hidden="true"></i>
						<span>Remover</span>
					</button>
				</td>
			</tr>
		</tbody>
	</table>
@stop

@push('line-js')
	<script type="text/javascript" charset="utf-8">
		multiuploads();
	</script>
@endpush
