@extends('admin::layouts.master')

@section('conteudo')
	{!! Form::model($FormModel, ['url' => $url, 'class' => 'secondary form-geral', 'files' => true]) !!}

		{!! Form::hidden($campohidden, $valorhidden, ['required' => 'true']) !!}

		@if (!empty($page['extrat']))
			<h4 class="column">
				{!! $page['extrat'] !!}
			</h4>
		@endif

		<div class="clearfix"></div>

		<div class="small-12 column">
			{!! Form::label('titulo', 'Título', ['class' => '']) !!}
			{!! Form::text('titulo', null, ['class' => '', 'required' => 'true']) !!}
		</div>

		<div class="small-12 column">
			{!! Form::label('descricao', 'Descrição', ['class' => '']) !!}
			{!! Form::textarea('descricao', null, ['class' => '', 'data-editor-simples' => '']) !!}
		</div>
		<p class="clearfix"></p>

		<div class="small-12 medium-12 large-6 column">
			<div class="small-6 float-left">
				{!! Form::labelHTML('imagem', 'Imagem na listagem') !!}
				{!! Form::label('imagem', 'Selecionar arquivo', ['class' => 'button expanded has-tip top file-arquivo', 'style' => 'margin-bottom:0;', 'data-tooltip' => '', 'aria-haspopup' => "true", 'data-disable-hover' => "false", 'tabindex' => "2", 'title' => "Selecionar Arquivo do tipo .jpg, jpeg, .gif ou .png" ]) !!}
				{!! Form::fileHTML('imagem', null, ['id' => 'imagem', 'class' => 'show-for-sr', 'style' => 'margin-bottom:0;']) !!}
			</div>
			<div class="small-6 column itemimagem">
				@if( !empty($FormModel -> imagem) )
					<a href="{{ route('thumb',[ 0, 0, 'blog/galerias/imagens/'.$FormModel -> imagem ]) }}" class="button margin-remove-all viewfile"> visualizar imagem </a>
					<a data-ajax="{{ url(''. $pagina .'/deletararquivo/id/'.$FormModel -> id.'/campo/imagem/path/blog/path1/galerias/path2/imagens') }}" data-texto="Deseja remover a imagem?" data-excluir="" data-remover=".itemimagem .deletfile, .itemimagem .viewfile" title="Excluir" class="deletfile button alert margin-remove-all">
						<i aria-hidden="true" class="fa fa-times"></i>
					</a>
				@endif
			</div>
		</div>

		<div class="small-12 medium-12 large-6 column status-field radius">
			{!! Form::label('', 'Status', ['class' => '']) !!}
			{!! Form::statuscor('ativo', ((!empty($FormModel->ativo) && !is_null($FormModel->ativo))? $FormModel->ativo : null) ) !!}
		</div>

		{!! Form::botoesform($pagina) !!}
	{!! Form::close() !!}
@stop
