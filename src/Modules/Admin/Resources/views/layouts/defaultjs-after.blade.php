<script>
    $(function () {
        $data_mask = $('.mascara-data:not(.no-datepicker)');
        if ($data_mask.length > 0) {
            if ($.fn.datepicker) {
                Datepicker = $data_mask.data('datepicker');
                Datepicker.fill()
            }
        }

        $data_hora_mask = $('.mascara-datahora:not(.no-datetimepicker)');
        if ($data_hora_mask.length > 0) {
            if ($.fn.datetimepicker) {
                DateTimepicker = $data_hora_mask.data('datetimepicker');
                DateTimepicker.fill();
                // $data_hora_mask.datetimepicker('refresh');
            }
        }

        $.fn.select2.defaults.set('language', {
            errorLoading: function () {
                return '{!! __('Os resultados não puderam ser carregados.') !!}';
            },
            inputTooLong: function (args) {
                var overChars = args.input.length - args.maximum;

                var message = '{!! __('Apague ') !!}' + overChars + ' {!! __('caracter') !!}';

                if (overChars != 1) {
                    var message = '{!! __('Apague ') !!}' + overChars + ' {!! __('caracteres') !!}';
                }

                return message;
            },
            inputTooShort: function (args) {
                var remainingChars = args.minimum - args.input.length;

                var message = '{!! __('Digite') !!} ' + remainingChars + ' {!! __('ou mais caracteres') !!}';

                return message;
            },
            loadingMore: function () {
                return '{!! __('Carregando mais resultados…') !!}';
            },
            maximumSelected: function (args) {
                if (args.maximum == 1) {
                    var message = '{!! __('Você só pode selecionar') !!} ' + args.maximum + ' {!! __('item') !!}';
                } else {
                    var message = '{!! __('Você só pode selecionar') !!} ' + args.maximum + ' {!! __('itens') !!}';
                }

                return message;
            },
            noResults: function () {
                return '{!! __('Nenhum resultado encontrado') !!}';
            },
            searching: function () {
                return '{!! __('Buscando…') !!}';
            },
            removeAllItems: function () {
                return '{!! __('Remover todos os itens') !!}';
            }
        });

        $('[data-scrollable="true"]').each(function () {
            var t = $(this);
            mUtil.scrollerInit(this, {
                disableForMobile: false,
                handleWindowResize: true,
                height: function () {
                    // parent.console.log(mUtil.isInResponsiveRange("tablet-and-mobile") && t.data("mobile-height"));
                    // parent.console.log(t.data("mobile-height"));
                    // parent.console.log(t.data("height"));
                    return mUtil.isInResponsiveRange("tablet-and-mobile") && t.data("mobile-height") ? t.data("mobile-height") : t.data("height")
                }
            })
        });

        $(document).on('submit', 'form', function(){
            parent.$('#loading').modal({
                backdrop: 'static',
                keyboard: false,
            });
        });

        {{--@if (isset($pagina))
                url = "{{ url('/').'/'.$pagina }}";
                $selecionado = $('ul > li > a[href="'+ url +'"]');
                $selecionado.addClass('ativo')
                $elpais = $selecionado.parents('.is-drilldown-submenu-parent')
                $elpais.each(function(){
                    $(this).find('> a').addClass('ativo')
                })
                // $('ul > li > a[href="'+ url +'"]').parent().addClass('ativo')
        @endif--}}
    })
</script>
