@extends('admin::layouts.master')

@section('conteudo')
  <div class="m-grid__item m-grid__item--fluid m-wrapper">

    <!--  <div class="m-subheader ">
         <div class="d-flex align-items-center">
             <div class="mr-auto">
                 <h3 class="m-subheader__title ">Meu perfil</h3>
             </div>
             <div>
                 <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                      m-dropdown-toggle="hover" aria-expanded="true">
                     <a href="#"
                        class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                         <i class="la la-plus m--hide"></i>
                         <i class="la la-ellipsis-h"></i>
                     </a>
                     <div class="m-dropdown__wrapper">
                         <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                         <div class="m-dropdown__inner">
                             <div class="m-dropdown__body">
                                 <div class="m-dropdown__content">
                                     <ul class="m-nav">
                                         <li class="m-nav__section m-nav__section--first m--hide">
                                             <span class="m-nav__section-text">Ações rapidas</span>
                                         </li>
                                         <li class="m-nav__item">
                                             <a href="" class="m-nav__link">
                                                 <i class="m-nav__link-icon flaticon-share"></i>
                                                 <span class="m-nav__link-text">Activity</span>
                                             </a>
                                         </li>
                                         <li class="m-nav__item">
                                             <a href="" class="m-nav__link">
                                                 <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                 <span class="m-nav__link-text">Messages</span>
                                             </a>
                                         </li>
                                         <li class="m-nav__item">
                                             <a href="" class="m-nav__link">
                                                 <i class="m-nav__link-icon flaticon-info"></i>
                                                 <span class="m-nav__link-text">FAQ</span>
                                             </a>
                                         </li>
                                         <li class="m-nav__item">
                                             <a href="" class="m-nav__link">
                                                 <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                 <span class="m-nav__link-text">Support</span>
                                             </a>
                                         </li>
                                         <li class="m-nav__separator m-nav__separator--fit">
                                         </li>
                                         <li class="m-nav__item">
                                             <a href="#"
                                                class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                         </li>
                                     </ul>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
     </div> -->

    <!-- END: Subheader -->
    <div class="m-content">
      <div class="row">
        <div class="col-xl-3 col-lg-4">
          <div class="m-portlet m-portlet--full-height  ">
            <div class="m-portlet__body">
              <div class="m-card-profile">
                <div class="m-card-profile__title m--hide">
                  Seu Perfil
                </div>
                <div class="m-card-profile__pic">
                  <div class="m-card-profile__pic-wrapper">
                    @if((!empty($FormModel->avatar_imagem) && file_exists(public_path('uploads/'.$FormModel->avatar_imagem))))
                      <img src="{{ route('thumbfit',[ 80, 80, $FormModel->avatar_imagem, 'ffffff', true]) }}"
                           alt="{!! $FormModel->nome !!}">
                    @else
                      <img src="metronic/app/media/img/users/user4.jpg" alt=""/>
                    @endif
                  </div>
                </div>
                <div class="m-card-profile__details">
                  <span class="m-card-profile__name">{!! $usuario->nome !!}</span>
                  <a href="" class="m-card-profile__email m-link">{!! $usuario->email !!}</a>
                </div>
              </div>
            <!--            <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">
                                <li class="m-nav__separator m-nav__separator--fit"></li>
                                <li class="m-nav__section m--hide">
                                    <span class="m-nav__section-text">Section</span>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{ url('/admin/usuarios') }}" class="m-nav__link">
                                        <i class="m-nav__link-icon flaticon-profile-1"></i>
                                        <span class="m-nav__link-title">
                                            <span class="m-nav__link-wrap">
                                                <span class="m-nav__link-text">Meu perfil</span>
                                                <span class="m-nav__link-badge">
                                                    <span class="m-badge m-badge--success">2</span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{ url('/admin/usuarios') }}" class="m-nav__link">
                                        <i class="m-nav__link-icon flaticon-share"></i>
                                        <span class="m-nav__link-text">Activity</span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{ url('/admin/usuarios') }}" class="m-nav__link">
                                        <i class="m-nav__link-icon flaticon-chat-1"></i>
                                        <span class="m-nav__link-text">Messages</span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{ url('/admin/usuarios') }}" class="m-nav__link">
                                        <i class="m-nav__link-icon flaticon-graphic-2"></i>
                                        <span class="m-nav__link-text">Sales</span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{ url('/admin/usuarios') }}" class="m-nav__link">
                                        <i class="m-nav__link-icon flaticon-time-3"></i>
                                        <span class="m-nav__link-text">Events</span>
                                    </a>
                                </li>
                                <li class="m-nav__item">
                                    <a href="{{ url('/admin/usuarios') }}" class="m-nav__link">
                                        <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                        <span class="m-nav__link-text">Support</span>
                                    </a>
                                </li>
                            </ul>
                            <div class="m-portlet__body-separator"></div>
                            <div class="m-widget1 m-widget1--paddingless">
                                <div class="m-widget1__item">
                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col">
                                            <h3 class="m-widget1__title">Member Profit</h3>
                                            <span class="m-widget1__desc">Awerage Weekly Profit</span>
                                        </div>
                                        <div class="col m--align-right">
                                            <span class="m-widget1__number m--font-brand">+$17,800</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-widget1__item">
                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col">
                                            <h3 class="m-widget1__title">Orders</h3>
                                            <span class="m-widget1__desc">Weekly Customer Orders</span>
                                        </div>
                                        <div class="col m--align-right">
                                            <span class="m-widget1__number m--font-danger">+1,800</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="m-widget1__item">
                                    <div class="row m-row--no-padding align-items-center">
                                        <div class="col">
                                            <h3 class="m-widget1__title">Issue Reports</h3>
                                            <span class="m-widget1__desc">System bugs and issues</span>
                                        </div>
                                        <div class="col m--align-right">
                                            <span class="m-widget1__number m--font-success">-27,49%</span>
                                        </div>
                                    </div>
                                </div>
                            </div> -->
            </div>
          </div>
        </div>
        <div class="col-xl-9 col-lg-8">
          <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
            <div class="m-portlet__head">
              <div class="m-portlet__head-tools">
                <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary"
                    role="tablist">
                  <li class="nav-item m-tabs__item">
                    <a class="nav-link m-tabs__link active" data-toggle="tab"
                       href="#atualizar_cadastro" role="tab">
                      <i class="flaticon-share m--hide"></i>
                      Atualizar Perfil
                    </a>
                  </li>
                  <li class="nav-item m-tabs__item">
                    <a class="nav-link m-tabs__link" data-toggle="tab"
                       href="#atualizar_senha" role="tab">
                      <i class="flaticon-share m--hide"></i>
                      Atualizar Senha
                    </a>
                  </li>
                </ul>
              </div>
              <!--         <div class="m-portlet__head-tools">
                          <ul class="m-portlet__nav">
                              <li class="m-portlet__nav-item m-portlet__nav-item--last">
                                  <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                       m-dropdown-toggle="hover" aria-expanded="true">
                                      <a href="#"
                                         class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                          <i class="la la-gear"></i>
                                      </a>
                                      <div class="m-dropdown__wrapper">
                                          <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                          <div class="m-dropdown__inner">
                                              <div class="m-dropdown__body">
                                                  <div class="m-dropdown__content">
                                                      <ul class="m-nav">
                                                          <li class="m-nav__section m-nav__section--first">
                                                              <span class="m-nav__section-text">Quick Actions</span>
                                                          </li>
                                                          <li class="m-nav__item">
                                                              <a href="" class="m-nav__link">
                                                                  <i class="m-nav__link-icon flaticon-share"></i>
                                                                  <span class="m-nav__link-text">Create Post</span>
                                                              </a>
                                                          </li>
                                                          <li class="m-nav__item">
                                                              <a href="" class="m-nav__link">
                                                                  <i class="m-nav__link-icon flaticon-chat-1"></i>
                                                                  <span class="m-nav__link-text">Send Messages</span>
                                                              </a>
                                                          </li>
                                                          <li class="m-nav__item">
                                                              <a href="" class="m-nav__link">
                                                                  <i class="m-nav__link-icon flaticon-multimedia-2"></i>
                                                                  <span class="m-nav__link-text">Upload File</span>
                                                              </a>
                                                          </li>
                                                          <li class="m-nav__section">
                                                              <span class="m-nav__section-text">Useful Links</span>
                                                          </li>
                                                          <li class="m-nav__item">
                                                              <a href="" class="m-nav__link">
                                                                  <i class="m-nav__link-icon flaticon-info"></i>
                                                                  <span class="m-nav__link-text">FAQ</span>
                                                              </a>
                                                          </li>
                                                          <li class="m-nav__item">
                                                              <a href="" class="m-nav__link">
                                                                  <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                                  <span class="m-nav__link-text">Support</span>
                                                              </a>
                                                          </li>
                                                          <li class="m-nav__separator m-nav__separator--fit m--hide">
                                                          </li>
                                                          <li class="m-nav__item m--hide">
                                                              <a href="#"
                                                                 class="btn btn-outline-danger m-btn m-btn--pill m-btn--wide btn-sm">Submit</a>
                                                          </li>
                                                      </ul>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </li>
                          </ul>
                      </div> -->
            </div>

            <div class="tab-content">
              <div class="tab-pane active" id="atualizar_cadastro">
                {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--fit m-form--label-align-right form-geral', 'files' => true]) !!}

                <div class="m-portlet__body">

                  <div class="form-group m-form__group row">
                    {!! Form::label('name', 'Name', ['class' => 'col-2 col-form-label']) !!}
                    <div class="col-7">
                      {!! Form::text('name', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    {!! Form::label('', __('Avatar'), [ 'class' => 'col-2 col-form-label' ]) !!}
                    <div class="col-7">
                      @if( !empty($FormModel->avatar) )
                        <label for="avatar"
                               class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo float-left mr-2">
                          <span>

                            <span>{!! __('Select a avatar') !!}</span>
                            {!! Form::fileHTML('avatar', null, ['id' => 'avatar', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                          </span>
                        </label>

                        <a href="{{ route('thumb',[ 0, 0, $FormModel->avatar_imagem ]) }}"
                           class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                          {!! __('Show avatar') !!}
                        </a>

                        <a
                          data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel -> id}/campo/avatar/path/users/path1/{$FormModel->id}/path2/avatar") }}"
                          data-texto="Deseja remover a avatar?" data-excluir=""
                          data-remover=".itemavatar .deletfile, .itemavatar .viewfile" title="Excluir"
                          class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                          <i aria-hidden="true" class="fa fa-times"></i>
                        </a>
                      @else
                        <label for="avatar"
                               class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                          <span>

                            <span>{!! __('Select a avatar') !!}</span>
                            {!! Form::fileHTML('avatar', null, ['id' => 'avatar', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                          </span>
                        </label>
                      @endif
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    {!! Form::label('cellphone', 'Cellphone', ['class' => 'col-2 col-form-label']) !!}
                    <div class="col-7">
                      {!! Form::text('cellphone', null, ['class' => 'form-control m-input mask-intl-phone']) !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    {!! Form::label('login', 'Login', ['class' => 'col-2 col-form-label']) !!}
                    <div class="col-7">
                      {!! Form::text('login', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    {!! Form::label('email', 'E-mail', ['class' => 'col-2 col-form-label']) !!}
                    <div class="col-7">
                      {!! Form::email('email', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                    </div>
                  </div>
                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions p-4">
                    <div class="row">
                      <div class="col-7 offset-2">
                        <button type="submit"
                                class="btn btn-accent m-btn m-btn--air m-btn--custom metronic">{!! __('Save') !!}</button>&nbsp;&nbsp;
                        {{--<button type="reset"
                                class="btn btn-secondary m-btn m-btn--air m-btn--custom">{!! __('Cancel') !!}</button>--}}
                      </div>
                    </div>
                  </div>
                </div>

                {!! Form::close() !!}
              </div>

              <div class="tab-pane" id="atualizar_senha">
                {!! Form::model($FormModel, ['url' => route('admin.anyroute', [ 'slug' => $urlpass ]), 'class' => 'm-form m-form--fit m-form--label-align-right form-geral', 'autocomplete' => 'off']) !!}

                <div class="m-portlet__body">
                  <div class="form-group m-form__group row">
                    {!! Form::label('password', 'Nova Senha', ['class' => 'col-2 col-form-label']) !!}
                    <div class="col-7">
                      {!! Form::password('password', array('class' => 'form-control m-input', 'required' => 'true')) !!}
                    </div>
                  </div>

                  <div class="form-group m-form__group row">
                    {!! Form::label('password_confirmation', 'Confirmar Nova Senha', ['class' => 'col-2 col-form-label']) !!}
                    <div class="col-7">
                      {!! Form::password('password_confirmation', array('class' => 'form-control m-input', 'required' => 'true')) !!}
                    </div>
                  </div>
                </div>

                <div class="m-portlet__foot m-portlet__foot--fit">
                  <div class="m-form__actions p-4">
                    <div class="row">
                      <div class="col-7 offset-2">
                        <button type="submit"
                                class="btn btn-accent m-btn m-btn--air m-btn--custom metronic">{!! __('Save') !!}</button>&nbsp;&nbsp;
                        {{--<button type="reset"
                                class="btn btn-secondary m-btn m-btn--air m-btn--custom">{!! __('Cancel') !!}</button>--}}
                      </div>
                    </div>
                  </div>
                </div>

                {!! Form::close() !!}
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>


@stop

@push('line-js')
  <style>
    .m-portlet.m-portlet--full-height {
      position: relative;
    }

    .m-portlet .m-portlet__foot:not(.m-portlet__no-border) {
      position: relative;
    }
  </style>
@endpush
