@extends('admin::layouts.master-clear')

@section('conteudo')
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet">
                    {!! Form::model($FormModel, ['url' => $urlform, 'class' => 'm-form m-form--label-align-right form-geral', 'method' => 'post', 'files' => true]) !!}

                        {!! Form::hidden($campohidden, $valorhidden, ['required' => 'true']) !!}
                        {!! Form::hidden($campohidden2, $valorhidden2, ['required' => 'true']) !!}

                        <div class="form-group m-form__group row m-0">
                            <div class="col-12 col-sm-6">
                                {!! Form::label('city', 'Cidade', ['class' => 'col-form-label']) !!}
                                {!! Form::text('city', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                            </div>
                        </div>

                        <div class="form-group m-form__group row m-0">
                            {!! Form::label('', 'Ativo?', ['class' => 'col-12 col-lg-2 col-form-label']) !!}
                            <div class="col-12 col-lg-5">
                                {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                            </div>
                        </div>

                        {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
