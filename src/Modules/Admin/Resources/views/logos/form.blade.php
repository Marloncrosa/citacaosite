@extends('admin::layouts.master-clear')

@section('conteudo')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet mb-0">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('title', 'Nome', ['class' => 'col-form-label']) !!}
                            {!! Form::text('title', null, ['class' => 'slugify form-control m-input', 'required' => 'true', 'data-slugged' => 'input#slug']) !!}
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('slug', 'URL Amigável', ['class' => 'col-form-label']) !!}
                            {!! Form::text('slug', null, ['class' => 'form-control m-input']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6 itemimage">
                            {!! Form::label('', __('Imagem'), [ 'class' => 'col-form-label d-inline-block w-100' ]) !!}
                            <br>
                            @if( !empty($FormModel->image) )
                                <label for="image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air float-left mr-1 file-arquivo">
                                    <span>
                                        <span>{!! __('Selecionar Imagem') !!}</span>
                                        {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;', 'accept' => 'image/*']) !!}
                                    </span>
                                </label>
                                <a href="{{ route('thumb',[ 0, 0, "products/{$FormModel->id}/{$FormModel->image}" ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air float-left mr-1 viewfile">
                                    visualizar
                                </a>
                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image/path/products/path1/{$FormModel->id}") }}"
                                   data-texto="Deseja remover?" data-excluir=""
                                   data-remover=".itemimage .deletfile, .itemimage .viewfile" title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air float-left">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                    <span>
                                        <span>{!! __('Selecionar Imagem') !!}</span>
                                        {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;', 'accept' => 'image/*']) !!}
                                    </span>
                                </label>
                            @endif
                        </div>
                        <div class="form-group m-form__group row m-0">
                            <div class="col-sm-6 itemimage">
                                {!! Form::label('', 'Imagem Listagem 1', [ 'class' => 'col-form-label d-inline-block w-100' ]) !!}
                                <br>
                                @if( !empty($FormModel->image) )
                                    <label for="image"
                                           class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air float-left mr-1 file-arquivo">
                                        <span>
                                            <span>{!! __('Selecionar') !!}</span>
                                            {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;', 'accept' => 'image/*']) !!}
                                        </span>
                                    </label>
                                    <a href="{{ route('thumb',[ 0, 0, "products/{$FormModel->id}/{$FormModel->image}" ]) }}"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air float-left mr-1 viewfile">
                                        visualizar
                                    </a>
                                    <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image/path/products/path1/{$FormModel->id}") }}"
                                       data-texto="Deseja remover?" data-excluir=""
                                       data-remover=".itemimage .deletfile, .itemimage .viewfile" title="Excluir"
                                       class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air float-left">
                                        <i aria-hidden="true" class="fa fa-times"></i>
                                    </a>
                                @else
                                    <label for="image"
                                           class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                        <span>
                                            <span>{!! __('Selecionar') !!}</span>
                                            {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;', 'accept' => 'image/*']) !!}
                                        </span>
                                    </label>
                                @endif
                            </div>

                        <div class="col-12 col-sm-6">
                            {!! Form::label('', __('Ativo?'), ['class' => 'col-form-label d-inline-block w-100']) !!}
                            {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
