@extends('admin::layouts.master-clear')

@section('conteudo')

  <div class="m-content">
    <div class="row">
      <div class="col-lg-12">
        <div class="m-portlet">
          {!! Form::model($FormModel, ['url' => $urlform, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

            {!! Form::hidden($campohidden, $valorhidden, ['required' => 'true']) !!}
            {!! Form::hidden($campohidden2, $valorhidden2, ['required' => 'true']) !!}

            {!! Form::hidden('ip', ((empty($FormModel->ip))? Request::ip() : null), ['required' => 'true']) !!}

            <div class="form-group m-form__group row m-0">
              <div class="col-12 col-sm-6">
                {!! Form::label('name', __('Name'), ['class' => 'col-form-label']) !!}
                {!! Form::text('name', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
              </div>
              <div class="col-12 col-sm-6">
                {!! Form::label('email', __('E-mail'), ['class' => 'col-form-label']) !!}
                {!! Form::email('email', null, ['class' => 'form-control m-input']) !!}
              </div>
            </div>

            <div class="form-group m-form__group row m-0">
              {!! Form::label('comment', __('Comment'), ['class' => 'col-12 col-lg-2 col-form-label']) !!}
              <div class="col-12 col-lg-5">
                {!! Form::textarea('comment', null, ['class' => '', 'data-editor-completo' => '']) !!}
              </div>
            </div>

            {!! Form::botoesform($pagina, true, true, true, false) !!}
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@stop
