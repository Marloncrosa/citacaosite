@extends('admin::layouts.master-clear')

@section('conteudo')

  <div class="m-content">
    <div class="row">
      <div class="col-lg-12">
        <div class="m-portlet m-portlet--space">
          <div class="m-form">
            <div class="m-portlet__body">
              @if (!empty($page['extrat']))
                <div class="m-form__heading">
                  <h3 class="m-form__heading-title">{!! $page['extrat'] !!}</h3>
                </div>
              @endif

              <div class="form-group m-form__group">
                <label for="example_input_full_name">{!! __('Infos') !!}</label>
                <div class="alert m-alert m-alert--default" role="alert">
                  {!! __('Created At') !!}: {{ $item->created_at }}

                  @if(!is_null($item->contact_subjects))
                    <br>
                    {!! __('Subject') !!}: {{ $item->contact_subjects->title }}
                  @endif
                  <br>
                </div>
              </div>

              <div class="form-group m-form__group">
                <label for="example_input_full_name">{!! __('Name') !!}</label>
                <div class="alert m-alert m-alert--default" role="alert">
                  {{ $item->name}}
                </div>
              </div>

              <div class="form-group m-form__group">
                <label for="example_input_full_name">{!! __('E-mail') !!}</label>
                <div class="alert m-alert m-alert--default" role="alert">
                  {{ $item->email}}
                </div>
              </div>

              <div class="form-group m-form__group">
                <label for="example_input_full_name">{!! __('Phone') !!}</label>
                <div class="alert m-alert m-alert--default" role="alert">
                  {{ $item->phone}}
                </div>
              </div>

              <div class="form-group m-form__group">
                <label for="example_input_full_name">{!! __('Cellphone') !!}</label>
                <div class="alert m-alert m-alert--default" role="alert">
                  {{ $item->cellphone}}
                </div>
              </div>

              <div class="form-group m-form__group">
                <label for="example_input_full_name">{!! __('Message') !!}</label>
                <div class="alert m-alert m-alert--default" role="alert">
                  {{ $item->message}}
                </div>
              </div>

            </div>



          </div>
        </div>
      </div>
    </div>
  </div>
@stop
