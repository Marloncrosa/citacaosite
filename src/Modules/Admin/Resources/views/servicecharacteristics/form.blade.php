@extends('admin::layouts.master-clear')

@section('conteudo')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet mb-0">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12">
                            {!! Form::label('title', __('Título'), [ 'class' => '' ]) !!}
                            <div class="">
                                {!! Form::text('title', null, ['class' => 'form-control m-input', 'id' => 'title', 'required' => 'true']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('', __('Ativo?'), ['class' => '']) !!}
                            <div class="mt-2 w-100">
                                {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                            </div>
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
