@extends('admin::layouts.master')

@section('conteudo')
    <br><br>
    <div class="container-fluid translation-page">
        <div class="col-12">
            <div class="alert alert-warning" role="alert">
                {!! __('Important!') !!}
                <br>
                {!! __('When you find some translation with :something, like :text, this part you doesnt translate, only the rest of the text that contains this tag') !!}
            </div>

            <p>
                {!! __('Warning, translations are not visible until they are exported back to the app/lang file, using') !!}
                <code>
                    php artisan translation:export
                </code>
                {!! __('command or publish button.') !!}
            </p>
            <div class="alert alert-success success-import" style="display:none;">
                <p>
                    {!! __('Done importing, processed') !!}
                    <strong class="counter">N</strong>
                    {!! __('items! Reload this page to refresh the groups!') !!}
                </p>
            </div>
            <div class="alert alert-success success-find" style="display:none;">
                <p>{!! __('Done searching for translations, found') !!} <strong
                            class="counter">N</strong> {!! __('items!') !!}</p>
            </div>
            <div class="alert alert-success success-publish" style="display:none;">
                <p>{!! __('Done publishing the translations for group') !!} '<?php echo $group ?>'!</p>
            </div>
            <div class="alert alert-success success-publish-all" style="display:none;">
                <p>{!! __('Done publishing the translations for all group!') !!}</p>
            </div>
            @if(Session::has('successPublish'))
                <div class="alert alert-info">
                    {!! Session::get('successPublish') !!}
                </div>
            @endif
            <div>
                @if(!isset($group))
                    <form class="form-import" method="POST"
                          action="{!! action('\Modules\Admin\Http\Controllers\TranslationsController@postImport') !!}"
                          data-remote="true" role="form">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <select name="replace" class="form-control">
                                        <option value="0">{!! __('Append new translations') !!}</option>
                                        <option value="1">{!! __('Replace existing translations') !!}</option>
                                    </select>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-success btn-block"
                                            data-disable-with="Loading..">
                                        {!! __('Import groups') !!}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form class="form-find" method="POST"
                          action="{!! action('\Modules\Admin\Http\Controllers\TranslationsController@postFind') !!}"
                          data-remote="true" role="form"
                          data-confirm="{!! __('Are you sure you want to scan you app folder? All found translation keys will be added to the database.') !!}">
                        <div class="form-group">
                            <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                            <button type="submit" class="btn btn-info"
                                    data-disable-with="{!! __('Searching..') !!}">
                                {!! __('Find translations in files') !!}
                            </button>
                        </div>
                    </form>
                @endif
                @if(isset($group))
                    <form class="form-inline form-publish" method="POST"
                          action="{!! action('\Modules\Admin\Http\Controllers\TranslationsController@postPublish', $group) !!}"
                          data-remote="true" role="form"
                          data-confirm="{!! __('Are you sure you want to publish the translations group') !!} {!! $group !!}? {!! __('This will overwrite existing language files.') !!}">
                        <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                        <button type="submit" class="btn btn-info"
                                data-disable-with="{!! __('Publishing..') !!}">{!! __('Publish translations') !!}</button>
                        <a href="{!! action('\Modules\Admin\Http\Controllers\TranslationsController@getIndex') !!}"
                           class="btn btn-default">{!! __('Back') !!}</a>
                    </form>
                @endif
            </div>

            <form role="form" method="POST"
                  action="{!! action('\Modules\Admin\Http\Controllers\TranslationsController@postAddGroup') !!}">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}">

                <div class="form-group">
                    <p>
                        {!! __('Choose a group to display the group translations.') !!} {!! __('If no groups are visisble, make sure you have run the migrations and imported the translations.') !!}
                    </p>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <select name="group" id="group" class="form-control group-select">
                                @foreach($groups as $key => $value)
                                    <option value="{!! $key !!}" {!! $key == $group ? ' selected' : '' !!}>{!! $value !!}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <a href="<?= action('\Modules\Admin\Http\Controllers\TranslationsController@getIndex') ?>"
                           class="btn btn-default">{!! __('Back') !!}</a>
                    </div>
                </div>
                {{--<div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label>{!! __('Enter a new group name and start edit translations in that group') !!}</label>
                            <input type="text" class="form-control" name="new-group"/>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <input type="submit" class="btn btn-default" name="add-group"
                           value="Add and edit keys"/>
                </div>--}}
            </form>

            @if($group)
                <form action="{!! action('\Modules\Admin\Http\Controllers\TranslationsController@postAdd', array($group)) !!}"
                      method="POST" role="form">
                    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
                    <div class="form-group">
                        <label>{!! __('Enter a new group name and start edit translations in that group') !!}</label>
                        <textarea class="form-control" rows="3" name="keys"
                                  placeholder="{!! __('Add 1 key per line, without the group prefix') !!}"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="{!! __('Add keys') !!}" class="btn btn-primary">
                    </div>
                </form>
                <hr>
                <h4>Total: {!! $numTranslations !!}, changed: {!! $numChanged !!}</h4>
                <table class="table">
                    <thead>
                    <tr>
                        <th width="15%">Key</th>
                        @foreach ($languages->pluck('shortname')->toArray() as $locale)
{{--                            @if(in_array($locale, $languages->pluck('shortname')->toArray() ))--}}
                                <th>{!! $locale !!}</th>
                            {{--@endif--}}
                        @endforeach
                        {{--@if ($deleteEnabled)
                            <th>&nbsp;</th>
                        @endif--}}
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($translations as $key => $translation)
                        <tr id="{!! htmlentities($key, ENT_QUOTES, 'UTF-8', false) !!}">
                            <td>
                                {!! htmlentities($key, ENT_QUOTES, 'UTF-8', false) !!}
                            </td>
                            @foreach ($languages->pluck('shortname')->toArray() as $locale)
{{--                                @if(in_array($locale, $languages->pluck('shortname')->toArray() ))--}}
                                    <?php
                                        $t = isset($translation[$locale]) ? $translation[$locale] : null;
                                    ?>
                                    <td>
                                        <a href="#edit"
                                           class="editable status-{!! $t ? $t->status : 0 !!} locale-{!! $locale !!}"
                                           data-locale="{!! $locale !!}"
                                           data-name="{!! $locale . "|" . htmlentities($key, ENT_QUOTES, 'UTF-8', false) !!}"
                                           id="username" data-type="textarea" data-pk="{!! $t ? $t->id : 0 !!}"
                                           data-url="{!! $editUrl !!}"
                                           data-title="{!! __('Enter translation') !!}">
                                            {!! $t ? htmlentities($t->value, ENT_QUOTES, 'UTF-8', false) : '' !!}
                                        </a>
                                    </td>
                                {{--@endif--}}
                            @endforeach
                            {{--@if ($deleteEnabled)
                                <td>
                                    <a href="{!! action('\Modules\Admin\Http\Controllers\TranslationsController@postDelete', [$group, $key]) !!}"
                                       class="delete-key"
                                       data-confirm="{!! __('Are you sure you want to delete the translations for') !!} {!! htmlentities($key, ENT_QUOTES, 'UTF-8', false) !!}?">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            @endif--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @else
                <fieldset>
                    <legend>{!! __('Supported locales') !!}</legend>
                    <p>
                        {!! __('Current supported locales:') !!}
                    </p>
                    <form class="form-remove-locale" method="POST" role="form"
                          action="{!! action('\Modules\Admin\Http\Controllers\TranslationsController@postRemoveLocale') !!}"
                          data-confirm="{!! __('Are you sure to remove this locale and all of data?') !!}">
                        <input type="hidden" name="_token" value="{!! csrf_token(); !!}">
                        <ul class="list-locales list-unstyled ">
                            @foreach($languages as $locale)
                                <li>
                                    <div class="form-group">
                                        <button type="submit" name="remove-locale[{!! $locale->shortname !!}]"
                                                class="btn btn-danger btn-xs" data-disable-with="...">
                                            &times;
                                        </button>
                                        <strong>{!! $locale->shortname !!} / {!! $locale->title !!}</strong>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </form>
                    {{--<form class="form-add-locale" method="POST" role="form"
                          action="{!! action('\Modules\Admin\Http\Controllers\TranslationsController@postAddLocale') !!}">
                        <input type="hidden" name="_token" value="{!! csrf_token(); !!}">
                        <div class="form-group">
                            <p>
                                {!! __('Enter new locale key:') !!}
                            </p>
                            <div class="row">
                                <div class="col-sm-3">
                                    <input type="text" name="new-locale" class="form-control"/>
                                </div>
                                <div class="col-sm-2">
                                    <button type="submit" class="btn btn-default btn-block"
                                            data-disable-with="{!! __('Adding..') !!}">
                                        {!! __('Add new locale') !!}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>--}}
                </fieldset>
                <fieldset>
                    <legend>{!! __('Export all translations') !!}</legend>
                    <form class="form-inline form-publish-all" method="POST"
                          action="<?php echo action('\Modules\Admin\Http\Controllers\TranslationsController@postPublish', '*') ?>"
                          data-remote="true" role="form"
                          data-confirm="{!! __('Are you sure you want to publish all translations group? This will overwrite existing language files.') !!}">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <button type="submit" class="btn btn-primary"
                                data-disable-with="{!! __('Publishing..') !!}">{!! __('Publish all') !!}</button>
                    </form>
                </fieldset>
            @endif
        </div>
    </div>
@stop

@push('line-js')
    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">--}}
    <link href="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css"
          rel="stylesheet"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ujs/1.2.2/rails.min.js"></script>
    <script>
        jQuery(document).ready(function ($) {

            $.ajaxSetup({
                beforeSend: function (xhr, settings) {
                    // console.log('beforesend');
                    settings.data += "&_token=<?php echo csrf_token() ?>";
                }
            });

            $('.editable').editable().on('hidden', function (e, reason) {
                var locale = $(this).data('locale');
                if (reason === 'save') {
                    $(this).removeClass('status-0').addClass('status-1');
                }
                if (reason === 'save' || reason === 'nochange') {
                    var $next = $(this).closest('tr').next().find('.editable.locale-' + locale);
                    setTimeout(function () {
                        $next.editable('show');
                    }, 300);
                }
            });

            $('.group-select').on('change', function () {
                var group = $(this).val();
                if (group != '' && group != undefined) {
                    window.location.href = '<?php echo action('\Modules\Admin\Http\Controllers\TranslationsController@getView') ?>/' + $(this).val();
                } else {
                    window.location.href = '<?php echo action('\Modules\Admin\Http\Controllers\TranslationsController@getIndex') ?>';
                }
            });

            $("a.delete-key").click(function (event) {
                event.preventDefault();
                var row = $(this).closest('tr');
                var url = $(this).attr('href');
                var id = row.attr('id');
                $.post(url, {id: id}, function () {
                    row.remove();
                });
            });

            $('.form-import').on('ajax:success', function (e, data) {
                $('div.success-import strong.counter').text(data.counter);
                $('div.success-import').slideDown();
                window.location.reload();
            });

            $('.form-find').on('ajax:success', function (e, data) {
                $('div.success-find strong.counter').text(data.counter);
                $('div.success-find').slideDown();
                window.location.reload();
            });

            $('.form-publish').on('ajax:success', function (e, data) {
                $('div.success-publish').slideDown();
            });

            $('.form-publish-all').on('ajax:success', function (e, data) {
                $('div.success-publish-all').slideDown();
            });

        })
    </script>
@endpush
