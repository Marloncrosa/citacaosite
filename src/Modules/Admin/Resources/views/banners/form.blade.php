@extends('admin::layouts.master-clear')

@section('conteudo')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet mb-0">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-12">
                            {!! Form::label('title', __('Título'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            {!! Form::text('title', null, ['class' => 'form-control m-input', 'id' => 'title', 'required' => 'true']) !!}
                        </div>


                        <div class="col-sm-12">
                            {!! Form::label('subtitle', __('Subtítulo'), [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            {!! Form::text('subtitle', null, ['class' => 'form-control m-input', 'id' => 'subtitle']) !!}
                        </div>


                        <div class="col-sm-12">
                            {!! Form::label('link', 'Link', [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            {!! Form::text('link', null, ['class' => 'form-control m-input', 'id' => 'link']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('description', 'Descrição', ['class' => 'col-12 col-lg-2 col-form-label']) !!}
                        <div class="col-12 col-lg-5">
                            {!! Form::textarea('description', null, ['class' => '', 'data-editor-completo' => '']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('start', __('Início'), [ 'class' => '' ]) !!}
                            {!! Form::text('start', null, ['class' => 'form-control m-input mascara-data', 'id' => 'start']) !!}
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('end', __('Fim'), [ 'class' => '' ]) !!}
                            {!! Form::text('end', null, ['class' => 'form-control m-input mascara-data', 'id' => 'end']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('video', __('Vídeo'), [ 'class' => '' ]) !!}
                            {!! Form::text('video', null, ['class' => 'form-control m-input', 'id' => 'video']) !!}
                        </div>

                        <div class="col-12 col-sm-6">
                            {!! Form::label('video_image', __('Capa do vídeo'), [ 'class' => '' ]) !!}
                            <div class="clearfix"></div>
                            @if( !empty($FormModel->video_image) )
                                <label for="video_image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                        <span>
                                            <span>{!! __('Selecionar capa') !!}</span>
                                            {!! Form::fileHTML('video_image', null, ['id' => 'video_image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                        </span>
                                </label>
                                <a href="{{ route('thumb',[ 0, 0, "banners/{$FormModel->id}/{$FormModel->video_image}" ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                    visualizar video_image </a>
                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/video_image/path/banners/path1/{$FormModel->id}") }}"
                                   data-texto="Deseja remover a video_image?" data-excluir=""
                                   data-remover=".itemvideo_image .deletfile, .itemvideo_image .viewfile"
                                   title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="video_image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                        <span>

                                            <span>{!! __('Selecionar capa') !!}</span>
                                            {!! Form::fileHTML('video_image', null, ['id' => 'video_image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                        </span>
                                </label>
                            @endif
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('', 'Imagem', [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            @if( !empty($FormModel->image) )
                                <label for="image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                <span>

                                    <span>{!! __('Selecionar Imagem') !!}</span>
                                    {!! Form::fileHTML('image', null, ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                </span>
                                </label>
                                <a href="{{ route('thumb',[ 0, 0, "banners/{$FormModel->id}/{$FormModel->image}" ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                    visualizar image </a>
                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image/path/banners/path1/{$FormModel->id}") }}"
                                   data-texto="Deseja remover a image?" data-excluir=""
                                   data-remover=".itemimage .deletfile, .itemimage .viewfile" title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                <span>

                                    <span>{!! __('Selecionar Imagem') !!}</span>
                                    {!! Form::fileHTML('image', null, ['id' => 'image','required' => 'true', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                </span>
                                </label>
                            @endif
                        </div>
                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('', 'Imagem Mobile (768 x 1024)', [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            @if( !empty($FormModel->image_mobile) )
                                <label for="image_mobile"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                <span>

                                    <span>{!! __('Selecionar Imagem Mobile') !!}</span>
                                    {!! Form::fileHTML('image_mobile', null, ['id' => 'image_mobile', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                </span>
                                </label>
                                <a href="{{ route('thumb',[ 0, 0, "banners/{$FormModel->id}/{$FormModel->image_mobile}" ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                    visualizar image_mobile </a>
                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image_mobile/path/banners/path1/{$FormModel->id}") }}"
                                   data-texto="Deseja remover a image_mobile?" data-excluir=""
                                   data-remover=".itemimage_mobile .deletfile, .itemimage_mobile .viewfile" title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="image_mobile"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                <span>

                                    <span>{!! __('Selecionar Imagem Mobile') !!}</span>
                                    {!! Form::fileHTML('image_mobile', null, ['id' => 'image_mobile','required' => 'true', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                </span>
                                </label>
                            @endif
                        </div>

                        <div class="col-sm-6">
                            {!! Form::label('', __('Ativo?'), ['class' => 'col-lg-2 col-form-label']) !!}
                            {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
