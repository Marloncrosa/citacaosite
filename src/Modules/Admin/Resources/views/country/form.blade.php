@extends('admin::layouts.master-clear')

@section('conteudo')
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'method' => 'post', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12">
                            {!! Form::label('country', __('País'), ['class' => 'col-form-label']) !!}
                            {!! Form::text('country', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-0">
                        <div class="col-12">
                            {!! Form::label('initials', __('Iniciais'), ['class' => 'col-form-label']) !!}
                            {!! Form::text('initials', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-0">
                        <div class="col-12">
                            {!! Form::label('country_code', __('Código do País'), ['class' => 'col-form-label']) !!}
                            {!! Form::text('country_code', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
