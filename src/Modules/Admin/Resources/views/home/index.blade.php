@extends('admin::layouts.master')

@section('conteudo')
{{--    <div class="container-fluid">--}}
{{--        <div class="row indicadores">--}}
{{--            <div class="col-sm item _001">--}}
{{--                <a href="{!! route('admin.anyroute', [ 'cart' ]) !!}">--}}
{{--                    <span>{!! $totalPedidos->count() !!}</span>--}}
{{--                    Pedidos<br> Realizados--}}
{{--                </a>--}}
{{--            </div>--}}

{{--            <div class="col-sm item _002">--}}
{{--                <a href="{!! route('admin.anyroute', [ 'cart' ]) !!}">--}}
{{--                    <span>{!! $totalVendasEfetivadas->count() !!}</span>--}}
{{--                    Vendas<br> Efetivadas--}}
{{--                </a>--}}
{{--            </div>--}}

{{--            <div class="col-sm item _003">--}}
{{--                <a href="{!! route('admin.anyroute', [ 'clientes' ]) !!}">--}}
{{--                    <span>{!! $totalClietnes->count() !!}</span>--}}
{{--                    Clientes<br> Cadastrados--}}
{{--                </a>--}}
{{--            </div>--}}

{{--            <div class="col-sm item _004">--}}
{{--                <a href="{!! route('admin.anyroute', [ 'products' ]) !!}">--}}
{{--                    <span>{!! $produtosAtivos->count() !!}</span>--}}
{{--                    Produtos<br> Ativos--}}
{{--                </a>--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="row ultimos-registros">--}}
{{--            @if(!empty($ultimosPedidos) && $ultimosPedidos->count() > 0)--}}
{{--                <div class="col-sm ultimos-pedidos">--}}
{{--                    <H2>Últimos Pedidos</H2>--}}
{{--                    <table class="table table-hover">--}}
{{--                        <thead class="thead-dark">--}}
{{--                            <tr>--}}
{{--                                <th scope="col">REF.</th>--}}
{{--                                <th scope="col">Cliente</th>--}}
{{--                                <th scope="col">Valor</th>--}}
{{--                                <th scope="col">Status</th>--}}
{{--                            </tr>--}}
{{--                        </thead>--}}
{{--                        <tbody>--}}
{{--                            @foreach ($ultimosPedidos as $item)--}}
{{--                                @php--}}
{{--                                dd($item->order_status)--}}
{{--                                @endphp--}}
{{--                                <tr>--}}
{{--                                    <th scope="row">{!! $item->id !!}</th>--}}
{{--                                    <td>{!! $item->client->name !!}</td>--}}
{{--                                    <td>{!! formataNumero($item->total) !!}</td>--}}
{{--                                    @if (!empty($item->order_status))--}}
{{--                                    <td>--}}
{{--                                        <span class="status"--}}
{{--                                              style="color: {!! $item->order_status->color !!};">{!! $item->order_status->name !!}</span>--}}
{{--                                    </td>--}}
{{--                                    @endif--}}
{{--                                </tr>--}}
{{--                            @endforeach--}}
{{--                        </tbody>--}}
{{--                    </table>--}}
{{--                </div>--}}
{{--            @endif--}}

{{--            <div class="col-sm ultimos-cadastros">--}}
{{--                <h2>Últimos Interessados</h2>--}}
{{--                <table class="table table-hover">--}}
{{--                    <thead class="thead-dark">--}}
{{--                        <tr>--}}
{{--                            <th scope="col">E-mail</th>--}}
{{--                            <th scope="col">Produto</th>--}}
{{--                            <th scope="col">Data</th>--}}
{{--                        </tr>--}}
{{--                    </thead>--}}
{{--                    <tbody>--}}
{{--                        @foreach ($ultimosInteressados as $item)--}}
{{--                            <tr>--}}
{{--                                <td>{!! $item->email !!}</td>--}}
{{--                                <td>{!! $item->product->name !!}</td>--}}
{{--                                <td>{!! $item->created_at->format('d/m/Y') !!}</td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
{{--                    </tbody>--}}
{{--                </table>--}}
{{--            </div>--}}

{{--        </div>--}}
{{--    </div>--}}
@stop

@push('line-js')
    <script src="metronic/app/js/dashboard.js" type="text/javascript"></script>
@endpush
