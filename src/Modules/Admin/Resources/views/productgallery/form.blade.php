@extends('admin::layouts.master-clear')

@section('conteudo')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet mb-0">
                    {!! Form::model($FormModel, ['url' => $urlform, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

                    {!! Form::hidden($campohidden, $valorhidden) !!}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-12">
                            {!! Form::label('title', __('Título'), [ 'class' => 'col-form-label' ]) !!}
                            {!! Form::text('title', null, ['class' => 'form-control m-input', 'id' => 'title']) !!}
                        </div>
                    </div>


{{--                    <div class="form-group m-form__group row m-0">--}}
{{--                        {!! Form::label('legend', 'Legenda', ['class' => 'col-12 col-form-label']) !!}--}}
{{--                        <div class="col-12 col-lg-5">--}}
{{--                            {!! Form::textarea('legend', null, ['class' => '']) !!}--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('', 'Imagem', [ 'class' => 'col-form-label' ]) !!}
                            <br>
                            @if( !empty($FormModel->image) )
                                <label for="image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                <span>

                                    <span>{!! __('Selecionar Imagem') !!}</span>
                                    {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                </span>
                                </label>
                                <a href="{{ route('thumb',[ 0, 0, $FormModel->image_show ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                    visualizar image </a>

                                <a data-ajax="{{ url("product_gallery/deletararquivo/{$campohidden}/{$valorhidden}/id/{$FormModel->id}/campo/image/path/products/path1/{$FormModel->product_id}/path2/gallery/") }}"
                                   data-texto="Deseja remover a image?" data-excluir=""
                                   data-remover=".itemimage .deletfile, .itemimage .viewfile" title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                <span>

                                    <span>{!! __('Selecionar Imagem') !!}</span>
                                    {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                </span>
                                </label>
                            @endif
                        </div>
                        <div class="form-group m-form__group row m-0">
                            <div class="col-sm-6">
                                {!! Form::label('', 'Imagem Mobile', [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                                @if( !empty($FormModel->image) )
                                    <label for="image_mobile"
                                           class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                        <span>
                        
                                            <span>{!! __('Selecionar Imagem Mobile') !!}</span>
                                            {!! Form::fileHTML('image_mobile', null, ['id' => 'image_mobile', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                        </span>
                                    </label>
                                    <a href="{{ route('thumb',[ 0, 0, "banners/{$FormModel->id}/{$FormModel->image_mobile}" ]) }}"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                        visualizar image_mobile </a>
                                    <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image_mobile/path/banners/path1/{$FormModel->id}") }}"
                                       data-texto="Deseja remover a image_mobile?" data-excluir=""
                                       data-remover=".itemimage_mobile .deletfile, .itemimage_mobile .viewfile" title="Excluir"
                                       class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                        <i aria-hidden="true" class="fa fa-times"></i>
                                    </a>
                                @else
                                    <label for="image_mobile"
                                           class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                        <span>
                        
                                            <span>{!! __('Selecionar Imagem Mobile') !!}</span>
                                            {!! Form::fileHTML('image_mobile', null, ['id' => 'image_mobile','required' => 'true', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                        </span>
                                    </label>
                                @endif
                            </div>

                        <div class="col-sm-6">
                            {!! Form::label('', __('Ativo?'), ['class' => 'col-form-label']) !!}
                            <br>
                            {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
