@extends('admin::layouts.master-clear')

@section('conteudo')
    
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet mb-0">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}
                    
                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('product_categories', __('Categorias'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                <select id="product_categories" name="product_categories[]"
                                        class="form-control m-select2"
                                        multiple="multiple" data-placeholder="Selecione as categorias"
                                        data-allow-clear="true">
                                    @if(!empty($product_categories))
                                        @foreach ($product_categories as $item)
                                            <option value="{!! $item->id!!}" @if(!is_null($FormModel->product_categories)
                                        && in_array($item->id, $FormModel->product_categories )) selected @endif>
                                                {!! $item->title !!}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-12 col-sm-6">
                            {!! Form::label('code', __('Código do Produto'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="">
                                {!! Form::text('code', null, ['class' => 'form-control m-input', 'id' => 'initial_quantity', 'required' => 'true']) !!}
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('title', 'Nome', ['class' => 'col-form-label']) !!}
                            {!! Form::text('title', null, ['class' => 'slugify form-control m-input', 'required' => 'true', 'data-slugged' => 'input#slug']) !!}
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('slug', 'URL Amigável', ['class' => 'col-form-label']) !!}
                            {!! Form::text('slug', null, ['class' => 'form-control m-input']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('additional', __('Adicionais'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                <select id="additional" name="additional[]" class="form-control m-select2"
                                        multiple="multiple" data-placeholder="Selecione os adicionais"
                                        data-allow-clear="true">
                                    @if(!empty($additionals))
                                        @foreach ($additionals as $additional)
                                            <option value="{!! $additional->id!!}"
                                                    @if(!is_null($FormModel->additional) && in_array($additional->id, $FormModel->additional )) selected @endif>
                                                {!! $additional->categories->title !!} - {!! $additional->title !!}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('brand_id', 'Marca', [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'brand_id',
                                      ['' => __('Selecione')] + $relacionamentos['brand']->pluck('title','id')->all(),
                                      null,
                                      ['class'=>'form-control m-select2', 'required'])
                                  !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('amount', __('Quantidade Geral'), [ 'class' => 'col-form-label' ]) !!}
                            {!! Form::text('amount', null, ['class' => 'form-control m-input', 'id' => 'amount']) !!}
                        </div>
                        
                        <div class="col-12">
                            {!! Form::label('sinopsys', 'Sinópse', ['class' => 'col-form-label']) !!}
                            {!! Form::textarea('sinopsys', null, ['class' => '', 'data-editor-completo' => '']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group m-form__group row m-0">
                        <div class="col-12">
                            {!! Form::label('description', 'Descrição', ['class' => 'col-form-label']) !!}
                            {!! Form::textarea('description', null, ['class' => '', 'data-editor-completo' => '']) !!}
                        </div>
                    </div>
                    
                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6 itemimage">
                            {!! Form::label('', 'Imagem Listagem 1', [ 'class' => 'col-form-label d-inline-block w-100' ]) !!}
                            <br>
                            @if( !empty($FormModel->image) )
                                <label for="image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air float-left mr-1 file-arquivo">
                                    <span>
                                        <span>{!! __('Selecionar') !!}</span>
                                        {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;', 'accept' => 'image/*']) !!}
                                    </span>
                                </label>
                                <a href="{{ route('thumb',[ 0, 0, "products/{$FormModel->id}/{$FormModel->image}" ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air float-left mr-1 viewfile">
                                    visualizar
                                </a>
                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image/path/products/path1/{$FormModel->id}") }}"
                                   data-texto="Deseja remover?" data-excluir=""
                                   data-remover=".itemimage .deletfile, .itemimage .viewfile" title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air float-left">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                    <span>
                                        <span>{!! __('Selecionar') !!}</span>
                                        {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;', 'accept' => 'image/*']) !!}
                                    </span>
                                </label>
                            @endif
                        </div>
                        
                        <div class="form-group m-form__group row m-0">
                            <div class="col-sm-6">
                                {!! Form::label('', 'Imagem Mobile', [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                                @if( !empty($FormModel->image_mobile) )
                                    <label for="image_mobile"
                                           class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                        <span>
                        
                                            <span>{!! __('Selecionar Imagem Mobile') !!}</span>
                                            {!! Form::fileHTML('image_mobile', null, ['id' => 'image_mobile', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                        </span>
                                    </label>
                                    <a href="{{ route('thumb',[ 0, 0, "banners/{$FormModel->id}/{$FormModel->image_mobile}" ]) }}"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                        visualizar image_mobile </a>
                                    <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image_mobile/path/banners/path1/{$FormModel->id}") }}"
                                       data-texto="Deseja remover a image_mobile?" data-excluir=""
                                       data-remover=".itemimage_mobile .deletfile, .itemimage_mobile .viewfile" title="Excluir"
                                       class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                        <i aria-hidden="true" class="fa fa-times"></i>
                                    </a>
                                @else
                                    <label for="image_mobile"
                                           class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                        <span>
                        
                                            <span>{!! __('Selecionar Imagem Mobile') !!}</span>
                                            {!! Form::fileHTML('image_mobile', null, ['id' => 'image_mobile', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                        </span>
                                    </label>
                                @endif
                            </div>
                        
                        <div class="col-12 col-sm-6">
                            {!! Form::label('', __('Ativo?'), ['class' => 'col-form-label d-inline-block w-100']) !!}
                            {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                        </div>
                    </div>
                    
                    {!! Form::botoesform($pagina, true, true, true, false) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

