@extends('admin::layouts.master-clear')

@section('conteudo')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('news_categorie_id', 'Categoria', [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'news_categorie_id',
                                      ['' => __('Selecione')] + $relacionamentos['news_categorie']->pluck('title','id')->all(),
                                      null,
                                      ['class'=>'form-control m-select2', 'required'])
                                  !!}
                            </div>
                        </div>

                        <div class="col-sm-6">
                        {!! Form::label('date_time', 'Data', ['class' => 'col-6 col-12 col-lg-2 col-form-label']) !!}
                            {!! Form::text('date_time', ((empty($FormModel->date_time))? Carbon\Carbon::now()->format('d/m/Y H:i:s') : null ), ['class' => 'mascara-datahora form-control m-input', 'required' => 'true']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('title', 'Titulo', ['class' => 'col-form-label']) !!}
                            {!! Form::text('title', null, ['class' => 'slugify form-control m-input', 'required' => 'true', 'data-slugged' => 'input#slug']) !!}
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('slug', 'URL Amigável', ['class' => 'col-form-label']) !!}
                            {!! Form::text('slug', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('video', 'Vídeo Url', ['class' => 'col-form-label']) !!}
                            {!! Form::text('video', null, ['class' => 'form-control m-input']) !!}
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('', 'Capa do vídeo', [ 'class' => 'col-form-label' ]) !!}
                            <div class="clearfix"></div>
                            @if( !empty($FormModel->video_cover) )
                                <label for="video_cover"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                    <span>
                                        <i class="fa flaticon-multimedia-1"></i>
                                        <span>{!! __('Selecione uma capa para o vídeo') !!}</span>
                                        {!! Form::fileHTML('video_cover', null, ['id' => 'video_cover', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                    </span>
                                </label>

                                <a href="{{ route('thumb',[ 0, 0, "news/{$FormModel->id}/{$FormModel -> video_cover}" ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                    {!! __('Show video cover') !!}
                                </a>

                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel -> id}/campo/video_cover/path/news/path1/{$FormModel->id}") }}"
                                   data-texto="Deseja remover a video_cover?" data-excluir=""
                                   data-remover=".itemvideo_cover .deletfile, .itemvideo_cover .viewfile" title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="video_cover"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                    <span>
                                        <i class="fa flaticon-multimedia-1"></i>
                                        <span>{!! __('Selecione uma capa para o vídeo') !!}</span>
                                        {!! Form::fileHTML('video_cover', null, ['id' => 'video_cover', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                    </span>
                                </label>
                            @endif
                        </div>
                    </div>

                    {{--<div class="form-group m-form__group row m-0">
                        {!! Form::label('topicos', 'Tópicos', ['class' => 'col-12 col-lg-2 col-form-label']) !!}
                        <div class="col-12 col-lg-5">
                            <div class="m-select2 m-select2--air">
                                <select name="topicos[]" class="form-control m-select2" multiple="multiple"
                                        data-tags="true"
                                        data-placeholder="Crie tópicos" data-allow-clear="true">
                                    @if(!empty($FormModel->topicos))
                                        @foreach ($FormModel->topicos as $topico)
                                            <option value="{!! $topico !!}" selected>
                                                {!! $topico !!}
                                            </option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>--}}

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('synopsis', 'Sinópse', ['class' => 'col-12 col-lg-2 col-form-label']) !!}
                        <div class="col-12 col-lg-5">
                            {!! Form::textarea('synopsis', null, ['class' => 'form-control m-input']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('description', 'Descrição', ['class' => 'col-12 col-lg-2 col-form-label']) !!}
                        <div class="col-12 col-lg-5">
                            {!! Form::textarea('description', null, ['class' => '', 'data-editor-completo' => '']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                        {!! Form::label('', 'Capa da Home', [ 'class' => 'col-12 col-lg-2 col-form-label' ]) !!}
                            @if( !empty($FormModel->home_cover) )
                                <label for="home_cover"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                    <span>
                                        <i class="fa flaticon-multimedia-1"></i>
                                        <span>{!! __('Selecionar uma capa da home') !!}</span>
                                        {!! Form::fileHTML('home_cover', null, ['id' => 'home_cover', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                    </span>
                                </label>

                                <a href="{{ route('thumb',[ 0, 0, "news/{$FormModel->id}/{$FormModel -> home_cover}" ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                    {!! __('Show home cover') !!}
                                </a>

                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel -> id}/campo/home_cover/path/news/path1/{$FormModel->id}") }}"
                                   data-texto="Deseja remover a home_cover?" data-excluir=""
                                   data-remover=".itemhome_cover .deletfile, .itemhome_cover .viewfile" title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="home_cover"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                <span>
                                    <i class="fa flaticon-multimedia-1"></i>
                                    <span>{!! __('Selecione uma capa para a home') !!}</span>
                                    {!! Form::fileHTML('home_cover', null, ['id' => 'home_cover', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                </span>
                                </label>
                                {{--{!! Form::labelHTML('image', 'Capa') !!}
                                {!! Form::label('image', 'Selecionar arquivo', [
                                    'class' => '',
                                    'style' => 'margin-bottom:0;',
                                    'data-tooltip' => '',
                                    'aria-haspopup' => "true",
                                    'data-disable-hover' => "false",
                                    'tabindex' => "2",
                                    'title' => "Selecionar Arquivo do tipo .jpg, jpeg, .gif ou .png"
                                ]) !!}--}}
                            @endif
                        </div>

                        <div class="col-sm-6">
                        {!! Form::label('', 'Capa da Lista', [ 'class' => 'col-12 col-lg-2 col-form-label' ]) !!}
                            @if( !empty($FormModel->list_cover) )
                                <label for="list_cover"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                    <span>
                                        <i class="fa flaticon-multimedia-1"></i>
                                        <span>{!! __('Selecione uma capa para a lista') !!}</span>
                                        {!! Form::fileHTML('list_cover', null, ['id' => 'list_cover', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                    </span>
                                </label>

                                <a href="{{ route('thumb',[ 0, 0, "news/{$FormModel->id}/{$FormModel -> list_cover}" ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                    {!! __('Show list cover') !!}
                                </a>

                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel -> id}/campo/list_cover/path/news/path1/{$FormModel->id}") }}"
                                   data-texto="Deseja remover a list_cover?" data-excluir=""
                                   data-remover=".itemlist_cover .deletfile, .itemlist_cover .viewfile" title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="list_cover"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                <span>
                                    <i class="fa flaticon-multimedia-1"></i>
                                    <span>{!! __('Selecione uma capa para a lista') !!}</span>
                                    {!! Form::fileHTML('list_cover', null, ['id' => 'list_cover', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                </span>
                                </label>
                                {{--{!! Form::labelHTML('image', 'Capa') !!}
                                {!! Form::label('image', 'Selecionar arquivo', [
                                    'class' => '',
                                    'style' => 'margin-bottom:0;',
                                    'data-tooltip' => '',
                                    'aria-haspopup' => "true",
                                    'data-disable-hover' => "false",
                                    'tabindex' => "2",
                                    'title' => "Selecionar Arquivo do tipo .jpg, jpeg, .gif ou .png"
                                ]) !!}--}}
                            @endif
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                        {!! Form::label('', 'Imagem', [ 'class' => 'col-12 col-lg-2 col-form-label' ]) !!}
                            @if( !empty($FormModel->image) )
                                <label for="image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                    <span>
                                        <i class="fa flaticon-multimedia-1"></i>
                                        <span>{!! __('Selecione uma imagem') !!}</span>
                                        {!! Form::fileHTML('image', null, ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                    </span>
                                </label>

                                <a href="{{ route('thumb',[ 0, 0, "news/{$FormModel->id}/{$FormModel -> image}" ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                    {!! __('Show image') !!}
                                </a>

                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel -> id}/campo/image/path/news/path1/{$FormModel->id}") }}"
                                   data-texto="Deseja remover a image?" data-excluir=""
                                   data-remover=".itemimage .deletfile, .itemimage .viewfile" title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                <span>
                                    <i class="fa flaticon-multimedia-1"></i>
                                    <span>{!! __('Selecione uma imagem') !!}</span>
                                    {!! Form::fileHTML('image', null, ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                </span>
                                </label>
                                {{--{!! Form::labelHTML('image', 'Capa') !!}
                                {!! Form::label('image', 'Selecionar arquivo', [
                                    'class' => '',
                                    'style' => 'margin-bottom:0;',
                                    'data-tooltip' => '',
                                    'aria-haspopup' => "true",
                                    'data-disable-hover' => "false",
                                    'tabindex' => "2",
                                    'title' => "Selecionar Arquivo do tipo .jpg, jpeg, .gif ou .png"
                                ]) !!}--}}
                            @endif
                        </div>
                    {{--<div class="form-group m-form__group row m-0">
                        {!! Form::label('', 'Destaque', ['class' => 'col-12 col-lg-2 col-form-label']) !!}
                        <div class="col-12 col-lg-5">
                            {!! Form::statuscor('destaque', ((!empty($FormModel->destaque) && !is_null($FormModel->destaque))? $FormModel->destaque : null) ) !!}
                        </div>
                    </div>--}}
                        <div class="form-group m-form__group row m-0">
                            <div class="col-sm-6 itemimage">
                                {!! Form::label('', 'Imagem Listagem 1', [ 'class' => 'col-form-label d-inline-block w-100' ]) !!}
                                <br>
                                @if( !empty($FormModel->image) )
                                    <label for="image"
                                           class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air float-left mr-1 file-arquivo">
                                        <span>
                                            <span>{!! __('Selecionar') !!}</span>
                                            {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;', 'accept' => 'image/*']) !!}
                                        </span>
                                    </label>
                                    <a href="{{ route('thumb',[ 0, 0, "products/{$FormModel->id}/{$FormModel->image}" ]) }}"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air float-left mr-1 viewfile">
                                        visualizar
                                    </a>
                                    <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image/path/products/path1/{$FormModel->id}") }}"
                                       data-texto="Deseja remover?" data-excluir=""
                                       data-remover=".itemimage .deletfile, .itemimage .viewfile" title="Excluir"
                                       class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air float-left">
                                        <i aria-hidden="true" class="fa fa-times"></i>
                                    </a>
                                @else
                                    <label for="image"
                                           class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                        <span>
                                            <span>{!! __('Selecionar') !!}</span>
                                            {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;', 'accept' => 'image/*']) !!}
                                        </span>
                                    </label>
                                @endif
                            </div>


                        <div class="col-sm-6">
                        {!! Form::label('', 'Ativo?', ['class' => 'col-12 col-lg-2 col-form-label']) !!}
                            {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
