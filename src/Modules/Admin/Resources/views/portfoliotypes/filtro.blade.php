<div class="row m--margin-bottom-20">
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
        <label>{!! __('Título') !!}:</label>
        <input name="title" type="text" class="form-control m-input" placeholder="{!! __('Procure por Título') !!}"
               data-col-index="3">
    </div>
</div>