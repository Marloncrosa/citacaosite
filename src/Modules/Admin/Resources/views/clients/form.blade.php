@extends('admin::layouts.master-clear')

@section('conteudo')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet mb-0">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

                    <div class="form-group m-form__group row m-0">
                        {!! Form::label('avatar', __('Avatar'), [ 'class' => 'col-2 col-form-label' ]) !!}
                        <div class="col-7">
                            @if( !empty($FormModel->avatar) )
                                <label for="avatar"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo float-left mr-2">
                                  <span>

                                    <span>{!! __('Selecionar um avatar') !!}</span>
                                      {!! Form::fileHTML('avatar', null, ['id' => 'avatar', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                  </span>
                                </label>

                                <a href="{{ route('thumb',[ 0, 0, $FormModel->avatar_imagem ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                    {!! __('Exibir avatar') !!}
                                </a>

                                <a
                                        data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel -> id}/campo/avatar/path/users/path1/{$FormModel->id}/path2/avatar") }}"
                                        data-texto="Deseja remover a avatar?" data-excluir=""
                                        data-remover=".itemavatar .deletfile, .itemavatar .viewfile" title="Excluir"
                                        class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="avatar"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                      <span>

                                        <span>{!! __('Selecionar um avatar') !!}</span>
                                          {!! Form::fileHTML('avatar', null, ['id' => 'avatar', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                      </span>
                                </label>
                            @endif
                        </div>
                    </div>
                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('', 'Imagem Mobile', [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            @if( !empty($FormModel->image) )
                                <label for="image_mobile"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                    <span>
                        
                                        <span>{!! __('Selecionar Imagem Mobile') !!}</span>
                                        {!! Form::fileHTML('image_mobile', null, ['id' => 'image_mobile', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                    </span>
                                </label>
                                <a href="{{ route('thumb',[ 0, 0, "banners/{$FormModel->id}/{$FormModel->image_mobile}" ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                    visualizar image_mobile </a>
                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image_mobile/path/banners/path1/{$FormModel->id}") }}"
                                   data-texto="Deseja remover a image_mobile?" data-excluir=""
                                   data-remover=".itemimage_mobile .deletfile, .itemimage_mobile .viewfile" title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="image_mobile"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                    <span>
                        
                                        <span>{!! __('Selecionar Imagem Mobile') !!}</span>
                                        {!! Form::fileHTML('image_mobile', null, ['id' => 'image_mobile','required' => 'true', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                    </span>
                                </label>
                            @endif
                        </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-12">
                            {!! Form::label('name', __('Nome'), ['class' => '']) !!}
                            {!! Form::text('name', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('email', __('E-mail'), ['class' => '']) !!}
                            {!! Form::email('email', null, ['class' => 'form-control m-input', 'required' => 'true']) !!}
                        </div>

                        <div class="col-12 col-sm-6">
                            {!! Form::label('cellphone', __('Celular'), ['class' => '']) !!}
                            {!! Form::text('cellphone', null, ['class' => 'form-control m-input mask-intl-phone']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('password', 'Nova Senha', ['class' => 'col-12 col-form-label']) !!}
                            {!! Form::password('password', array('class' => 'form-control m-input', 'required' => 'true')) !!}
                        </div>

                        <div class="col-sm-6">
                            {!! Form::label('password_confirmation', 'Confirmar Nova Senha', ['class' => 'col-12 col-form-label']) !!}
                            {!! Form::password('password_confirmation', array('class' => 'form-control m-input', 'required' => 'true')) !!}
                        </div>
                    </div>


                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-4">
                            {!! Form::label('document', __('Documento'), [ 'class' => '' ]) !!}
                            {!! Form::text('document', null, ['class' => 'form-control m-input mascara-documento', 'id' => 'document']) !!}
                        </div>

                        <div class="col-12 col-sm-4">
                            {!! Form::label('birthdate', __('Data de Nascimento'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('birthdate', null, ['class' => 'form-control m-input mascara-data', 'id' => 'birthday']) !!}
                            </div>
                        </div>

                        <div class="col-sm-4">
                            {!! Form::label('gender', 'Sexo', [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'gender',
                                      [
                                        '' => __('Selecionar'),
                                        'male' => __('Masculino'),
                                        'female' => __('Feminino'),
                                       ],
                                      null,
                                      ['class'=>'form-control m-select2'])
                                  !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-4">
                            {!! Form::label('country_id', __('País'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'country_id',
                                      ['' => __('Seleccionar')] + $relacionamentos['country']->pluck('country','initials')->all(),
                                      null,
                                      ['class'=>'form-control m-select2', 'id' => 'country_id'])
                                  !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            {!! Form::label('state_id', __('Estado'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'state_id',
                                      ['' => __('Seleccionar')],
                                      null,
                                      ['class'=>'form-control m-select2', 'id' => 'state_id'])
                                  !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-4">
                            {!! Form::label('city_id', __('Cidade'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'city_id',
                                      ['' => __('Selecionar')],
                                      null,
                                      ['class'=>'form-control m-select2', 'id' => 'city_id'])
                                  !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-3">
                            {!! Form::label('postal_code', __('Código Postal'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!!
                                    Form::text('postal_code', null, [
                                        'class' => 'form-control m-input mascara-cep cep-preenche',
                                        'id' => 'postal_code',
                                        'data-estado' => '#state_id',
                                        'data-cidade' => '#city_id',
                                        'data-endereco' => '#address',
                                        'data-bairro' => '#neighborhood',
                                        'data-numero' => '#number',
                                        'data-pais' => '#country_id',
                                    ])
                                !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('address', __('Endereço'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('address', null, ['class' => 'form-control m-input', 'id' => 'address']) !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-3">
                            {!! Form::label('number', __('Número'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('number', null, ['class' => 'form-control m-input', 'id' => 'number']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('address_complement', __('Complemento'), [ 'class' => '' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::text('address_complement', null, ['class' => 'form-control m-input', 'id' => 'address_complement']) !!}
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('neighborhood', __('Bairro'), [ 'class' => '' ]) !!}
                            <div class="">
                                {!! Form::text('neighborhood', null, ['class' => 'form-control m-input', 'id' => 'neighborhood']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-6">
                            {!! Form::label('', __('Ativo?'), ['class' => 'col-lg-2 col-form-label']) !!}
                            {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                        </div>

                        <div class="col-sm-6">
                            {!! Form::label('', __('Link Externo?'), ['class' => 'col-lg-2 col-form-label']) !!}
                            {!! Form::statuscor('external_link', ((!empty($FormModel->external_link) && !is_null($FormModel->external_link))? $FormModel->external_link : null) ) !!}
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
@stop

