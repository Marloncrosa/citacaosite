@extends('admin::layouts.master-clear')

@section('conteudo')
    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet">
                    {!! Form::model($FormModel, ['url' => $urlform, 'class' => 'm-form m-form--label-align-right form-geral', 'method' => 'post', 'files' => true]) !!}

                    {!! Form::hidden($campohidden, $valorhidden, ['required' => 'true']) !!}


                    <div class="form-group m-form__group row m-0">
                        <div class="col-12">
                            {!! Form::label('permissions_group_id', __('Groups'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                {!! Form::select(
                                      'permissions_group_id',
                                      ['' => __('Select')] + $groups->pluck('name','id')->all(),
                                      null,
                                      ['class'=>'form-control m-select2', 'required'])
                                  !!}
                            </div>
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop
