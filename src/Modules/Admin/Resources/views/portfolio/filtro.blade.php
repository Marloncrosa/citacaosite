<div class="row m--margin-bottom-20">
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
        <label>{!! __('Nome') !!}:</label>
        <input name="name" type="text" class="form-control m-input" placeholder="{!! __('Procure por nome') !!}" data-col-index="3">
    </div>
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
        <label>{!! __('E-mail') !!}:</label>
        <input name="email" type="text" class="form-control m-input" placeholder="{!! __('Procure por e-mail') !!}" data-col-index="4">
    </div>
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
        <label>{!! __('Documento (CPF/CNPJ)') !!}:</label>
        <input name="document" type="text" class="form-control m-input mascara-documento" placeholder="{!! __('Procure por documento') !!}" data-col-index="5">
    </div>
    <div class="col-lg-3 m--margin-bottom-10-tablet-and-mobile">
        <label>{!! __('Telefone') !!}:</label>
        <input name="cellphone" type="text" class="form-control m-input mask-intl-phone"  data-col-index="5">
    </div>
</div>