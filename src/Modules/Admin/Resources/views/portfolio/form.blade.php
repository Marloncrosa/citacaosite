@extends('admin::layouts.master-clear')

@section('conteudo')

    <div class="m-content">
        <div class="row">
            <div class="col-lg-12">
                <div class="m-portlet mb-0">
                    {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}

                    @if(!empty($portfolio_types))
                        <div class="col-sm-12">
                            {!! Form::label('line_id', __('Tipos de Portifólios'), [ 'class' => 'col-form-label' ]) !!}
                            <div class="m-select2 m-select2--air">
                                <select id="portfolio_types" name="portfolio_types[]" class="form-control m-select2"
                                        multiple="multiple" data-placeholder="Selecione as Tipos Portfólio"
                                        data-allow-clear="true">
                                    @foreach ($portfolio_types as $postfolio_type)
                                        <option value="{!! $postfolio_type->id!!}" @if(!is_null($FormModel->portfolio_type_id)
                                                            && in_array($postfolio_type->id, $FormModel->portfolio_type_id )) selected @endif>
                                            {!! $postfolio_type->title !!}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    @endif

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12 col-sm-6">
                            {!! Form::label('title', 'Título', ['class' => 'col-form-label']) !!}
                            {!! Form::text('title', null, ['class' => 'slugify form-control m-input', 'required' => 'true', 'data-slugged' => 'input#slug']) !!}
                        </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('slug', 'URL Amigável', ['class' => 'col-form-label']) !!}
                            {!! Form::text('slug', null, ['class' => 'form-control m-input']) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-12">
                            {!! Form::label('sinopsys', 'Sinópse', ['class' => 'col-form-label']) !!}
                            {!! Form::textarea('sinopsys', null, ['class' => '',]) !!}
                        </div>
                    </div>

                    <div class="form-group m-form__group row m-0">
                        <div class="col-sm-4">
                            {!! Form::label('', 'Imagem', [ 'class' => 'col-lg-2 col-form-label' ]) !!}
                            @if( !empty($FormModel->feature_image) )
                                <label for="feature_image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                <span>

                                    <span>{!! __('Selecionar Imagem') !!}</span>
                                    {!! Form::fileHTML('feature_image', null, ['id' => 'feature_image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                </span>
                                </label>
                                <a href="{{ route('thumb',[ 0, 0, "banners/{$FormModel->id}/{$FormModel->feature_image}" ]) }}"
                                   class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top viewfile">
                                    visualizar feature_image </a>
                                <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/feature_image/path/banners/path1/{$FormModel->id}") }}"
                                   data-texto="Deseja remover a feature_image?" data-excluir=""
                                   data-remover=".itemfeature_image .deletfile, .itemfeature_image .viewfile" title="Excluir"
                                   class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air">
                                    <i aria-hidden="true" class="fa fa-times"></i>
                                </a>
                            @else
                                <label for="feature_image"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                <span>

                                    <span>{!! __('Selecionar Imagem') !!}</span>
                                    {!! Form::fileHTML('feature_image', null, ['id' => 'feature_image', 'class' => 'd-none', 'style' => 'margin-bottom:0;']) !!}
                                </span>
                                </label>
                            @endif
                        </div>
                        
                        
                        
                        <div class="form-group m-form__group row m-0">
                            <div class="col-sm-6 itemimage">
                                {!! Form::label('', 'Imagem Listagem 1', [ 'class' => 'col-form-label d-inline-block w-100' ]) !!}
                                <br>
                                @if( !empty($FormModel->image) )
                                    <label for="image"
                                           class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air float-left mr-1 file-arquivo">
                                        <span>
                                            <span>{!! __('Selecionar') !!}</span>
                                            {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;', 'accept' => 'image/*']) !!}
                                        </span>
                                    </label>
                                    <a href="{{ route('thumb',[ 0, 0, "products/{$FormModel->id}/{$FormModel->image}" ]) }}"
                                       class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air float-left mr-1 viewfile">
                                        visualizar
                                    </a>
                                    <a data-ajax="{{ url("{$pagina}/deletararquivo/id/{$FormModel->id}/campo/image/path/products/path1/{$FormModel->id}") }}"
                                       data-texto="Deseja remover?" data-excluir=""
                                       data-remover=".itemimage .deletfile, .itemimage .viewfile" title="Excluir"
                                       class="deletfile btn btn-outline-danger m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--outline-2x m-btn--pill m-btn--air float-left">
                                        <i aria-hidden="true" class="fa fa-times"></i>
                                    </a>
                                @else
                                    <label for="image"
                                           class="btn btn-outline-accent m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air has-tip top file-arquivo">
                                        <span>
                                            <span>{!! __('Selecionar') !!}</span>
                                            {!! Form::file('image', ['id' => 'image', 'class' => 'd-none', 'style' => 'margin-bottom:0;', 'accept' => 'image/*']) !!}
                                        </span>
                                    </label>
                                @endif
                            </div>
                        <div class="col-12 col-sm-6">
                            {!! Form::label('', __('Ativo?'), ['class' => 'col-form-label d-inline-block w-100']) !!}
                            {!! Form::statuscor('active', ((!empty($FormModel->active) && !is_null($FormModel->active))? $FormModel->active : null) ) !!}
                        </div>
                    </div>

                    {!! Form::botoesform($pagina, true, true, true, false) !!}

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop

