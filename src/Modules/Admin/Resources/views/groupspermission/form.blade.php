@extends('admin::layouts.master-clear')

@section('conteudo')
  <div class="m-content">
    <div class="row">
      <div class="col-lg-12">
        <div class="m-portlet">
          {!! Form::model($FormModel, ['url' => $url, 'class' => 'm-form m-form--label-align-right form-geral', 'files' => true]) !!}


          <div class="form-group m-form__group row col m-0">
            {!! Form::label('name', __('Nome'), ['class' => 'col-2 col-form-label']) !!}
            <div class="col-7">
              {!! Form::text('name', null, ['class' => 'form-control m-input', 'required' => 'true', 'id' => 'name']) !!}
            </div>
          </div>

          <div class="form-group m-form__group row col m-0">
            {!! Form::label('permissions', __('Permissões'), ['class' => 'col-2 col-form-label']) !!}
            <div class="clearfix"></div>
            <div class="col-12 form-group m-form__group ro">
              <div class="col-8 col-lg-4">
                <div class="input-group">
                  <input type="text" name="search" class="form-control m-input" placeholder="{!! __('Procure por...') !!}">
                  <div class="input-group-append">
                    <button class="btn btn-secondary clear" type="reset">{!! __('Limpar') !!}</button>
                    <button class="btn btn-secondary search" type="button">{!! __('Pesquisar') !!}</button>
                  </div>
                </div>

              </div>
              <br>
            </div>
            <div class="col-12">
              <div id="permissions" class="tree-demo" role="tree" aria-multiselectable="true" tabindex="0" aria-activedescendant="j3_3" aria-busy="false">
                <ul>
                  <li id="todas_permissioes" data-jstree='{"opened":true}' data-inputname="all_permissions" data-inputvalue="all">
                    {{--<input type="checkbox" name="permission_all"> --}}
                    {!! __('Todas as permissões') !!}
                    <ul>
                      @foreach ($permissions as $key => $permission)
                        <li data-inputname="permission[]" data-inputvalue="{!! $permission->id !!}" data-inputchecked="{!! (!is_null($FormModel->permissions()->wherePivot('permissions_id', $permission->id)->first()))? true : false!!}">
                          {{--<input type="checkbox" name="permission_{!! $permission->id !!}" value="{!! $permission->id !!}">--}}
                          {!! $permission->name !!}
                        </li>
                      @endforeach
                    </ul>
                  </li>
                </ul>
              </div>

            </div>
          </div>

          {!! Form::botoesform($pagina, true, true, true, false) !!}

          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
@stop

@push('line-js')
  <script>
    // real checkboxes
    (function ($, undefined) {
      "use strict";

      var inp = document.createElement("input");
      inp.type = "checkbox";
      inp.className = "jstree-checkbox jstree-realcheckbox";

      $.jstree.defaults.realcheckboxes = {};

      $.jstree.plugins.realcheckboxes = function (options, parent) {
        this.bind = function () {
          parent.bind.call(this);
          this._data.realcheckboxes.uto = false;
          this.element
            .on('changed.jstree uncheck_node.jstree check_node.jstree uncheck_all.jstree check_all.jstree move_node.jstree copy_node.jstree redraw.jstree open_node.jstree ready.jstree loaded.jstree', $.proxy(function () {
                // only if undetermined is in setting
              if(this._data.realcheckboxes.uto) { clearTimeout(this._data.realcheckboxes.uto); }
              this._data.realcheckboxes.uto = setTimeout($.proxy(this._realcheckboxes, this), 50);
            }, this));
        };
        this.redraw_node = function(obj, deep, callback, force_draw) {
          obj = parent.redraw_node.call(this, obj, deep, callback, force_draw);
          if(obj) {
            var i, j, tmp = null, chk = inp.cloneNode(true);

            for(i = 0, j = obj.childNodes.length; i < j; i++) {
              if(obj.childNodes[i] && obj.childNodes[i].className && obj.childNodes[i].className.indexOf("jstree-anchor") !== -1) {
                tmp = obj.childNodes[i];
                break;
              }
            }

            if(tmp) {
              for(i = 0, j = tmp.childNodes.length; i < j; i++) {
                if(tmp.childNodes[i] && tmp.childNodes[i].className && tmp.childNodes[i].className.indexOf("jstree-checkbox") !== -1) {
                  tmp = tmp.childNodes[i];
                  break;
                }
              }
            }

            if(this.settings.realcheckboxes.name){
              chk.name = this.settings.realcheckboxes.name;
            }

            if(tmp && tmp.tagName === "I") {
              // chk.style.backgroundColor = "transparent";
              // chk.style.backgroundImage = "none";
              chk.style.display = "none";
              tmp.appendChild(chk);
            }

            var vinput = $(chk),
              li = vinput.parents('.jstree-node'),
              inputchecked = (li.data('inputchecked'))? li.data('inputchecked') : false,
              inputname = (li.data('inputname'))? li.data('inputname') : false,
              inputvalue = (li.data('inputvalue'))? li.data('inputvalue') : false
            ;

            if(inputvalue){
              vinput.val(inputvalue);
            }
            if(inputname && (vinput.attr('name') == '' || vinput.attr('name') == undefined)){
              vinput.attr('name', inputname)
            }
            if(inputchecked){
              setTimeout(function(){
                vinput.prop('checked', inputchecked);
                $('#permissions').jstree('check_node', li);
              },150)
            }

          }
          return obj;
        };
        this._realcheckboxes = function () {
          var ts = this.settings.checkbox.tie_selection;

          $('.jstree-realcheckbox').each(function () {
            this.checked = (!ts && this.parentNode.parentNode.className.indexOf("jstree-checked") !== -1) || (ts && this.parentNode.parentNode.className.indexOf('jstree-clicked') !== -1);
            this.indeterminate = this.parentNode.className.indexOf("jstree-undetermined") !== -1;
            this.disabled = this.parentNode.parentNode.className.indexOf("disabled") !== -1;
          });
        };
      };
    })(jQuery);

    $(function () {
      var tree = $('#permissions');

      tree.jstree({
        'plugins': ["html_data", "search", "checkbox", "realcheckboxes", "types"],
        'core': {
          "themes": {
            "responsive": false
          }
        },
        "types": {
          "default": {
            "icon": "far fa-list-alt m--font-primary"
          }
        }
      });

      /*tree.on('ready.jstree', function(e, data){
        // regeneratePermission();
      });*/

      tree.on('search.jstree', function (e, data) {
        console.log('aki');
        if((data.res).length <= 0){
          parent.swal({
            text: '{!! __('Permission not found') !!}',
            icon: "error",
          });
        }
      });

      var to = setTimeout(function () {}, 100);
      var search = $('[name="search"]');

      search.on('keydown', function(e){
        if(e.keyCode == 13)
        {
          $('button.search').trigger("click");
          return false;
        }
      });

      $('button.search').on('click', function () {
        clearTimeout(to);
        to = setTimeout(function () {
          var v = search.val();
          tree.jstree(true).search(v, false, true);
        }, 250);
      });

      $('button.clear').on('click', function () {
        clearTimeout(to);
        tree.jstree(true).clear_search();
        search.val('');
      });

    });
  </script>
@endpush
