<?php
/**
 * This file is part of the Laravel Auditing package.
 *
 * @author     Antério Vieira <anteriovieira@gmail.com>
 * @author     Quetzy Garcia  <quetzyg@altek.org>
 * @author     Raphael França <raphaelfrancabsb@gmail.com>
 * @copyright  2015-2018
 *
 * For the full copyright and license information,
 * please view the LICENSE.md file that was distributed
 * with this source code.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');

        Schema::defaultStringLength(191);

        Schema::create('audits', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_type',255)->nullable();

            $table->unsignedBigInteger('user_id')->nullable();

            $table->string('event',255);
            $table->morphs('auditable');

            $table->text('old_values')->nullable();
            $table->text('new_values')->nullable();

            $table->text('url')->nullable();
            $table->ipAddress('ip_address')->nullable();

            $table->text('user_agent')->nullable();
            $table->text('tags')->nullable();

            $table->timestamps();

//            $table->index(['user_id', 'user_type']);
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::drop('audits');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
