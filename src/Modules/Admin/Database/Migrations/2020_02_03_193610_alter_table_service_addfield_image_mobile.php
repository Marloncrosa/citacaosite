<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableServiceAddfieldImageMobile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('service', function (Blueprint $table) {
            if (!Schema::hasColumn('service', 'image_mobile')) {
                $table->string('image_mobile')->nullable();
            }
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('service', function (Blueprint $table) {
            if (Schema::hasColumn('service', 'image_mobile')) {
                $table->dropColumn([
                    'image_mobile',
                ]);
            }
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
