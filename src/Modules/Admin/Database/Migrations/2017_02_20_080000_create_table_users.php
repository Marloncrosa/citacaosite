<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('users', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';

            // PK
            $table->increments('id');
            // Fields
            $table->string('name', 255);
            $table->string('email', 255)->unique();
            $table->string('login', 255)->unique();

            $table->string('password', 255)->nullable();

            $table->string('avatar', 255)->nullable();
            $table->string('cellphone', 30)->nullable();

            $table->json('permissions')->nullable();

            $table->boolean('webmaster')->unsigned()->default(false);
            $table->boolean('active')->unsigned()->default(false);

            $table->rememberToken();

            // Fields of created_at and updated_at
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropifExists('users');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
