<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('service', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';

            // PK
            $table->increments('id');

            $table->json('service_characteristics_id')->nullable();
//            $table
//                ->foreign('service_characteristics_id')
//                ->references('id')
//                ->on('service_characteristics')
//                ->onDelete('set null')
//                ->onUpdate('set null');

            $table->json('service_characteristics_highlights_id')->nullable();
//            $table
//                ->foreign('service_characteristics_highlights_id')
//                ->references('id')
//                ->on('service_characteristics')
//                ->onDelete('set null')
//                ->onUpdate('set null');

            // Fields
            $table->text('title');
            $table->text('description');
            $table->decimal('value');
            $table->boolean('active')->unsigned()->default(true);

            $table->timestamps();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('service');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
