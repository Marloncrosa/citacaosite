<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLeadsStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('leads_status', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';

            // PK
            $table->increments('id');

            // FK
            $table->integer('language_id')->unsigned()->nullable();
            $table
                ->foreign('language_id')
                ->references('id')
                ->on('language')
                ->onDelete('set null')
                ->onUpdate('set null')
            ;

            $table->integer('unit_id')->unsigned()->nullable();
            $table
                ->foreign('unit_id')
                ->references('id')
                ->on('unitleads_statuss')
                ->onDelete('set null')
                ->onUpdate('set null')
            ;

            $table->integer('similars_id')->unsigned()->nullable();
            $table
                ->foreign('similars_id')
                ->references('id')
                ->on('leads_status')
                ->onDelete('set null')
                ->onUpdate('set null')
            ;

            // Fields
            $table->string('title');
            $table->string('color')->nullable();

            $table->boolean('active')->unsigned()->default(false);

            // Fields of created_at and updated_at
            $table->timestamps();

            // Soft Delete
            $table->softDeletes();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('leads_status');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
