<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrableNews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('news', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';

            // PK
            $table->increments('id');

            // FK
            $table->integer('news_categorie_id')->unsigned()->nullable();
            $table
                ->foreign('news_categorie_id')
                ->references('id')
                ->on('news_categories')
                ->onDelete('set null')
                ->onUpdate('set null');

            // Fields
            $table->string('slug');
            $table->string('title');
            $table->text('synopsis')->nullable();
            $table->text('description')->nullable();
            $table->text('video')->nullable();
            $table->string('video_cover')->nullable();
            $table->string('image')->nullable();
            $table->string('home_cover')->nullable();
            $table->string('list_cover')->nullable();
            $table->dateTime('date_time')->nullable();
            $table->string('metadescription')->nullable();
            $table->boolean('active')->unsigned()->default(false);

            // Fields of created_at and updated_at
            $table->timestamps();

            // Soft Delete
            $table->softDeletes();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('news');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
