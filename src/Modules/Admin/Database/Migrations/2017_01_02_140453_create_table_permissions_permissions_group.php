<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermissionsPermissionsGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions_permissionsgroup', function (Blueprint $table) {
            // Charset's e Collation
            $table->charset = 'utf8'; // Charset's e Collation
            $table->collation = 'utf8_unicode_ci';
            $table->engine = 'InnoDB'; // Table type

            // Fields
            $table
                ->integer('permissions_id')
                ->nullable()
                ->unsigned(); // Chave estrangeira (FK)
            $table
                ->foreign('permissions_id')
                ->references('id')
                ->on('permissions')
                ->onDelete('restrict')
                ->onUpdate('cascade'); // Cria o relacionamento

            $table
                ->integer('permissions_group_id')
                ->nullable()
                ->unsigned(); // Chave estrangeira (FK)
            $table
                ->foreign('permissions_group_id')
                ->references('id')
                ->on('permissions_group')
                ->onDelete('cascade')
                ->onUpdate('cascade'); // Cria o relacionamento

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('permissions_permissionsgroup');
        Schema::enableForeignKeyConstraints();
    }
}
