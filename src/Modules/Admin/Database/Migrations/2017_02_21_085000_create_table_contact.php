<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContact extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('contact', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';

            // PK
            $table->increments('id')->index();

            // FK
            $table->integer('contact_subjects_id')->unsigned()->nullable();
            $table
                ->foreign('contact_subjects_id')
                ->references('id')
                ->on('contact_subjects')
                ->onDelete('set null')
                ->onUpdate('set null')
            ;

            // Fields
            $table->string('name')->nullable();
            $table->string('email');
            $table->string('phone', 30)->nullable();
            $table->string('cellphone', 30)->nullable();
            $table->text('message');


            // Fields of created_at and updated_at
            $table->timestamps();

            // Soft Delete
            $table->softDeletes();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('contact');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}
