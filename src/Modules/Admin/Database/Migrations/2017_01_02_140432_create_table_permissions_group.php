<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermissionsGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions_group', function (Blueprint $table) {
            // Charset's e Collation
            $table->charset = 'utf8'; // Charset's e Collation
            $table->collation = 'utf8_unicode_ci';
            $table->engine = 'InnoDB'; // Table type

            // Fields
            $table->increments('id'); // Campo de identificação, chave primária, auto incremento
            $table->string('name', 255); // Campo do tipo varchar (255), referente ao nome da permissão

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('permissions_group');
        Schema::enableForeignKeyConstraints();
    }
}
