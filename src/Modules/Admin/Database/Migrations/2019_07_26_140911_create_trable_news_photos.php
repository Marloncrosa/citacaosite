<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrableNewsPhotos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('news_photos', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';

            // PK
            $table->increments('id');

            // FK
            $table->integer('news_id')->unsigned()->nullable();
            $table
                ->foreign('news_id')
                ->references('id')
                ->on('news')
                ->onDelete('set null')
                ->onUpdate('set null');

            // Fields
            $table->string('title')->nullable();
            $table->string('image');
            $table->string('legend')->nullable();
            $table->boolean('active')->unsigned()->default(true);

            // Fields of created_at and updated_at
            $table->timestamps();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('news_photos');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
