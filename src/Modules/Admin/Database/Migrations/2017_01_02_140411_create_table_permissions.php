<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            // Charset's e Collation
            $table->charset = 'utf8'; // Charset's e Collation
            $table->collation = 'utf8_unicode_ci';
            $table->engine = 'InnoDB'; // Table type

            // Fields
            $table->increments('id'); // Campo de identificação, chave primária, auto incremento
            $table->string('name', 255); // Campo do tipo varchar (255), referente ao nome da permissão
            $table->string('slug', 255); // Campo do tipo varchar (255), referente ao slug que é gerado baseado no nome da permissão

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('permissions');
        Schema::enableForeignKeyConstraints();
    }
}
