<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriarTableUserPermissionsGroup extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_permissions_group', function (Blueprint $table) {
            // Charset's e Collation
            $table->charset = 'utf8'; // Charset's e Collation
            $table->collation = 'utf8_unicode_ci';
            $table->engine = 'InnoDB'; // Table type

            // Fields
            $table
                ->integer('user_id')
                ->nullable()
                ->unsigned(); // Chave estrangeira (FK)
            $table
                ->foreign('user_id', 'user_id_foreign')
                ->references('id')
                ->on('users')
                ->onDelete('restrict')->onUpdate('cascade'); // Cria o relacionamento

            $table
                ->integer('permissions_group_id')
                ->nullable()
                ->unsigned(); // Chave estrangeira (FK)
            $table
                ->foreign('permissions_group_id', 'permissions_group_id_foreign')
                ->references('id')
                ->on('permissions_group')
                ->onDelete('restrict')->onUpdate('cascade'); // Cria o relacionamento

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('user_permissions_group');
        Schema::enableForeignKeyConstraints();
    }
}
