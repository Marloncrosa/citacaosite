<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('products', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';
            // PK
            $table->increments('id');
            // FK
            $table->json('product_categories')->nullable();
            // FK
            $table->json('product_aditionals')->nullable();
            // FK
            $table->json('brands')->nullable();
            // FK
            $table->json('product_lines')->nullable();
            // FK
            $table->json('product_collections')->nullable();

            // Fields
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->string('code')->nullable();
            $table->text('sinopsys')->nullable();
            $table->text('description')->nullable();
            $table->decimal('amount')->nullable();
            $table->text('image')->nullable();
            $table->decimal('value')->nullable();
            $table->boolean('active')->unsigned()->default(false);

            // Fields of created_at and updated_at
            $table->timestamps();

            // Soft Delete
            $table->softDeletes();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('products');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
