<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLeads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('leads', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';

            // PK
            $table->increments('id');

            // FK
            $table->integer('leads_origin_id')->unsigned()->nullable();
            $table
                ->foreign('leads_origin_id')
                ->references('id')
                ->on('leads_origin')
                ->onDelete('set null')
                ->onUpdate('set null')
            ;

            // FK
            $table->integer('leads_status_id')->unsigned()->nullable();
            $table
                ->foreign('leads_status_id')
                ->references('id')
                ->on('leads_status')
                ->onDelete('set null')
                ->onUpdate('set null')
            ;

            // FK
            $table->integer('operator_id')->unsigned()->nullable();
            $table
                ->foreign('operator_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null')
                ->onUpdate('set null')
            ;

            // FK
            $table->integer('country_id')->unsigned()->nullable();
            $table
                ->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('set null')
                ->onUpdate('set null');


            // FK
            $table->integer('state_id')->unsigned()->nullable();
            $table
                ->foreign('state_id')
                ->references('id')
                ->on('states')
                ->onDelete('set null')
                ->onUpdate('set null');

            // FK
            $table->integer('city_id')->unsigned()->nullable();
            $table
                ->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onDelete('set null')
                ->onUpdate('set null');

            // FK
            $table->integer('client_id')->unsigned()->nullable();
            $table
                ->foreign('client_id')
                ->references('id')
                ->on('clients')
                ->onDelete('set null')
                ->onUpdate('set null');

            // Fields
            $table->string('name', 255)->nullable();
            $table->string('cellphone')->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('email')->nullable();
            $table->string('cpf', 20)->nullable();
            $table->string('rg', 20)->nullable();

            $table->string('profession')->nullable();
            $table->date('birthdate')->nullable();

            // Address
            $table->string('address')->nullable();
            $table->string('postal_code', 30)->nullable();
            $table->string('number', 11)->nullable();
            $table->string('address_complement')->nullable();
            $table->string('neighborhood')->nullable();

            $table->boolean('term')->unsigned()->default(false);

            $table->json('answers')->nullable();

            $table->text('observations')->nullable();

            // $table->boolean('already_client')->default(false);

            $table->timestamps();

            // Soft Delete
            $table->softDeletes();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('leads');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
