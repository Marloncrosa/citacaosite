<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('clients', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';

            // PK
            $table->increments('id');

            // FK
            $table->integer('country_id')->unsigned()->nullable();
            $table
                ->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onDelete('set null')
                ->onUpdate('set null');

            $table->integer('state_id')->unsigned()->nullable();
            $table
                ->foreign('state_id')
                ->references('id')
                ->on('states')
                ->onDelete('set null')
                ->onUpdate('set null');

            $table->integer('city_id')->unsigned()->nullable();
            $table
                ->foreign('city_id')
                ->references('id')
                ->on('cities')
                ->onDelete('set null')
                ->onUpdate('set null');

            // Fields
            $table->string('name', 255);
            $table->string('email', 255)->unique()->nullable();
            $table->string('cellphone', 45);


            $table->enum('gender',['female', 'male'])->nullable();;
            $table->string('document', 255)->nullable();
            $table->string('password', 255)->nullable();

            $table->string('avatar')->nullable();

            $table->date('birthdate')->nullable();

            // Address
            $table->string('postal_code', 30)->nullable();
            $table->string('address')->nullable();
            $table->string('number', 11)->nullable();
            $table->string('address_complement')->nullable();
            $table->string('neighborhood')->nullable();

            $table->decimal('value')->nullable();

            $table->boolean('external_link')->unsigned()->default(false);
            $table->boolean('active')->unsigned()->default(false);

            $table->rememberToken();

            // Fields of created_at and updated_at
            $table->timestamps();

            // Soft Delete
            $table->softDeletes();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('clients');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
