<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLeadsOrigin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('leads_origin', function (Blueprint $table) {
            $table->charset = 'utf8mb4';
            $table->collation = 'utf8mb4_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';

            // PK
            $table->increments('id');

            // Fields
            $table->string('title');

            $table->boolean('active')->unsigned()->default(false);

            // Fields of created_at and updated_at
            $table->timestamps();

            // Soft Delete
            $table->softDeletes();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('leads_origin');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
