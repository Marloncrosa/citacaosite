<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePortfolioGallery extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('portfolio_gallery', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';
            // PK
            $table->increments('id');

            // FK
            $table->integer('portfolio_id')->unsigned();
            $table
                ->foreign('portfolio_id')
                ->references('id')
                ->on('portfolio')
                ->onDelete('restrict')
                ->onUpdate('cascade')
            ;

            // Fields
            $table->string('title')->nullable();
            $table->string('image')->nullable();
            $table->string('legend')->nullable();
            $table->boolean('active')->unsigned()->default(true);

            // Fields of created_at and updated_at
            $table->timestamps();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('portfolio_gallery');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
