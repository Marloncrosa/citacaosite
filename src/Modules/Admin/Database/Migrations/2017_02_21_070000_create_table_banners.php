<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBanners extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('banners', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';

            // PK
            $table->increments('id');

            // FK
            $table->integer('language_id')->unsigned()->nullable();
            $table
                ->foreign('language_id')
                ->references('id')
                ->on('language')
                ->onDelete('set null')
                ->onUpdate('set null')
            ;

            // Fields
            $table->string('title');
            $table->string('subtitle')->nullable();
            $table->text('description')->nullable();
            $table->string('video')->nullable();
            $table->string('video_image')->nullable();
            $table->string('image')->nullable();

            $table->date('start')->nullable();
            $table->date('end')->nullable();

            $table->string('link')->nullable();

            $table->boolean('active')->unsigned()->default(false);

            // Fields of created_at and updated_at
            $table->timestamps();

            // Soft Delete
            $table->softDeletes();

        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::drop('banners');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}
