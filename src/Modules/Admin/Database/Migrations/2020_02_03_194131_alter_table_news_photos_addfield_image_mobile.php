<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNewsPhotosAddfieldImageMobile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('news_photos', function (Blueprint $table) {
            if (!Schema::hasColumn('news_photos', 'image_mobile')) {
                $table->string('image_mobile')->nullable();
            }
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('news_photos', function (Blueprint $table) {
            if (Schema::hasColumn('news_photos', 'image_mobile')) {
                $table->dropColumn([
                    'image_mobile',
                ]);
            }
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
