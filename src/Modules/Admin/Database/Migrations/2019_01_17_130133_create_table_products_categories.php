<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProductsCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::create('products_categories', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';

            // Table type
            $table->engine = 'InnoDB';

            //Parent ID
            $table->increments('id');

            $table->integer('parent_id')->unsigned()->nullable();
            $table
                ->foreign('parent_id')
                ->references('id')
                ->on('products_categories')
                ->onDelete('set null')
                ->onUpdate('set null')
            ;

            // Fields
            $table->string('title', 255)->nullable();
            $table->string('slug')->nullable();
            $table->string('video_link')->nullable();
            $table->string('image')->nullable();
            $table->string('background_image')->nullable();

            $table->boolean('active')->default(false);

            $table->timestamps();

            $table->softDeletes();
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::dropIfExists('products_categories');
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
