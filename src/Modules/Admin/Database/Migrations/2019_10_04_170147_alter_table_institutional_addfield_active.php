<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableInstitutionalAddfieldActive extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('institutional', function (Blueprint $table) {
            if (!Schema::hasColumn('institutional', 'active')) {
                $table->boolean('active')->default(true);
            }
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('institutional', function (Blueprint $table) {
            if (Schema::hasColumn('institutional', 'active')) {
                $table->dropColumn([
                    'active',
                ]);
            }
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
