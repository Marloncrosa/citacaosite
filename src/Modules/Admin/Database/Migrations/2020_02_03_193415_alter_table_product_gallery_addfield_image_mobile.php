<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableProductGalleryAddfieldImageMobile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('product_gallery', function (Blueprint $table) {
            if (!Schema::hasColumn('product_gallery', 'image_mobile')) {
                $table->string('image_mobile')->nullable();
            }
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Schema::table('product_gallery', function (Blueprint $table) {
            if (Schema::hasColumn('product_gallery', 'image_mobile')) {
                $table->dropColumn([
                    'image_mobile',
                ]);
            }
        });
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
