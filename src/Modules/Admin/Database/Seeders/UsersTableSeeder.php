<?php namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\Users;
use Modules\Admin\Entities\PermissionsGroup;

class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if (Users::get()->count() <= 0) {
            Users::truncate();

            // Usuários Admin
            $user = Users::create([
                'name' => 'Aireset (Master)',
                'email' => 'felipe@aireset.com.br',
                'login' => 'aireset',
                'password' => '@ir&sEt2019',
                'webmaster' => '1',
                'active' => '1'
            ]);


            $user->groups()->sync(
                PermissionsGroup::pluck('id')->all()
            );
        }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
