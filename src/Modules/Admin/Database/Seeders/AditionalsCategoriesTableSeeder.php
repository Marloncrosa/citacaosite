<?php namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use DB;
use Modules\Admin\Entities\AditionalsCategories;

class AditionalsCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if (AditionalsCategories::get()->count() <= 0) {
            $content = [
                [ 'title' => 'capa', 'slug' => str_slug('capa'), 'active' => true ],
            ];
            AditionalsCategories::insert($content);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
