<?php namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use DB;
use Modules\Admin\Entities\ProductsCategories;

class ProductsCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if (ProductsCategories::get()->count() <= 0) {
            $content = [
                [ 'id' => '1' ,'title' => 'Modelos', 'slug' => str_slug('Modelos'), 'active' => true ],
                [ 'id' => '2' ,'title' => 'Formato', 'slug' => str_slug('Formato'), 'active' => true ],
                [ 'id' => '3' ,'title' => 'Acessórios', 'slug' => str_slug('Acessórios'), 'active' => true ],
            ];
            ProductsCategories::insert($content);
        }
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
