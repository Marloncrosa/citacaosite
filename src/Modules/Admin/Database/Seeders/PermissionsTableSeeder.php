<?php namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\Permissions as ModelC;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (ModelC::get()->count() <= 0) {
            ModelC::truncate();

            $permissions = [
                'Acesso ao Menu permissões',
                'Listar permissões',
                'Criar/Editar permissões',
                'Deletar permissão',

                'Acesso ao Menu grupo de permissões',
                'Listar grupo de permissões',
                'Criar/Editar grupo de permissões',
                'Deletar grupo de permissão',

                'Acesso ao Menu usuários',
                'Listar usuários',
                'Criar/Editar usuários',
                'Deletar usuário',

                'Acesso ao Menu paises',
                'Listar paises',
                'Criar/Editar paises',
                'Deletar país',

                'Acesso ao Menu estados',
                'Listar estados',
                'Criar/Editar estados',
                'Deletar estado',

                'Acesso ao Menu cidades',
                'Listar cidades',
                'Criar/Editar cidades',
                'Deletar cidade',

                'Acesso ao Menu Contatos',
                'Listar Contatos',
                'Criar/Editar Contatos',
                'Deletar Contatos',

                'Acesso ao Menu newsletter',
                'Listar newsletter',
                'Criar/Editar newsletter',
                'Deletar newsletter',

                'Acesso ao Menu Testimony',
                'Listar Testimony',
                'Criar/Editar Testimony',
                'Deletar Testimony',

                'Acesso ao Menu Pages',
                'Listar Pages',
                'Criar/Editar Pages',
                'Deletar Pages',

                'Acesso ao Menu FAQ',
                'Listar FAQ',
                'Criar/Editar FAQ',
                'Deletar FAQ',

                'Acesso ao Menu contatos',
                'Listar contatos',
                'Criar/Editar contatos',
                'Deletar contatos',

                'Acesso ao Menu assuntos contatos',
                'Listar Acesso ao Menu assuntos contatos',
                'Criar/Editar Acesso ao Menu assuntos contatos',
                'Deletar Menu assuntos contatos',

                'Acesso ao Menu clientes',
                'Listar clientes',
                'Criar/Editar clientes',
                'Deletar clientes',

                'Acesso ao Menu produtos',
                'Listar produtos',
                'Criar/Editar produtos',
                'Deletar produtos',

                'Acesso ao Menu categorias de produtos',
                'Listar categorias de produtos',
                'Criar/Editar categorias de produtos',
                'Deletar categorias de produtos',

                'Acesso ao Menu Banners',
                'Listar Banners',
                'Criar/Editar Banners',
                'Deletar Banners',

                'Acesso ao Menu institucional',
                'Listar institucional',
                'Criar/Editar institucional',
                'Deletar institucional',

                'Acesso ao Menu logos',
                'Listar logos',
                'Criar/Editar logos',
                'Deletar logos',

                'Acesso ao Menu categorias blog',
                'Listar categorias blog',
                'Criar/Editar categorias blog',
                'Deletar categorias blog',

                'Acesso ao Menu blog',
                'Listar blog',
                'Criar/Editar blog',
                'Deletar blog',

                'Acesso ao Menu caracteristicas',
                'Listar caracteristicas',
                'Criar/Editar caracteristicas',
                'Deletar caracteristicas',

                'Acesso ao Menu servicos',
                'Listar servicos',
                'Criar/Editar servicos',
                'Deletar Acesso servicos',
            ];

            foreach ($permissions as $permission) {
                if( ModelC::where('slug',str_slug($permission))->get()->count() <= 0 ){
                    ModelC::create([
                        'name' => $permission,
                        'slug' => str_slug($permission)
                    ]);
                }
            }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
