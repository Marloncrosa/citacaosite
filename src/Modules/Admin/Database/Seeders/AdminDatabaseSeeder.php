<?php namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class AdminDatabaseSeeder extends Seeder
{

    private $obseeds = [
        PermissionsTableSeeder::class,
        PermissionsGroupTableSeeder::class,
        UsersTableSeeder::class,
        CountryTableSeeder::class,

        ## Brazil
        \Modules\Admin\Database\Seeders\Cidades\Brasil\StatesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\AcreCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\AlagoasCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\AmapaCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\AmazonasCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\BahiaCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\CearaCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\DistritoFederalCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\EspiritoSantoCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\GoiasCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\MaranhaoCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\MatoGrossoCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\MatoGrossoDoSulCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\MinasGeraisCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\ParaCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\ParaibaCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\ParanaCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\PernambucoCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\PiauiCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\RiodeJaneiroCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\RioGrandedoNorteCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\RioGrandedoSulCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\RondoniaCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\RoraimaCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\SantaCatarinaCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\SaoPauloCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\SergipeCitiesTableSeeder::class,
        \Modules\Admin\Database\Seeders\Cidades\Brasil\TocantinsCitiesTableSeeder::class,

        // TypesAddressTableSeeder::class,
        // OrderStatusTableSeeder::class,
        ProductsCategoriesTableSeeder::class,
        // AditionalsCategoriesTableSeeder::class,
    ];

    private $seeds = [
        // UsersTableSeeder::class,
    ];

    private $testseed = [
//        NewsCategoriesTableSeeder::class,
//        NewsTableSeeder::class,
//        ProductsCategoriesTableSeeder::class,
//        ProductsTableSeeder::class,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!\File::exists('public/uploads')) \File::makeDirectory('public/uploads');
        Model::unguard();

        // Seeders obrigatórios
        foreach ($this->obseeds as $seed) {
            $this->call($seed);
        }

        if (getenv('APP_ENV') == 'local') {
            foreach ($this->testseed as $seed) {
                $this->call($seed);
            }
        } else {
            foreach ($this->seeds as $seed) {
                $this->call($seed);
            }
        }


        Model::reguard();
    }

}
