<?php namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\TypesAddress;
use Modules\Admin\Entities\PermissionsGroup;

class TypesAddressTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if (TypesAddress::get()->count() <= 0) {
            TypesAddress::truncate();

            TypesAddress::create([
                'title' => 'Casa',
                'active' => '1'
            ]);
            TypesAddress::create([
                'title' => 'Apartamento',
                'active' => '1'
            ]);
            TypesAddress::create([
                'title' => 'Comercial',
                'active' => '1'
            ]);
        }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
