<?php namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\OrderStatus;
use Modules\Admin\Entities\PermissionsGroup;

class OrderStatusTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if (OrderStatus::get()->count() <= 0) {
            OrderStatus::truncate();

            OrderStatus::create([
                'name' => 'Pendente',
                'slug' => str_slug('Pendente'),
                'active' => '1'
            ]);
            OrderStatus::create([
                'name' => 'Em Andamento',
                'slug' => str_slug('Em Andamento'),
                'active' => '1'
            ]);
            OrderStatus::create([
                'name' => 'Concluído',
                'slug' => str_slug('Concluído'),
                'active' => '1'
            ]);
        }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }

}
