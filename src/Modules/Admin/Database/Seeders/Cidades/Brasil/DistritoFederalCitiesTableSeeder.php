<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class DistritoFederalCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        // mb_strtoupper("")
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
            // City::truncate();
            $cities = [
                ['id' => 5570, 'state_id' => 57, 'city' => mb_strtoupper("Brasília", "UTF-8")],
            ];

            // foreach ($cities as $city) {
            //     try {
                    City::insert($cities);
                // } catch (\Exception $e) {
                //
                // }
            // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
