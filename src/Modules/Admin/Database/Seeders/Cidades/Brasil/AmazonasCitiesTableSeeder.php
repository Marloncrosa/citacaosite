<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class AmazonasCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
            // City::truncate();
            $cities = [
                ['id' => 75, 'state_id' => 54, 'city' => mb_strtoupper("Alvarães", "UTF-8")],
                ['id' => 76, 'state_id' => 54, 'city' => mb_strtoupper("Amaturá", "UTF-8")],
                ['id' => 77, 'state_id' => 54, 'city' => mb_strtoupper("Anamã", "UTF-8")],
                ['id' => 78, 'state_id' => 54, 'city' => mb_strtoupper("Anori", "UTF-8")],
                ['id' => 79, 'state_id' => 54, 'city' => mb_strtoupper("Apuí", "UTF-8")],
                ['id' => 80, 'state_id' => 54, 'city' => mb_strtoupper("Atalaia do Norte", "UTF-8")],
                ['id' => 81, 'state_id' => 54, 'city' => mb_strtoupper("Autazes", "UTF-8")],
                ['id' => 82, 'state_id' => 54, 'city' => mb_strtoupper("Barcelos", "UTF-8")],
                ['id' => 83, 'state_id' => 54, 'city' => mb_strtoupper("Barreirinha", "UTF-8")],
                ['id' => 84, 'state_id' => 54, 'city' => mb_strtoupper("Benjamin Constant", "UTF-8")],
                ['id' => 85, 'state_id' => 54, 'city' => mb_strtoupper("Beruri", "UTF-8")],
                ['id' => 86, 'state_id' => 54, 'city' => mb_strtoupper("Boa Vista do Ramos", "UTF-8")],
                ['id' => 87, 'state_id' => 54, 'city' => mb_strtoupper("Boca do Acre", "UTF-8")],
                ['id' => 88, 'state_id' => 54, 'city' => mb_strtoupper("Borba", "UTF-8")],
                ['id' => 89, 'state_id' => 54, 'city' => mb_strtoupper("Caapiranga", "UTF-8")],
                ['id' => 90, 'state_id' => 54, 'city' => mb_strtoupper("Canutama", "UTF-8")],
                ['id' => 91, 'state_id' => 54, 'city' => mb_strtoupper("Carauari", "UTF-8")],
                ['id' => 92, 'state_id' => 54, 'city' => mb_strtoupper("Careiro", "UTF-8")],
                ['id' => 93, 'state_id' => 54, 'city' => mb_strtoupper("Careiro da Várzea", "UTF-8")],
                ['id' => 94, 'state_id' => 54, 'city' => mb_strtoupper("Coari", "UTF-8")],
                ['id' => 95, 'state_id' => 54, 'city' => mb_strtoupper("Codajás", "UTF-8")],
                ['id' => 96, 'state_id' => 54, 'city' => mb_strtoupper("Eirunepé", "UTF-8")],
                ['id' => 97, 'state_id' => 54, 'city' => mb_strtoupper("Envira", "UTF-8")],
                ['id' => 98, 'state_id' => 54, 'city' => mb_strtoupper("Fonte Boa", "UTF-8")],
                ['id' => 99, 'state_id' => 54, 'city' => mb_strtoupper("Guajará", "UTF-8")],
                ['id' => 100, 'state_id' => 54, 'city' => mb_strtoupper("Humaitá", "UTF-8")],
                ['id' => 101, 'state_id' => 54, 'city' => mb_strtoupper("Ipixuna", "UTF-8")],
                ['id' => 102, 'state_id' => 54, 'city' => mb_strtoupper("Iranduba", "UTF-8")],
                ['id' => 103, 'state_id' => 54, 'city' => mb_strtoupper("Itacoatiara", "UTF-8")],
                ['id' => 104, 'state_id' => 54, 'city' => mb_strtoupper("Itamarati", "UTF-8")],
                ['id' => 105, 'state_id' => 54, 'city' => mb_strtoupper("Itapiranga", "UTF-8")],
                ['id' => 106, 'state_id' => 54, 'city' => mb_strtoupper("Japurá", "UTF-8")],
                ['id' => 107, 'state_id' => 54, 'city' => mb_strtoupper("Juruá", "UTF-8")],
                ['id' => 108, 'state_id' => 54, 'city' => mb_strtoupper("Jutaí", "UTF-8")],
                ['id' => 109, 'state_id' => 54, 'city' => mb_strtoupper("Lábrea", "UTF-8")],
                ['id' => 110, 'state_id' => 54, 'city' => mb_strtoupper("Manacapuru", "UTF-8")],
                ['id' => 111, 'state_id' => 54, 'city' => mb_strtoupper("Manaquiri", "UTF-8")],
                ['id' => 112, 'state_id' => 54, 'city' => mb_strtoupper("Manaus", "UTF-8")],
                ['id' => 113, 'state_id' => 54, 'city' => mb_strtoupper("Manicoré", "UTF-8")],
                ['id' => 114, 'state_id' => 54, 'city' => mb_strtoupper("Maraã", "UTF-8")],
                ['id' => 115, 'state_id' => 54, 'city' => mb_strtoupper("Maués", "UTF-8")],
                ['id' => 116, 'state_id' => 54, 'city' => mb_strtoupper("Nhamundá", "UTF-8")],
                ['id' => 117, 'state_id' => 54, 'city' => mb_strtoupper("Nova Olinda do Norte", "UTF-8")],
                ['id' => 118, 'state_id' => 54, 'city' => mb_strtoupper("Novo Airão", "UTF-8")],
                ['id' => 119, 'state_id' => 54, 'city' => mb_strtoupper("Novo Aripuanã", "UTF-8")],
                ['id' => 120, 'state_id' => 54, 'city' => mb_strtoupper("Parintins", "UTF-8")],
                ['id' => 121, 'state_id' => 54, 'city' => mb_strtoupper("Pauini", "UTF-8")],
                ['id' => 122, 'state_id' => 54, 'city' => mb_strtoupper("Presidente Figueiredo", "UTF-8")],
                ['id' => 123, 'state_id' => 54, 'city' => mb_strtoupper("Rio Preto da Eva", "UTF-8")],
                ['id' => 124, 'state_id' => 54, 'city' => mb_strtoupper("Santa Isabel do Rio Negro", "UTF-8")],
                ['id' => 125, 'state_id' => 54, 'city' => mb_strtoupper("Santo Antônio do Içá", "UTF-8")],
                ['id' => 126, 'state_id' => 54, 'city' => mb_strtoupper("São Gabriel da Cachoeira", "UTF-8")],
                ['id' => 127, 'state_id' => 54, 'city' => mb_strtoupper("São Paulo de Olivença", "UTF-8")],
                ['id' => 128, 'state_id' => 54, 'city' => mb_strtoupper("São Sebastião do Uatumã", "UTF-8")],
                ['id' => 129, 'state_id' => 54, 'city' => mb_strtoupper("Silves", "UTF-8")],
                ['id' => 130, 'state_id' => 54, 'city' => mb_strtoupper("Tabatinga", "UTF-8")],
                ['id' => 131, 'state_id' => 54, 'city' => mb_strtoupper("Tapauá", "UTF-8")],
                ['id' => 132, 'state_id' => 54, 'city' => mb_strtoupper("Tefé", "UTF-8")],
                ['id' => 133, 'state_id' => 54, 'city' => mb_strtoupper("Tonantins", "UTF-8")],
                ['id' => 134, 'state_id' => 54, 'city' => mb_strtoupper("Uarini", "UTF-8")],
                ['id' => 135, 'state_id' => 54, 'city' => mb_strtoupper("Urucará", "UTF-8")],
                ['id' => 136, 'state_id' => 54, 'city' => mb_strtoupper("Urucurituba", "UTF-8")],
            ];

            // foreach ($cities as $city) {
            //     try {
                    City::insert($cities);
                // } catch (\Exception $e) {
                //
                // }
            // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
