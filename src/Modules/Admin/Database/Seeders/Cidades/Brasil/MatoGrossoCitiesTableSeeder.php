<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class MatoGrossoCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
            // City::truncate();
            $cities = [
                ['id' => 5183, 'state_id' => 61, 'city' => mb_strtoupper("Acorizal", "UTF-8")],
                ['id' => 5184, 'state_id' => 61, 'city' => mb_strtoupper("Água Boa", "UTF-8")],
                ['id' => 5185, 'state_id' => 61, 'city' => mb_strtoupper("Alta Floresta", "UTF-8")],
                ['id' => 5186, 'state_id' => 61, 'city' => mb_strtoupper("Alto Araguaia", "UTF-8")],
                ['id' => 5187, 'state_id' => 61, 'city' => mb_strtoupper("Alto Boa Vista", "UTF-8")],
                ['id' => 5188, 'state_id' => 61, 'city' => mb_strtoupper("Alto Garças", "UTF-8")],
                ['id' => 5189, 'state_id' => 61, 'city' => mb_strtoupper("Alto Paraguai", "UTF-8")],
                ['id' => 5190, 'state_id' => 61, 'city' => mb_strtoupper("Alto Taquari", "UTF-8")],
                ['id' => 5191, 'state_id' => 61, 'city' => mb_strtoupper("Apiacás", "UTF-8")],
                ['id' => 5192, 'state_id' => 61, 'city' => mb_strtoupper("Araguaiana", "UTF-8")],
                ['id' => 5193, 'state_id' => 61, 'city' => mb_strtoupper("Araguainha", "UTF-8")],
                ['id' => 5194, 'state_id' => 61, 'city' => mb_strtoupper("Araputanga", "UTF-8")],
                ['id' => 5195, 'state_id' => 61, 'city' => mb_strtoupper("Arenápolis", "UTF-8")],
                ['id' => 5196, 'state_id' => 61, 'city' => mb_strtoupper("Aripuanã", "UTF-8")],
                ['id' => 5197, 'state_id' => 61, 'city' => mb_strtoupper("Barão de Melgaço", "UTF-8")],
                ['id' => 5198, 'state_id' => 61, 'city' => mb_strtoupper("Barra do Bugres", "UTF-8")],
                ['id' => 5199, 'state_id' => 61, 'city' => mb_strtoupper("Barra do Garças", "UTF-8")],
                ['id' => 5200, 'state_id' => 61, 'city' => mb_strtoupper("Bom Jesus do Araguaia", "UTF-8")],
                ['id' => 5201, 'state_id' => 61, 'city' => mb_strtoupper("Brasnorte", "UTF-8")],
                ['id' => 5202, 'state_id' => 61, 'city' => mb_strtoupper("Cáceres", "UTF-8")],
                ['id' => 5203, 'state_id' => 61, 'city' => mb_strtoupper("Campinápolis", "UTF-8")],
                ['id' => 5204, 'state_id' => 61, 'city' => mb_strtoupper("Campo Novo do Parecis", "UTF-8")],
                ['id' => 5205, 'state_id' => 61, 'city' => mb_strtoupper("Campo Verde", "UTF-8")],
                ['id' => 5206, 'state_id' => 61, 'city' => mb_strtoupper("Campos de Júlio", "UTF-8")],
                ['id' => 5207, 'state_id' => 61, 'city' => mb_strtoupper("Canabrava do Norte", "UTF-8")],
                ['id' => 5208, 'state_id' => 61, 'city' => mb_strtoupper("Canarana", "UTF-8")],
                ['id' => 5209, 'state_id' => 61, 'city' => mb_strtoupper("Carlinda", "UTF-8")],
                ['id' => 5210, 'state_id' => 61, 'city' => mb_strtoupper("Castanheira", "UTF-8")],
                ['id' => 5211, 'state_id' => 61, 'city' => mb_strtoupper("Chapada dos Guimarães", "UTF-8")],
                ['id' => 5212, 'state_id' => 61, 'city' => mb_strtoupper("Cláudia", "UTF-8")],
                ['id' => 5213, 'state_id' => 61, 'city' => mb_strtoupper("Cocalinho", "UTF-8")],
                ['id' => 5214, 'state_id' => 61, 'city' => mb_strtoupper("Colíder", "UTF-8")],
                ['id' => 5215, 'state_id' => 61, 'city' => mb_strtoupper("Colniza", "UTF-8")],
                ['id' => 5216, 'state_id' => 61, 'city' => mb_strtoupper("Comodoro", "UTF-8")],
                ['id' => 5217, 'state_id' => 61, 'city' => mb_strtoupper("Confresa", "UTF-8")],
                ['id' => 5218, 'state_id' => 61, 'city' => mb_strtoupper("Conquista D'Oeste", "UTF-8")],
                ['id' => 5219, 'state_id' => 61, 'city' => mb_strtoupper("Cotriguaçu", "UTF-8")],
                ['id' => 5220, 'state_id' => 61, 'city' => mb_strtoupper("Cuiabá", "UTF-8")],
                ['id' => 5221, 'state_id' => 61, 'city' => mb_strtoupper("Curvelândia", "UTF-8")],
                ['id' => 5222, 'state_id' => 61, 'city' => mb_strtoupper("Denise", "UTF-8")],
                ['id' => 5223, 'state_id' => 61, 'city' => mb_strtoupper("Diamantino", "UTF-8")],
                ['id' => 5224, 'state_id' => 61, 'city' => mb_strtoupper("Dom Aquino", "UTF-8")],
                ['id' => 5225, 'state_id' => 61, 'city' => mb_strtoupper("Feliz Natal", "UTF-8")],
                ['id' => 5226, 'state_id' => 61, 'city' => mb_strtoupper("Figueirópolis D'Oeste", "UTF-8")],
                ['id' => 5227, 'state_id' => 61, 'city' => mb_strtoupper("Gaúcha do Norte", "UTF-8")],
                ['id' => 5228, 'state_id' => 61, 'city' => mb_strtoupper("General Carneiro", "UTF-8")],
                ['id' => 5229, 'state_id' => 61, 'city' => mb_strtoupper("Glória D'Oeste", "UTF-8")],
                ['id' => 5230, 'state_id' => 61, 'city' => mb_strtoupper("Guarantã do Norte", "UTF-8")],
                ['id' => 5231, 'state_id' => 61, 'city' => mb_strtoupper("Guiratinga", "UTF-8")],
                ['id' => 5232, 'state_id' => 61, 'city' => mb_strtoupper("Indiavaí", "UTF-8")],
                ['id' => 5233, 'state_id' => 61, 'city' => mb_strtoupper("Ipiranga do Norte", "UTF-8")],
                ['id' => 5234, 'state_id' => 61, 'city' => mb_strtoupper("Itanhangá", "UTF-8")],
                ['id' => 5235, 'state_id' => 61, 'city' => mb_strtoupper("Itaúba", "UTF-8")],
                ['id' => 5236, 'state_id' => 61, 'city' => mb_strtoupper("Itiquira", "UTF-8")],
                ['id' => 5237, 'state_id' => 61, 'city' => mb_strtoupper("Jaciara", "UTF-8")],
                ['id' => 5238, 'state_id' => 61, 'city' => mb_strtoupper("Jangada", "UTF-8")],
                ['id' => 5239, 'state_id' => 61, 'city' => mb_strtoupper("Jauru", "UTF-8")],
                ['id' => 5240, 'state_id' => 61, 'city' => mb_strtoupper("Juara", "UTF-8")],
                ['id' => 5241, 'state_id' => 61, 'city' => mb_strtoupper("Juína", "UTF-8")],
                ['id' => 5242, 'state_id' => 61, 'city' => mb_strtoupper("Juruena", "UTF-8")],
                ['id' => 5243, 'state_id' => 61, 'city' => mb_strtoupper("Juscimeira", "UTF-8")],
                ['id' => 5244, 'state_id' => 61, 'city' => mb_strtoupper("Lambari D'Oeste", "UTF-8")],
                ['id' => 5245, 'state_id' => 61, 'city' => mb_strtoupper("Lucas do Rio Verde", "UTF-8")],
                ['id' => 5246, 'state_id' => 61, 'city' => mb_strtoupper("Luciara", "UTF-8")],
                ['id' => 5247, 'state_id' => 61, 'city' => mb_strtoupper("Vila Bela da Santíssima Trindade", "UTF-8")],
                ['id' => 5248, 'state_id' => 61, 'city' => mb_strtoupper("Marcelândia", "UTF-8")],
                ['id' => 5249, 'state_id' => 61, 'city' => mb_strtoupper("Matupá", "UTF-8")],
                ['id' => 5250, 'state_id' => 61, 'city' => mb_strtoupper("Mirassol d'Oeste", "UTF-8")],
                ['id' => 5251, 'state_id' => 61, 'city' => mb_strtoupper("Nobres", "UTF-8")],
                ['id' => 5252, 'state_id' => 61, 'city' => mb_strtoupper("Nortelândia", "UTF-8")],
                ['id' => 5253, 'state_id' => 61, 'city' => mb_strtoupper("Nossa Senhora do Livramento", "UTF-8")],
                ['id' => 5254, 'state_id' => 61, 'city' => mb_strtoupper("Nova Bandeirantes", "UTF-8")],
                ['id' => 5255, 'state_id' => 61, 'city' => mb_strtoupper("Nova Nazaré", "UTF-8")],
                ['id' => 5256, 'state_id' => 61, 'city' => mb_strtoupper("Nova Lacerda", "UTF-8")],
                ['id' => 5257, 'state_id' => 61, 'city' => mb_strtoupper("Nova Santa Helena", "UTF-8")],
                ['id' => 5258, 'state_id' => 61, 'city' => mb_strtoupper("Nova Brasilândia", "UTF-8")],
                ['id' => 5259, 'state_id' => 61, 'city' => mb_strtoupper("Nova Canaã do Norte", "UTF-8")],
                ['id' => 5260, 'state_id' => 61, 'city' => mb_strtoupper("Nova Mutum", "UTF-8")],
                ['id' => 5261, 'state_id' => 61, 'city' => mb_strtoupper("Nova Olímpia", "UTF-8")],
                ['id' => 5262, 'state_id' => 61, 'city' => mb_strtoupper("Nova Ubiratã", "UTF-8")],
                ['id' => 5263, 'state_id' => 61, 'city' => mb_strtoupper("Nova Xavantina", "UTF-8")],
                ['id' => 5264, 'state_id' => 61, 'city' => mb_strtoupper("Novo Mundo", "UTF-8")],
                ['id' => 5265, 'state_id' => 61, 'city' => mb_strtoupper("Novo Horizonte do Norte", "UTF-8")],
                ['id' => 5266, 'state_id' => 61, 'city' => mb_strtoupper("Novo São Joaquim", "UTF-8")],
                ['id' => 5267, 'state_id' => 61, 'city' => mb_strtoupper("Paranaíta", "UTF-8")],
                ['id' => 5268, 'state_id' => 61, 'city' => mb_strtoupper("Paranatinga", "UTF-8")],
                ['id' => 5269, 'state_id' => 61, 'city' => mb_strtoupper("Novo Santo Antônio", "UTF-8")],
                ['id' => 5270, 'state_id' => 61, 'city' => mb_strtoupper("Pedra Preta", "UTF-8")],
                ['id' => 5271, 'state_id' => 61, 'city' => mb_strtoupper("Peixoto de Azevedo", "UTF-8")],
                ['id' => 5272, 'state_id' => 61, 'city' => mb_strtoupper("Planalto da Serra", "UTF-8")],
                ['id' => 5273, 'state_id' => 61, 'city' => mb_strtoupper("Poconé", "UTF-8")],
                ['id' => 5274, 'state_id' => 61, 'city' => mb_strtoupper("Pontal do Araguaia", "UTF-8")],
                ['id' => 5275, 'state_id' => 61, 'city' => mb_strtoupper("Ponte Branca", "UTF-8")],
                ['id' => 5276, 'state_id' => 61, 'city' => mb_strtoupper("Pontes e Lacerda", "UTF-8")],
                ['id' => 5277, 'state_id' => 61, 'city' => mb_strtoupper("Porto Alegre do Norte", "UTF-8")],
                ['id' => 5278, 'state_id' => 61, 'city' => mb_strtoupper("Porto dos Gaúchos", "UTF-8")],
                ['id' => 5279, 'state_id' => 61, 'city' => mb_strtoupper("Porto Esperidião", "UTF-8")],
                ['id' => 5280, 'state_id' => 61, 'city' => mb_strtoupper("Porto Estrela", "UTF-8")],
                ['id' => 5281, 'state_id' => 61, 'city' => mb_strtoupper("Poxoréu", "UTF-8")],
                ['id' => 5282, 'state_id' => 61, 'city' => mb_strtoupper("Primavera do Leste", "UTF-8")],
                ['id' => 5283, 'state_id' => 61, 'city' => mb_strtoupper("Querência", "UTF-8")],
                ['id' => 5284, 'state_id' => 61, 'city' => mb_strtoupper("São José dos Quatro Marcos", "UTF-8")],
                ['id' => 5285, 'state_id' => 61, 'city' => mb_strtoupper("Reserva do Cabaçal", "UTF-8")],
                ['id' => 5286, 'state_id' => 61, 'city' => mb_strtoupper("Ribeirão Cascalheira", "UTF-8")],
                ['id' => 5287, 'state_id' => 61, 'city' => mb_strtoupper("Ribeirãozinho", "UTF-8")],
                ['id' => 5288, 'state_id' => 61, 'city' => mb_strtoupper("Rio Branco", "UTF-8")],
                ['id' => 5289, 'state_id' => 61, 'city' => mb_strtoupper("Santa Carmem", "UTF-8")],
                ['id' => 5290, 'state_id' => 61, 'city' => mb_strtoupper("Santo Afonso", "UTF-8")],
                ['id' => 5291, 'state_id' => 61, 'city' => mb_strtoupper("São José do Povo", "UTF-8")],
                ['id' => 5292, 'state_id' => 61, 'city' => mb_strtoupper("São José do Rio Claro", "UTF-8")],
                ['id' => 5293, 'state_id' => 61, 'city' => mb_strtoupper("São José do Xingu", "UTF-8")],
                ['id' => 5294, 'state_id' => 61, 'city' => mb_strtoupper("São Pedro da Cipa", "UTF-8")],
                ['id' => 5295, 'state_id' => 61, 'city' => mb_strtoupper("Rondolândia", "UTF-8")],
                ['id' => 5296, 'state_id' => 61, 'city' => mb_strtoupper("Rondonópolis", "UTF-8")],
                ['id' => 5297, 'state_id' => 61, 'city' => mb_strtoupper("Rosário Oeste", "UTF-8")],
                ['id' => 5298, 'state_id' => 61, 'city' => mb_strtoupper("Santa Cruz do Xingu", "UTF-8")],
                ['id' => 5299, 'state_id' => 61, 'city' => mb_strtoupper("Salto do Céu", "UTF-8")],
                ['id' => 5300, 'state_id' => 61, 'city' => mb_strtoupper("Santa Rita do Trivelato", "UTF-8")],
                ['id' => 5301, 'state_id' => 61, 'city' => mb_strtoupper("Santa Terezinha", "UTF-8")],
                ['id' => 5302, 'state_id' => 61, 'city' => mb_strtoupper("Santo Antônio do Leste", "UTF-8")],
                ['id' => 5303, 'state_id' => 61, 'city' => mb_strtoupper("Santo Antônio do Leverger", "UTF-8")],
                ['id' => 5304, 'state_id' => 61, 'city' => mb_strtoupper("São Félix do Araguaia", "UTF-8")],
                ['id' => 5305, 'state_id' => 61, 'city' => mb_strtoupper("Sapezal", "UTF-8")],
                ['id' => 5306, 'state_id' => 61, 'city' => mb_strtoupper("Serra Nova Dourada", "UTF-8")],
                ['id' => 5307, 'state_id' => 61, 'city' => mb_strtoupper("Sinop", "UTF-8")],
                ['id' => 5308, 'state_id' => 61, 'city' => mb_strtoupper("Sorriso", "UTF-8")],
                ['id' => 5309, 'state_id' => 61, 'city' => mb_strtoupper("Tabaporã", "UTF-8")],
                ['id' => 5310, 'state_id' => 61, 'city' => mb_strtoupper("Tangará da Serra", "UTF-8")],
                ['id' => 5311, 'state_id' => 61, 'city' => mb_strtoupper("Tapurah", "UTF-8")],
                ['id' => 5312, 'state_id' => 61, 'city' => mb_strtoupper("Terra Nova do Norte", "UTF-8")],
                ['id' => 5313, 'state_id' => 61, 'city' => mb_strtoupper("Tesouro", "UTF-8")],
                ['id' => 5314, 'state_id' => 61, 'city' => mb_strtoupper("Torixoréu", "UTF-8")],
                ['id' => 5315, 'state_id' => 61, 'city' => mb_strtoupper("União do Sul", "UTF-8")],
                ['id' => 5316, 'state_id' => 61, 'city' => mb_strtoupper("Vale de São Domingos", "UTF-8")],
                ['id' => 5317, 'state_id' => 61, 'city' => mb_strtoupper("Várzea Grande", "UTF-8")],
                ['id' => 5318, 'state_id' => 61, 'city' => mb_strtoupper("Vera", "UTF-8")],
                ['id' => 5319, 'state_id' => 61, 'city' => mb_strtoupper("Vila Rica", "UTF-8")],
                ['id' => 5320, 'state_id' => 61, 'city' => mb_strtoupper("Nova Guarita", "UTF-8")],
                ['id' => 5321, 'state_id' => 61, 'city' => mb_strtoupper("Nova Marilândia", "UTF-8")],
                ['id' => 5322, 'state_id' => 61, 'city' => mb_strtoupper("Nova Maringá", "UTF-8")],
                ['id' => 5323, 'state_id' => 61, 'city' => mb_strtoupper("Nova Monte Verde", "UTF-8")],

            ];

            // foreach ($cities as $city) {
            //     try {
                    City::insert($cities);
                // } catch (\Exception $e) {
                //
                // }
            // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
