<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class SergipeCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
        $cities = [
            ['id' => 1753, 'state_id' => 76, 'city' => mb_strtoupper("Amparo de São Francisco", "UTF-8")],
            ['id' => 1754, 'state_id' => 76, 'city' => mb_strtoupper("Aquidabã", "UTF-8")],
            ['id' => 1755, 'state_id' => 76, 'city' => mb_strtoupper("Aracaju", "UTF-8")],
            ['id' => 1756, 'state_id' => 76, 'city' => mb_strtoupper("Arauá", "UTF-8")],
            ['id' => 1757, 'state_id' => 76, 'city' => mb_strtoupper("Areia Branca", "UTF-8")],
            ['id' => 1758, 'state_id' => 76, 'city' => mb_strtoupper("Barra dos Coqueiros", "UTF-8")],
            ['id' => 1759, 'state_id' => 76, 'city' => mb_strtoupper("Boquim", "UTF-8")],
            ['id' => 1760, 'state_id' => 76, 'city' => mb_strtoupper("Brejo Grande", "UTF-8")],
            ['id' => 1761, 'state_id' => 76, 'city' => mb_strtoupper("Campo do Brito", "UTF-8")],
            ['id' => 1762, 'state_id' => 76, 'city' => mb_strtoupper("Canhoba", "UTF-8")],
            ['id' => 1763, 'state_id' => 76, 'city' => mb_strtoupper("Canindé de São Francisco", "UTF-8")],
            ['id' => 1764, 'state_id' => 76, 'city' => mb_strtoupper("Capela", "UTF-8")],
            ['id' => 1765, 'state_id' => 76, 'city' => mb_strtoupper("Carira", "UTF-8")],
            ['id' => 1766, 'state_id' => 76, 'city' => mb_strtoupper("Carmópolis", "UTF-8")],
            ['id' => 1767, 'state_id' => 76, 'city' => mb_strtoupper("Cedro de São João", "UTF-8")],
            ['id' => 1768, 'state_id' => 76, 'city' => mb_strtoupper("Cristinápolis", "UTF-8")],
            ['id' => 1769, 'state_id' => 76, 'city' => mb_strtoupper("Cumbe", "UTF-8")],
            ['id' => 1770, 'state_id' => 76, 'city' => mb_strtoupper("Divina Pastora", "UTF-8")],
            ['id' => 1771, 'state_id' => 76, 'city' => mb_strtoupper("Estância", "UTF-8")],
            ['id' => 1772, 'state_id' => 76, 'city' => mb_strtoupper("Feira Nova", "UTF-8")],
            ['id' => 1773, 'state_id' => 76, 'city' => mb_strtoupper("Frei Paulo", "UTF-8")],
            ['id' => 1774, 'state_id' => 76, 'city' => mb_strtoupper("Gararu", "UTF-8")],
            ['id' => 1775, 'state_id' => 76, 'city' => mb_strtoupper("General Maynard", "UTF-8")],
            ['id' => 1776, 'state_id' => 76, 'city' => mb_strtoupper("Gracho Cardoso", "UTF-8")],
            ['id' => 1777, 'state_id' => 76, 'city' => mb_strtoupper("Ilha das Flores", "UTF-8")],
            ['id' => 1778, 'state_id' => 76, 'city' => mb_strtoupper("Indiaroba", "UTF-8")],
            ['id' => 1779, 'state_id' => 76, 'city' => mb_strtoupper("Itabaiana", "UTF-8")],
            ['id' => 1780, 'state_id' => 76, 'city' => mb_strtoupper("Itabaianinha", "UTF-8")],
            ['id' => 1781, 'state_id' => 76, 'city' => mb_strtoupper("Itabi", "UTF-8")],
            ['id' => 1782, 'state_id' => 76, 'city' => mb_strtoupper("Itaporanga d'Ajuda", "UTF-8")],
            ['id' => 1783, 'state_id' => 76, 'city' => mb_strtoupper("Japaratuba", "UTF-8")],
            ['id' => 1784, 'state_id' => 76, 'city' => mb_strtoupper("Japoatã", "UTF-8")],
            ['id' => 1785, 'state_id' => 76, 'city' => mb_strtoupper("Lagarto", "UTF-8")],
            ['id' => 1786, 'state_id' => 76, 'city' => mb_strtoupper("Laranjeiras", "UTF-8")],
            ['id' => 1787, 'state_id' => 76, 'city' => mb_strtoupper("Macambira", "UTF-8")],
            ['id' => 1788, 'state_id' => 76, 'city' => mb_strtoupper("Malhada dos Bois", "UTF-8")],
            ['id' => 1789, 'state_id' => 76, 'city' => mb_strtoupper("Malhador", "UTF-8")],
            ['id' => 1790, 'state_id' => 76, 'city' => mb_strtoupper("Maruim", "UTF-8")],
            ['id' => 1791, 'state_id' => 76, 'city' => mb_strtoupper("Moita Bonita", "UTF-8")],
            ['id' => 1792, 'state_id' => 76, 'city' => mb_strtoupper("Monte Alegre de Sergipe", "UTF-8")],
            ['id' => 1793, 'state_id' => 76, 'city' => mb_strtoupper("Muribeca", "UTF-8")],
            ['id' => 1794, 'state_id' => 76, 'city' => mb_strtoupper("Neópolis", "UTF-8")],
            ['id' => 1795, 'state_id' => 76, 'city' => mb_strtoupper("Nossa Senhora Aparecida", "UTF-8")],
            ['id' => 1796, 'state_id' => 76, 'city' => mb_strtoupper("Nossa Senhora da Glória", "UTF-8")],
            ['id' => 1797, 'state_id' => 76, 'city' => mb_strtoupper("Nossa Senhora das Dores", "UTF-8")],
            ['id' => 1798, 'state_id' => 76, 'city' => mb_strtoupper("Nossa Senhora de Lourdes", "UTF-8")],
            ['id' => 1799, 'state_id' => 76, 'city' => mb_strtoupper("Nossa Senhora do Socorro", "UTF-8")],
            ['id' => 1800, 'state_id' => 76, 'city' => mb_strtoupper("Pacatuba", "UTF-8")],
            ['id' => 1801, 'state_id' => 76, 'city' => mb_strtoupper("Pedra Mole", "UTF-8")],
            ['id' => 1802, 'state_id' => 76, 'city' => mb_strtoupper("Pedrinhas", "UTF-8")],
            ['id' => 1803, 'state_id' => 76, 'city' => mb_strtoupper("Pinhão", "UTF-8")],
            ['id' => 1804, 'state_id' => 76, 'city' => mb_strtoupper("Pirambu", "UTF-8")],
            ['id' => 1805, 'state_id' => 76, 'city' => mb_strtoupper("Poço Redondo", "UTF-8")],
            ['id' => 1806, 'state_id' => 76, 'city' => mb_strtoupper("Poço Verde", "UTF-8")],
            ['id' => 1807, 'state_id' => 76, 'city' => mb_strtoupper("Porto da Folha", "UTF-8")],
            ['id' => 1808, 'state_id' => 76, 'city' => mb_strtoupper("Propriá", "UTF-8")],
            ['id' => 1809, 'state_id' => 76, 'city' => mb_strtoupper("Riachão do Dantas", "UTF-8")],
            ['id' => 1810, 'state_id' => 76, 'city' => mb_strtoupper("Riachuelo", "UTF-8")],
            ['id' => 1811, 'state_id' => 76, 'city' => mb_strtoupper("Ribeirópolis", "UTF-8")],
            ['id' => 1812, 'state_id' => 76, 'city' => mb_strtoupper("Rosário do Catete", "UTF-8")],
            ['id' => 1813, 'state_id' => 76, 'city' => mb_strtoupper("Salgado", "UTF-8")],
            ['id' => 1814, 'state_id' => 76, 'city' => mb_strtoupper("Santa Luzia do Itanhy", "UTF-8")],
            ['id' => 1815, 'state_id' => 76, 'city' => mb_strtoupper("Santana do São Francisco", "UTF-8")],
            ['id' => 1816, 'state_id' => 76, 'city' => mb_strtoupper("Santa Rosa de Lima", "UTF-8")],
            ['id' => 1817, 'state_id' => 76, 'city' => mb_strtoupper("Santo Amaro das Brotas", "UTF-8")],
            ['id' => 1818, 'state_id' => 76, 'city' => mb_strtoupper("São Cristóvão", "UTF-8")],
            ['id' => 1819, 'state_id' => 76, 'city' => mb_strtoupper("São Domingos", "UTF-8")],
            ['id' => 1820, 'state_id' => 76, 'city' => mb_strtoupper("São Francisco", "UTF-8")],
            ['id' => 1821, 'state_id' => 76, 'city' => mb_strtoupper("São Miguel do Aleixo", "UTF-8")],
            ['id' => 1822, 'state_id' => 76, 'city' => mb_strtoupper("Simão Dias", "UTF-8")],
            ['id' => 1823, 'state_id' => 76, 'city' => mb_strtoupper("Siriri", "UTF-8")],
            ['id' => 1824, 'state_id' => 76, 'city' => mb_strtoupper("Telha", "UTF-8")],
            ['id' => 1825, 'state_id' => 76, 'city' => mb_strtoupper("Tobias Barreto", "UTF-8")],
            ['id' => 1826, 'state_id' => 76, 'city' => mb_strtoupper("Tomar do Geru", "UTF-8")],
            ['id' => 1827, 'state_id' => 76, 'city' => mb_strtoupper("Umbaúba", "UTF-8")],
        ];

        // foreach ($cities as $city) {
        // try {
            City::insert($cities);
        // } catch (\Exception $e) {
        //
        // }
        // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
