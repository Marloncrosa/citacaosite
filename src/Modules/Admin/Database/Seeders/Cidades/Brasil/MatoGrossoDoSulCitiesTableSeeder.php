<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class MatoGrossoDoSulCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
            // City::truncate();
            $cities = [
                ['id' => 5104, 'state_id' => 62, 'city' => mb_strtoupper("Água Clara", "UTF-8")],
                ['id' => 5105, 'state_id' => 62, 'city' => mb_strtoupper("Alcinópolis", "UTF-8")],
                ['id' => 5106, 'state_id' => 62, 'city' => mb_strtoupper("Amambai", "UTF-8")],
                ['id' => 5107, 'state_id' => 62, 'city' => mb_strtoupper("Anastácio", "UTF-8")],
                ['id' => 5108, 'state_id' => 62, 'city' => mb_strtoupper("Anaurilândia", "UTF-8")],
                ['id' => 5109, 'state_id' => 62, 'city' => mb_strtoupper("Angélica", "UTF-8")],
                ['id' => 5110, 'state_id' => 62, 'city' => mb_strtoupper("Antônio João", "UTF-8")],
                ['id' => 5111, 'state_id' => 62, 'city' => mb_strtoupper("Aparecida do Taboado", "UTF-8")],
                ['id' => 5112, 'state_id' => 62, 'city' => mb_strtoupper("Aquidauana", "UTF-8")],
                ['id' => 5113, 'state_id' => 62, 'city' => mb_strtoupper("Aral Moreira", "UTF-8")],
                ['id' => 5114, 'state_id' => 62, 'city' => mb_strtoupper("Bandeirantes", "UTF-8")],
                ['id' => 5115, 'state_id' => 62, 'city' => mb_strtoupper("Bataguassu", "UTF-8")],
                ['id' => 5116, 'state_id' => 62, 'city' => mb_strtoupper("Batayporã", "UTF-8")],
                ['id' => 5117, 'state_id' => 62, 'city' => mb_strtoupper("Bela Vista", "UTF-8")],
                ['id' => 5118, 'state_id' => 62, 'city' => mb_strtoupper("Bodoquena", "UTF-8")],
                ['id' => 5119, 'state_id' => 62, 'city' => mb_strtoupper("Bonito", "UTF-8")],
                ['id' => 5120, 'state_id' => 62, 'city' => mb_strtoupper("Brasilândia", "UTF-8")],
                ['id' => 5121, 'state_id' => 62, 'city' => mb_strtoupper("Caarapó", "UTF-8")],
                ['id' => 5122, 'state_id' => 62, 'city' => mb_strtoupper("Camapuã", "UTF-8")],
                ['id' => 5123, 'state_id' => 62, 'city' => mb_strtoupper("Campo Grande", "UTF-8")],
                ['id' => 5124, 'state_id' => 62, 'city' => mb_strtoupper("Caracol", "UTF-8")],
                ['id' => 5125, 'state_id' => 62, 'city' => mb_strtoupper("Cassilândia", "UTF-8")],
                ['id' => 5126, 'state_id' => 62, 'city' => mb_strtoupper("Chapadão do Sul", "UTF-8")],
                ['id' => 5127, 'state_id' => 62, 'city' => mb_strtoupper("Corguinho", "UTF-8")],
                ['id' => 5128, 'state_id' => 62, 'city' => mb_strtoupper("Coronel Sapucaia", "UTF-8")],
                ['id' => 5129, 'state_id' => 62, 'city' => mb_strtoupper("Corumbá", "UTF-8")],
                ['id' => 5130, 'state_id' => 62, 'city' => mb_strtoupper("Costa Rica", "UTF-8")],
                ['id' => 5131, 'state_id' => 62, 'city' => mb_strtoupper("Coxim", "UTF-8")],
                ['id' => 5132, 'state_id' => 62, 'city' => mb_strtoupper("Deodápolis", "UTF-8")],
                ['id' => 5133, 'state_id' => 62, 'city' => mb_strtoupper("Dois Irmãos do Buriti", "UTF-8")],
                ['id' => 5134, 'state_id' => 62, 'city' => mb_strtoupper("Douradina", "UTF-8")],
                ['id' => 5135, 'state_id' => 62, 'city' => mb_strtoupper("Dourados", "UTF-8")],
                ['id' => 5136, 'state_id' => 62, 'city' => mb_strtoupper("Eldorado", "UTF-8")],
                ['id' => 5137, 'state_id' => 62, 'city' => mb_strtoupper("Fátima do Sul", "UTF-8")],
                ['id' => 5138, 'state_id' => 62, 'city' => mb_strtoupper("Figueirão", "UTF-8")],
                ['id' => 5139, 'state_id' => 62, 'city' => mb_strtoupper("Glória de Dourados", "UTF-8")],
                ['id' => 5140, 'state_id' => 62, 'city' => mb_strtoupper("Guia Lopes da Laguna", "UTF-8")],
                ['id' => 5141, 'state_id' => 62, 'city' => mb_strtoupper("Iguatemi", "UTF-8")],
                ['id' => 5142, 'state_id' => 62, 'city' => mb_strtoupper("Inocência", "UTF-8")],
                ['id' => 5143, 'state_id' => 62, 'city' => mb_strtoupper("Itaporã", "UTF-8")],
                ['id' => 5144, 'state_id' => 62, 'city' => mb_strtoupper("Itaquiraí", "UTF-8")],
                ['id' => 5145, 'state_id' => 62, 'city' => mb_strtoupper("Ivinhema", "UTF-8")],
                ['id' => 5146, 'state_id' => 62, 'city' => mb_strtoupper("Japorã", "UTF-8")],
                ['id' => 5147, 'state_id' => 62, 'city' => mb_strtoupper("Jaraguari", "UTF-8")],
                ['id' => 5148, 'state_id' => 62, 'city' => mb_strtoupper("Jardim", "UTF-8")],
                ['id' => 5149, 'state_id' => 62, 'city' => mb_strtoupper("Jateí", "UTF-8")],
                ['id' => 5150, 'state_id' => 62, 'city' => mb_strtoupper("Juti", "UTF-8")],
                ['id' => 5151, 'state_id' => 62, 'city' => mb_strtoupper("Ladário", "UTF-8")],
                ['id' => 5152, 'state_id' => 62, 'city' => mb_strtoupper("Laguna Carapã", "UTF-8")],
                ['id' => 5153, 'state_id' => 62, 'city' => mb_strtoupper("Maracaju", "UTF-8")],
                ['id' => 5154, 'state_id' => 62, 'city' => mb_strtoupper("Miranda", "UTF-8")],
                ['id' => 5155, 'state_id' => 62, 'city' => mb_strtoupper("Mundo Novo", "UTF-8")],
                ['id' => 5156, 'state_id' => 62, 'city' => mb_strtoupper("Naviraí", "UTF-8")],
                ['id' => 5157, 'state_id' => 62, 'city' => mb_strtoupper("Nioaque", "UTF-8")],
                ['id' => 5158, 'state_id' => 62, 'city' => mb_strtoupper("Nova Alvorada do Sul", "UTF-8")],
                ['id' => 5159, 'state_id' => 62, 'city' => mb_strtoupper("Nova Andradina", "UTF-8")],
                ['id' => 5160, 'state_id' => 62, 'city' => mb_strtoupper("Novo Horizonte do Sul", "UTF-8")],
                ['id' => 5161, 'state_id' => 62, 'city' => mb_strtoupper("Paraíso das Águas", "UTF-8")],
                ['id' => 5162, 'state_id' => 62, 'city' => mb_strtoupper("Paranaíba", "UTF-8")],
                ['id' => 5163, 'state_id' => 62, 'city' => mb_strtoupper("Paranhos", "UTF-8")],
                ['id' => 5164, 'state_id' => 62, 'city' => mb_strtoupper("Pedro Gomes", "UTF-8")],
                ['id' => 5165, 'state_id' => 62, 'city' => mb_strtoupper("Ponta Porã", "UTF-8")],
                ['id' => 5166, 'state_id' => 62, 'city' => mb_strtoupper("Porto Murtinho", "UTF-8")],
                ['id' => 5167, 'state_id' => 62, 'city' => mb_strtoupper("Ribas do Rio Pardo", "UTF-8")],
                ['id' => 5168, 'state_id' => 62, 'city' => mb_strtoupper("Rio Brilhante", "UTF-8")],
                ['id' => 5169, 'state_id' => 62, 'city' => mb_strtoupper("Rio Negro", "UTF-8")],
                ['id' => 5170, 'state_id' => 62, 'city' => mb_strtoupper("Rio Verde de Mato Grosso", "UTF-8")],
                ['id' => 5171, 'state_id' => 62, 'city' => mb_strtoupper("Rochedo", "UTF-8")],
                ['id' => 5172, 'state_id' => 62, 'city' => mb_strtoupper("Santa Rita do Pardo", "UTF-8")],
                ['id' => 5173, 'state_id' => 62, 'city' => mb_strtoupper("São Gabriel do Oeste", "UTF-8")],
                ['id' => 5174, 'state_id' => 62, 'city' => mb_strtoupper("Sete Quedas", "UTF-8")],
                ['id' => 5175, 'state_id' => 62, 'city' => mb_strtoupper("Selvíria", "UTF-8")],
                ['id' => 5176, 'state_id' => 62, 'city' => mb_strtoupper("Sidrolândia", "UTF-8")],
                ['id' => 5177, 'state_id' => 62, 'city' => mb_strtoupper("Sonora", "UTF-8")],
                ['id' => 5178, 'state_id' => 62, 'city' => mb_strtoupper("Tacuru", "UTF-8")],
                ['id' => 5179, 'state_id' => 62, 'city' => mb_strtoupper("Taquarussu", "UTF-8")],
                ['id' => 5180, 'state_id' => 62, 'city' => mb_strtoupper("Terenos", "UTF-8")],
                ['id' => 5181, 'state_id' => 62, 'city' => mb_strtoupper("Três Lagoas", "UTF-8")],
                ['id' => 5182, 'state_id' => 62, 'city' => mb_strtoupper("Vicentina", "UTF-8")],
            ];

            // foreach ($cities as $city) {
            //     try {
                    City::insert($cities);
                // } catch (\Exception $e) {
                //
                // }
            // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
