<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class RondoniaCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
        $cities = [
            ['id' => 1, 'state_id' => 72, 'city' => mb_strtoupper("Alta Floresta D'Oeste", "UTF-8")],
            ['id' => 2, 'state_id' => 72, 'city' => mb_strtoupper("Ariquemes", "UTF-8")],
            ['id' => 3, 'state_id' => 72, 'city' => mb_strtoupper("Cabixi", "UTF-8")],
            ['id' => 4, 'state_id' => 72, 'city' => mb_strtoupper("Cacoal", "UTF-8")],
            ['id' => 5, 'state_id' => 72, 'city' => mb_strtoupper("Cerejeiras", "UTF-8")],
            ['id' => 6, 'state_id' => 72, 'city' => mb_strtoupper("Colorado do Oeste", "UTF-8")],
            ['id' => 7, 'state_id' => 72, 'city' => mb_strtoupper("Corumbiara", "UTF-8")],
            ['id' => 8, 'state_id' => 72, 'city' => mb_strtoupper("Costa Marques", "UTF-8")],
            ['id' => 9, 'state_id' => 72, 'city' => mb_strtoupper("Espigão D'Oeste", "UTF-8")],
            ['id' => 10, 'state_id' => 72, 'city' => mb_strtoupper("Guajará-Mirim", "UTF-8")],
            ['id' => 11, 'state_id' => 72, 'city' => mb_strtoupper("Jaru", "UTF-8")],
            ['id' => 12, 'state_id' => 72, 'city' => mb_strtoupper("Ji-Paraná", "UTF-8")],
            ['id' => 13, 'state_id' => 72, 'city' => mb_strtoupper("Machadinho D'Oeste", "UTF-8")],
            ['id' => 14, 'state_id' => 72, 'city' => mb_strtoupper("Nova Brasilândia D'Oeste", "UTF-8")],
            ['id' => 15, 'state_id' => 72, 'city' => mb_strtoupper("Ouro Preto do Oeste", "UTF-8")],
            ['id' => 16, 'state_id' => 72, 'city' => mb_strtoupper("Pimenta Bueno", "UTF-8")],
            ['id' => 17, 'state_id' => 72, 'city' => mb_strtoupper("Porto Velho", "UTF-8")],
            ['id' => 18, 'state_id' => 72, 'city' => mb_strtoupper("Presidente Médici", "UTF-8")],
            ['id' => 19, 'state_id' => 72, 'city' => mb_strtoupper("Rio Crespo", "UTF-8")],
            ['id' => 20, 'state_id' => 72, 'city' => mb_strtoupper("Rolim de Moura", "UTF-8")],
            ['id' => 21, 'state_id' => 72, 'city' => mb_strtoupper("Santa Luzia D'Oeste", "UTF-8")],
            ['id' => 22, 'state_id' => 72, 'city' => mb_strtoupper("Vilhena", "UTF-8")],
            ['id' => 23, 'state_id' => 72, 'city' => mb_strtoupper("São Miguel do Guaporé", "UTF-8")],
            ['id' => 24, 'state_id' => 72, 'city' => mb_strtoupper("Nova Mamoré", "UTF-8")],
            ['id' => 25, 'state_id' => 72, 'city' => mb_strtoupper("Alvorada D'Oeste", "UTF-8")],
            ['id' => 26, 'state_id' => 72, 'city' => mb_strtoupper("Alto Alegre dos Parecis", "UTF-8")],
            ['id' => 27, 'state_id' => 72, 'city' => mb_strtoupper("Alto Paraíso", "UTF-8")],
            ['id' => 28, 'state_id' => 72, 'city' => mb_strtoupper("Buritis", "UTF-8")],
            ['id' => 29, 'state_id' => 72, 'city' => mb_strtoupper("Novo Horizonte do Oeste", "UTF-8")],
            ['id' => 30, 'state_id' => 72, 'city' => mb_strtoupper("Cacaulândia", "UTF-8")],
            ['id' => 31, 'state_id' => 72, 'city' => mb_strtoupper("Campo Novo de Rondônia", "UTF-8")],
            ['id' => 32, 'state_id' => 72, 'city' => mb_strtoupper("Candeias do Jamari", "UTF-8")],
            ['id' => 33, 'state_id' => 72, 'city' => mb_strtoupper("Castanheiras", "UTF-8")],
            ['id' => 34, 'state_id' => 72, 'city' => mb_strtoupper("Chupinguaia", "UTF-8")],
            ['id' => 35, 'state_id' => 72, 'city' => mb_strtoupper("Cujubim", "UTF-8")],
            ['id' => 36, 'state_id' => 72, 'city' => mb_strtoupper("Governador Jorge Teixeira", "UTF-8")],
            ['id' => 37, 'state_id' => 72, 'city' => mb_strtoupper("Itapuã do Oeste", "UTF-8")],
            ['id' => 38, 'state_id' => 72, 'city' => mb_strtoupper("Ministro Andreazza", "UTF-8")],
            ['id' => 39, 'state_id' => 72, 'city' => mb_strtoupper("Mirante da Serra", "UTF-8")],
            ['id' => 40, 'state_id' => 72, 'city' => mb_strtoupper("Monte Negro", "UTF-8")],
            ['id' => 41, 'state_id' => 72, 'city' => mb_strtoupper("Nova União", "UTF-8")],
            ['id' => 42, 'state_id' => 72, 'city' => mb_strtoupper("Parecis", "UTF-8")],
            ['id' => 43, 'state_id' => 72, 'city' => mb_strtoupper("Pimenteiras do Oeste", "UTF-8")],
            ['id' => 44, 'state_id' => 72, 'city' => mb_strtoupper("Primavera de Rondônia", "UTF-8")],
            ['id' => 45, 'state_id' => 72, 'city' => mb_strtoupper("São Felipe D'Oeste", "UTF-8")],
            ['id' => 46, 'state_id' => 72, 'city' => mb_strtoupper("São Francisco do Guaporé", "UTF-8")],
            ['id' => 47, 'state_id' => 72, 'city' => mb_strtoupper("Seringueiras", "UTF-8")],
            ['id' => 48, 'state_id' => 72, 'city' => mb_strtoupper("Teixeirópolis", "UTF-8")],
            ['id' => 49, 'state_id' => 72, 'city' => mb_strtoupper("Theobroma", "UTF-8")],
            ['id' => 50, 'state_id' => 72, 'city' => mb_strtoupper("Urupá", "UTF-8")],
            ['id' => 51, 'state_id' => 72, 'city' => mb_strtoupper("Vale do Anari", "UTF-8")],
            ['id' => 52, 'state_id' => 72, 'city' => mb_strtoupper("Vale do Paraíso", "UTF-8")],
        ];

        // foreach ($cities as $city) {
        // try {
            City::insert($cities);
        // } catch (\Exception $e) {
        //
        // }
        // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
