<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class RoraimaCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
        $cities = [
            ['id' => 137, 'state_id' => 73, 'city' => mb_strtoupper("Amajari", "UTF-8")],
            ['id' => 138, 'state_id' => 73, 'city' => mb_strtoupper("Alto Alegre", "UTF-8")],
            ['id' => 139, 'state_id' => 73, 'city' => mb_strtoupper("Boa Vista", "UTF-8")],
            ['id' => 140, 'state_id' => 73, 'city' => mb_strtoupper("Bonfim", "UTF-8")],
            ['id' => 141, 'state_id' => 73, 'city' => mb_strtoupper("Cantá", "UTF-8")],
            ['id' => 142, 'state_id' => 73, 'city' => mb_strtoupper("Caracaraí", "UTF-8")],
            ['id' => 143, 'state_id' => 73, 'city' => mb_strtoupper("Caroebe", "UTF-8")],
            ['id' => 144, 'state_id' => 73, 'city' => mb_strtoupper("Iracema", "UTF-8")],
            ['id' => 145, 'state_id' => 73, 'city' => mb_strtoupper("Mucajaí", "UTF-8")],
            ['id' => 146, 'state_id' => 73, 'city' => mb_strtoupper("Normandia", "UTF-8")],
            ['id' => 147, 'state_id' => 73, 'city' => mb_strtoupper("Pacaraima", "UTF-8")],
            ['id' => 148, 'state_id' => 73, 'city' => mb_strtoupper("Rorainópolis", "UTF-8")],
            ['id' => 149, 'state_id' => 73, 'city' => mb_strtoupper("São João da Baliza", "UTF-8")],
            ['id' => 150, 'state_id' => 73, 'city' => mb_strtoupper("São Luiz", "UTF-8")],
            ['id' => 151, 'state_id' => 73, 'city' => mb_strtoupper("Uiramutã", "UTF-8")],
        ];

        // foreach ($cities as $city) {
        // try {
        City::insert($cities);
        // } catch (\Exception $e) {
        //
        // }
        // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
