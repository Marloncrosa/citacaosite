<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class EspiritoSantoCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
            // City::truncate();
            $cities = [
                ['id' => 3098, 'state_id' => 58, 'city' => mb_strtoupper("Afonso Cláudio", "UTF-8")],
                ['id' => 3099, 'state_id' => 58, 'city' => mb_strtoupper("Águia Branca", "UTF-8")],
                ['id' => 3100, 'state_id' => 58, 'city' => mb_strtoupper("Água Doce do Norte", "UTF-8")],
                ['id' => 3101, 'state_id' => 58, 'city' => mb_strtoupper("Alegre", "UTF-8")],
                ['id' => 3102, 'state_id' => 58, 'city' => mb_strtoupper("Alfredo Chaves", "UTF-8")],
                ['id' => 3103, 'state_id' => 58, 'city' => mb_strtoupper("Alto Rio Novo", "UTF-8")],
                ['id' => 3104, 'state_id' => 58, 'city' => mb_strtoupper("Anchieta", "UTF-8")],
                ['id' => 3105, 'state_id' => 58, 'city' => mb_strtoupper("Apiacá", "UTF-8")],
                ['id' => 3106, 'state_id' => 58, 'city' => mb_strtoupper("Aracruz", "UTF-8")],
                ['id' => 3107, 'state_id' => 58, 'city' => mb_strtoupper("Atilio Vivacqua", "UTF-8")],
                ['id' => 3108, 'state_id' => 58, 'city' => mb_strtoupper("Baixo Guandu", "UTF-8")],
                ['id' => 3109, 'state_id' => 58, 'city' => mb_strtoupper("Barra de São Francisco", "UTF-8")],
                ['id' => 3110, 'state_id' => 58, 'city' => mb_strtoupper("Boa Esperança", "UTF-8")],
                ['id' => 3111, 'state_id' => 58, 'city' => mb_strtoupper("Bom Jesus do Norte", "UTF-8")],
                ['id' => 3112, 'state_id' => 58, 'city' => mb_strtoupper("Brejetuba", "UTF-8")],
                ['id' => 3113, 'state_id' => 58, 'city' => mb_strtoupper("Cachoeiro de Itapemirim", "UTF-8")],
                ['id' => 3114, 'state_id' => 58, 'city' => mb_strtoupper("Cariacica", "UTF-8")],
                ['id' => 3115, 'state_id' => 58, 'city' => mb_strtoupper("Castelo", "UTF-8")],
                ['id' => 3116, 'state_id' => 58, 'city' => mb_strtoupper("Colatina", "UTF-8")],
                ['id' => 3117, 'state_id' => 58, 'city' => mb_strtoupper("Conceição da Barra", "UTF-8")],
                ['id' => 3118, 'state_id' => 58, 'city' => mb_strtoupper("Conceição do Castelo", "UTF-8")],
                ['id' => 3119, 'state_id' => 58, 'city' => mb_strtoupper("Divino de São Lourenço", "UTF-8")],
                ['id' => 3120, 'state_id' => 58, 'city' => mb_strtoupper("Domingos Martins", "UTF-8")],
                ['id' => 3121, 'state_id' => 58, 'city' => mb_strtoupper("Dores do Rio Preto", "UTF-8")],
                ['id' => 3122, 'state_id' => 58, 'city' => mb_strtoupper("Ecoporanga", "UTF-8")],
                ['id' => 3123, 'state_id' => 58, 'city' => mb_strtoupper("Fundão", "UTF-8")],
                ['id' => 3124, 'state_id' => 58, 'city' => mb_strtoupper("Governador Lindenberg", "UTF-8")],
                ['id' => 3125, 'state_id' => 58, 'city' => mb_strtoupper("Guaçuí", "UTF-8")],
                ['id' => 3126, 'state_id' => 58, 'city' => mb_strtoupper("Guarapari", "UTF-8")],
                ['id' => 3127, 'state_id' => 58, 'city' => mb_strtoupper("Ibatiba", "UTF-8")],
                ['id' => 3128, 'state_id' => 58, 'city' => mb_strtoupper("Ibiraçu", "UTF-8")],
                ['id' => 3129, 'state_id' => 58, 'city' => mb_strtoupper("Ibitirama", "UTF-8")],
                ['id' => 3130, 'state_id' => 58, 'city' => mb_strtoupper("Iconha", "UTF-8")],
                ['id' => 3131, 'state_id' => 58, 'city' => mb_strtoupper("Irupi", "UTF-8")],
                ['id' => 3132, 'state_id' => 58, 'city' => mb_strtoupper("Itaguaçu", "UTF-8")],
                ['id' => 3133, 'state_id' => 58, 'city' => mb_strtoupper("Itapemirim", "UTF-8")],
                ['id' => 3134, 'state_id' => 58, 'city' => mb_strtoupper("Itarana", "UTF-8")],
                ['id' => 3135, 'state_id' => 58, 'city' => mb_strtoupper("Iúna", "UTF-8")],
                ['id' => 3136, 'state_id' => 58, 'city' => mb_strtoupper("Jaguaré", "UTF-8")],
                ['id' => 3137, 'state_id' => 58, 'city' => mb_strtoupper("Jerônimo Monteiro", "UTF-8")],
                ['id' => 3138, 'state_id' => 58, 'city' => mb_strtoupper("João Neiva", "UTF-8")],
                ['id' => 3139, 'state_id' => 58, 'city' => mb_strtoupper("Laranja da Terra", "UTF-8")],
                ['id' => 3140, 'state_id' => 58, 'city' => mb_strtoupper("Linhares", "UTF-8")],
                ['id' => 3141, 'state_id' => 58, 'city' => mb_strtoupper("Mantenópolis", "UTF-8")],
                ['id' => 3142, 'state_id' => 58, 'city' => mb_strtoupper("Marataízes", "UTF-8")],
                ['id' => 3143, 'state_id' => 58, 'city' => mb_strtoupper("Marechal Floriano", "UTF-8")],
                ['id' => 3144, 'state_id' => 58, 'city' => mb_strtoupper("Marilândia", "UTF-8")],
                ['id' => 3145, 'state_id' => 58, 'city' => mb_strtoupper("Mimoso do Sul", "UTF-8")],
                ['id' => 3146, 'state_id' => 58, 'city' => mb_strtoupper("Montanha", "UTF-8")],
                ['id' => 3147, 'state_id' => 58, 'city' => mb_strtoupper("Mucurici", "UTF-8")],
                ['id' => 3148, 'state_id' => 58, 'city' => mb_strtoupper("Muniz Freire", "UTF-8")],
                ['id' => 3149, 'state_id' => 58, 'city' => mb_strtoupper("Muqui", "UTF-8")],
                ['id' => 3150, 'state_id' => 58, 'city' => mb_strtoupper("Nova Venécia", "UTF-8")],
                ['id' => 3151, 'state_id' => 58, 'city' => mb_strtoupper("Pancas", "UTF-8")],
                ['id' => 3152, 'state_id' => 58, 'city' => mb_strtoupper("Pedro Canário", "UTF-8")],
                ['id' => 3153, 'state_id' => 58, 'city' => mb_strtoupper("Pinheiros", "UTF-8")],
                ['id' => 3154, 'state_id' => 58, 'city' => mb_strtoupper("Piúma", "UTF-8")],
                ['id' => 3155, 'state_id' => 58, 'city' => mb_strtoupper("Ponto Belo", "UTF-8")],
                ['id' => 3156, 'state_id' => 58, 'city' => mb_strtoupper("Presidente Kennedy", "UTF-8")],
                ['id' => 3157, 'state_id' => 58, 'city' => mb_strtoupper("Rio Bananal", "UTF-8")],
                ['id' => 3158, 'state_id' => 58, 'city' => mb_strtoupper("Rio Novo do Sul", "UTF-8")],
                ['id' => 3159, 'state_id' => 58, 'city' => mb_strtoupper("Santa Leopoldina", "UTF-8")],
                ['id' => 3160, 'state_id' => 58, 'city' => mb_strtoupper("Santa Maria de Jetibá", "UTF-8")],
                ['id' => 3161, 'state_id' => 58, 'city' => mb_strtoupper("Santa Teresa", "UTF-8")],
                ['id' => 3162, 'state_id' => 58, 'city' => mb_strtoupper("São Domingos do Norte", "UTF-8")],
                ['id' => 3163, 'state_id' => 58, 'city' => mb_strtoupper("São Gabriel da Palha", "UTF-8")],
                ['id' => 3164, 'state_id' => 58, 'city' => mb_strtoupper("São José do Calçado", "UTF-8")],
                ['id' => 3165, 'state_id' => 58, 'city' => mb_strtoupper("São Mateus", "UTF-8")],
                ['id' => 3166, 'state_id' => 58, 'city' => mb_strtoupper("São Roque do Canaã", "UTF-8")],
                ['id' => 3167, 'state_id' => 58, 'city' => mb_strtoupper("Serra", "UTF-8")],
                ['id' => 3168, 'state_id' => 58, 'city' => mb_strtoupper("Sooretama", "UTF-8")],
                ['id' => 3169, 'state_id' => 58, 'city' => mb_strtoupper("Vargem Alta", "UTF-8")],
                ['id' => 3170, 'state_id' => 58, 'city' => mb_strtoupper("Venda Nova do Imigrante", "UTF-8")],
                ['id' => 3171, 'state_id' => 58, 'city' => mb_strtoupper("Viana", "UTF-8")],
                ['id' => 3172, 'state_id' => 58, 'city' => mb_strtoupper("Vila Pavão", "UTF-8")],
                ['id' => 3173, 'state_id' => 58, 'city' => mb_strtoupper("Vila Valério", "UTF-8")],
                ['id' => 3174, 'state_id' => 58, 'city' => mb_strtoupper("Vila Velha", "UTF-8")],
                ['id' => 3175, 'state_id' => 58, 'city' => mb_strtoupper("Vitória", "UTF-8")],

            ];

            // foreach ($cities as $city) {
            //     try {
                    City::insert($cities);
                // } catch (\Exception $e) {
                //
                // }
            // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
