<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class RiodeJaneiroCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
            $cities = [
                ['id' => 3176, 'state_id' => 69, 'city' => mb_strtoupper("Angra dos Reis", "UTF-8")],
                ['id' => 3177, 'state_id' => 69, 'city' => mb_strtoupper("Aperibé", "UTF-8")],
                ['id' => 3178, 'state_id' => 69, 'city' => mb_strtoupper("Araruama", "UTF-8")],
                ['id' => 3179, 'state_id' => 69, 'city' => mb_strtoupper("Areal", "UTF-8")],
                ['id' => 3180, 'state_id' => 69, 'city' => mb_strtoupper("Armação dos Búzios", "UTF-8")],
                ['id' => 3181, 'state_id' => 69, 'city' => mb_strtoupper("Arraial do Cabo", "UTF-8")],
                ['id' => 3182, 'state_id' => 69, 'city' => mb_strtoupper("Barra do Piraí", "UTF-8")],
                ['id' => 3183, 'state_id' => 69, 'city' => mb_strtoupper("Barra Mansa", "UTF-8")],
                ['id' => 3184, 'state_id' => 69, 'city' => mb_strtoupper("Belford Roxo", "UTF-8")],
                ['id' => 3185, 'state_id' => 69, 'city' => mb_strtoupper("Bom Jardim", "UTF-8")],
                ['id' => 3186, 'state_id' => 69, 'city' => mb_strtoupper("Bom Jesus do Itabapoana", "UTF-8")],
                ['id' => 3187, 'state_id' => 69, 'city' => mb_strtoupper("Cabo Frio", "UTF-8")],
                ['id' => 3188, 'state_id' => 69, 'city' => mb_strtoupper("Cachoeiras de Macacu", "UTF-8")],
                ['id' => 3189, 'state_id' => 69, 'city' => mb_strtoupper("Cambuci", "UTF-8")],
                ['id' => 3190, 'state_id' => 69, 'city' => mb_strtoupper("Carapebus", "UTF-8")],
                ['id' => 3191, 'state_id' => 69, 'city' => mb_strtoupper("Comendador Levy Gasparian", "UTF-8")],
                ['id' => 3192, 'state_id' => 69, 'city' => mb_strtoupper("Campos dos Goytacazes", "UTF-8")],
                ['id' => 3193, 'state_id' => 69, 'city' => mb_strtoupper("Cantagalo", "UTF-8")],
                ['id' => 3194, 'state_id' => 69, 'city' => mb_strtoupper("Cardoso Moreira", "UTF-8")],
                ['id' => 3195, 'state_id' => 69, 'city' => mb_strtoupper("Carmo", "UTF-8")],
                ['id' => 3196, 'state_id' => 69, 'city' => mb_strtoupper("Casimiro de Abreu", "UTF-8")],
                ['id' => 3197, 'state_id' => 69, 'city' => mb_strtoupper("Conceição de Macabu", "UTF-8")],
                ['id' => 3198, 'state_id' => 69, 'city' => mb_strtoupper("Cordeiro", "UTF-8")],
                ['id' => 3199, 'state_id' => 69, 'city' => mb_strtoupper("Duas Barras", "UTF-8")],
                ['id' => 3200, 'state_id' => 69, 'city' => mb_strtoupper("Duque de Caxias", "UTF-8")],
                ['id' => 3201, 'state_id' => 69, 'city' => mb_strtoupper("Engenheiro Paulo de Frontin", "UTF-8")],
                ['id' => 3202, 'state_id' => 69, 'city' => mb_strtoupper("Guapimirim", "UTF-8")],
                ['id' => 3203, 'state_id' => 69, 'city' => mb_strtoupper("Iguaba Grande", "UTF-8")],
                ['id' => 3204, 'state_id' => 69, 'city' => mb_strtoupper("Itaboraí", "UTF-8")],
                ['id' => 3205, 'state_id' => 69, 'city' => mb_strtoupper("Itaguaí", "UTF-8")],
                ['id' => 3206, 'state_id' => 69, 'city' => mb_strtoupper("Italva", "UTF-8")],
                ['id' => 3207, 'state_id' => 69, 'city' => mb_strtoupper("Itaocara", "UTF-8")],
                ['id' => 3208, 'state_id' => 69, 'city' => mb_strtoupper("Itaperuna", "UTF-8")],
                ['id' => 3209, 'state_id' => 69, 'city' => mb_strtoupper("Itatiaia", "UTF-8")],
                ['id' => 3210, 'state_id' => 69, 'city' => mb_strtoupper("Japeri", "UTF-8")],
                ['id' => 3211, 'state_id' => 69, 'city' => mb_strtoupper("Laje do Muriaé", "UTF-8")],
                ['id' => 3212, 'state_id' => 69, 'city' => mb_strtoupper("Macaé", "UTF-8")],
                ['id' => 3213, 'state_id' => 69, 'city' => mb_strtoupper("Macuco", "UTF-8")],
                ['id' => 3214, 'state_id' => 69, 'city' => mb_strtoupper("Magé", "UTF-8")],
                ['id' => 3215, 'state_id' => 69, 'city' => mb_strtoupper("Mangaratiba", "UTF-8")],
                ['id' => 3216, 'state_id' => 69, 'city' => mb_strtoupper("Maricá", "UTF-8")],
                ['id' => 3217, 'state_id' => 69, 'city' => mb_strtoupper("Mendes", "UTF-8")],
                ['id' => 3218, 'state_id' => 69, 'city' => mb_strtoupper("Mesquita", "UTF-8")],
                ['id' => 3219, 'state_id' => 69, 'city' => mb_strtoupper("Miguel Pereira", "UTF-8")],
                ['id' => 3220, 'state_id' => 69, 'city' => mb_strtoupper("Miracema", "UTF-8")],
                ['id' => 3221, 'state_id' => 69, 'city' => mb_strtoupper("Natividade", "UTF-8")],
                ['id' => 3222, 'state_id' => 69, 'city' => mb_strtoupper("Nilópolis", "UTF-8")],
                ['id' => 3223, 'state_id' => 69, 'city' => mb_strtoupper("Niterói", "UTF-8")],
                ['id' => 3224, 'state_id' => 69, 'city' => mb_strtoupper("Nova Friburgo", "UTF-8")],
                ['id' => 3225, 'state_id' => 69, 'city' => mb_strtoupper("Nova Iguaçu", "UTF-8")],
                ['id' => 3226, 'state_id' => 69, 'city' => mb_strtoupper("Paracambi", "UTF-8")],
                ['id' => 3227, 'state_id' => 69, 'city' => mb_strtoupper("Paraíba do Sul", "UTF-8")],
                ['id' => 3228, 'state_id' => 69, 'city' => mb_strtoupper("Paraty", "UTF-8")],
                ['id' => 3229, 'state_id' => 69, 'city' => mb_strtoupper("Paty do Alferes", "UTF-8")],
                ['id' => 3230, 'state_id' => 69, 'city' => mb_strtoupper("Petrópolis", "UTF-8")],
                ['id' => 3231, 'state_id' => 69, 'city' => mb_strtoupper("Pinheiral", "UTF-8")],
                ['id' => 3232, 'state_id' => 69, 'city' => mb_strtoupper("Piraí", "UTF-8")],
                ['id' => 3233, 'state_id' => 69, 'city' => mb_strtoupper("Porciúncula", "UTF-8")],
                ['id' => 3234, 'state_id' => 69, 'city' => mb_strtoupper("Porto Real", "UTF-8")],
                ['id' => 3235, 'state_id' => 69, 'city' => mb_strtoupper("Quatis", "UTF-8")],
                ['id' => 3236, 'state_id' => 69, 'city' => mb_strtoupper("Queimados", "UTF-8")],
                ['id' => 3237, 'state_id' => 69, 'city' => mb_strtoupper("Quissamã", "UTF-8")],
                ['id' => 3238, 'state_id' => 69, 'city' => mb_strtoupper("Resende", "UTF-8")],
                ['id' => 3239, 'state_id' => 69, 'city' => mb_strtoupper("Rio Bonito", "UTF-8")],
                ['id' => 3240, 'state_id' => 69, 'city' => mb_strtoupper("Rio Claro", "UTF-8")],
                ['id' => 3241, 'state_id' => 69, 'city' => mb_strtoupper("Rio das Flores", "UTF-8")],
                ['id' => 3242, 'state_id' => 69, 'city' => mb_strtoupper("Rio das Ostras", "UTF-8")],
                ['id' => 3243, 'state_id' => 69, 'city' => mb_strtoupper("Rio de Janeiro", "UTF-8")],
                ['id' => 3244, 'state_id' => 69, 'city' => mb_strtoupper("Santa Maria Madalena", "UTF-8")],
                ['id' => 3245, 'state_id' => 69, 'city' => mb_strtoupper("Santo Antônio de Pádua", "UTF-8")],
                ['id' => 3246, 'state_id' => 69, 'city' => mb_strtoupper("São Francisco de Itabapoana", "UTF-8")],
                ['id' => 3247, 'state_id' => 69, 'city' => mb_strtoupper("São Fidélis", "UTF-8")],
                ['id' => 3248, 'state_id' => 69, 'city' => mb_strtoupper("São Gonçalo", "UTF-8")],
                ['id' => 3249, 'state_id' => 69, 'city' => mb_strtoupper("São João da Barra", "UTF-8")],
                ['id' => 3250, 'state_id' => 69, 'city' => mb_strtoupper("São João de Meriti", "UTF-8")],
                ['id' => 3251, 'state_id' => 69, 'city' => mb_strtoupper("São José de Ubá", "UTF-8")],
                ['id' => 3252, 'state_id' => 69, 'city' => mb_strtoupper("São José do Vale do Rio Preto", "UTF-8")],
                ['id' => 3253, 'state_id' => 69, 'city' => mb_strtoupper("São Pedro da Aldeia", "UTF-8")],
                ['id' => 3254, 'state_id' => 69, 'city' => mb_strtoupper("São Sebastião do Alto", "UTF-8")],
                ['id' => 3255, 'state_id' => 69, 'city' => mb_strtoupper("Sapucaia", "UTF-8")],
                ['id' => 3256, 'state_id' => 69, 'city' => mb_strtoupper("Saquarema", "UTF-8")],
                ['id' => 3257, 'state_id' => 69, 'city' => mb_strtoupper("Seropédica", "UTF-8")],
                ['id' => 3258, 'state_id' => 69, 'city' => mb_strtoupper("Silva Jardim", "UTF-8")],
                ['id' => 3259, 'state_id' => 69, 'city' => mb_strtoupper("Sumidouro", "UTF-8")],
                ['id' => 3260, 'state_id' => 69, 'city' => mb_strtoupper("Tanguá", "UTF-8")],
                ['id' => 3261, 'state_id' => 69, 'city' => mb_strtoupper("Teresópolis", "UTF-8")],
                ['id' => 3262, 'state_id' => 69, 'city' => mb_strtoupper("Trajano de Moraes", "UTF-8")],
                ['id' => 3263, 'state_id' => 69, 'city' => mb_strtoupper("Três Rios", "UTF-8")],
                ['id' => 3264, 'state_id' => 69, 'city' => mb_strtoupper("Valença", "UTF-8")],
                ['id' => 3265, 'state_id' => 69, 'city' => mb_strtoupper("Varre-Sai", "UTF-8")],
                ['id' => 3266, 'state_id' => 69, 'city' => mb_strtoupper("Vassouras", "UTF-8")],
                ['id' => 3267, 'state_id' => 69, 'city' => mb_strtoupper("Volta Redonda", "UTF-8")],
            ];

            // foreach ($cities as $city) {
            //     try {
                    City::insert($cities);
                // } catch (\Exception $e) {
                //
                // }
            // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
