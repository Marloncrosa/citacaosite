<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class AlagoasCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
            // City::truncate();
            $cities = [
                ['id' => 1651, 'state_id' => 52, 'city' => mb_strtoupper("Água Branca", "UTF-8")],
                ['id' => 1652, 'state_id' => 52, 'city' => mb_strtoupper("Anadia", "UTF-8")],
                ['id' => 1653, 'state_id' => 52, 'city' => mb_strtoupper("Arapiraca", "UTF-8")],
                ['id' => 1654, 'state_id' => 52, 'city' => mb_strtoupper("Atalaia", "UTF-8")],
                ['id' => 1655, 'state_id' => 52, 'city' => mb_strtoupper("Barra de Santo Antônio", "UTF-8")],
                ['id' => 1656, 'state_id' => 52, 'city' => mb_strtoupper("Barra de São Miguel", "UTF-8")],
                ['id' => 1657, 'state_id' => 52, 'city' => mb_strtoupper("Batalha", "UTF-8")],
                ['id' => 1658, 'state_id' => 52, 'city' => mb_strtoupper("Belém", "UTF-8")],
                ['id' => 1659, 'state_id' => 52, 'city' => mb_strtoupper("Belo Monte", "UTF-8")],
                ['id' => 1660, 'state_id' => 52, 'city' => mb_strtoupper("Boca da Mata", "UTF-8")],
                ['id' => 1661, 'state_id' => 52, 'city' => mb_strtoupper("Branquinha", "UTF-8")],
                ['id' => 1662, 'state_id' => 52, 'city' => mb_strtoupper("Cacimbinhas", "UTF-8")],
                ['id' => 1663, 'state_id' => 52, 'city' => mb_strtoupper("Cajueiro", "UTF-8")],
                ['id' => 1664, 'state_id' => 52, 'city' => mb_strtoupper("Campestre", "UTF-8")],
                ['id' => 1665, 'state_id' => 52, 'city' => mb_strtoupper("Campo Alegre", "UTF-8")],
                ['id' => 1666, 'state_id' => 52, 'city' => mb_strtoupper("Campo Grande", "UTF-8")],
                ['id' => 1667, 'state_id' => 52, 'city' => mb_strtoupper("Canapi", "UTF-8")],
                ['id' => 1668, 'state_id' => 52, 'city' => mb_strtoupper("Capela", "UTF-8")],
                ['id' => 1669, 'state_id' => 52, 'city' => mb_strtoupper("Carneiros", "UTF-8")],
                ['id' => 1670, 'state_id' => 52, 'city' => mb_strtoupper("Chã Preta", "UTF-8")],
                ['id' => 1671, 'state_id' => 52, 'city' => mb_strtoupper("Coité do Nóia", "UTF-8")],
                ['id' => 1672, 'state_id' => 52, 'city' => mb_strtoupper("Colônia Leopoldina", "UTF-8")],
                ['id' => 1673, 'state_id' => 52, 'city' => mb_strtoupper("Coqueiro Seco", "UTF-8")],
                ['id' => 1674, 'state_id' => 52, 'city' => mb_strtoupper("Coruripe", "UTF-8")],
                ['id' => 1675, 'state_id' => 52, 'city' => mb_strtoupper("Craíbas", "UTF-8")],
                ['id' => 1676, 'state_id' => 52, 'city' => mb_strtoupper("Delmiro Gouveia", "UTF-8")],
                ['id' => 1677, 'state_id' => 52, 'city' => mb_strtoupper("Dois Riachos", "UTF-8")],
                ['id' => 1678, 'state_id' => 52, 'city' => mb_strtoupper("Estrela de Alagoas", "UTF-8")],
                ['id' => 1679, 'state_id' => 52, 'city' => mb_strtoupper("Feira Grande", "UTF-8")],
                ['id' => 1680, 'state_id' => 52, 'city' => mb_strtoupper("Feliz Deserto", "UTF-8")],
                ['id' => 1681, 'state_id' => 52, 'city' => mb_strtoupper("Flexeiras", "UTF-8")],
                ['id' => 1682, 'state_id' => 52, 'city' => mb_strtoupper("Girau do Ponciano", "UTF-8")],
                ['id' => 1683, 'state_id' => 52, 'city' => mb_strtoupper("Ibateguara", "UTF-8")],
                ['id' => 1684, 'state_id' => 52, 'city' => mb_strtoupper("Igaci", "UTF-8")],
                ['id' => 1685, 'state_id' => 52, 'city' => mb_strtoupper("Igreja Nova", "UTF-8")],
                ['id' => 1686, 'state_id' => 52, 'city' => mb_strtoupper("Inhapi", "UTF-8")],
                ['id' => 1687, 'state_id' => 52, 'city' => mb_strtoupper("Jacaré dos Homens", "UTF-8")],
                ['id' => 1688, 'state_id' => 52, 'city' => mb_strtoupper("Jacuípe", "UTF-8")],
                ['id' => 1689, 'state_id' => 52, 'city' => mb_strtoupper("Japaratinga", "UTF-8")],
                ['id' => 1690, 'state_id' => 52, 'city' => mb_strtoupper("Jaramataia", "UTF-8")],
                ['id' => 1691, 'state_id' => 52, 'city' => mb_strtoupper("Jequiá da Praia", "UTF-8")],
                ['id' => 1692, 'state_id' => 52, 'city' => mb_strtoupper("Joaquim Gomes", "UTF-8")],
                ['id' => 1693, 'state_id' => 52, 'city' => mb_strtoupper("Jundiá", "UTF-8")],
                ['id' => 1694, 'state_id' => 52, 'city' => mb_strtoupper("Junqueiro", "UTF-8")],
                ['id' => 1695, 'state_id' => 52, 'city' => mb_strtoupper("Lagoa da Canoa", "UTF-8")],
                ['id' => 1696, 'state_id' => 52, 'city' => mb_strtoupper("Limoeiro de Anadia", "UTF-8")],
                ['id' => 1697, 'state_id' => 52, 'city' => mb_strtoupper("Maceió", "UTF-8")],
                ['id' => 1698, 'state_id' => 52, 'city' => mb_strtoupper("Major Isidoro", "UTF-8")],
                ['id' => 1699, 'state_id' => 52, 'city' => mb_strtoupper("Maragogi", "UTF-8")],
                ['id' => 1700, 'state_id' => 52, 'city' => mb_strtoupper("Maravilha", "UTF-8")],
                ['id' => 1701, 'state_id' => 52, 'city' => mb_strtoupper("Marechal Deodoro", "UTF-8")],
                ['id' => 1702, 'state_id' => 52, 'city' => mb_strtoupper("Maribondo", "UTF-8")],
                ['id' => 1703, 'state_id' => 52, 'city' => mb_strtoupper("Mar Vermelho", "UTF-8")],
                ['id' => 1704, 'state_id' => 52, 'city' => mb_strtoupper("Mata Grande", "UTF-8")],
                ['id' => 1705, 'state_id' => 52, 'city' => mb_strtoupper("Matriz de Camaragibe", "UTF-8")],
                ['id' => 1706, 'state_id' => 52, 'city' => mb_strtoupper("Messias", "UTF-8")],
                ['id' => 1707, 'state_id' => 52, 'city' => mb_strtoupper("Minador do Negrão", "UTF-8")],
                ['id' => 1708, 'state_id' => 52, 'city' => mb_strtoupper("Monteirópolis", "UTF-8")],
                ['id' => 1709, 'state_id' => 52, 'city' => mb_strtoupper("Murici", "UTF-8")],
                ['id' => 1710, 'state_id' => 52, 'city' => mb_strtoupper("Novo Lino", "UTF-8")],
                ['id' => 1711, 'state_id' => 52, 'city' => mb_strtoupper("Olho d'Água das Flores", "UTF-8")],
                ['id' => 1712, 'state_id' => 52, 'city' => mb_strtoupper("Olho d'Água do Casado", "UTF-8")],
                ['id' => 1713, 'state_id' => 52, 'city' => mb_strtoupper("Olho d'Água Grande", "UTF-8")],
                ['id' => 1714, 'state_id' => 52, 'city' => mb_strtoupper("Olivença", "UTF-8")],
                ['id' => 1715, 'state_id' => 52, 'city' => mb_strtoupper("Ouro Branco", "UTF-8")],
                ['id' => 1716, 'state_id' => 52, 'city' => mb_strtoupper("Palestina", "UTF-8")],
                ['id' => 1717, 'state_id' => 52, 'city' => mb_strtoupper("Palmeira dos Índios", "UTF-8")],
                ['id' => 1718, 'state_id' => 52, 'city' => mb_strtoupper("Pão de Açúcar", "UTF-8")],
                ['id' => 1719, 'state_id' => 52, 'city' => mb_strtoupper("Pariconha", "UTF-8")],
                ['id' => 1720, 'state_id' => 52, 'city' => mb_strtoupper("Paripueira", "UTF-8")],
                ['id' => 1721, 'state_id' => 52, 'city' => mb_strtoupper("Passo de Camaragibe", "UTF-8")],
                ['id' => 1722, 'state_id' => 52, 'city' => mb_strtoupper("Paulo Jacinto", "UTF-8")],
                ['id' => 1723, 'state_id' => 52, 'city' => mb_strtoupper("Penedo", "UTF-8")],
                ['id' => 1724, 'state_id' => 52, 'city' => mb_strtoupper("Piaçabuçu", "UTF-8")],
                ['id' => 1725, 'state_id' => 52, 'city' => mb_strtoupper("Pilar", "UTF-8")],
                ['id' => 1726, 'state_id' => 52, 'city' => mb_strtoupper("Pindoba", "UTF-8")],
                ['id' => 1727, 'state_id' => 52, 'city' => mb_strtoupper("Piranhas", "UTF-8")],
                ['id' => 1728, 'state_id' => 52, 'city' => mb_strtoupper("Poço das Trincheiras", "UTF-8")],
                ['id' => 1729, 'state_id' => 52, 'city' => mb_strtoupper("Porto Calvo", "UTF-8")],
                ['id' => 1730, 'state_id' => 52, 'city' => mb_strtoupper("Porto de Pedras", "UTF-8")],
                ['id' => 1731, 'state_id' => 52, 'city' => mb_strtoupper("Porto Real do Colégio", "UTF-8")],
                ['id' => 1732, 'state_id' => 52, 'city' => mb_strtoupper("Quebrangulo", "UTF-8")],
                ['id' => 1733, 'state_id' => 52, 'city' => mb_strtoupper("Rio Largo", "UTF-8")],
                ['id' => 1734, 'state_id' => 52, 'city' => mb_strtoupper("Roteiro", "UTF-8")],
                ['id' => 1735, 'state_id' => 52, 'city' => mb_strtoupper("Santa Luzia do Norte", "UTF-8")],
                ['id' => 1736, 'state_id' => 52, 'city' => mb_strtoupper("Santana do Ipanema", "UTF-8")],
                ['id' => 1737, 'state_id' => 52, 'city' => mb_strtoupper("Santana do Mundaú", "UTF-8")],
                ['id' => 1738, 'state_id' => 52, 'city' => mb_strtoupper("São Brás", "UTF-8")],
                ['id' => 1739, 'state_id' => 52, 'city' => mb_strtoupper("São José da Laje", "UTF-8")],
                ['id' => 1740, 'state_id' => 52, 'city' => mb_strtoupper("São José da Tapera", "UTF-8")],
                ['id' => 1741, 'state_id' => 52, 'city' => mb_strtoupper("São Luís do Quitunde", "UTF-8")],
                ['id' => 1742, 'state_id' => 52, 'city' => mb_strtoupper("São Miguel dos Campos", "UTF-8")],
                ['id' => 1743, 'state_id' => 52, 'city' => mb_strtoupper("São Miguel dos Milagres", "UTF-8")],
                ['id' => 1744, 'state_id' => 52, 'city' => mb_strtoupper("São Sebastião", "UTF-8")],
                ['id' => 1745, 'state_id' => 52, 'city' => mb_strtoupper("Satuba", "UTF-8")],
                ['id' => 1746, 'state_id' => 52, 'city' => mb_strtoupper("Senador Rui Palmeira", "UTF-8")],
                ['id' => 1747, 'state_id' => 52, 'city' => mb_strtoupper("Tanque d'Arca", "UTF-8")],
                ['id' => 1748, 'state_id' => 52, 'city' => mb_strtoupper("Taquarana", "UTF-8")],
                ['id' => 1749, 'state_id' => 52, 'city' => mb_strtoupper("Teotônio Vilela", "UTF-8")],
                ['id' => 1750, 'state_id' => 52, 'city' => mb_strtoupper("Traipu", "UTF-8")],
                ['id' => 1751, 'state_id' => 52, 'city' => mb_strtoupper("União dos Palmares", "UTF-8")],
                ['id' => 1752, 'state_id' => 52, 'city' => mb_strtoupper("Viçosa", "UTF-8")],
            ];

            // foreach ($cities as $city) {
            //     try {
                    City::insert($cities);
                // } catch (\Exception $e) {
                //
                // }
            // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
