<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class AmapaCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
            // City::truncate();
            $cities = [
                ['id' => 296, 'state_id' => 53, 'city' => mb_strtoupper("Serra do Navio", "UTF-8")],
                ['id' => 297, 'state_id' => 53, 'city' => mb_strtoupper("Amapá", "UTF-8")],
                ['id' => 298, 'state_id' => 53, 'city' => mb_strtoupper("Pedra Branca do Amapari", "UTF-8")],
                ['id' => 299, 'state_id' => 53, 'city' => mb_strtoupper("Calçoene", "UTF-8")],
                ['id' => 300, 'state_id' => 53, 'city' => mb_strtoupper("Cutias", "UTF-8")],
                ['id' => 301, 'state_id' => 53, 'city' => mb_strtoupper("Ferreira Gomes", "UTF-8")],
                ['id' => 302, 'state_id' => 53, 'city' => mb_strtoupper("Itaubal", "UTF-8")],
                ['id' => 303, 'state_id' => 53, 'city' => mb_strtoupper("Laranjal do Jari", "UTF-8")],
                ['id' => 304, 'state_id' => 53, 'city' => mb_strtoupper("Macapá", "UTF-8")],
                ['id' => 305, 'state_id' => 53, 'city' => mb_strtoupper("Mazagão", "UTF-8")],
                ['id' => 306, 'state_id' => 53, 'city' => mb_strtoupper("Oiapoque", "UTF-8")],
                ['id' => 307, 'state_id' => 53, 'city' => mb_strtoupper("Porto Grande", "UTF-8")],
                ['id' => 308, 'state_id' => 53, 'city' => mb_strtoupper("Pracuúba", "UTF-8")],
                ['id' => 309, 'state_id' => 53, 'city' => mb_strtoupper("Santana", "UTF-8")],
                ['id' => 310, 'state_id' => 53, 'city' => mb_strtoupper("Tartarugalzinho", "UTF-8")],
                ['id' => 311, 'state_id' => 53, 'city' => mb_strtoupper("Vitória do Jari", "UTF-8")],
            ];

            // foreach ($cities as $city) {
            //     try {
                    City::insert($cities);
                // } catch (\Exception $e) {
                //
                // }
            // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
