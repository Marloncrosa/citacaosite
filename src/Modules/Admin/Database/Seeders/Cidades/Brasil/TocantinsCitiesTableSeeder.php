<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class TocantinsCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
        $cities = [
            ['id' => 312, 'state_id' => 77, 'city' => mb_strtoupper("Abreulândia", "UTF-8")],
            ['id' => 313, 'state_id' => 77, 'city' => mb_strtoupper("Aguiarnópolis", "UTF-8")],
            ['id' => 314, 'state_id' => 77, 'city' => mb_strtoupper("Aliança do Tocantins", "UTF-8")],
            ['id' => 315, 'state_id' => 77, 'city' => mb_strtoupper("Almas", "UTF-8")],
            ['id' => 316, 'state_id' => 77, 'city' => mb_strtoupper("Alvorada", "UTF-8")],
            ['id' => 317, 'state_id' => 77, 'city' => mb_strtoupper("Ananás", "UTF-8")],
            ['id' => 318, 'state_id' => 77, 'city' => mb_strtoupper("Angico", "UTF-8")],
            ['id' => 319, 'state_id' => 77, 'city' => mb_strtoupper("Aparecida do Rio Negro", "UTF-8")],
            ['id' => 320, 'state_id' => 77, 'city' => mb_strtoupper("Aragominas", "UTF-8")],
            ['id' => 321, 'state_id' => 77, 'city' => mb_strtoupper("Araguacema", "UTF-8")],
            ['id' => 322, 'state_id' => 77, 'city' => mb_strtoupper("Araguaçu", "UTF-8")],
            ['id' => 323, 'state_id' => 77, 'city' => mb_strtoupper("Araguaína", "UTF-8")],
            ['id' => 324, 'state_id' => 77, 'city' => mb_strtoupper("Araguanã", "UTF-8")],
            ['id' => 325, 'state_id' => 77, 'city' => mb_strtoupper("Araguatins", "UTF-8")],
            ['id' => 326, 'state_id' => 77, 'city' => mb_strtoupper("Arapoema", "UTF-8")],
            ['id' => 327, 'state_id' => 77, 'city' => mb_strtoupper("Arraias", "UTF-8")],
            ['id' => 328, 'state_id' => 77, 'city' => mb_strtoupper("Augustinópolis", "UTF-8")],
            ['id' => 329, 'state_id' => 77, 'city' => mb_strtoupper("Aurora do Tocantins", "UTF-8")],
            ['id' => 330, 'state_id' => 77, 'city' => mb_strtoupper("Axixá do Tocantins", "UTF-8")],
            ['id' => 331, 'state_id' => 77, 'city' => mb_strtoupper("Babaçulândia", "UTF-8")],
            ['id' => 332, 'state_id' => 77, 'city' => mb_strtoupper("Bandeirantes do Tocantins", "UTF-8")],
            ['id' => 333, 'state_id' => 77, 'city' => mb_strtoupper("Barra do Ouro", "UTF-8")],
            ['id' => 334, 'state_id' => 77, 'city' => mb_strtoupper("Barrolândia", "UTF-8")],
            ['id' => 335, 'state_id' => 77, 'city' => mb_strtoupper("Bernardo Sayão", "UTF-8")],
            ['id' => 336, 'state_id' => 77, 'city' => mb_strtoupper("Bom Jesus do Tocantins", "UTF-8")],
            ['id' => 337, 'state_id' => 77, 'city' => mb_strtoupper("Brasilândia do Tocantins", "UTF-8")],
            ['id' => 338, 'state_id' => 77, 'city' => mb_strtoupper("Brejinho de Nazaré", "UTF-8")],
            ['id' => 339, 'state_id' => 77, 'city' => mb_strtoupper("Buriti do Tocantins", "UTF-8")],
            ['id' => 340, 'state_id' => 77, 'city' => mb_strtoupper("Cachoeirinha", "UTF-8")],
            ['id' => 341, 'state_id' => 77, 'city' => mb_strtoupper("Campos Lindos", "UTF-8")],
            ['id' => 342, 'state_id' => 77, 'city' => mb_strtoupper("Cariri do Tocantins", "UTF-8")],
            ['id' => 343, 'state_id' => 77, 'city' => mb_strtoupper("Carmolândia", "UTF-8")],
            ['id' => 344, 'state_id' => 77, 'city' => mb_strtoupper("Carrasco Bonito", "UTF-8")],
            ['id' => 345, 'state_id' => 77, 'city' => mb_strtoupper("Caseara", "UTF-8")],
            ['id' => 346, 'state_id' => 77, 'city' => mb_strtoupper("Centenário", "UTF-8")],
            ['id' => 347, 'state_id' => 77, 'city' => mb_strtoupper("Chapada de Areia", "UTF-8")],
            ['id' => 348, 'state_id' => 77, 'city' => mb_strtoupper("Chapada da Natividade", "UTF-8")],
            ['id' => 349, 'state_id' => 77, 'city' => mb_strtoupper("Colinas do Tocantins", "UTF-8")],
            ['id' => 350, 'state_id' => 77, 'city' => mb_strtoupper("Combinado", "UTF-8")],
            ['id' => 351, 'state_id' => 77, 'city' => mb_strtoupper("Conceição do Tocantins", "UTF-8")],
            ['id' => 352, 'state_id' => 77, 'city' => mb_strtoupper("Couto Magalhães", "UTF-8")],
            ['id' => 353, 'state_id' => 77, 'city' => mb_strtoupper("Cristalândia", "UTF-8")],
            ['id' => 354, 'state_id' => 77, 'city' => mb_strtoupper("Crixás do Tocantins", "UTF-8")],
            ['id' => 355, 'state_id' => 77, 'city' => mb_strtoupper("Darcinópolis", "UTF-8")],
            ['id' => 356, 'state_id' => 77, 'city' => mb_strtoupper("Dianópolis", "UTF-8")],
            ['id' => 357, 'state_id' => 77, 'city' => mb_strtoupper("Divinópolis do Tocantins", "UTF-8")],
            ['id' => 358, 'state_id' => 77, 'city' => mb_strtoupper("Dois Irmãos do Tocantins", "UTF-8")],
            ['id' => 359, 'state_id' => 77, 'city' => mb_strtoupper("Dueré", "UTF-8")],
            ['id' => 360, 'state_id' => 77, 'city' => mb_strtoupper("Esperantina", "UTF-8")],
            ['id' => 361, 'state_id' => 77, 'city' => mb_strtoupper("Fátima", "UTF-8")],
            ['id' => 362, 'state_id' => 77, 'city' => mb_strtoupper("Figueirópolis", "UTF-8")],
            ['id' => 363, 'state_id' => 77, 'city' => mb_strtoupper("Filadélfia", "UTF-8")],
            ['id' => 364, 'state_id' => 77, 'city' => mb_strtoupper("Formoso do Araguaia", "UTF-8")],
            ['id' => 365, 'state_id' => 77, 'city' => mb_strtoupper("Fortaleza do Tabocão", "UTF-8")],
            ['id' => 366, 'state_id' => 77, 'city' => mb_strtoupper("Goianorte", "UTF-8")],
            ['id' => 367, 'state_id' => 77, 'city' => mb_strtoupper("Goiatins", "UTF-8")],
            ['id' => 368, 'state_id' => 77, 'city' => mb_strtoupper("Guaraí", "UTF-8")],
            ['id' => 369, 'state_id' => 77, 'city' => mb_strtoupper("Gurupi", "UTF-8")],
            ['id' => 370, 'state_id' => 77, 'city' => mb_strtoupper("Ipueiras", "UTF-8")],
            ['id' => 371, 'state_id' => 77, 'city' => mb_strtoupper("Itacajá", "UTF-8")],
            ['id' => 372, 'state_id' => 77, 'city' => mb_strtoupper("Itaguatins", "UTF-8")],
            ['id' => 373, 'state_id' => 77, 'city' => mb_strtoupper("Itapiratins", "UTF-8")],
            ['id' => 374, 'state_id' => 77, 'city' => mb_strtoupper("Itaporã do Tocantins", "UTF-8")],
            ['id' => 375, 'state_id' => 77, 'city' => mb_strtoupper("Jaú do Tocantins", "UTF-8")],
            ['id' => 376, 'state_id' => 77, 'city' => mb_strtoupper("Juarina", "UTF-8")],
            ['id' => 377, 'state_id' => 77, 'city' => mb_strtoupper("Lagoa da Confusão", "UTF-8")],
            ['id' => 378, 'state_id' => 77, 'city' => mb_strtoupper("Lagoa do Tocantins", "UTF-8")],
            ['id' => 379, 'state_id' => 77, 'city' => mb_strtoupper("Lajeado", "UTF-8")],
            ['id' => 380, 'state_id' => 77, 'city' => mb_strtoupper("Lavandeira", "UTF-8")],
            ['id' => 381, 'state_id' => 77, 'city' => mb_strtoupper("Lizarda", "UTF-8")],
            ['id' => 382, 'state_id' => 77, 'city' => mb_strtoupper("Luzinópolis", "UTF-8")],
            ['id' => 383, 'state_id' => 77, 'city' => mb_strtoupper("Marianópolis do Tocantins", "UTF-8")],
            ['id' => 384, 'state_id' => 77, 'city' => mb_strtoupper("Mateiros", "UTF-8")],
            ['id' => 385, 'state_id' => 77, 'city' => mb_strtoupper("Maurilândia do Tocantins", "UTF-8")],
            ['id' => 386, 'state_id' => 77, 'city' => mb_strtoupper("Miracema do Tocantins", "UTF-8")],
            ['id' => 387, 'state_id' => 77, 'city' => mb_strtoupper("Miranorte", "UTF-8")],
            ['id' => 388, 'state_id' => 77, 'city' => mb_strtoupper("Monte do Carmo", "UTF-8")],
            ['id' => 389, 'state_id' => 77, 'city' => mb_strtoupper("Monte Santo do Tocantins", "UTF-8")],
            ['id' => 390, 'state_id' => 77, 'city' => mb_strtoupper("Palmeiras do Tocantins", "UTF-8")],
            ['id' => 391, 'state_id' => 77, 'city' => mb_strtoupper("Muricilândia", "UTF-8")],
            ['id' => 392, 'state_id' => 77, 'city' => mb_strtoupper("Natividade", "UTF-8")],
            ['id' => 393, 'state_id' => 77, 'city' => mb_strtoupper("Nazaré", "UTF-8")],
            ['id' => 394, 'state_id' => 77, 'city' => mb_strtoupper("Nova Olinda", "UTF-8")],
            ['id' => 395, 'state_id' => 77, 'city' => mb_strtoupper("Nova Rosalândia", "UTF-8")],
            ['id' => 396, 'state_id' => 77, 'city' => mb_strtoupper("Novo Acordo", "UTF-8")],
            ['id' => 397, 'state_id' => 77, 'city' => mb_strtoupper("Novo Alegre", "UTF-8")],
            ['id' => 398, 'state_id' => 77, 'city' => mb_strtoupper("Novo Jardim", "UTF-8")],
            ['id' => 399, 'state_id' => 77, 'city' => mb_strtoupper("Oliveira de Fátima", "UTF-8")],
            ['id' => 400, 'state_id' => 77, 'city' => mb_strtoupper("Palmeirante", "UTF-8")],
            ['id' => 401, 'state_id' => 77, 'city' => mb_strtoupper("Palmeirópolis", "UTF-8")],
            ['id' => 402, 'state_id' => 77, 'city' => mb_strtoupper("Paraíso do Tocantins", "UTF-8")],
            ['id' => 403, 'state_id' => 77, 'city' => mb_strtoupper("Paranã", "UTF-8")],
            ['id' => 404, 'state_id' => 77, 'city' => mb_strtoupper("Pau D'Arco", "UTF-8")],
            ['id' => 405, 'state_id' => 77, 'city' => mb_strtoupper("Pedro Afonso", "UTF-8")],
            ['id' => 406, 'state_id' => 77, 'city' => mb_strtoupper("Peixe", "UTF-8")],
            ['id' => 407, 'state_id' => 77, 'city' => mb_strtoupper("Pequizeiro", "UTF-8")],
            ['id' => 408, 'state_id' => 77, 'city' => mb_strtoupper("Colméia", "UTF-8")],
            ['id' => 409, 'state_id' => 77, 'city' => mb_strtoupper("Pindorama do Tocantins", "UTF-8")],
            ['id' => 410, 'state_id' => 77, 'city' => mb_strtoupper("Piraquê", "UTF-8")],
            ['id' => 411, 'state_id' => 77, 'city' => mb_strtoupper("Pium", "UTF-8")],
            ['id' => 412, 'state_id' => 77, 'city' => mb_strtoupper("Ponte Alta do Bom Jesus", "UTF-8")],
            ['id' => 413, 'state_id' => 77, 'city' => mb_strtoupper("Ponte Alta do Tocantins", "UTF-8")],
            ['id' => 414, 'state_id' => 77, 'city' => mb_strtoupper("Porto Alegre do Tocantins", "UTF-8")],
            ['id' => 415, 'state_id' => 77, 'city' => mb_strtoupper("Porto Nacional", "UTF-8")],
            ['id' => 416, 'state_id' => 77, 'city' => mb_strtoupper("Praia Norte", "UTF-8")],
            ['id' => 417, 'state_id' => 77, 'city' => mb_strtoupper("Presidente Kennedy", "UTF-8")],
            ['id' => 418, 'state_id' => 77, 'city' => mb_strtoupper("Pugmil", "UTF-8")],
            ['id' => 419, 'state_id' => 77, 'city' => mb_strtoupper("Recursolândia", "UTF-8")],
            ['id' => 420, 'state_id' => 77, 'city' => mb_strtoupper("Riachinho", "UTF-8")],
            ['id' => 421, 'state_id' => 77, 'city' => mb_strtoupper("Rio da Conceição", "UTF-8")],
            ['id' => 422, 'state_id' => 77, 'city' => mb_strtoupper("Rio dos Bois", "UTF-8")],
            ['id' => 423, 'state_id' => 77, 'city' => mb_strtoupper("Rio Sono", "UTF-8")],
            ['id' => 424, 'state_id' => 77, 'city' => mb_strtoupper("Sampaio", "UTF-8")],
            ['id' => 425, 'state_id' => 77, 'city' => mb_strtoupper("Sandolândia", "UTF-8")],
            ['id' => 426, 'state_id' => 77, 'city' => mb_strtoupper("Santa Fé do Araguaia", "UTF-8")],
            ['id' => 427, 'state_id' => 77, 'city' => mb_strtoupper("Santa Maria do Tocantins", "UTF-8")],
            ['id' => 428, 'state_id' => 77, 'city' => mb_strtoupper("Santa Rita do Tocantins", "UTF-8")],
            ['id' => 429, 'state_id' => 77, 'city' => mb_strtoupper("Santa Rosa do Tocantins", "UTF-8")],
            ['id' => 430, 'state_id' => 77, 'city' => mb_strtoupper("Santa Tereza do Tocantins", "UTF-8")],
            ['id' => 431, 'state_id' => 77, 'city' => mb_strtoupper("Santa Terezinha do Tocantins", "UTF-8")],
            ['id' => 432, 'state_id' => 77, 'city' => mb_strtoupper("São Bento do Tocantins", "UTF-8")],
            ['id' => 433, 'state_id' => 77, 'city' => mb_strtoupper("São Félix do Tocantins", "UTF-8")],
            ['id' => 434, 'state_id' => 77, 'city' => mb_strtoupper("São Miguel do Tocantins", "UTF-8")],
            ['id' => 435, 'state_id' => 77, 'city' => mb_strtoupper("São Salvador do Tocantins", "UTF-8")],
            ['id' => 436, 'state_id' => 77, 'city' => mb_strtoupper("São Sebastião do Tocantins", "UTF-8")],
            ['id' => 437, 'state_id' => 77, 'city' => mb_strtoupper("São Valério", "UTF-8")],
            ['id' => 438, 'state_id' => 77, 'city' => mb_strtoupper("Silvanópolis", "UTF-8")],
            ['id' => 439, 'state_id' => 77, 'city' => mb_strtoupper("Sítio Novo do Tocantins", "UTF-8")],
            ['id' => 440, 'state_id' => 77, 'city' => mb_strtoupper("Sucupira", "UTF-8")],
            ['id' => 441, 'state_id' => 77, 'city' => mb_strtoupper("Taguatinga", "UTF-8")],
            ['id' => 442, 'state_id' => 77, 'city' => mb_strtoupper("Taipas do Tocantins", "UTF-8")],
            ['id' => 443, 'state_id' => 77, 'city' => mb_strtoupper("Talismã", "UTF-8")],
            ['id' => 444, 'state_id' => 77, 'city' => mb_strtoupper("Palmas", "UTF-8")],
            ['id' => 445, 'state_id' => 77, 'city' => mb_strtoupper("Tocantínia", "UTF-8")],
            ['id' => 446, 'state_id' => 77, 'city' => mb_strtoupper("Tocantinópolis", "UTF-8")],
            ['id' => 447, 'state_id' => 77, 'city' => mb_strtoupper("Tupirama", "UTF-8")],
            ['id' => 448, 'state_id' => 77, 'city' => mb_strtoupper("Tupiratins", "UTF-8")],
            ['id' => 449, 'state_id' => 77, 'city' => mb_strtoupper("Wanderlândia", "UTF-8")],
            ['id' => 450, 'state_id' => 77, 'city' => mb_strtoupper("Xambioá", "UTF-8")],

        ];

        // foreach ($cities as $city) {
        // try {
            City::insert($cities);
        // } catch (\Exception $e) {
        //
        // }
        // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
