<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\State;
use DB;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if (State::get()->count() <= 0) {
            $states = [
                [ 'id' => 51, 'initials' => 'AC', 'state' => "Acre", 'country_id' => 30],
                [ 'id' => 52, 'initials' => 'AL', 'state' => "Alagoas", 'country_id' => 30],
                [ 'id' => 53, 'initials' => 'AP', 'state' => "Amapá", 'country_id' => 30],
                [ 'id' => 54, 'initials' => 'AM', 'state' => "Amazonas", 'country_id' => 30],
                [ 'id' => 55, 'initials' => 'BA', 'state' => "Bahia", 'country_id' => 30],
                [ 'id' => 56, 'initials' => 'CE', 'state' => "Ceará", 'country_id' => 30],
                [ 'id' => 57, 'initials' => 'DF', 'state' => "Distrito Federal", 'country_id' => 30],
                [ 'id' => 58, 'initials' => 'ES', 'state' => "Espírito Santo", 'country_id' => 30],
                [ 'id' => 59, 'initials' => 'GO', 'state' => "Goiás", 'country_id' => 30],
                [ 'id' => 60, 'initials' => 'MA', 'state' => "Maranhão", 'country_id' => 30],
                [ 'id' => 61, 'initials' => 'MT', 'state' => "Mato Grosso", 'country_id' => 30],
                [ 'id' => 62, 'initials' => 'MS', 'state' => "Mato Grosso do Sul", 'country_id' => 30],
                [ 'id' => 63, 'initials' => 'MG', 'state' => "Minas Gerais", 'country_id' => 30],

                [ 'id' => 64, 'initials' => 'PA', 'state' => "Pará", 'country_id' => 30],
                [ 'id' => 65, 'initials' => 'PB', 'state' => "Paraíba", 'country_id' => 30],
                [ 'id' => 66, 'initials' => 'PR', 'state' => "Paraná", 'country_id' => 30],
                [ 'id' => 67, 'initials' => 'PE', 'state' => "Pernambuco", 'country_id' => 30],
                [ 'id' => 68, 'initials' => 'PI', 'state' => "Piauí", 'country_id' => 30],
                [ 'id' => 69, 'initials' => 'RJ', 'state' => "Rio de Janeiro", 'country_id' => 30],
                [ 'id' => 70, 'initials' => 'RN', 'state' => "Rio Grande do Norte", 'country_id' => 30],
                [ 'id' => 71, 'initials' => 'RS', 'state' => "Rio Grande do Sul", 'country_id' => 30],

                [ 'id' => 72, 'initials' => 'RO', 'state' => "Rondônia", 'country_id' => 30],
                [ 'id' => 73, 'initials' => 'RR', 'state' => "Roraima", 'country_id' => 30],
                [ 'id' => 74, 'initials' => 'SC', 'state' => "Santa Catarina", 'country_id' => 30],
                [ 'id' => 75, 'initials' => 'SP', 'state' => "São Paulo", 'country_id' => 30],
                [ 'id' => 76, 'initials' => 'SE', 'state' => "Sergipe", 'country_id' => 30],
                [ 'id' => 77, 'initials' => 'TO', 'state' => "Tocantins", 'country_id' => 30],
            ];

            State::insert($states);
        }
    }
}
