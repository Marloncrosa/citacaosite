<?php namespace Modules\Admin\Database\Seeders\Cidades\Brasil;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\City;
use DB;

class AcreCitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        // if (City::get()->count() <= 0) {
            City::truncate();
            $cities = [
                ['id' => 53, 'state_id' => 51, 'city' => mb_strtoupper("Acrelândia", "UTF-8")],
                ['id' => 54, 'state_id' => 51, 'city' => mb_strtoupper("Assis Brasil", "UTF-8")],
                ['id' => 55, 'state_id' => 51, 'city' => mb_strtoupper("Brasiléia", "UTF-8")],
                ['id' => 56, 'state_id' => 51, 'city' => mb_strtoupper("Bujari", "UTF-8")],
                ['id' => 57, 'state_id' => 51, 'city' => mb_strtoupper("Capixaba", "UTF-8")],
                ['id' => 58, 'state_id' => 51, 'city' => mb_strtoupper("Cruzeiro do Sul", "UTF-8")],
                ['id' => 59, 'state_id' => 51, 'city' => mb_strtoupper("Epitaciolândia", "UTF-8")],
                ['id' => 60, 'state_id' => 51, 'city' => mb_strtoupper("Feijó", "UTF-8")],
                ['id' => 61, 'state_id' => 51, 'city' => mb_strtoupper("Jordão", "UTF-8")],
                ['id' => 62, 'state_id' => 51, 'city' => mb_strtoupper("Mâncio Lima", "UTF-8")],
                ['id' => 63, 'state_id' => 51, 'city' => mb_strtoupper("Manoel Urbano", "UTF-8")],
                ['id' => 64, 'state_id' => 51, 'city' => mb_strtoupper("Marechal Thaumaturgo", "UTF-8")],
                ['id' => 65, 'state_id' => 51, 'city' => mb_strtoupper("Plácido de Castro", "UTF-8")],
                ['id' => 66, 'state_id' => 51, 'city' => mb_strtoupper("Porto Walter", "UTF-8")],
                ['id' => 67, 'state_id' => 51, 'city' => mb_strtoupper("Rio Branco", "UTF-8")],
                ['id' => 68, 'state_id' => 51, 'city' => mb_strtoupper("Rodrigues Alves", "UTF-8")],
                ['id' => 69, 'state_id' => 51, 'city' => mb_strtoupper("Santa Rosa do Purus", "UTF-8")],
                ['id' => 70, 'state_id' => 51, 'city' => mb_strtoupper("Senador Guiomard", "UTF-8")],
                ['id' => 71, 'state_id' => 51, 'city' => mb_strtoupper("Sena Madureira", "UTF-8")],
                ['id' => 72, 'state_id' => 51, 'city' => mb_strtoupper("Tarauacá", "UTF-8")],
                ['id' => 73, 'state_id' => 51, 'city' => mb_strtoupper("Xapuri", "UTF-8")],
                ['id' => 74, 'state_id' => 51, 'city' => mb_strtoupper("Porto Acre", "UTF-8")],

            ];

            // foreach ($cities as $city) {
            //     try {
                    City::insert($cities);
                // } catch (\Exception $e) {
                //
                // }
            // }
        // }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
