<?php namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\PermissionsGroup as ModelC;
use Modules\Admin\Entities\Permissions;

class PermissionsGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        if (ModelC::get()->count() <= 0) {
            ModelC::truncate();

            $grupo = ModelC::create([
                'name' => "Todos as permissões",
            ]);

            $grupo->permissions()->sync(
                Permissions::pluck('id')->all()
            );
        }
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
