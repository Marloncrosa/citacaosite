<?php

namespace Modules\Admin\Notifications;

use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    /**
     * The password reset token.
     *
     * @var string
     */
    public $token;
    public $user;

    /**
     * Create a notification instance.
     *
     * @param  string $token
     *
     * @return void
     */
    public function __construct($token, $user)
    {
        $request = app('request');

        $this->token = $token;
        $this->user = $user;
    }

    /**
     * Get the notification's channels.
     *
     * @param  mixed $notifiable
     *
     * @return array|string
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $request = app('request');

        return (new MailMessage)
            // ->cc('felipe.almeman@gmail.com')
            ->greeting(__("Olá, ") . $this->user->nome)
            ->subject(__("Esqueceu sua senha"))
            ->line(__("Você está recebendo este e-mail por que nós recebemos uma solicitação de alteração de senha da sua conta."))
            ->action(__("Alterar Senha"), route('admin.resetar-senha.reset',
                ['token' => $this->token]))
            ->line(__("Caso não tenha sido você que tenha feito essa requisição, desconsidere o e-mail."))
            ->markdown('admin::notifications.email')// -> view('admin::notifications.email')
            ;
    }

    /**
     * Build the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function teste()
    {
        $request = app('request');

        return (new MailMessage)
            // ->cc('felipe.almeman@gmail.com')
            ->greeting(__("Olá, ") . $this->user->nome)
            ->subject(__("Esqueceu sua senha"))
            ->line(__("Você está recebendo este e-mail por que nós recebemos uma solicitação de alteração de senha da sua conta."))
            ->action(__("Alterar Senha"), route('admin.resetar-senha.reset',
                ['token' => $this->token]))
            ->line(__("Caso não tenha sido você que tenha feito essa requisição, desconsidere o e-mail."))// -> markdown('admin::notifications.email')
            ;
    }
}
