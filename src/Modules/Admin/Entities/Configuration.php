<?php namespace Modules\Admin\Entities;


/**
 * Modules\Admin\Entities\Configuration
 *
 * @property int $id
 * @property string $campo
 * @property string|null $texto
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $imagem
 * @property-read mixed $atributos
 * @property-write mixed $data
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Configuration whereCampo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Configuration whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Configuration whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Configuration whereImagem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Configuration whereTexto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Configuration whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Configuration extends Model
{
    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'configuration';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'language_id',
        'field',
        'value',
        'meaning',
    ];

    public function getAtributosAttribute()
    {
        return [
            'campo' => ['titulo' => 'Campo', 'listar' => true, 'adicionar' => true, 'editar' => true],
            'texto' => ['titulo' => 'Texto', 'listar' => false, 'adicionar' => true, 'editar' => true],
            'imagem' => ['titulo' => 'Imagem', 'listar' => false, 'adicionar' => true, 'editar' => true, 'searchable' => false, 'uploadpath' => "configuracoes/{$this->id}/"],
        ];
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];


    /**
     * Alterar coluna de ações
     */
    public function getAcaoAttribute()
    {
        return [
            'tamanho' => '80',

            'edit' => [
                'html' => '
              <a class="button secondary" title="' . __('Edit') . '" href="{$url}/edit/{$pk}/{$id}"><i class="far fa-edit" aria-hidden="true"></i></a>
            '
            ],

            'delete' => [
                'html' => '
              <a class="button alert" title="' . __('Delete') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                <i class="fa fa-times" aria-hidden="true"></i>
              </a>
            '
            ],
        ];
    }

    // Getters
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    // Setters
    public function setDataAttribute($value)
    {
        $this->attributes['data'] = formataDataHoraMysql($value . ':00');
    }
}
