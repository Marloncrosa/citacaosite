<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Modules\Admin\Entities\Leads
 *
 * @property int $id
 * @property string|null $nome
 * @property string $email
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Leads whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Leads whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Leads whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Leads whereNome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Leads whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Logos extends Model
{
    use SoftDeletes;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'logos';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'image',
        'image_mobile',
        'active',
    ];

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'similars',
        'leads',
    ];

    public function similars()
    {
        return $this->belongsTo(LeadsStatus::class, 'similars_id')->whereNull('similars_id');
    }

    public function leads()
    {
        return $this->hasMany(Leads::class, 'leads_origin_id');
    }

//    public $acao = '';

    /**
     * Regras de validação
     * @var array
     */
    public function getRulesAttribute()
    {
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }
        if (!isset($id)) {
            $id = null;
        }

        return [
            'title' => 'required',
            'active' => 'boolean',
        ];
    }

    public function messages()
    {
        return [

            'title.required' => __('validation.required', ['attribute' => __("Título")]),
            'active.boolean' => __('validation.boolean', ['attribute' => __("Ativo")]),
        ];
    }

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'image' => ['titulo' => __('Imagem'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "logos/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'image_mobile' => ['titulo' => __('Imagem Mobile'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "logos/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'title' => ['titulo' => __('Título'), 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    public function getAcaoAttribute()
    {
        return [
            'active' => [
                'campo' => 'active',
                'validar' => 'boolean',
                'html' => '
                    <a class="dropdown-item" title="' . __('Ativar') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Ativar') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Desativar') . '"
                    >
                      <i class="fas fa-circle" aria-hidden="true"></i> <span class="text">' . __('Ativar') . '</span>
                    </a>
                ',
                'html2' => '
                    <a class="dropdown-item" title="' . __('Desativar') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Ativar') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Desativar') . '"
                    >
                      <i class="far fa-dot-circle" aria-hidden="true"></i> <span class="text">' . __('Desativar') . '</span>
                    </a>
                ',
            ],

            'editar' => [
                'html' => '
                    <a class="dropdown-item open-form-sidebar" title="' . __('Editar') . '" href="{$url}/edit/{$pk}/{$id}">
                      <i class="far fa-edit" aria-hidden="true"></i> ' . __('Editar') . '
                    </a>
                '
            ],

            'delete' => [
                'html' => '
                    <a class="dropdown-item" title="' . __('Remover') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                      <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remover') . '
                    </a>
                '
            ],
        ];
    }

    // Getters
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getImageCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = $this->atributos['image']['uploadpath'] . $this->image;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'logos/' . $this->image;
            }
        }

        return $retorno;
    }
    public function getImageMobileCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image_mobile)) {
            $retorno = $this->atributos['image_mobile']['uploadpath'] . $this->image_mobile;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'logos/' . $this->image_mobile;
            }
        }

        return $retorno;
    }

}
