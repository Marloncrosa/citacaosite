<?php namespace Modules\Admin\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactSubjects extends Model
{
    use SoftDeletes;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'contact_subjects';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'active',
    ];

    /**
     * Algumas definições
     * @var [type]
     */
    public $atributos = [
        'title' => ['titulo' => 'Título', 'listar' => true, 'adicionar' => true, 'editar' => true, 'validacao' => 'required'],
    ];


    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'contact',
    ];

    public function contact()
    {
        return $this->hasMany(Contact::class, 'contact_subjects_id');
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function scopeAtivoelinguagem($query, $language_id)
    {
        return $query->where('active', true)->where('language_id', $language_id);
    }
}
