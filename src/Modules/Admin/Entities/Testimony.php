<?php namespace Modules\Admin\Entities;

use Carbon\Carbon;

/**
 * Modules\Admin\Entities\Contato
 *
 * @property int $id
 * @property string|null $nome
 * @property string $email
 * @property string|null $telefone
 * @property string $mensagem
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Modules\Admin\Entities\City $cidades
 * @property-read \Modules\Admin\Entities\State $estados
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contato whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contato whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contato whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contato whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contato whereMensagem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contato whereNome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contato whereTelefone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contato whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Testimony extends Model
{
    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'testimony';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'image',
        'image_mobile',
        'function',
        'description',
    ];

    /**
     * Algumas definições
     * @var [type]
     */

    public function getAtributosAttribute()
    {
        return [
            'image' => ['titulo' => 'Imagem', 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "testimony/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'image_mobile' => ['titulo' => 'Imagem', 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "testimony/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'name' => ['titulo' => 'Cliente', 'listar' => true, 'adicionar' => true, 'editar' => true],
            'description' => ['titulo' => 'Descrição', 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'clients',
    ];

    public function clients()
    {
        return $this->belongsTo(Clients::class, 'client_id', 'id');
    }


    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'update_at',
        'created_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    public function getImageCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = $this->atributos['image']['uploadpath'] . $this->image;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'testimony/' . $this->image;
            }
        }

        return $retorno;
    }
    public function getImageMobileCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image_mobile)) {
            $retorno = $this->atributos['image_mobile']['uploadpath'] . $this->image_mobile;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'testimony/' . $this->image_mobile;
            }
        }

        return $retorno;
    }
   }
