<?php namespace Modules\Admin\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Modules\Admin\Entities\Contato
 *
 * @property int $id
 * @property string|null $nome
 * @property string $email
 * @property string|null $telefone
 * @property string $mensagem
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Modules\Admin\Entities\City $cidades
 * @property-read \Modules\Admin\Entities\State $estados
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereMensagem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereNome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereTelefone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Contact extends Model
{
    use SoftDeletes;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'contact';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'contact_subjects_id',
        'name',
        'email',
        'phone',
        'cellphone',
        'message',
    ];

    /**
     * Algumas definições
     * @var [type]
     */
    public $atributos = [
        'name' => ['titulo' => 'Nome', 'listar' => true, 'adicionar' => true, 'editar' => true, 'validacao' => 'required'],
        'email' => ['titulo' => 'E-mail', 'listar' => true, 'adicionar' => true, 'editar' => true, 'validacao' => 'required'],
        'cellphone' => ['titulo' => 'Telefone', 'listar' => false, 'adicionar' => true, 'editar' => true, 'validacao' => 'required'],
        'created_at' => ['titulo' => 'Data/Hora', 'listar' => true, 'adicionar' => false, 'editar' => false],
    ];

    /**
     * Alterar coluna de ações
     */
    public function getAcaoAttribute()
    {
        return [
            'show' => [
                'html' => '
					<a class="dropdown-item open-form-sidebar" title="' . __('Show') . '" href="{$url}/show/{$pk}/{$id}">
					  <i class="far fa-edit" aria-hidden="true"></i> ' . __('Show') . '
					</a>
				'
            ],

            'delete' => [
                'html' => '
					<a class="dropdown-item" title="' . __('Remove') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
						<i class="fas fa-times" aria-hidden="true"></i> ' . __('Remove') . '
					</a>
				'
            ],
        ];
    }

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'contact_subjects',
    ];

    public function contact_subjects()
    {
        return $this->belongsTo(ContactSubjects::class, 'contact_subjects_id');
    }

//    public function contact_sector()
//    {
//        return $this->belongsTo(ContactSubjects::class, 'contact_sector_id');
//    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    // Getters
//    public function dataenvio()
//    {
//        $data = Carbon::parse($this->attributes['created_at']);
//        return $data->format('d/m/Y');
//    }
//
    public function getCreatedAtAttribute($value)
    {
        $create_at = Carbon::parse($value);
//        return $create_at->format('d/m/Y H:i');
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    // Setters
    // public function setCreatedAtAttribute($value){
    // 	return formataDataHoraMysql($value);
    // }
    // public function setUpdatedAtAttribute($value){
    // 	return formataDataHoraMysql($value);
    // }
}
