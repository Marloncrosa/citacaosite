<?php namespace Modules\Admin\Entities;


use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Admin\Entities\Banners
 *
 * @property int $id
 * @property string $titulo
 * @property string|null $imagem
 * @property string|null $imagem_media
 * @property string|null $imagem_pequena
 * @property string|null $link
 * @property bool $ativo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereAtivo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereImagem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereImagemMedia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereImagemPequena($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereTitulo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Institutional extends Model
{

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'institutional';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'subtitle',
        'description1',
        'image',
        'description2',
        'active',
    ];
//
//    /**
//     * Converte a coluna em um tipo de variavel
//     */
   protected $casts = [
       'active' => 'boolean',
   ];

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'image' => ['titulo' => 'Imagem', 'listar' => false, 'adicionar' => true, 'editar' => true, 'uploadpath' => "institutional/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'title' => ['titulo' => 'Titulo', 'listar' => true, 'adicionar' => true, 'editar' => true],
            'active' => ['titulo' => 'Ativo', 'listar' => false, 'adicionar' => true, 'editar' => true],
            'description1' => ['titulo' => 'Descrição', 'listar' => false, 'adicionar' => true, 'editar' => true],
        ];
    }

//    /**
//     * Seta relacionamentos
//     */
//    public $relacionamentos = [
//        'language',
//    ];
//
//    public function language()
//    {
//        return $this->belongsTo(Language::class, 'language_id');
//    }

    public $rules = [
        'titulo' => 'required',
        'imagem' => 'image',
        'active' => 'boolean',
    ];

    public function messages()
    {
        return [
            'titulo.required' => __("validation.required", ['attribute' => 'Título']),
            'imagem.image' => __("validation.image", ['attribute' => 'Imagem']),
            'active.boolean' => __('validation.boolean', ['attribute' => 'Ativo']),
        ];
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // Getters
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getImageCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = $this->atributos['image']['uploadpath'] . $this->image;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'institutional/' . $this->image;
            }
        }

        return $retorno;
    }
}
