<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;
use Illuminate\Validation\Rule;
use Modules\Admin\Notifications\ResetPassword;
use Modules\Admin\Notifications\NewUser;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Modules\Admin\Repositories\DatabaseTokenRepository;
use Modules\Admin\Traits\PermissionTrait;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

class Users extends Authenticatable implements AuditableContract
{
//    use Notifiable, Auditable, SoftDeletes, PermissionTrait;
    use Notifiable, Auditable, PermissionTrait;

//    protected $guard = 'admin';

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
//    protected $guarded = ['id'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'login',
        'password',
        'avatar',
        'cellphone',
        'webmaster',
        'active',
    ];

    /**
     * Algumas definições
     * @var [type]
     */
//    public function getControlPermissionsAttribute()
//    {
//        return (object) [
//            'list' => 'List users'
//        ];
//    }
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'webmaster' => 'boolean',
        'active' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'avatar' => ['titulo' => __('Avatar'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "users/{$this->id}/avatar/"],

            'name' => ['titulo' => __('Name'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'trocarsenha' => 'false'],
            'login' => ['titulo' => __('Login'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'trocarsenha' => 'false'],
            'email' => ['titulo' => __('E-mail'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'trocarsenha' => 'false'],
            'cellphone' => ['titulo' => __('Cellphone'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'trocarsenha' => 'false'],
            'active' => ['titulo' => __('Active'), 'listar' => false, 'adicionar' => true, 'editar' => true, 'trocarsenha' => 'false'],

            'password' => ['titulo' => __('Password'), 'listar' => false, 'adicionar' => true, 'editar' => false, 'trocarsenha' => true],
        ];
    }

    /**
     * Regras de validação
     * @var array
     */
    public function getRulesAttribute()
    {
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }
        if (!isset($id)) {
            $id = null;
        }

        return [
            'nome' => 'required|min:2',
            'login' => [
                'regra' => [
                    'required',
                    'min:4',
                    Rule::unique('users', 'login')->ignore($id)
                ]
            ],
            'email' => [
                'regra' => [
                    'required',
                    'min:3',
                    'email',
                    Rule::unique('users', 'email')->ignore($id)
                ]
            ],
            'active' => 'boolean',
        ];
    }

    /**
     * Mensagens de validação
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __("validation.required", ['attribute' => __('Name')]),
            'login.required' => __("validation.required", ['attribute' => 'Login']),
            'email.required' => __("validation.required", ['attribute' => 'E-mail']),
            'password.required' => __("validation.required", ['attribute' => 'Senha']),

            'name.min' => __("validation.min", ['attribute' => __('Name')]),
            'login.min' => __("validation.min", ['attribute' => 'Login']),
            'email.min' => __("validation.min", ['attribute' => 'E-mail']),
            'password.min' => __("validation.min", ['attribute' => 'Senha']),

            'email.email' => __("validation.email", ['attribute' => 'E-mail']),

            'password.confirmed' => __("validation.confirmed", ['attribute' => 'Senha']),

            'active.boolean' => __("validation.required", ['attribute' => 'Ativo']),

            'login.unique' => __("validation.unique", ['attribute' => 'Login']),
            'email.unique' => __("validation.unique", ['attribute' => 'E-mail']),
        ];
    }

    public function getAvatarImagemAttribute()
    {
        $retorno = null;
        if (!empty($this->avatar)) {
            $retorno = $this->atributos['avatar']['uploadpath'] . $this->avatar;
        }

        return $retorno;
    }

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        if (!is_null($value)) {
            $data = bcrypt($value);
            $this->attributes['password'] = $data;
        }
    }

    /**
     * @param $value
     */
    public function setSenhaAttribute($value)
    {
        if (!is_null($value)) {
            $data = bcrypt($value);
            $this->attributes['password'] = $data;
        }
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     *
     * @return void
     */
    public function sendPasswordResetNotificationNew($token, $user)
    {
        $notification = new ResetPassword($token, $user);

        // Teste - descomentar até o dd
//     $novo_markdown = [
//     	'theme' => 'padrao',
//     	'paths' => [
//     		resource_path('../Modules/Admin/Resources/views/mail'),
//     	],
//     ];
//     $markdown = new \Illuminate\Mail\Markdown(view(), $novo_markdown);
//     $data = $notification->teste() -> toArray();
//     // dump( $data );
//     echo $markdown->render('admin::notifications.email', $data);
//     dd();

        config(
            [
                'mail.markdown.theme' => 'padrao',
                'mail.markdown.paths' => [
                    resource_path('../Modules/Admin/Resources/views/mail'),
                ],
            ]
        );

        $this->notify($notification);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string $token
     *
     * @return void
     */
    public function sendNewPasswordNotification($token, $user)
    {
        $notification = new NewUser($token, $user);

        // Teste - descomentar até o dd
//     $novo_markdown = [
//     	'theme' => 'padrao',
//     	'paths' => [
//     		resource_path('../Modules/Admin/Resources/views/mail'),
//     	],
//     ];
//     $markdown = new \Illuminate\Mail\Markdown(view(), $novo_markdown);
//     $data = $notification->teste() -> toArray();
//     // dump( $data );
//     echo $markdown->render('admin::notifications.email', $data);
//     dd();

        config(
            [
                'mail.markdown.theme' => 'padrao',
                'mail.markdown.paths' => [
                    resource_path('../Modules/Admin/Resources/views/mail'),
                ],
            ]
        );

        $this->notify($notification);
    }

    // Trazer os grupos que possuem esas permission

    /**
     * Get the user for the given credentials.
     *
     * @param  array $credentials
     *
     * @return \Illuminate\Contracts\Auth\CanResetPassword
     *
     * @throws \UnexpectedValueException
     */
    public function getUser(array $credentials)
    {
        $credentials = Arr::except($credentials, ['token']);

        $user = static::where('email', $credentials['email'])->first();

        if ($user && !$user instanceof CanResetPasswordContract) {
            throw new \UnexpectedValueException('User must implement CanResetPassword interface.');
        }

        return $user;
    }

    public function groups()
    {
        return $this->belongsToMany(
            PermissionsGroup::class,
            'user_permissions_group',
            'user_id',
            'permissions_group_id'
        )->withTimestamps()->withPivot('user_id', 'permissions_group_id');
    }

    // Traz as permissões deste grupo
    public function permissions()
    {
        $permission = collect(
            json_decode($this->getAttributes()['permissions'])
        );

        return $permission;
    }

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
    ];

    public function getAcaoAttribute()
    {
        return [
            'groups' => [
                'html' => '
                    <a class="dropdown-item" title="' . __('Groups Permissions') . '" href="' . route('admin.anyroute', ['user_groups/index/user_id']) . '/{$id}">
                      ' . __('Groups Permissions') . '
                    </a>
                '
            ],

            /*'permissions' => [
                'html' => '
                    <a class="dropdown-item open-form-sidebar" title="' . __('Permissions') . '" href="' . route('admin.anyroute', ['users/permissions/id']) . '/{$id}">
                      ' . __('Permissions') . '
                    </a>
                '
            ],*/

            'active' => [
                'campo' => 'active',
                'validar' => 'boolean',
                'html' => '
                    <a class="dropdown-item" title="' . __('Activate') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Activate') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Deactivate') . '"
                    >
                      <i class="fas fa-circle" aria-hidden="true"></i> <span class="text">' . __('Activate') . '</span>
                    </a>
                ',
                'html2' => '
                    <a class="dropdown-item" title="' . __('Deactivate') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Activate') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Deactivate') . '"
                    >
                      <i class="far fa-dot-circle" aria-hidden="true"></i> <span class="text">' . __('Deactivate') . '</span>
                    </a>
                ',
            ],

            'editar' => [
                'html' => '
                    <a class="dropdown-item open-form-sidebar" title="' . __('Edit') . '" href="{$url}/edit/{$pk}/{$id}">
                      <i class="far fa-edit" aria-hidden="true"></i> ' . __('Edit') . '
                    </a>
                '
            ],

            'delete' => [
                'html' => '
                    <a class="dropdown-item" title="' . __('Remove') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                      <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remove') . '
                    </a>
                '
            ],
        ];
    }

    protected static function boot()
    {
        parent::boot();

        static::created(function ($user) {
            if(empty($user->password)){
                // and before finish, try to send a password link
                try {
                    $tokens = new DatabaseTokenRepository(app('db')->connection(), app('hash'),
                        config('auth')['passwords']['site']['table'], app('config')['app.key'],
                        config('auth')['passwords']['site']['expire']);

                    // Generate Token
                    $token = $tokens->create($user);

                    // Send Reset Password
                    $user->sendNewPasswordNotification($token, $user);
                } catch (\Exception $e) {
                    \Log::alert('Error on user create event');
                    \Log::alert($e);
                }
            }
        });
    }
}
