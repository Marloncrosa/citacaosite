<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

class Leads extends Model
{
    use SoftDeletes;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'leads';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'leads_origin_id',
        'leads_status_id',

        'operator_id',
        'client_id',

        'client_draft_interess',

        'country_id',
        'state_id',
        'city_id',

        'name',
        'birthdate',
        'phone',
        'cpf',
        'rg',
        'email',
        'cellphone',
        'profession',

        'address',
        'postal_code',
        'number',
        'address_complement',
        'neighborhood',

        'answers',
        'observations',

        'term',
    ];

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'answers' => 'json',
        'term' => 'boolean',
    ];

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'lead_origin',
        'lead_status',
        'operator',
        'country',
        'city',
        'state',
        'client',
        'draft_interess',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function lead_origin()
    {
        return $this->belongsTo(LeadsOrigin::class, 'leads_origin_id');
    }

    public function lead_status()
    {
        return $this->belongsTo(LeadsStatus::class, 'leads_status_id');
    }

    public function operator()
    {
        return $this->belongsTo(Users::class, 'operator_id');
    }

    public function client()
    {
        return $this->belongsTo(Clients::class, 'client_id');
    }

    public function draft_interess()
    {
        return $this->belongsTo(Drafts::class, 'client_draft_interess');
    }

    public function getAcaoAttribute()
    {
        $action = [];

        $action['file'] = [
            'html' => '
                    <a class="dropdown-item" title="/' . __('Galeria de Arquivos') . '" href="' . route('admin.anyroute', ['leads_files/index/lead_id']) . '/{$id}">
                       <i class="fas fa-file" aria-hidden="true"></i>  ' . __('Galeria de Arquivos') . '
                    </a>
                '
        ];

        $action['status'] = [
            'html' => '
                    <a class="dropdown-item open-form-sidebar" title="' . __('Alterar Status') . '" href="{$url}/edit_status/{$pk}/{$id}">
                      <i class="flaticon-edit" aria-hidden="true"></i> ' . __('Alterar Status') . '
                    </a>
                '
        ];

        $action['editar'] = [
            'html' => '
                    <a class="dropdown-item open-form-sidebar" title="' . __('Editar') . '" href="{$url}/edit/{$pk}/{$id}">
                      <i class="flaticon-edit" aria-hidden="true"></i> ' . __('Editar') . '
                    </a>
                '
        ];

        $action['convert'] = [
            'html' => '
                    <a class="dropdown-item open-form-sidebar" title="' . __('Converter para Contrato') . '" href="{$url}/convert/{$pk}/{$id}">
                      <i class="flaticon-edit" aria-hidden="true"></i> ' . __('Converter para Contrato') . '
                    </a>
                '
        ];

        $action['delete'] = [
            'html' => '
                    <a class="dropdown-item" title="' . __('Remover') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                      <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remover') . '
                    </a>
                '
        ];


        if (count($action) <= 0) {
            $action = false;
        }

        return $action;
    }

    /**
     * Regras de validação
     * @var array
     */
    public function getRulesAttribute()
    {
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }
        if (!isset($id)) {
            $id = null;
        }

        return [
            // 'name' => 'required',
            // 'lastname' => 'required',
            // 'email' => "required",
            // 'phone' => "required",

            // 'language_id' => 'integer|exists:language,id',
            // 'unit_id' => 'integer|exists:unit,id',
            // 'email' => "required|unique:leads,email,$id,id",
            // 'phone' => "required|unique:leads,phone,$id,id",
            // 'country' => 'required',
            // 'state' => 'required',
            // 'city' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'lead_id.required' => __('validation.required', ['attribute' => __("Lead")]),
            'operator_id.required' => __('validation.required', ['attribute' => __("Operador")]),

            'name.required' => __('validation.required', ['attribute' => __("Nome")]),
            'lastname.required' => __('validation.required', ['attribute' => __("Sobrenome")]),
            'email.required' => __('validation.required', ['attribute' => __("E-mail")]),
            'phone.required' => __('validation.required', ['attribute' => __("Telefone")]),
            'country.required' => __('validation.required', ['attribute' => __("País")]),
            'state.required' => __('validation.required', ['attribute' => __("Estado")]),
            'city.required' => __('validation.required', ['attribute' => __("Cidade")]),
        ];
    }

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'id' => ['titulo' => __('ID'), 'listar' => true, 'checkbox' => true, 'adicionar' => false, 'editar' => false],
            'created_at' => ['titulo' => __('Data'), 'listar' => true, 'adicionar' => false, 'editar' => false],
            'draft_interess.title' => ['titulo' => __('Interesse'), 'listar' => true, 'adicionar' => false, 'editar' => false],
            'lead_origin.title' => ['titulo' => __('Origem'), 'listar' => true, 'adicionar' => false, 'editar' => false],
            'operator.name' => ['titulo' => __('Operador'), 'listar' => true, 'adicionar' => false, 'editar' => false],

            'name' => ['titulo' => __('Nome'), 'listar' => true, 'adicionar' => true, 'editar' => true],

            'email' => ['titulo' => __('E-mail'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'phone' => ['titulo' => __('Telefone'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'lead_status.title' => ['titulo' => __('Status'), 'listar' => true, 'adicionar' => false, 'editar' => false],
        ];
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    /*protected $hidden = [
        'created_at',
        'update_at',
    ];*/

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    // Getters
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getBirthdateAttribute($value)
    {
        return formataData($value);
    }

    public function getLastInteractionAttribute($value)
    {
        return '';
    }

    public function getLeadStatustitleDatatablesAttribute()
    {
        $model = $this->lead_status;
        $return = null;
        if (!empty($model)) {

            $color = 'background-color:'.$model->color.'; color:#000;';

            // switch ($model->id) {
            //     case 1:
            //         $color = 'background-color:#f4516c; color:#fff;';
            //         break;
            //     case 2:
            //         $color = 'background-color:#34bfa3; color:#fff;';
            //         break;
            //     default:
            //         $color = 'background-color:#5867dd; color:#fff;';
            // }
            $return = '<span class="m-badge m-badge--wide" style="' . $color . ';">' . $model->title . '</span>';
        } else {
            $return = '<span class="m-badge m-badge--wide"> -- </span>';
        }
        return $return;
    }

    public function setBirthdateAttribute($value)
    {
        if (!empty($value)) {
            $value = formataDataMysql($value);
        } else {
            $value = null;
        }
        $this->attributes['birthdate'] = $value;
    }

    public function setCountryIdAttribute($value)
    {
        if (!empty($value)) {
            $value = Country::where('initials', $value)->first()->id;
        } else {
            $value = null;
        }
        $this->attributes['country_id'] = $value;
    }

    public function setStateIdAttribute($value)
    {
        if (empty($value)) {
            $value = null;
        }
        $this->attributes['state_id'] = $value;
    }

    public function setCityIdAttribute($value)
    {
        if (empty($value)) {
            $value = null;
        }
        $this->attributes['city_id'] = $value;
    }
}
