<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;

use Modules\Site\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Nexmo\Call\Collection;

class Portfolio extends Authenticatable
{
    use SoftDeletes, Notifiable;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Nome da tabela
     *
     * @var string
     */
    protected $table = 'portfolio';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'portfolio_types',
        'types',
        'title',
        'slug',
        'sinopsys',
        'description',
        'link',
        'feature_image',
        'image_mobile',
        'active',
    ];


    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [

        'gallery',
        // 'types',
    ];

    public function gallery()
    {
        return $this->hasMany(PortfolioGallery::class, 'portfolio_id');
    }

    // public function types()
    // {
    //     return $this->hasMany(PortfolioTypes::class, 'portfolio_id');
    // }


    /**
     * Regras de validação
     * @var array
     */
    public function getRulesAttribute()
    {
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }
        if (!isset($id)) {
            $id = null;
        }

        return [
            'title' => 'required',
            'types' => 'json',

        ];
    }


    public function setValueAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['value'] = formataNumeroMysql($value);
        }
    }

    public function setFinalValueAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['final_value'] = formataNumeroMysql($value);
        }
    }

    public function messages()
    {
        return [
            'title.required' => __("validation.required", ['attribute' => __('Título')]),
        ];
    }

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'portfolio_types' => 'json',
    ];

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'feature_image' => ['titulo' => __('Imagem'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "portifolio/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'image_mobile' => ['titulo' => __('Imagem Mobile'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "portifolio/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'title' => ['titulo' => __('Título'), 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }


    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function getAcaoAttribute()
    {
        $action = [];

        if (auth('admin')->user()->canpermission('Listar galeria')) {
            $action['galeria'] = [
                'html' => '
                    <a class="dropdown-item" title="/' . __('Galeria de Fotos') . '" href="' . route('admin.anyroute', ['portfolio_gallery/index/portfolio_id']) . '/{$id}">
                        ' . __('Galeria de Fotos') . '
                    </a>
                '
            ];
        }


        if (auth('admin')->user()->canpermission('Criar/Editar Portfólio')) {
            $action['active'] = [
                'campo' => 'active',
                'validar' => 'boolean',
                'html' => '
                    <a class="dropdown-item" title="' . __('Ativar') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Ativar') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Desativar') . '"
                    >
                      <i class="fas fa-circle" aria-hidden="true"></i> <span class="text">' . __('Ativar') . '</span>
                    </a>
                ',
                'html2' => '
                    <a class="dropdown-item" title="' . __('Desativar') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Ativar') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Desativar') . '"
                    >
                      <i class="far fa-dot-circle" aria-hidden="true"></i> <span class="text">' . __('Desativar') . '</span>
                    </a>
                ',
            ];
        }

        if (auth('admin')->user()->canpermission('Criar/Editar Portfólio')) {
            $action['edit'] = [
                'html' => '
                      <a class="dropdown-item open-form-sidebar" title="' . __('Editar') . '" href="{$url}/edit/{$pk}/{$id}">
                        <i class="far fa-edit" aria-hidden="true"></i> ' . __('Editar') . '
                      </a>
                    '
            ];
        }

        if (auth('admin')->user()->canpermission('Deletar Portfólio')) {
            $action['delete'] = [
                'html' => '
                      <a class="dropdown-item" title="' . __('Remover') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                        <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remover') . '
                      </a>
                    '
            ];
        }

        if (empty($action)) {
            $action = '';
        }

        return $action;
    }

    public function getImageCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->feature_image)) {
            $retorno = $this->atributos['feature_image']['uploadpath'] . $this->feature_image;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'portifolio/' . $this->feature_image;
            }
        }

        return $retorno;
    }

    public function getImageShowApiAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = route('thumb', [150, 0, '']) . '/' . $this->image_show;
        }

        return $retorno;
    }

    public function getImageBackgroundCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->background_image)) {
            $retorno = $this->atributos['background_image']['uploadpath'] . $this->background_image;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'products/' . $this->background_image;
            }
        }

        return $retorno;
    }

    public function getImageVideoCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->video_cover)) {
            $retorno = $this->atributos['video_cover']['uploadpath'] . $this->video_cover;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'news/' . $this->video_cover;
            }
        }

        return $retorno;
    }
    public function getImageShowAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = $this->atributos['image']['uploadpath'] . $this->image;
        }

        return $retorno;
    }

    public function getFeatureImageCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->feature_image)) {
            $retorno = $this->atributos['feature_image']['uploadpath'] . $this->feature_image;
        }

        return $retorno;
    }
    public function getImageMobileCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image_mobile)) {
            $retorno = $this->atributos['image_mobile']['uploadpath'] . $this->image_mobile;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'portfolio/' . $this->image_mobile;
            }
        }

        return $retorno;
    }
}
