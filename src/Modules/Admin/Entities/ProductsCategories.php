<?php namespace Modules\Admin\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class ProductsCategories extends Model
{
    use SoftDeletes;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Nome da tabela
     *
     * @var string
     */
    protected $table = 'products_categories';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'parent_id',
        'title',
        'slug',
        'image',
        'active',
    ];

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'id' => ['titulo' => __('ID'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'image' => ['titulo' => __('Imagem de destaque'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "products_categories/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'title' => ['titulo' => __('Título'), 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }


    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'categorie',
    ];


    public function categorie()
    {
        return $this->hasMany(ProductsCategories::class, 'parent_id');
    }

    public function recursive_parent()
    {
        return $this->belongsTo(ProductsCategories::class, 'parent_id')->with(['recursive_parent', 'products']);
    }

    public function subcategories()
    {
        return $this->hasMany(ProductsCategories::class, 'parent_id')->with(['subcategories', 'products']);
    }

    /**
     * @return array
     */
    public function getAcaoAttribute($object = '')
    {

        $action = [];

//        $this->count_parents;

        if ($this->count_parents < 3) {

            $action['others'] = [
                'dynamic_vars' => [
                    'account_plan_id',
                ],
                'html' => '
                    <a class="dropdown-item" title="' . __('Sub Categorias') . '" href="{$url}/index/parent_id/{$id}">
                      ' . __('Sub Categorias') . '
                    </a>
                '
            ];
        }

        if (auth()->user()->canpermission('Activate/Deactivate categories')) {
            $action['active'] = [
                'campo' => 'active',
                'validar' => 'boolean',
                'html' => '
                    <a class="dropdown-item" title="' . __('Ativar') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Ativar') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Desativar') . '"
                    >
                      <i class="fas fa-circle" aria-hidden="true"></i> <span class="text">' . __('Ativar') . '</span>
                    </a>
                ',
                'html2' => '
                    <a class="dropdown-item" title="' . __('Desativar') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Ativar') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Desativar') . '"
                    >
                      <i class="far fa-dot-circle" aria-hidden="true"></i> <span class="text">' . __('Desativar') . '</span>
                    </a>
                ',
            ];
        }

        if (auth()->user()->canpermission('Create/Edit categories')) {
            $action['editar'] = [
                'html' => '
                    <a class="dropdown-item open-form-sidebar" title="' . __('Editar') . '" href="{$url}/edit/{$pk}/{$id}">
                      <i class="far fa-edit" aria-hidden="true"></i> ' . __('Editar') . '
                    </a>
                '
            ];
        }

        if (auth()->user()->canpermission('Delete categories')) {
            $action['delete'] = [
                'html' => '
                    <a class="dropdown-item" title="' . __('Remover') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                      <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remover') . '
                    </a>
                '
            ];
        }

        if (empty($action)) {
            $action = '';
        }

        return $action;
    }

    // Getters
    public function getImageShowAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = $this->atributos['image']['uploadpath'] . $this->image;
        }

        return $retorno;
    }

    public function getImageShowApiAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = route('thumb', [150, 0, '']) . '/' . $this->image_show;
        }

        return $retorno;
    }

    /**
     * Regras de validação
     * @var array
     */
    public function getRulesAttribute()
    {
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }
        if (!isset($id)) {
            $id = null;
        }

        return [
            'code' => 'required',
            'value' => 'required',
            'active' => 'boolean',
        ];
    }

    public function messages()
    {
        return [
            'code.required' => __('validation.required', ['attribute' => __('Código')]),
            'value.required' => __('validation.required', ['attribute' => __('Valor')]),
            'active.boolean' => __('validation.boolean', ['attribute' => __('Activo')]),
        ];
    }

    public function getCodeDatatablesAttribute()
    {
        return str_pad($this->code, 2, '0', STR_PAD_LEFT);
    }

    public function getParentCodeAttribute($data = null, $object = '')
    {
        $result = [];
        if (!empty($data) && !is_null($object)) {
            $item[] = str_pad($object->id, 2, '0', STR_PAD_LEFT);
            $result = array_merge($item, $data);
        } else {
            $result[] = str_pad($this->id, 2, '0', STR_PAD_LEFT);
        }

        if ($object == '') {
            $object = $this;
        }

        if (!is_null($object->recursive_parent) && $object->recursive_parent->count() > 0) {
            return $this->getParentCodeAttribute($result, $object->recursive_parent);
        }

        return implode($result, '.');
    }

}
