<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Request;

class PortfolioTypes extends Model
{
    use SoftDeletes;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'portfolio_types';

    protected static function boot()
    {
        parent::boot();
    }

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'active',
    ];

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'active' => 'boolean',
    ];
    //
    // /**
    //  * Seta relacionamentos
    //  */
    // public $relacionamentos = [
    //     'news',
    // ];
    //
    // public function news()
    // {
    //     return $this->hasMany(News::class, 'news_categorie_id', 'id');
    // }
    //
    // /**
    //  * Algumas definições
    //  * @var [type]
    //  */\
    // }

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'id' => ['titulo' => 'ID', 'listar' => true, 'adicionar' => true, 'editar' => true],
            'title' => ['titulo' => 'Título', 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }

    public $rules = [
        'title' => 'required',
        'active' => 'boolean',
    ];

    public function messages()
    {
        return [
            'title.required' => __('validation.required', ['attribute' => 'Title']),
            'active.boolean' => __('validation.boolean', ['attribute' => 'Active']),
        ];
    }


    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'update_at',
    ];

    public function scopeAtivoelinguagem($query, $language_id)
    {
        return $query->where('active', true)->where('language_id', $language_id);
    }


    // Getters
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    // Setters
    // public function setCreatedAtAttribute($value){
    // 	return formataDataHoraMysql($value);
    // }
    // public function setUpdatedAtAttribute($value){
    // 	return formataDataHoraMysql($value);
    // }
}
