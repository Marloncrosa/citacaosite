<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model as MainModel;
use OwenIt\Auditing\Auditable;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;
use Illuminate\Validation\Rule;

/**
 * Class
 *
 * ** mix them in as instance methods
 *
 * @mixin \Illuminate\Database\Eloquent\Builder
 * @mixin \Illuminate\Database\Query\Builder
 *
 *   ** mix them in as static method calls
 * @mixin static \Illuminate\Database\Eloquent\Builder
 * @mixin static \Illuminate\Database\Query\Builder
 */
class Model extends MainModel implements AuditableContract
{
    use Auditable;

}
