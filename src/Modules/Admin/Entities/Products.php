<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Products extends Authenticatable
{
    use SoftDeletes, Notifiable;

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'categories',
        'galeria',
    ];
    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * Nome da tabela
     *
     * @var string
     */
    protected $table = 'products';
    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'product_categories',
        'product_aditionals',
        'product_lines',
        'product_collections',
        // 'brand_id',

        'title',
        'slug',
        'code',
        'sinopsys',
        'description',
        'amount',
        'image',
        'image_mobile',
        'value',

        'active',
    ];

    // public function cart()
    // {
    //     return $this->belongsTo(Cart::class, 'products_id');
    // }

    // public function brand()
    // {
    //     return $this->belongsTo(Brands::class, 'brand_id');
    // }

    /* public function brand()
     {
         return $this->belongsTo(Brands::class, 'brand_id');
     }*/

    // public function line_categories()
    // {
    //     return $this->belongsTo(lines::class, 'lines_id');
    // }
    //
    // public function coll_prod()
    // {
    //     return $this->belongsTo(Collections::class, 'collection_id');
    // }
    //
    // public function shippings()
    // {
    //     return $this->hasMany(ProductsShippings::class, 'product_id');
    // }

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'active' => 'boolean',
        'featured' => 'boolean',
        'sale_off' => 'boolean',
        'additional' => 'json',
        'color_id' => 'json',
        'product_categories' => 'json',
        'product_aditionals' => 'json',
        'product_lines' => 'json',
        'product_collections' => 'json',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
    ];

    // public function color()
    // {
    //     return $this->hasMany(Colors::class, 'product_id');
    // }
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    public function galeria()
    {
        return $this->hasMany(ProductGallery::class, 'product_id', 'id');
    }

    public function categories()
    {
        return $this->belongsTo(ProductsCategories::class, 'product_categories_id');
    }

    /**
     * Regras de validação
     * @var array
     */
    public function getRulesAttribute()
    {
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }
        if (!isset($id)) {
            $id = null;
        }

        return [
            'title' => 'required',
            'slug' => "required|unique:products,slug,$id,id",
            'code' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => __("validation.required", ['attribute' => __('Título')]),
            'slug.required' => __('validation.required', ['attribute' => 'Url Amigável']),
            'code.required' => __("validation.required", ['attribute' => __('Código')]),

            'slug.unique' => __('validation.unique', ['attribute' => 'Url Amigável']),
        ];
    }

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'image' => ['titulo' => __('Imagem 1'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "products/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'image_mobile' => ['titulo' => __('Imagem Mobile'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "products/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'title' => ['titulo' => __('Título'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'id' => ['titulo' => __('ID'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'code' => ['titulo' => __('Código'), 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }

    public function getAcaoAttribute()
    {

        // if (auth('admin')->user()->canpermission('Criar/Editar produtos')) {
        //     $action['active'] = [
        //         'campo' => 'active',
        //         'validar' => 'boolean',
        //         'html' => '
        //             <a class="dropdown-item" title="' . __('Ativar') . '" href="javascript:void(0);"
        //                 data-alterarbool
        //                 data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
        //                 data-icone1="fas fa-circle"
        //                 data-texto1="' . __('Ativar') . '"
        //                 data-icone2="far fa-dot-circle"
        //                 data-texto2="' . __('Desativar') . '"
        //             >
        //               <i class="fas fa-circle" aria-hidden="true"></i> <span class="text">' . __('Ativar') . '</span>
        //             </a>
        //         ',
        //         'html2' => '
        //             <a class="dropdown-item" title="' . __('Desativar') . '" href="javascript:void(0);"
        //                 data-alterarbool
        //                 data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
        //                 data-icone1="fas fa-circle"
        //                 data-texto1="' . __('Ativar') . '"
        //                 data-icone2="far fa-dot-circle"
        //                 data-texto2="' . __('Desativar') . '"
        //             >
        //               <i class="far fa-dot-circle" aria-hidden="true"></i> <span class="text">' . __('Desativar') . '</span>
        //             </a>
        //         ',
        //     ];
        // }

        if (auth('admin')->user()->canpermission('Criar/Editar produtos')) {
            $action['edit'] = [
                'html' => '
                      <a class="dropdown-item open-form-sidebar" title="' . __('Editar') . '" href="{$url}/edit/{$pk}/{$id}">
                        <i class="far fa-edit" aria-hidden="true"></i> ' . __('Editar') . '
                      </a>
                    '
            ];
        }

        if (auth('admin')->user()->canpermission('Deletar produtos')) {
            $action['delete'] = [
                'html' => '
                      <a class="dropdown-item" title="' . __('Remover') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                        <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remover') . '
                      </a>
                    '
            ];
        }

        if (empty($action)) {
            $action = '';
        }

        return $action;
    }

    public function getImageCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = $this->atributos['image']['uploadpath'] . $this->image;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'products/' . $this->image;
            }
        }

        return $retorno;
    }

    public function getImageBackgroundCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->background_image)) {
            $retorno = $this->atributos['background_image']['uploadpath'] . $this->background_image;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'products/' . $this->background_image;
            }
        }

        return $retorno;
    }

    public function getBackogrundCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->background_image)) {
            $retorno = $this->atributos['background_image']['uploadpath'] . $this->background_image;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'products/' . $this->background_image;
            }
        }

        return $retorno;
    }

    public function getImageMobileCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image_mobile)) {
            $retorno = $this->atributos['image_mobile']['uploadpath'] . $this->image_mobile;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'products/' . $this->image_mobile;
            }
        }

        return $retorno;
    }

    public function getValueAttribute($value)
    {
        return formataNumero($value);
    }

    public function getFinalValueAttribute($value)
    {
        return formataNumero($value);
    }

    public function getFormatValueAttribute($value)
    {
        return formataNumero($value);
    }


    public function getFormatFinalValueAttribute($value)
    {
        return formataNumero($value);
    }

    public function setSlugAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['slug'] = strtolower($value);
        } else {
            $this->attributes['slug'] = strtolower(str_slug($this->title));
        }
    }

    public function setValueAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['value'] = formataNumeroMysql($value);
        }
    }

    public function setFinalValueAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['final_value'] = formataNumeroMysql($value);
        }
    }

}
