<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Validation\Rule;

class Permissions extends Model
{
    use SoftDeletes;

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
    ];

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'groups',
    ];

    // Trazer os grupos que possuem esas permission
    public function groups()
    {
        return $this->belongsToMany(
            PermissionsGroup::class,
            'permissions_permissionsgroup',
            'permissions_id',
            'permissions_group_id'
        )->withTimestamps();
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'pivot',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Regras de validação
     * @var array
     */
    public function getRulesAttribute()
    {
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }
        if (!isset($id)) {
            $id = null;
        }

        return [
            'name' => [
                'regra' => [
                    'required',
                    Rule::unique('permissions', 'name')->ignore($id)
                ]
            ],
            'slug' => [
                'regra' => [
                    'required',
                    Rule::unique('permissions', 'slug')->ignore($id)
                ]
            ]
        ];
    }

    /**
     * Mensagens de validação
     * @return array
     */
    public function messages()
    {
        return [
            'user_id.required' => __("validation.required", ['attribute' => __('User')]),
            'permissions_group_id.required' => __("validation.required", ['attribute' => __('Group')]),

            'user_id.exists' => __("validation.exists", ['attribute' => __('User')]),
            'permissions_group_id.exists' => __("validation.exists", ['attribute' => __('Group')]),

            'user_id.unique' => __("validation.unique", ['attribute' => __('User')]),
            'permissions_group_id.unique' => __("validation.unique", ['attribute' => __('Group')]),
        ];
    }

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'name' => ['titulo' => __('Name'), 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }

    public function getAcaoAttribute()
    {
        if (auth('admin')->user()->webmaster) {
            return [
                'editar' => [
                    'html' => '
                  <a class="dropdown-item open-form-sidebar" title="' . __('Editar') . '" href="{$url}/edit/{$pk}/{$id}">
                    <i class="far fa-edit" aria-hidden="true"></i> ' . __('Editar') . '
                  </a>
                '
                ],

                'delete' => [
                    'html' => '
                  <a class="dropdown-item" title="' . __('Remover') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                    <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remover') . '
                  </a>
                '
                ],
            ];
        }else{
            return '';
        }
    }

    // Setters
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = !is_null($value) ? strtolower($value) : null;
    }
}
