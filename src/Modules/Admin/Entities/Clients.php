<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;

use Modules\Site\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Clients extends Authenticatable
{
    use SoftDeletes, Notifiable;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Nome da tabela
     *
     * @var string
     */
    protected $table = 'clients';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'state_id',
        'city_id',

        'facebook_id',

        'name',
        'email',

        'phone',
        'cellphone',

        'gender',
        'document',
        'password',

        'avatar',
        'image_mobile',

        'birthdate',

        'postal_code',
        'address',
        'number',
        'address_complement',
        'neighborhood',
        'value',

        'external_link',
        'active',
    ];

    /**
     * Regras de validação
     * @var array
     */
    public function getRulesAttribute()
    {
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }
        if (!isset($id)) {
            $id = null;
        }

        return [
            'name' => 'required',
            // 'document' => 'required',
            // 'birthday' => 'required',
            // 'gender' => 'required',
            // 'email' => 'required',
            'cellphone' => 'required',
            // 'types_address_id' => 'required',
            // 'cep' => 'required',
            // 'endereco' => 'required',
            // 'numero' => 'required',
            // 'bairro' => 'required',
            // 'estado' => 'required',

        ];
    }

    public function messages()
    {
        return [
            'name.required' => __("validation.required", ['attribute' => __('Nome')]),
            'cellphone.required' => __("validation.required", ['attribute' => __('Celular')]),
        ];
    }

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'active' => 'boolean',
        'external_link' => 'boolean',
    ];

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'city',
        'state',
        'country',
    ];


    public function city()
    {
        return $this->belongsTo(City::class, 'city_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }


    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'avatar' => ['titulo' => __('Avatar'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "clients/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'image_mobile' => ['titulo' => __('Imagem_Mobile'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "clients/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'name' => ['titulo' => __('Nome'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'document' => ['titulo' => __('Documento'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'email' => ['titulo' => __('E-mail'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'cellphone' => ['titulo' => __('Celular'), 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }


    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        //        'password',
        'created_at',
        'update_at',
        'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    // Getters
    public function getAvatarShowAttribute()
    {
        $retorno = null;
        if (!empty($this->avatar)) {
            $retorno = $this->atributos['avatar']['uploadpath'] . $this->avatar;
        }

        return $retorno;
    }
    public function getImageMobileCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image_mobile)) {
            $retorno = $this->atributos['image_mobile']['uploadpath'] . $this->image_mobile;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'clients/' . $this->background_image;
            }
        }

        return $retorno;
    }

    public function getBirthdateAttribute($value)
    {
        return formataData($value);
    }

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        if (!is_null($value)) {
            $data = bcrypt($value);
            $this->attributes['password'] = $data;
        }
    }

    /**
     * @param $value
     */
    public function setSenhaAttribute($value)
    {
        if (!is_null($value)) {
            $data = bcrypt($value);
            $this->attributes['password'] = $data;
        }
    }

    public function setPasswordClearAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['password'] = $value;
        }
    }

    /**
     * Send the password reset notification.
     *
     * @param string $token
     *
     * @return void
     */
    public function sendPasswordResetNotificationNew($token, $user)
    {
        $notification = new ResetPassword($token, $user);

        // Teste - descomentar até o dd
        // $novo_markdown = [
        //     'mail.markdown.theme' => 'padrao',
        //     'mail.markdown.paths' => [
        //         resource_path('../Modules/Site/Resources/views/mail'),
        //     ],
        // ];
        // $markdown = new \Illuminate\Mail\Markdown(view(), $novo_markdown);
        // $data = $notification->teste()->toArray();
        // // dump( $data );
        // echo $markdown->render('site::notifications.email', $data);
        // die;

        config(
            [
                'mail.markdown.theme' => 'padrao',
                'mail.markdown.paths' => [
                    resource_path('../Modules/Site/Resources/views/mail'),
                ],
            ]
        );

        //        dd($notification);

        $this->notify($notification);
    }

    /**
     * Get the user for the given credentials.
     *
     * @param array $credentials
     *
     * @return \Illuminate\Contracts\Auth\CanResetPassword
     *
     * @throws \UnexpectedValueException
     */
    public function getUser(array $credentials)
    {
        $credentials = Arr::except($credentials, ['token']);

        $user = static::where('email', $credentials['email'])->first();

        if ($user && !$user instanceof CanResetPasswordContract) {
            throw new \UnexpectedValueException('User must implement CanResetPassword interface.');
        }

        return $user;
    }

    public function setCountryIdAttribute($value)
    {
        if (!empty($value)) {
            $value = Country::where('initials', $value)->first()->id;
        } else {
            $value = null;
        }
        $this->attributes['country_id'] = $value;
    }

    public function setStateIdAttribute($value)
    {
        if (empty($value)) {
            $value = null;
        }
        $this->attributes['state_id'] = $value;
    }

    public function setCityIdAttribute($value)
    {
        if (empty($value)) {
            $value = null;
        }
        $this->attributes['city_id'] = $value;
    }

    public function setBirthdateAttribute($value)
    {
        if (!empty($value)) {
            $value = formataDataMysql($value);
        } else {
            $value = null;
        }
        $this->attributes['birthdate'] = $value;
    }

    // protected static function boot()
    // {
    //     parent::boot();
    // }

    public function getAcaoAttribute()
    {
        $action = [];

        if (auth('admin')->user()->canpermission('Endereços Clientes')) {
            $action['pedidos'] = [
                'html' => '
                    <a class="dropdown-item" title="/' . __('Pedidos') . '" href="' . route('admin.anyroute', ['client_cart/index/client_id']) . '/{$id}">
                        ' . __('Pedidos') . '
                    </a>
                '
            ];
        }

        if (auth('admin')->user()->canpermission('Endereços Clientes')) {
            $action['enderecos'] = [
                'html' => '
                    <a class="dropdown-item" title="/' . __('Endereços') . '" href="' . route('admin.anyroute', ['clients_address/index/client_id']) . '/{$id}">
                        ' . __('Endereços') . '
                    </a>
                '
            ];
        }

        if (auth('admin')->user()->canpermission('Endereços Clientes')) {
            $action['pagamentos'] = [
                'html' => '
                    <a class="dropdown-item" title="/' . __('Pagamentos') . '" href="' . route('admin.anyroute', ['client_payments/index/client_id']) . '/{$id}">
                        ' . __('Pagamentos') . '
                    </a>
                '
            ];
        }

        if (auth('admin')->user()->canpermission('Criar/Editar clientes')) {
            $action['active'] = [
                'campo' => 'active',
                'validar' => 'boolean',
                'html' => '
                    <a class="dropdown-item" title="' . __('Ativar') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Ativar') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Desativar') . '"
                    >
                      <i class="fas fa-circle" aria-hidden="true"></i> <span class="text">' . __('Ativar') . '</span>
                    </a>
                ',
                'html2' => '
                    <a class="dropdown-item" title="' . __('Desativar') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Ativar') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Desativar') . '"
                    >
                      <i class="far fa-dot-circle" aria-hidden="true"></i> <span class="text">' . __('Desativar') . '</span>
                    </a>
                ',
            ];
        }

        if (auth('admin')->user()->canpermission('Criar/Editar clientes')) {
            $action['edit'] = [
                'html' => '
                      <a class="dropdown-item open-form-sidebar" title="' . __('Editar') . '" href="{$url}/edit/{$pk}/{$id}">
                        <i class="far fa-edit" aria-hidden="true"></i> ' . __('Editar') . '
                      </a>
                    '
            ];
        }

        if (auth('admin')->user()->canpermission('Deletar cliente')) {
            $action['delete'] = [
                'html' => '
                      <a class="dropdown-item" title="' . __('Remover') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                        <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remover') . '
                      </a>
                    '
            ];
        }

        if (empty($action)) {
            $action = '';
        }

        return $action;
    }
}
