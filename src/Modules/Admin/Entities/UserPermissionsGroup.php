<?php

namespace Modules\Admin\Entities;

use Illuminate\Validation\Rule;

use Illuminate\Database\Eloquent\Model;

class UserPermissionsGroup extends Model
{
    protected $primaryKey = null;

    public $incrementing = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_permissions_group';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'permissions_group_id',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
    ];


    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'users',
        'groups',
    ];
    public function users()
    {
        return $this->belongsTo(Users::class, 'user_id');
    }

    public function groups()
    {
        return $this->belongsTo(PermissionsGroup::class, 'permissions_group_id');
    }

    /**
     * Regras de validação
     * @var array
     */
    public function getRulesAttribute()
    {
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }
        if (!isset($id)) {
            $id = null;
        }

        return [
            'user_id' => [
                'regra' => [
                    'required',
                    'exists:users,id',
                    Rule::unique('user_permissions_group', 'user_id')
                        ->where('permissions_group_id', request()->permissions_group_id)
                        ->ignore($id)
                ]
            ],
            'permissions_group_id' => [
                'regra' => [
                    'required',
                    'exists:permissions_group,id',
                    Rule::unique('user_permissions_group', 'permissions_group_id')
                        ->where('user_id', request()->user_id)
                        ->ignore($id)
                ]
            ]
        ];
    }

    /**
     * Mensagens de validação
     * @return array
     */
    public function messages()
    {
        return [
            'user_id.required' => __("validation.required", ['attribute' => __('Usuário')]),
            'permissions_group_id.required' => __("validation.required", ['attribute' => __('Grupo')]),

            'user_id.exists' => __("validation.exists", ['attribute' => __('Usuário')]),
            'permissions_group_id.exists' => __("validation.exists", ['attribute' => __('Grupo')]),

            'user_id.unique' => __("validation.unique", ['attribute' => __('Usuário')]),
            'permissions_group_id.unique' => __("validation.unique", ['attribute' => __('Grupo')]),
        ];
    }

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'permissions_group_id' => ['titulo' => __('ID'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'groups.name' => ['titulo' => __('Grupo'), 'listar' => true, 'adicionar' => false, 'editar' => false],
        ];
    }

    public function getAcaoAttribute()
    {
        $action = [];

        if (auth('admin')->user()->canpermission('Deletar grupo de permissão')) {
            $action['delete'] = [
                'dynamic_vars' => [
                    'user_id',
                ],
                'html' => '
                      <a class="dropdown-item" title="' . __('Remove') . '" data-excluir data-ajax="{$url}/deleteajax/user_id/{$user_id}/permissions_group_id/'.$this->permissions_group_id.'">
                        <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remove') . '
                      </a>
                    '
            ];
        }

        if(empty($action)){
            $action = '';
        }

        return $action;
    }
}
