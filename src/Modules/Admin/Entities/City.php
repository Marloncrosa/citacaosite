<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Modules\Admin\Entities\City
 *
 * @property int $id
 * @property int $idestado
 * @property string $cidade
 * @property bool $active
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property-read \Modules\Admin\Entities\State $estados
 * @property-read \Illuminate\Database\Eloquent\Collection|\Modules\Admin\Entities\Representantes[] $representantes
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Admin\Entities\City onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\City whereAtivo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\City whereCidade($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\City whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\City whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\City whereIdestado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\City whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Modules\Admin\Entities\City withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\Modules\Admin\Entities\City withoutTrashed()
 * @mixin \Eloquent
 */
class City extends Model
{
    use SoftDeletes;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'cities';

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'active' => 'boolean'
    ];

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'state_id',
        'city',
        'viacao_garcia_value',
        'active',

    ];

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'city' => ['titulo' => 'Cidade', 'listar' => true, 'adicionar' => true, 'editar' => true],
            'viacao_garcia_value' => ['titulo' => 'Frete Viação Garcia (R$)', 'listar' => true, 'adicionar' => true, 'editar' => true],
            'active' => ['titulo' => 'Ativo', 'listar' => false, 'adicionar' => true, 'editar' => true],
        ];
    }

    /**
     * Regtras de validação
     * @var array
     */
    public $rules = [
        'state_id' => 'required|integer|exists:state,id',
        'city' => 'required',
        'active' => 'required|boolean',
    ];

    public function messages()
    {
        return [
            'state_id.required' => __("validation.required", ['attribute' => __('State')]),
            'state_id.integer' => __("validation.integer", ['attribute' => __('State')]),
            'state_id.exists' => __("validation.exists", ['attribute' => __('State')]),

            'city.required' => __("validation.required", ['attribute' => __('City')]),

            'active.required' => __("validation.required", ['attribute' => __('Active')]),
            'active.boolean' => __("validation.boolean", ['attribute' => __('Active')]),
        ];
    }


    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'state',
    ];

    /**
     * Relação de state
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state()
    {
        return $this->belongsTo(State::class, 'state_id');
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    // Getters

    /**
     * @param $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    /**
     * @param $value
     * @return string
     */
    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    /**
     * @param $value
     * @return string
     */
    public function getDeletedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    // Setters
    // public function setCreatedAtAttribute($value){
    //  return formataDataHoraMysql($value);
    // }
    // public function setUpdatedAtAttribute($value){
    //  return formataDataHoraMysql($value);
    // }


    public function getAcaoAttribute()
    {
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }

        return [

            'active' => [
                'campo' => 'active',
                'validar' => 'boolean',
                'dynamic_vars' => [
                    'country_id',
                    'state_id',
                ],
                'html' => '
					<a class="dropdown-item" title="'. __('Ativar') .'" href="javascript:void(0);"
						data-alterarbool
						data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}/country_id/'.$country_id.'/state_id/'.$state_id.'"
						data-icone1="fas fa-circle"
                        data-texto1="'. __('Ativar') .'"
                        data-icone2="far fa-dot-circle"
                        data-texto2="'. __('Desativar') .'"
					>
						<i class="fas fa-circle" aria-hidden="true"></i> <span class="text">'. __('Ativar') .'</span>
					</a>
				',
                'html2' => '
					<a class="dropdown-item" title="'. __('Desativar') .'" href="javascript:void(0);"
						data-alterarbool
						data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}/country_id/'.$country_id.'/state_id/'.$state_id.'"
						data-icone1="fas fa-circle"
                        data-texto1="'. __('Ativar') .'"
                        data-icone2="far fa-dot-circle"
                        data-texto2="'. __('Desativar') .'"
					>
						<i class="far fa-dot-circle" aria-hidden="true"></i> <span class="text">'. __('Desativar') .'</span>
					</a>
				',
            ],

            'editar' => [
                'dynamic_vars' => [
                    'country_id',
                    'state_id',
                ],
                'html' => '
					<a class="dropdown-item open-form-sidebar" title="'. __('Editar') .'" href="{$url}/edit/{$pk}/{$id}/country_id/'.$country_id.'/state_id/'.$state_id.'">
					  <i class="far fa-edit" aria-hidden="true"></i> '. __('Editar') .' 
					</a>
				'
            ],

            'delete' => [
                'dynamic_vars' => [
                    'country_id',
                    'state_id',
                ],
                'html' => '
					<a class="dropdown-item" title="'. __('Remover') .'" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}/country_id/'.$country_id.'/state_id/'.$state_id.'">
						<i class="fas fa-times" aria-hidden="true"></i> '. __('Remover') .'
					</a>
				'
            ],
        ];
    }
}
