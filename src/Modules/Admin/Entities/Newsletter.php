<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Modules\Admin\Entities\Leads
 *
 * @property int $id
 * @property string|null $nome
 * @property string $email
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Leads whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Leads whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Leads whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Leads whereNome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Leads whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Newsletter extends Model
{
    use SoftDeletes;

    /**
     * Seta relacionamentos
     */
//    public $relacionamentos = [
//    ];

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'newsletter';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'newsletter_subject_id',
        'newsletter_interest_id',

        'name',
        'email',
    ];

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'newsletter_interest',
//        'newsletter_subject',
    ];

//    public function newsletter_subject()
//    {
//        return $this->belongsTo(Newsletter::class, 'newsletter_subject_id');
//    }

    public function newsletter_interest()
    {
        return $this->belongsTo(NewsletterInterests::class, 'newsletter_interest_id');
    }


    /**
     * Alterar coluna de ações
     */
    public function getAcaoAttribute()
    {
        return [
            'delete' => [
                'html' => '
                  <a class="dropdown-item" title="' . __('Remove') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                    <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remove') . '
                  </a>
                '
            ],
        ];
    }

    /**
     * Regras de validação
     * @var array
     */
    public function getRulesAttribute()
    {
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }
        if (!isset($id)) {
            $id = null;
        }

        return [
            'email' => 'required|unique:newsletter,email',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => __('validation.required', ['attribute' => __("E-mail")]),
            'email.unique' => __('validation.unique', ['attribute' => __("E-mail")]),
        ];
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    /*protected $hidden = [
        'created_at',
        'update_at',
    ];*/

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'created_at' => ['titulo' => __('Data'), 'listar' => true, 'adicionar' => false, 'editar' => false],
            'name' => ['titulo' => __('Nome'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'email' => ['titulo' => __('E-mail'), 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }

    // Getters

    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getLastInteractionAttribute($value)
    {
        return '';
    }
}
