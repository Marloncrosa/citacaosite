<?php namespace Modules\Admin\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Modules\Admin\Entities\Contato
 *
 * @property int $id
 * @property string|null $nome
 * @property string $email
 * @property string|null $telefone
 * @property string $mensagem
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Modules\Admin\Entities\City $cidades
 * @property-read \Modules\Admin\Entities\State $estados
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereMensagem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereNome($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereTelefone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Contact whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ServiceCharacteristics extends Model
{

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'service_characteristics';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'active',
    ];

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'service'
    ];

    public function service()
    {
        return $this->hasMany(Service::class, 'service_characteristics_id');
    }

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'id' => ['titulo' => 'ID', 'listar' => true, 'adicionar' => false, 'editar' => false],
            'title' => ['titulo' => 'Nome', 'listar' => true, 'adicionar' => true, 'editar' => true, 'validacao' => 'required'],
        ];
    }


    /**
     * Alterar coluna de ações
     */
    public function getAcaoAttribute()
    {
        return [
            'show' => [
                'html' => '
					<a class="dropdown-item open-form-sidebar" title="' . __('Editar') . '" href="{$url}/edit/{$pk}/{$id}">
					  <i class="far fa-edit" aria-hidden="true"></i> ' . __('Editar') . '
					</a>
				'
            ],

            'delete' => [
                'html' => '
					<a class="dropdown-item" title="' . __('Remove') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
						<i class="fas fa-times" aria-hidden="true"></i> ' . __('Remove') . '
					</a>
				'
            ],
        ];
    }

       /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    // Getters
    //    public function dataenvio()
    //    {
    //        $data = Carbon::parse($this->attributes['created_at']);
    //        return $data->format('d/m/Y');
    //    }
    //
    public function getCreatedAtAttribute($value)
    {
        $create_at = Carbon::parse($value);
        //        return $create_at->format('d/m/Y H:i');
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getDatetimeStartAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getDatetimeEndAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getValueAttribute($value)
    {
        return formataNumero($value);
    }

    public function getDatetimeStartCalendarAttribute()
    {
        if (empty($this->datetime_start)) {
            return null;
        }
        return Carbon::createFromFormat('d/m/Y H:i:s', $this->datetime_start)->format('Y-m-d H:i:s');
    }

    public function getDatetimeEndCalendarAttribute($value)
    {
        if (empty($this->datetime_end)) {
            return null;
        }
        return Carbon::createFromFormat('d/m/Y H:i:s', $this->datetime_end)->format('Y-m-d H:i:s');
    }

    // Setters
    // public function setCreatedAtAttribute($value){
    // 	return formataDataHoraMysql($value);
    // }
    // public function setUpdatedAtAttribute($value){
    // 	return formataDataHoraMysql($value);
    // }

    public function setValueAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['value'] = formataNumeroMysql($value);
        }
    }

    public function setDatetimeStartAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['datetime_start'] = formataDataHoraMysql($value);
        }
    }

    public function setDatetimeEndAttribute($value)
    {
        if (!is_null($value)) {
            $this->attributes['datetime_end'] = formataDataHoraMysql($value);
        }
    }
}
