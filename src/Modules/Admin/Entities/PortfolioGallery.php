<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Request;

class PortfolioGallery extends Model
{
//    use SoftDeletes;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'portfolio_gallery';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'portfolio_id',
        'title',
        'image',
        'image_mobile',
        'legend',
        'active',
    ];

    /**
     * Algumas definições
     * @var [type]
     */

    public function getAtributosAttribute()
    {
        return [
            'image' => ['titulo' => __('Imagem'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'multiploupload' => true, 'uploadpath' => "product_gallery/{$this->product_id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'image_mobile' => ['titulo' => __('Imagem Mobile'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'multiploupload' => true, 'uploadpath' => "product_gallery/{$this->product_id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'title' => ['titulo' => __('Título'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'active' => ['titulo' => __('Ativo'), 'listar' => false, 'adicionar' => true, 'editar' => true],
        ];
    }

    public $rules = [
        'title' => 'required',
        // 'image' => 'required|image',
        'active' => 'boolean',
    ];

    public function messages()
    {
        return [
            'title.required' => __("validation.required", ['attribute' => 'Title']),
            'image.required' => __("validation.required", ['attribute' => 'Image']),
            'image.image' => __("validation.image", ['attribute' => 'Image']),
            'active.boolean' => __('validation.boolean', ['attribute' => 'Active']),
        ];
    }

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'portfolio'
    ];

    public function portfolio()
    {
        return $this->belongsTo(Portfolio::class, 'portfolio_id', 'id');
    }


    /**
     * Alterar coluna de ações
     */
    public function getAcaoAttribute()
    {
        $action = [];


        if (auth('admin')->user()->canpermission('Ativar/Desativar portfólio')) {
            $action['active'] = [
                'campo' => 'active',
                'validar' => 'boolean',
                'html' => '
                    <a class="dropdown-item" title="' . __('Ativar') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Ativar') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Desativar') . '"
                    >
                      <i class="fas fa-circle" aria-hidden="true"></i> <span class="text">' . __('Ativar') . '</span>
                    </a>
                ',
                'html2' => '
                    <a class="dropdown-item" title="' . __('Desativar') . '" href="javascript:void(0);"
                        data-alterarbool
                        data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                        data-icone1="fas fa-circle"
                        data-texto1="' . __('Ativar') . '"
                        data-icone2="far fa-dot-circle"
                        data-texto2="' . __('Desativar') . '"
                    >
                      <i class="far fa-dot-circle" aria-hidden="true"></i> <span class="text">' . __('Desativar') . '</span>
                    </a>
                ',
            ];
        }

        if (auth('admin')->user()->canpermission('Criar/Editar produtos')) {
            $action['editar'] = [
                'dynamic_vars' => [
                    'product_id',
                ],
                'html' => '
                    <a class="dropdown-item open-form-sidebar" title="' . __('Editar') . '" href="{$url}/edit/{$pk}/{$id}/portfolioid/{$portfolioid}">
                      <i class="far fa-edit" aria-hidden="true"></i> ' . __('Editar') . '
                    </a>
                '
            ];
        }

        if (auth('admin')->user()->canpermission('Deletar produtos')) {
            $action['delete'] = [
                'html' => '
                      <a class="dropdown-item" title="' . __('Remover') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                        <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remover') . '
                      </a>
                    '
            ];
        }

        if (empty($action)) {
            $action = '';
        }

        return $action;
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    // Getters
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    // Setters
    // public function setCreatedAtAttribute($value){
    // 	return formataDataHoraMysql($value);
    // }
    // public function setUpdatedAtAttribute($value){
    // 	return formataDataHoraMysql($value);
    // }

    public function getImageShowApiAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = route('thumb', [150, 0, '']) . '/' . $this->image_show;
        }

        return $retorno;
    }

    public function getImageCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = $this->atributos['image']['uploadpath'] . $this->image;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'portfolio/' . $this->image;
            }
        }

        return $retorno;
    }
    public function getImageMobileCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image_mobile)) {
            $retorno = $this->atributos['image_mobile']['uploadpath'] . $this->image_mobile;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'portfolio_gallery/' . $this->image_mobile;
            }
        }

        return $retorno;
    }
}
