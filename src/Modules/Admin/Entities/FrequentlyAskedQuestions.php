<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;

class FrequentlyAskedQuestions extends Model
{
    use SoftDeletes;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Nome da tabela
     *
     * @var string
     */
    protected $table = 'frequently_asked_questions';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'slug',
        'title',
        'description',
        'likes',
        'active',
    ];

    /**
     * Regras de Validação
     *
     * @var array
     */
//    public $rules = [
//        'name' => 'required',
//        'idestado' => 'required|integer|exists:estados,id',
//        'idcidade' => 'required|integer|exists:cidades,id',
//
//        'email' => 'required',
//        'cpf' => 'required',
//        'telefone' => 'required',
//        'cep' => 'required',
//        'endereco' => 'required',
//        'numero' => 'required',
//        'bairro' => 'required',
//        'ativo' => 'required'
//    ];

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'active' => 'boolean',
    ];

//    /**
//     * Seta relacionamentos
//     */
//    public $relacionamentos = [
//        'course',
//    ];

       /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'title' => ['titulo' => __('Titulo'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'description' => ['titulo' => __('Descrição'), 'listar' => true, 'adicionar' => true, 'editar' => true],

        ];
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
        'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    // Getters
    public function getPriceFormatAttribute()
    {
        $value = $this->price;
        return (!is_null($value) && $value > 0) ? 'R$ ' . formataNumero($value) : null;
    }

    public function getDiscountFormatAttribute()
    {
        $value = (!fmod($this->discount, 1)) ? (int)$this->discount : formataNumero($this->discount);
        return (!is_null($this->discount) && $this->discount > 0) ? $value : null;
    }

    public function getMaxInstallmentsPriceAttribute()
    {
        $value = $this->price / $this->max_installments;
        return (!is_null($value) && $value > 0) ? 'R$ ' . formataNumero($value) : null;
    }

    /**
     * @param $value
     */
    public function getPasswordAttribute()
    {
        return $this->senha;
    }

//    public function getIdestadoDatatablesAttribute()
//    {
//        return (!empty($this->idestado))? $this->estados->estado : '';
//    }
//
//    public function getIdcidadeDatatablesAttribute()
//    {
//        return (!empty($this->idcidade))? $this->cidades->cidade: '';
//    }

    public function getImageShowAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = $this->atributos['image']['uploadpath'] . $this->image;
        }

        return $retorno;
    }

    public function getLogoShowAttribute()
    {
        $retorno = null;
        if (!empty($this->logo)) {
            $retorno = $this->atributos['logo']['uploadpath'] . $this->logo;
        }

        return $retorno;
    }

    public function scopeAtivoelinguagem($query, $language_id)
    {
        return $query->where('active', true)->where('language_id', $language_id);
    }

}
