<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Modules\Admin\Entities\State
 *
 * @property int $id
 * @property string $uf
 * @property string $estado
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\State whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\State whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\State whereEstado($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\State whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\State whereUf($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\State whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class State extends Model
{
    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'states';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'country_id',
        'initials',
        'state',
    ];

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'country_id' => ['titulo' => __('país'), 'listar' => false, 'adicionar' => true, 'editar' => true],
            'initials' => ['titulo' => __('Iniciais'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'state' => ['titulo' => __('Estado'), 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }

    public $rules = [
        'country_id' => 'required|integer|exists:countries,id',
        'initials' => 'required',
        'state' => 'required',
    ];

    public function messages()
    {
        return [
            'country_id.required' => __("validation.required", ['attribute' => __('Country')]),
            'country_id.integer' => __("validation.integer", ['attribute' => __('Country')]),
            'country_id.exists' => __("validation.exists", ['attribute' => __('Country')]),

            'initials.required' => __("validation.required", ['attribute' => __('Initials')]),
            'state.required' => __("validation.required", ['attribute' => __('State')]),
        ];
    }

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'country',
        'cities',
    ];

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function cities()
    {
        return $this->hasMany(City::class, 'state_id');
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    // Getters
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    // Setters
    // public function setCreatedAtAttribute($value){
    //  return formataDataHoraMysql($value);
    // }
    // public function setUpdatedAtAttribute($value){
    //  return formataDataHoraMysql($value);
    // }


    public function getAcaoAttribute()
    {
        return [
            'cities' => [
                'dynamic_vars' => [
                    'country_id',
                ],
                'html' => '
                    <a class="dropdown-item" title="'. __('Cidades') .'" href="'. route('admin.anyroute' , [ 'slug' => 'cities/index' ]) .'/country_id/{$country_id}/state_id/{$id}">
                      '. __('Cidades') .'
                    </a>
                '
            ],

            'editar' => [
                'dynamic_vars' => [
                    'country_id',
                ],
                'html' => '
					<a class="dropdown-item open-form-sidebar" title="'. __('Editar') .'" href="{$url}/edit/{$pk}/{$id}/country_id/{$country_id}">
					  <i class="far fa-edit" aria-hidden="true"></i> '. __('Editar') .'
					</a>
				'
            ],

            'delete' => [
                'dynamic_vars' => [
                    'country_id',
                ],
                'html' => '
					<a class="dropdown-item" title="'. __('Remover') .'" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}/country_id/{$country_id}">
						<i class="fas fa-times" aria-hidden="true"></i> '. __('Remover') .'
					</a>
				'
            ],
        ];
    }
}
