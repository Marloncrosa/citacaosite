<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;

class PermissionsGroup extends Model
{

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'permissions',
    ];

    /**
     * Primary key
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'permissions_group';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($group) {
            if (request()->has('permission')) {
                $group->permissions()->sync(request()->permission);
            } else {
                $group->permissions()->detach();
            }

            cache()->forget('groups');
            $groups = cache()->remember('groups', 60, function () {
                return PermissionsGroup::with('permissions')->get();
            });
            foreach ($groups as $group) {
                cache()->forget("group_{$group->id}");
                cache()->remember("group_{$group->id}", 60, function () use ($group) {
                    return $group;
                });
            }
//            dump(
//                request()->permission
//            );
//            dump(
//                $group->permissions()->get()
//            );
//            dump(
//                cache()->get('group_8')
//            );
//            die;
        });
    }

    // Traz as permissões deste grupo
    public function permissions()
    {
        return $this->belongsToMany(
            Permissions::class,
            'permissions_permissionsgroup',
            'permissions_group_id',
            'permissions_id'
        )
            ->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany(
            Users::class,
            'user_permissions_group',
            'permissions_group_id',
            'user_id'
        )->withTimestamps();
    }

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'name' => ['titulo' => __('Nome'), 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }

    public function getAcaoAttribute()
    {
        $action = [];

        if (auth('admin')->user()->canpermission('Create/Edit permissions group')) {
            $action['editar'] = [
                'html' => '
                      <a class="dropdown-item open-form-sidebar" title="' . __('Edit') . '" href="{$url}/edit/{$pk}/{$id}">
                        <i class="far fa-edit" aria-hidden="true"></i> ' . __('Edit') . '
                      </a>
                    '
            ];
        }

        if (auth('admin')->user()->canpermission('Delete permission group')) {
            $action['delete'] = [
                'html' => '
                      <a class="dropdown-item" title="' . __('Remove') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                        <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remove') . '
                      </a>
                    '
            ];
        }

        if (empty($action)) {
            $action = '';
        }

        return $action;
    }

//    protected static function boot()
//    {
//        parent::boot();
//
//        static::saved(function ($groups) {
//
//        });
//    }
}
