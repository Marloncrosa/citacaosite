<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Modules\Admin\Entities\Banners
 *
 * @property int $id
 * @property string $titulo
 * @property string|null $imagem
 * @property string|null $imagem_media
 * @property string|null $imagem_pequena
 * @property string|null $link
 * @property bool $ativo
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereAtivo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereImagem($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereImagemMedia($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereImagemPequena($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereTitulo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Modules\Admin\Entities\Banners whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Banners extends Model
{
    use SoftDeletes;

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'banners';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'video',
        'video_image',
        'image',
        'image_mobile',
        'start',
        'end',
        'link',
        'active',
    ];

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'active' => 'boolean',
    ];

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'image' => ['titulo' => 'Imagem', 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "banners/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'image_mobile' => ['titulo' => 'Imagem Mobile', 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "banners/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'video_image' => ['titulo' => 'Imagem do Vídeo', 'listar' => false, 'adicionar' => true, 'editar' => true, 'uploadpath' => "banners/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'title' => ['titulo' => 'Titulo', 'listar' => true, 'adicionar' => true, 'editar' => true],
            'description' => ['titulo' => 'Descrição', 'listar' => true, 'adicionar' => true, 'editar' => true],
            'ativo' => ['titulo' => 'Ativo', 'listar' => false, 'adicionar' => true, 'editar' => true],
            'link' => ['titulo' => 'link', 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }

//    /**
//     * Seta relacionamentos
//     */
//    public $relacionamentos = [
//        'language',
//    ];
//
//    public function language()
//    {
//        return $this->belongsTo(Language::class, 'language_id');
//    }

    public $rules = [
        'titulo' => 'required',
        'image' => 'required|image',
        'ativo' => 'boolean',
    ];

    public function messages()
    {
        return [
            'titulo.required' => __("validation.required", ['attribute' => 'Título']),
            'image.required' => __("validation.required", ['attribute' => 'Imagem']),
            'image.image' => __("validation.image", ['attribute' => 'Imagem']),
            'ativo.boolean' => __('validation.boolean', ['attribute' => 'Ativo']),
        ];
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    // Getters
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getImageCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = $this->atributos['image']['uploadpath'] . $this->image;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'banners/' . $this->image;
            }
        }

        return $retorno;
    }

    public function getImageMobileCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image_mobile)) {
            $retorno = $this->atributos['image_mobile']['uploadpath'] . $this->image_mobile;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'banners/' . $this->image_mobile;
            }
        }

        return $retorno;
    }

    public function getSmallBeforeTitleAttribute()
    {
        $retorno = 'Diplomatura Universitaria en';

        return $retorno;
    }

    public function scopeAtivoelinguagem($query, $language_id)
    {
        return $query
            ->where('active', true)//            ->where('language_id', $language_id)
            ;
    }

    public function setStartAttribute($value)
    {
        if (!empty($value)) {
            $value = formataDataMysql($value);
        } else {
            $value = null;
        }
        $this->attributes['start'] = $value;
    }

    public function setEndAttribute($value)
    {
        if (!empty($value)) {
            $value = formataDataMysql($value);
        } else {
            $value = null;
        }
        $this->attributes['end'] = $value;
    }
}
