<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Request;

class News extends Model
{

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'news';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'news_categorie_id',
        // 'user_id',
        'date_time',
        'slug',
        'title',
        'synopsis',
        'description',
        'video',
        'image',
        'image_mobile',
        'video_cover',
        'home_cover',
        'list_cover',
        'metadescription',
        'active',
    ];

    /**
     * Converte a coluna em um tipo de variavel
     */
    protected $casts = [
        'tags' => 'json',
        'active' => 'boolean',
        'featured' => 'boolean',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'datahora'
    ];

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'news_categorie',
        // 'news_product',
        // 'new_photo_blog'
    ];

    public function news_categorie()
    {
        return $this->belongsTo(NewsCategories::class, 'news_categorie_id');
    }

    // public function news_user()
    // {
    //     return $this->belongsTo(Users::class, 'user_id', 'id');
    // }
    // public function news_product()
    // {
    //     return $this->belongsTo(products::class, 'product_id');
    // }


    // public function new_photo_blog()
    // {
    //     return $this->hasMany(NewsPhotos::class, 'news_id');
    // }

//    public function language()
//    {
//        return $this->belongsTo(Language::class, 'language_id');
//    }
//     /**
//      * Regras de validação
//      * @var array
//      */
//     public function getRulesAttribute()
//     {
//         // Parametros adicionais dinamicos vindos da url
//         foreach (request()->segments() as $key => $value) {
//             if ($key > 2) {
//                 ${request()->segment($key)} = request()->segment($key + 1);
//             }
//         }
//         if (!isset($id)) {
//             $id = null;
//         }
//
//         return [
//             'news_categories_id' => 'required|integer|exists:news_categories,id',
//             'date_time' => 'required',
//             'slug' => [
//                 'regra' => [
//                     'required',
//                     Rule::unique('news', 'slug')->ignore($id)
//                 ]
//             ],
//             'title' => 'required',
//             'featured' => 'boolean',
//             'active' => 'boolean',
//         ];
//     }

    public function messages()
    {
        return [
            'idcategorie.required' => __('validation.required', ['attribute' => 'Categorias']),
            'date_time.required' => __('validation.required', ['attribute' => 'Data']),
            'slug.required' => __('validation.required', ['attribute' => 'Url Amigável']),
            'title.required' => __('validation.required', ['attribute' => 'Título']),
            'idcategorie.integer' => __("validation.integer", ['attribute' => 'Categorias']),
            'idcategorie.exists' => __("validation.exists", ['attribute' => 'Categorias']),
            'slug.unique' => __("validation.unique", ['attribute' => 'Url Amigável']),
            'featured.boolean' => __('validation.boolean', ['attribute' => 'Destaque']),
            'active.boolean' => __('validation.boolean', ['attribute' => 'Ativo']),
        ];
    }

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'image' => ['titulo' => __('Capa da Imagem'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "news/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'image_mobile' => ['titulo' => __('Imagem Mobile'), 'listar' => true, 'adicionar' => true, 'editar' => true, 'uploadpath' => "news/{$this->id}/", 'dynamic_attributes_title' => ['width' => '100']],
            'news_categorie.title' => ['titulo' => __('Categorias'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'title' => ['titulo' => __('Título'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'synopsis' => ['titulo' => __('Sinópse'), 'listar' => true, 'adicionar' => true, 'editar' => false],
        ];
    }

    public function getAcaoAttribute()
    {
        return [

            'active' => [
                'campo' => 'active',
                'validar' => 'boolean',
                'html' => '
                  <a class="dropdown-item" title="' . __('Ativar') . '" href="javascript:void(0);"
                    data-alterarbool
                    data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                    data-icone1="fas fa-circle"
                                data-texto1="' . __('Ativar') . '"
                                data-icone2="far fa-dot-circle"
                                data-texto2="' . __('Desativar') . '"
                  >
                    <i class="fas fa-circle" aria-hidden="true"></i> <span class="text">' . __('Ativar') . '</span>
                  </a>
                ',
                'html2' => '
                  <a class="dropdown-item" title="' . __('Desativar') . '" href="javascript:void(0);"
                    data-alterarbool
                    data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}"
                    data-icone1="fas fa-circle"
                                data-texto1="' . __('Ativar') . '"
                                data-icone2="far fa-dot-circle"
                                data-texto2="' . __('Desativar') . '"
                  >
                    <i class="far fa-dot-circle" aria-hidden="true"></i> <span class="text">' . __('Desativar') . '</span>
                  </a>
                ',
            ],

            'editar' => [
                'html' => '
                  <a class="dropdown-item open-form-sidebar" title="' . __('Editar') . '" href="{$url}/edit/{$pk}/{$id}">
                    <i class="far fa-edit" aria-hidden="true"></i> ' . __('Editar') . '
                  </a>
                '
            ],

            'delete' => [
                'html' => '
                  <a class="dropdown-item" title="' . __('Remover') . '" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
                    <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remover') . '
                  </a>
                '
            ],
        ];
    }

    // Getters
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getDateTimeAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUnformatDataAttribute()
    {
        return Carbon::createFromFormat('d/m/Y H:i:s', $this->date_time);
    }

    public function getNewsCategoriesIdDatatablesAttribute()
    {
        return (!empty($this->news_categories_id)) ? $this->categories->title : '';
    }

    public function scopeAtivoelinguagem($query, $language_id)
    {
        return $query
            ->where('active', true)
//            ->where('language_id', $language_id)
            ;
    }

    public function getImageCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image)) {
            $retorno = $this->atributos['image']['uploadpath'] . $this->image;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'news/' . $this->image;
            }
        }

        return $retorno;
    }

    public function getImageHomeCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->home_cover)) {
            $retorno = $this->atributos['home_cover']['uploadpath'] . $this->home_cover;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'news/' . $this->home_cover;
            }
        }

        return $retorno;
    }

    public function getImageListCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->list_cover)) {
            $retorno = $this->atributos['list_cover']['uploadpath'] . $this->list_cover;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'news/' . $this->list_cover;
            }
        }

        return $retorno;
    }

    public function getImageVideoCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->video_cover)) {
            $retorno = $this->atributos['video_cover']['uploadpath'] . $this->video_cover;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'news/' . $this->video_cover;
            }
        }

        return $retorno;
    }
    public function getImageMobileCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image_mobile)) {
            $retorno = $this->atributos['image_mobile']['uploadpath'] . $this->image_mobile;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'news/' . $this->image_mobile;
            }
        }

        return $retorno;
    }

    // Setters
    public function setDateTimeAttribute($value)
    {
        $this->attributes['date_time'] = !is_null($value) ? formataDataHoraMysql($value) : null;
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = !is_null($value) ? strtolower($value) : null;
    }
//    public function setTagsAttribute($value)
//    {
//        $this->attributes['tag'] = json_encode($value);
//    }

    // public function setCreatedAtAttribute($value){
    // 	return formataDataHoraMysql($value);
    // }
    // public function setUpdatedAtAttribute($value){
    // 	return formataDataHoraMysql($value);
    // }

    public function getControlPermissionsAttribute()
    {
        return (object)[
            'list' => 'List news',
            'create' => 'Create/Edit news',
            'edit' => 'Create/Edit news',
            'delete' => 'Delete new',
        ];
    }
}
