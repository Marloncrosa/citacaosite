<?php namespace Modules\Admin\Entities;

class Country extends Model
{
    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'initials',
        'country',
        'country_code',
    ];

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        return [
            'initials' => ['titulo' => __('Iniciais'), 'listar' => true, 'adicionar' => true, 'editar' => true],
            'country' => ['titulo' => __('País'), 'listar' => true, 'adicionar' => true, 'editar' => true],
//            'country_code' => ['titulo' => __('Code'), 'listar' => true, 'adicionar' => true, 'editar' => true],
        ];
    }

    public $rules = [
        'initials' => 'required',
        'country' => 'required',
        'country_code' => 'required',
    ];

    public function messages()
    {
        return [
            'initials.required' => __("validation.required", ['attribute' => __('Initials')]),
            'country.required' => __("validation.required", ['attribute' => __('Country')]),
            'country_code.required' => __("validation.required", ['attribute' => __('Code')]),
        ];
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    // Getters
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    // Setters
    // public function setCreatedAtAttribute($value){
    //  return formataDataHoraMysql($value);
    // }
    // public function setUpdatedAtAttribute($value){
    //  return formataDataHoraMysql($value);
    // }


    public function getAcaoAttribute()
    {
        return [
            'states' => [
                'html' => '
                    <a class="dropdown-item" title="'. __('Estados') .'" href="'. route('admin.anyroute' , [ 'slug' => 'states/index' ]) .'/country_id/{$id}">
                      '. __('Estados') .'
                    </a>
                '
            ],

            'editar' => [
                'html' => '
					<a class="dropdown-item open-form-sidebar" title="'. __('Editar') .'" href="{$url}/edit/{$pk}/{$id}">
					  <i class="far fa-edit" aria-hidden="true"></i> '. __('Editar') .'
					</a>
				'
            ],

            'delete' => [
                'html' => '
					<a class="dropdown-item" title="'. __('Remover') .'" data-excluir data-ajax="{$url}/deleteajax/{$pk}/{$id}">
						<i class="fas fa-times" aria-hidden="true"></i> '. __('Remover') .'
					</a>
				'
            ],
        ];
    }
}
