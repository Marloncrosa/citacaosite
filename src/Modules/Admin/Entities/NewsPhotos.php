<?php namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Request;

class NewsPhotos extends Model
{

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Chave primária da tabela
     *
     * @var string
     */
    protected $table = 'news_photos';

    /**
     * The attributes that are mass assignable.
     * Campos que também são visiveis na listagem e busca
     *
     * @var array
     */
    protected $fillable = [
        'news_id',
        'title',
        'image',
        'image_mobile',
        'legend',
        'active',
    ];

    /**
     * Algumas definições
     * @var [type]
     */
    public function getAtributosAttribute()
    {
        $news_id = '{$kp}';
        // Parametros adicionais dinamicos vindos da url
        foreach (request()->segments() as $key => $value) {
            if ($key > 2) {
                ${request()->segment($key)} = request()->segment($key + 1);
            }
        }
        if (!isset($id)) {
            $id = null;
        }

        return [
            'image' => [ 'titulo' => 'Image', 'listar' => true, 'adicionar' => true, 'editar' => true, 'multiploupload' => true, 'uploadpath' => "news/{$this->news_id}/", 'dynamic_attributes_title' => ['width' => '100'] ],
            'image_mobile' => [ 'titulo' => 'Image Mobile', 'listar' => true, 'adicionar' => true, 'editar' => true, 'multiploupload' => true, 'uploadpath' => "news/{$this->news_id}/", 'dynamic_attributes_title' => ['width' => '100'] ],
            'title' => [ 'titulo' => 'Title', 'listar' => true, 'adicionar' => true, 'editar' => true ],
            'active' => [ 'titulo' => 'Active', 'listar' => false, 'adicionar' => true, 'editar' => true ],
        ];
    }

    public $rules = [
        'news_id' => 'required|exists:news',
        'title' => 'required',
        'image' => 'required|image',
        'active' => 'boolean',
    ];

    public function messages()
    {
        return [
            'news_id.required' => __("validation.required", ['attribute' => 'News']),
            'title.required' => __("validation.required", ['attribute' => 'Title']),
            'image.required' => __("validation.required", ['attribute' => 'Image']),
            'image.image' => __("validation.image", ['attribute' => 'Image']),
            'news_id.exists' => __("validation.exists", ['attribute' => 'News']),
            'active.boolean' => __('validation.boolean', ['attribute' => 'Active']),
        ];
    }

    /**
     * Seta relacionamentos
     */
    public $relacionamentos = [
        'new_photo_blog'
    ];

    public function new_photo_prod()
    {
        return $this->belongsTo(News::class, 'news_id', 'id');
    }

    /**
     * Alterar coluna de açõess
     */
    public function getAcaoAttribute()
    {
        return [
            'tamanho' => '140',
            'ativar' => [
                'campo' => 'active',
                'validar' => 'boolean',
                'dynamic_vars' => [
                    'news_id'
                ],
                'html' => '
                    <a class="button warning" title="" href="javascript:void(0);" 
                      data-alterarbool 
                      data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}/news_id/{$news_id}/"
                      data-icone1="fa fa-circle-o"
                      data-icone2="far fa-dot-circle"
                    >
                      <i class="fa fa-circle-o" aria-hidden="true"></i>
                    </a>
                  ',
                'html2' => '
                    <a class="button warning" title="" href="javascript:void(0);" 
                      data-alterarbool 
                      data-ajax="{$url}/alterarbool/{$pk}/{$id}/campo/{$campo}/news_id/{$news_id}/"
                      data-icone1="fa fa-circle-o"
                      data-icone2="far fa-dot-circle"
                    >
                      <i class="far fa-dot-circle" aria-hidden="true"></i>
                    </a>
                  ',
            ],
            'editar' => [
                'dynamic_vars' => [
                    'news_id'
                ],
                'html' => '
                    <a class="button secondary" title="' . __('Edit') . '" href="{$url}/edit/{$pk}/{$id}/news_id/{$news_id}/"><i class="far fa-edit" aria-hidden="true"></i></a>
                  '
            ],
            'delete' => [
                'dynamic_vars' => [
                    'news_id'
                ],
                'html' => '
                  <a class="button alert" title="' . __('Delete') . '" data-excluir data-ajax="{$url}/deletararquivo/{$pk}/{$id}/news_id/{$news_id}/removeregistro/1/campo/image">
                    <i class="fa fa-times" aria-hidden="true"></i>
                  </a>
                '
            ],
        ];
    }

    /**
     * The attributes excluded from the model's JSON form.
     * Campos que não são exibidos na listagem e busca
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'update_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at'
    ];
    public function getImageMobileCoverAttribute()
    {
        $retorno = null;
        if (!empty($this->image_mobile)) {
            $retorno = $this->atributos['image_mobile']['uploadpath'] . $this->image_mobile;
            if (!file_exists(public_path('uploads/' . $retorno))) {
                $retorno = 'news_photos/' . $this->image_mobile;
            }
        }

        return $retorno;
    }

    // Getters
    public function getCreatedAtAttribute($value)
    {
        return formataDataHora($value);
    }

    public function getUpdatedAtAttribute($value)
    {
        return formataDataHora($value);
    }
}
