<?php

use Carbon\Carbon;
use App\Http\Controllers\Mailable;
use Illuminate\Support\Collection;
use Modules\Admin\Entities\ComissionsPayments;
use Modules\Admin\Entities\Configuration;

if (!function_exists('encurtartexto')) {
    /**
     * Encurtar texto
     * @param  [type] $texto  [description]
     * @param  [type] $limite [description]
     * @param  string $textoS [description]
     * @return [type]         [description]
     */
    function encurtartexto($texto, $limite, $textoS = '...')
    {
        $tamanho = strlen($texto);
        if ($tamanho <= $limite) {
            $novo_texto = $texto;
        } else {
            if ($textoS == '<br>') {
                $novo_texto = trim(substr($texto, 0, $limite)) . $textoS . trim(substr($texto, $limite, $tamanho));
            } elseif ($textoS == '') {
                $ultimo_espaco = strrpos(substr($texto, 0, $limite), " ");
                $novo_texto = trim(substr($texto, 0, $ultimo_espaco));
            } else {
                $ultimo_espaco = strrpos(substr($texto, 0, $limite), " ");
                $novo_texto = trim(substr($texto, 0, $ultimo_espaco)) . $textoS;
            }
        }
        return $novo_texto;
    }
}

if (!function_exists('formataNumero')) {
    /**
     * Função formataNumero
     * Formata numeros para decimais
     * @param  [type]  $numero
     * @param  integer $decimais - quantidade de casas decimais
     * @return retorna o numero no formato brasileiro
     */
    function formataNumero($numero, $decimais = 2)
    {
        if (empty($numero)) return '';
        // $currency = session('localeObject')->currency;
        // if(!is_null($currency)){
        //     $decimais = $currency->decimal;
        //     $decimal_separator = $currency->decimal_separator;
        //     $thousands_separator = $currency->thousands_separator;
        //     $prepend_string = $currency->prepend_string;
        // }else{
            $decimal_separator = ',';
            $thousands_separator = '.';
            $prepend_string = 'R$ ';
        // }
        return $prepend_string.number_format($numero, $decimais, $decimal_separator, $thousands_separator);
    }
}


if (!function_exists('formataNumeroMysql')) {
    /**
     * Função formataNumeroMysql
     * Formata numeros decimais brasileiro para mysql
     * @param  [type]  $value
     * @return retorna o numero no formato mysql
     */
    function formataNumeroMysql($value)
    {
        if (empty($value) || $value == '') return '0';
        $value = str_replace(" %", "", $value);
        $value = str_replace("R$ ", "", $value);
        $value = str_replace("$ ", "", $value);
        $value = str_replace("$ ", "", $value);
        $value = preg_replace("/[^0-9.,]*/", "", $value);
        $value = str_replace(".", "", $value);
        $value = str_replace(",", ".", $value);
        return $value;
    }
}

if (!function_exists('formataDataHora')) {
    /**
     * formataDataHora
     * @param  [type] $value [data e hora enviada para função]
     * @return [type] [description]
     */
    function formataDataHora($value)
    {
        if (empty($value) || ($value == '0000-00-00 00:00:00')) return '';
        // $resposta_data_hora_array = explode(" ", $value);
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
}

if (!function_exists('formataData')) {
    /**
     * formataData
     * @param  [type] $value [data enviada para função]
     * @return [type] [description]
     */
    function formataData($value)
    {
        if (empty($value) || ($value == '0000-00-00')) return '';
        return Carbon::parse($value)->format('d/m/Y');
    }
}

if (!function_exists('formataHora')) {
    /**
     * formataHora
     * @param  [type] $value [data enviada para função]
     * @return [type] [description]
     */
    function formataHora($value)
    {
        if (empty($value) || ($value == '00:00:00')) return '';
        return Carbon::parse($value)->format('H:i');
    }
}

if (!function_exists('formataDataMysql')) {
    /**
     * formataDataHoraMysql
     * @param  [type] $value [data enviada para função]
     * @return [type] [description]
     */
    function formataDataMysql($value)
    {
        if (empty($value)) return '0000-00-00';
        return Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }
}

if (!function_exists('formataDataHoraMysql')) {
    /**
     * formataDataHoraMysql
     * @param  [type] $value [data e hora enviada para função]
     * @return [type] [description]
     */
    function formataDataHoraMysql($value)
    {
        if (empty($value)) return '0000-00-00 00:00';
        return Carbon::createFromFormat('d/m/Y H:i', $value)->format('Y-m-d H:i:s');
    }
}

if (!function_exists('formataDataHoraMysql2')) {
    /**
     * formataDataHoraMysql
     * @param  [type] $value [data e hora enviada para função]
     * @return [type] [description]
     */
    function formataDataHoraMysql2($value)
    {
        if (empty($value)) return '0000-00-00 00:00:00';
        return Carbon::createFromFormat('d/m/Y H:i:s', $value)->format('Y-m-d H:i:s');
    }
}

if (!function_exists('formataHoraMysql')) {
    /**
     * formataHoraMysql
     * @param  [type] $value [hora enviada para função]
     * @return [type] [description]
     */
    function formataHoraMysql($value)
    {
        if (empty($value)) return '00:00';
        return Carbon::createFromFormat('H:i', $value)->format('H:i:s');
    }
}


if (!function_exists('formataHoraMysql2')) {
    /**
     * formataHoraMysql
     * @param  [type] $value [hora enviada para função]
     * @return [type] [description]
     */
    function formataHoraMysql2($value)
    {
        if (empty($value)) return '00:00:00';
        return Carbon::createFromFormat('H:i:s', $value)->format('H:i');
    }
}

if (!function_exists('enviaEmail')) {
    /**
     * Função enviaEmail
     * Esta função envia através de SMTP e-mails
     *
     * @param  [array]  $config      [Enviar os paramentros abaixo como index do array]
     * @param  [type]  $assunto      [description]
     * @param  [type]  $template     [o nome do template de e-mail]
     * @param  boolean $enviar_copia [para enviar e-mail de copia]
     * @param  string $para_cc [para quais e-mails de cópia]
     * @param  string $para_bcc [para quais e-mails de cópia oculta]
     *
     * @return [boolena] retorna se executo o envio ou não
     */
    function enviaEmail(
        Collection $Data = null,
        $enviaparaosite = true,
        $enviacopiacliente = false,
        $TemplateMarkDown,
        $TemplateHtml = null,
        $ShowInView = false,
        array $config = null
    )
    {
        // verifica se foi enviada alguma configuração
        if (!is_null($config)) {
            config($config);
        } else { // Configuração padrão de e-mail do sistema
            config([
                'mail.markdown.theme' => 'padrao',
                'mail.markdown.paths' => [resource_path('../Modules/Sistema/Resources/views/mail')],
            ]);
        }

        // Instância o e-mail
        $email = new Mailable();

        // Seta o markdown
        if (is_null($TemplateHtml)) {
            $email->markdown($TemplateMarkDown);
        } else {
            $email->view($TemplateHtml);
        }

        // Transforma toda a coleção $Data em dados
        if (!is_null($Data) && $Data->count() > 0) {

            // Tipo de botão
            if (!$Data->has('level')) {
                $Data->put('level', 'info');
            }

            if ($Data->has('assunto')) {
                $email->subject($Data->get('assunto') . ' - ' . getenv('MAIL_SITE_TITLE') . ' |' . date('YmdHis') . '|');
            }

            // transforma toda coleção $Data em dados
            if (!is_null($Data)) {
                $dataNova = array_merge($email->viewData, $Data->toArray());
                $email->viewData = $dataNova;
            }
        }

        /**
         * Quem está enviando o e-mail
         */
        $sendermail = isset($config['sender']['email']) ? $config['sender']['email'] : getenv('MAIL_SENDER');
        $sendername = isset($config['sender']['name']) ? $config['sender']['name'] : getenv('MAIL_SENDER_NAME');
        $email->from($sendermail, $sendername);

        /**
         * Para quem irá enviar o e-mail como cópia
         */
        if (isset($Data['dados']->email)) {
            $toemail = isset($Data['dados']->email) ? explode(',', $Data['dados']->email) : '';
            $tonome = isset($Data['dados']->nome) ? explode(',', $Data['dados']->nome) : '';
        } elseif (isset($Data['dados']['email'])) {
            $toemail = isset($Data['dados']['email']) ? explode(',', $Data['dados']['email']) : '';
            $tonome = isset($Data['dados']['nome']) ? explode(',', $Data['dados']['nome']) : '';
        }

        // Rendereiza o e-mail
        if ($ShowInView) {
            return $email->buildView()['html'];
        }

        /* Envia o para próprio site */
        if ($enviaparaosite) {
            $emailsite = clone $email;

            // Responder para
            if (isset($config['data']['email'])) {
                $emailsite->replyTo($config['data']['email'], null);
            } elseif (isset($toemail)) {
                $emailsite->replyTo($toemail, $tonome);
            }

            // Tem e-mail de cópia adiciona?
            if (isset($config['cc'])) {
                if (!is_array($config['cc'])) {
                    $emailsite->cc($config['cc']['email'], isset($config['cc']['name']) ? $config['cc']['name'] : null);
                } else {
                    foreach ($config['cc'] as $key => $value) {
                        $emailsite->cc($value['email'], isset($value['name']) ? $value['name'] : null);
                    }
                }
            }

            // Tem e-mail de cópia oculta adiciona?
            if (isset($config['bcc'])) {
                if (!is_array($config['bcc'])) {
                    $emailsite->bcc($config['bcc']['email'], isset($config['bcc']['name']) ? $config['bcc']['name'] : null);
                } else {
                    foreach ($config['bcc'] as $key => $value) {
                        $emailsite->bcc($value['email'], isset($value['name']) ? $value['name'] : null);
                    }
                }
            }

            /**
             * Para quem vai enviar quando for para o site
             */
            if ($sendermail == 'testeGPRODATA@gmail.com') {
                $emailsite->to('felipe@GPRODATA.com.br', $sendername);
            } else {
                $emailsite->to($sendermail, $sendername);
            }

            $emailsite->sendNew();
        }

        /* Envia o para próprio site */

        /* Envia e-mail de Cópia */
        if ($enviacopiacliente && isset($toemail)) {
            $emailcliente = clone $email;

            /**
             * Reponde para quem
             */
            $replyToemail = isset($config['replyTo']['email']) ? $config['replyTo']['email'] : getenv('MAIL_SENDER');
            $replyToname = isset($config['replyTo']['name']) ? $config['replyTo']['name'] : getenv('MAIL_SENDER_NAME');
            $emailcliente->replyTo($replyToemail, $replyToname);

            $emailcliente->to($toemail, $tonome);

            $emailcliente->sendNew();
        }
        /* Envia e-mail de Cópia */

        // Verifica se ouve algum erro ao disparar
        if (count(Mail::failures()) > 0) {
            $mensagem = "houve um ou mais erros ao enviar. Para os e-mail: <br />";
            foreach (Mail::failures() as $email_address) {
                $mensagem .= " - $email_address <br />";
            }
            return json_encode([
                'success' => false,
                'message' => $mensagem
            ]);
        } else {
            return json_encode([
                'success' => true,
                'message' => $Data->get('assunto') . ' realizado com sucesso!'
            ]);
        }
    }
}


if (!function_exists('salvarArquivos')) {
    /**
     * Função salvarArquivos
     * Executa upload de todos os campos
     *
     * @param  [model] $modelItem [ Envia a model/item que será feito o upload/atualizado ]
     *
     * @return [type]       [description]
     */
    function salvarArquivos($modelItem, $request)
    {
        $arrayteste = collect($modelItem->toArray());

        // Pega todos o campos do tipo file
        if (!empty($request->allFiles())) {
            foreach ($request->allFiles() as $key => $value) {
                // Pega o caminho de upload
                $caminho = 'uploads/' . $modelItem->atributos[$key]['uploadpath'];
                // Verifica se é uma atualização ou inclusão!
                if (!empty($arrayteste)) {
                    // Valida se o campo exite no banco
                    if ($arrayteste->has($key)) {
                        // Valida se o campo está preenchido
                        if (!empty($modelItem->$key) && !is_null($modelItem->$key)) {
                            // Remove o arquivo anterior
                            \File::delete($caminho . $modelItem->$key);

                            // Limpa o campo do banco
                            $modelItem->$key = '';
                            $modelItem->save();
                        }
                        // Valida o campo para ver se existe arquivo
                        if ($request->file($key)->isValid()) {
                            // Atribui o file a um arquivo
                            $arquivo = $request->file($key);

                            // Pega a extenão
                            $extensao = strtolower($arquivo->extension());

                            $nome_arquivo_original = str_slug(str_replace("." . $extensao, '', strtolower($arquivo->getClientOriginalName())));

                            // Gera o novo nome do arquivo
                            $nome = date('dmYHis') . '_' . $nome_arquivo_original . '.' . $extensao;
                            // Salva o arquivo novo
                            $arquivo->move($caminho, $nome);

                            // Salva o campo do banco
                            $modelItem->$key = $nome;
                            $modelItem->save();
                        }
                    }
                } else { //se for uma inserção
                }
            }
        }
    }
}

if (!function_exists('IntervaloTempo')) {
    function IntervaloTempo($de, $ate, $minutos)
    {
        $arrayIntervalo = array();
        $de = formataHoraMysql($de);
        $ate = formataHoraMysql($ate);
        if ($ate == '00:00:00') {
            $ate = '24:00:00';
        }
        for ($i = 0; $i < 1000000000000000000000000; $i += $minutos) { // faz do final para o de
            $decesce = ($i * 60);
            $Ate = date("H:i:s", strtotime($de) + $decesce);

            if ($Ate >= $ate) {
                // if($i >= 600) {
                $arrayIntervalo[] = $Ate;
                break;
            } else {
                if ($ate == '24:00:00' && $Ate == '00:00:00') {
                    $arrayIntervalo[] = $Ate;
                    break;
                }
                // dump($Ate.' > '.$de);
                // echo "$Ate <br>";
                $arrayIntervalo[] = $Ate;
            }
        }
        return $arrayIntervalo;
    }
}

if (!function_exists('imagem_video')) {
    /**
     * Funciton imagem_video
     * Função para gerar imagem apartir de um url de video
     * Feita para youtube e vimeo
     *
     * @param  [stringo] $url [url do video]
     *
     * @return [string]      [retorna url de thumb do video]
     */
    function imagem_video($url)
    {
        $image_url = parse_url($url);
        if ($image_url['host'] == 'www.youtube.com' || $image_url['host'] == 'youtube.com') {
            $array = explode("&", $image_url['query']);
            return "http://img.youtube.com/vi/" . substr($array[0], 2) . "/hqdefault.jpg";
        } else if ($image_url['host'] == 'www.youtu.be' || $image_url['host'] == 'youtu.be') {
            $array = explode("/", $image_url['path']);
            return "http://img.youtube.com/vi/" . $array[1] . "/hqdefault.jpg";
        } else if ($image_url['host'] == 'www.vimeo.com' || $image_url['host'] == 'vimeo.com') {
            $hash =
                unserialize(
                    file_get_contents("http://vimeo.com/api/v2/video/" . substr($image_url['path'], 1) . ".php")
                );
            return $hash[0]["thumbnail_small"];
        }
    }
}

if (!function_exists('atualiza_latlong')) {
    /**
     * Function atualiza_latlong
     * Função para atualizar latitude e longitude da model
     *
     * @param  [object] $model [model que será atualizada]
     *
     * @return [object] $model [retorna a model atualizada, para uso]
     */
    function atualiza_latlong($model)
    {
        $address = urlencode("{$model->endereco}, {$model->numero}, {$model->bairro}, {$model->cidades->cidade} - {$model->estados->uf}, Brazil");

        $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
        $json = json_decode($json);

        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
        $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};

        $model->latitude = $lat;
        $model->longitude = $long;
        $model->save();

        return $model;
    }
}

if (!function_exists('latitude_longitude')) {
    /**
     * Function atualiza_latlong
     * Função para atualizar latitude e longitude da model
     *
     * @param  [object] $model [model que será atualizada]
     *
     * @return [object] $model [retorna a model atualizada, para uso]
     */
    function latitude_longitude($endereco)
    {
        $endereco = str_replace('-', ' ', $endereco);
        $address = urlencode("{$endereco}, Brazil");
        $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
        $json = json_decode($json);

        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
        $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};

        return ['latitude' => $lat, 'longitude' => $long,];
    }
}

if (!function_exists('getConfiguracoes')) {
    function getConfiguracoes($campo)
    {
        $configuracao = Configuration::where('field', $campo)->first();
        return !is_null($configuracao) ? $configuracao->value : '';
    }
}


if (!function_exists('getEnumValues')) {
    function getEnumValues($table, $column)
    {
        $type = DB::select(DB::raw("SHOW COLUMNS FROM $table WHERE Field = '{$column}'"))[0]->Type;
        preg_match('/^enum\((.*)\)$/', $type, $matches);
        $enum = array();
        foreach (explode(',', $matches[1]) as $value) {
            $v = trim($value, "'");
            $enum = array_add($enum, $v, $v);
        }
        return $enum;
    }
}

if (!function_exists('randomPassword')) {
    function randomPassword($len = 8)
    {
        //enforce min length 8
        if ($len < 8)
            $len = 8;

        //define character libraries - remove ambiguous characters like iIl|1 0oO
        $sets = array();
        $sets[] = 'ABCDEFGHJKLMNPQRSTUVWXYZ';
        $sets[] = 'abcdefghjkmnpqrstuvwxyz';
        $sets[] = '23456789';
        $sets[] = '~!@#$%^&*(){}[],./?';

        $password = '';

        //append a character from each set - gets first 4 characters
        foreach ($sets as $set) {
            $password .= $set[array_rand(str_split($set))];
        }

        //use all characters to fill up to $len
        while (strlen($password) < $len) {
            //get a random set
            $randomSet = $sets[array_rand($sets)];

            //add a random char from the random set
            $password .= $randomSet[array_rand(str_split($randomSet))];
        }

        //shuffle the password string before returning!
        return str_shuffle($password);
    }
}

if (!function_exists('listTimezone')) {
    function listTimezone()
    {
        $timezone = timezone_identifiers_list();
        $array = [];
        foreach ($timezone as $item) {
            $array[$item] = $item;
        }

        //shuffle the password string before returning!
        return $array;
    }
}

if (!function_exists('localeArray')) {
    function localeArray()
    {
        return [
            'aa_DJ' => 'Afar (Djibouti)',
            'aa_ER' => 'Afar (Eritrea)',
            'aa_ET' => 'Afar (Ethiopia)',
            'af_ZA' => 'Afrikaans (South Africa)',
            'sq_AL' => 'Albanian (Albania)',
            'sq_MK' => 'Albanian (Macedonia)',
            'am_ET' => 'Amharic (Ethiopia)',
            'ar_DZ' => 'Arabic (Algeria)',
            'ar_BH' => 'Arabic (Bahrain)',
            'ar_EG' => 'Arabic (Egypt)',
            'ar_IN' => 'Arabic (India)',
            'ar_IQ' => 'Arabic (Iraq)',
            'ar_JO' => 'Arabic (Jordan)',
            'ar_KW' => 'Arabic (Kuwait)',
            'ar_LB' => 'Arabic (Lebanon)',
            'ar_LY' => 'Arabic (Libya)',
            'ar_MA' => 'Arabic (Morocco)',
            'ar_OM' => 'Arabic (Oman)',
            'ar_QA' => 'Arabic (Qatar)',
            'ar_SA' => 'Arabic (Saudi Arabia)',
            'ar_SD' => 'Arabic (Sudan)',
            'ar_SY' => 'Arabic (Syria)',
            'ar_TN' => 'Arabic (Tunisia)',
            'ar_AE' => 'Arabic (United Arab Emirates)',
            'ar_YE' => 'Arabic (Yemen)',
            'an_ES' => 'Aragonese (Spain)',
            'hy_AM' => 'Armenian (Armenia)',
            'as_IN' => 'Assamese (India)',
            'ast_ES' => 'Asturian (Spain)',
            'az_AZ' => 'Azerbaijani (Azerbaijan)',
            'az_TR' => 'Azerbaijani (Turkey)',
            'eu_FR' => 'Basque (France)',
            'eu_ES' => 'Basque (Spain)',
            'be_BY' => 'Belarusian (Belarus)',
            'bem_ZM' => 'Bemba (Zambia)',
            'bn_BD' => 'Bengali (Bangladesh)',
            'bn_IN' => 'Bengali (India)',
            'ber_DZ' => 'Berber (Algeria)',
            'ber_MA' => 'Berber (Morocco)',
            'byn_ER' => 'Blin (Eritrea)',
            'bs_BA' => 'Bosnian (Bosnia and Herzegovina)',
            'br_FR' => 'Breton (France)',
            'bg_BG' => 'Bulgarian (Bulgaria)',
            'my_MM' => 'Burmese (Myanmar [Burma])',
            'ca_AD' => 'Catalan (Andorra)',
            'ca_FR' => 'Catalan (France)',
            'ca_IT' => 'Catalan (Italy)',
            'ca_ES' => 'Catalan (Spain)',
            'zh_CN' => 'Chinese (China)',
            'zh_HK' => 'Chinese (Hong Kong SAR China)',
            'zh_SG' => 'Chinese (Singapore)',
            'zh_TW' => 'Chinese (Taiwan)',
            'cv_RU' => 'Chuvash (Russia)',
            'kw_GB' => 'Cornish (United Kingdom)',
            'crh_UA' => 'Crimean Turkish (Ukraine)',
            'hr_HR' => 'Croatian (Croatia)',
            'cs_CZ' => 'Czech (Czech Republic)',
            'da_DK' => 'Danish (Denmark)',
            'dv_MV' => 'Divehi (Maldives)',
            'nl_AW' => 'Dutch (Aruba)',
            'nl_BE' => 'Dutch (Belgium)',
            'nl_NL' => 'Dutch (Netherlands)',
            'dz_BT' => 'Dzongkha (Bhutan)',
            'en_AG' => 'English (Antigua and Barbuda)',
            'en_AU' => 'English (Australia)',
            'en_BW' => 'English (Botswana)',
            'en_CA' => 'English (Canada)',
            'en_DK' => 'English (Denmark)',
            'en_HK' => 'English (Hong Kong SAR China)',
            'en_IN' => 'English (India)',
            'en_IE' => 'English (Ireland)',
            'en_NZ' => 'English (New Zealand)',
            'en_NG' => 'English (Nigeria)',
            'en_PH' => 'English (Philippines)',
            'en_SG' => 'English (Singapore)',
            'en_ZA' => 'English (South Africa)',
            'en_GB' => 'English (United Kingdom)',
            'en_US' => 'English (United States)',
            'en_ZM' => 'English (Zambia)',
            'en_ZW' => 'English (Zimbabwe)',
            'eo' => 'Esperanto',
            'et_EE' => 'Estonian (Estonia)',
            'fo_FO' => 'Faroese (Faroe Islands)',
            'fil_PH' => 'Filipino (Philippines)',
            'fi_FI' => 'Finnish (Finland)',
            'fr_BE' => 'French (Belgium)',
            'fr_CA' => 'French (Canada)',
            'fr_FR' => 'French (France)',
            'fr_LU' => 'French (Luxembourg)',
            'fr_CH' => 'French (Switzerland)',
            'fur_IT' => 'Friulian (Italy)',
            'ff_SN' => 'Fulah (Senegal)',
            'gl_ES' => 'Galician (Spain)',
            'lg_UG' => 'Ganda (Uganda)',
            'gez_ER' => 'Geez (Eritrea)',
            'gez_ET' => 'Geez (Ethiopia)',
            'ka_GE' => 'Georgian (Georgia)',
            'de_AT' => 'German (Austria)',
            'de_BE' => 'German (Belgium)',
            'de_DE' => 'German (Germany)',
            'de_LI' => 'German (Liechtenstein)',
            'de_LU' => 'German (Luxembourg)',
            'de_CH' => 'German (Switzerland)',
            'el_CY' => 'Greek (Cyprus)',
            'el_GR' => 'Greek (Greece)',
            'gu_IN' => 'Gujarati (India)',
            'ht_HT' => 'Haitian (Haiti)',
            'ha_NG' => 'Hausa (Nigeria)',
            'iw_IL' => 'Hebrew (Israel)',
            'he_IL' => 'Hebrew (Israel)',
            'hi_IN' => 'Hindi (India)',
            'hu_HU' => 'Hungarian (Hungary)',
            'is_IS' => 'Icelandic (Iceland)',
            'ig_NG' => 'Igbo (Nigeria)',
            'id_ID' => 'Indonesian (Indonesia)',
            'ia' => 'Interlingua',
            'iu_CA' => 'Inuktitut (Canada)',
            'ik_CA' => 'Inupiaq (Canada)',
            'ga_IE' => 'Irish (Ireland)',
            'it_IT' => 'Italian (Italy)',
            'it_CH' => 'Italian (Switzerland)',
            'ja_JP' => 'Japanese (Japan)',
            'kl_GL' => 'Kalaallisut (Greenland)',
            'kn_IN' => 'Kannada (India)',
            'ks_IN' => 'Kashmiri (India)',
            'csb_PL' => 'Kashubian (Poland)',
            'kk_KZ' => 'Kazakh (Kazakhstan)',
            'km_KH' => 'Khmer (Cambodia)',
            'rw_RW' => 'Kinyarwanda (Rwanda)',
            'ky_KG' => 'Kirghiz (Kyrgyzstan)',
            'kok_IN' => 'Konkani (India)',
            'ko_KR' => 'Korean (South Korea)',
            'ku_TR' => 'Kurdish (Turkey)',
            'lo_LA' => 'Lao (Laos)',
            'lv_LV' => 'Latvian (Latvia)',
            'li_BE' => 'Limburgish (Belgium)',
            'li_NL' => 'Limburgish (Netherlands)',
            'lt_LT' => 'Lithuanian (Lithuania)',
            'nds_DE' => 'Low German (Germany)',
            'nds_NL' => 'Low German (Netherlands)',
            'mk_MK' => 'Macedonian (Macedonia)',
            'mai_IN' => 'Maithili (India)',
            'mg_MG' => 'Malagasy (Madagascar)',
            'ms_MY' => 'Malay (Malaysia)',
            'ml_IN' => 'Malayalam (India)',
            'mt_MT' => 'Maltese (Malta)',
            'gv_GB' => 'Manx (United Kingdom)',
            'mi_NZ' => 'Maori (New Zealand)',
            'mr_IN' => 'Marathi (India)',
            'mn_MN' => 'Mongolian (Mongolia)',
            'ne_NP' => 'Nepali (Nepal)',
            'se_NO' => 'Northern Sami (Norway)',
            'nso_ZA' => 'Northern Sotho (South Africa)',
            'nb_NO' => 'Norwegian Bokmål (Norway)',
            'nn_NO' => 'Norwegian Nynorsk (Norway)',
            'oc_FR' => 'Occitan (France)',
            'or_IN' => 'Oriya (India)',
            'om_ET' => 'Oromo (Ethiopia)',
            'om_KE' => 'Oromo (Kenya)',
            'os_RU' => 'Ossetic (Russia)',
            'pap_AN' => 'Papiamento (Netherlands Antilles)',
            'ps_AF' => 'Pashto (Afghanistan)',
            'fa_IR' => 'Persian (Iran)',
            'pl_PL' => 'Polish (Poland)',
            'pt_BR' => 'Portuguese (Brazil)',
            'pt_PT' => 'Portuguese (Portugal)',
            'pa_IN' => 'Punjabi (India)',
            'pa_PK' => 'Punjabi (Pakistan)',
            'ro_RO' => 'Romanian (Romania)',
            'ru_RU' => 'Russian (Russia)',
            'ru_UA' => 'Russian (Ukraine)',
            'sa_IN' => 'Sanskrit (India)',
            'sc_IT' => 'Sardinian (Italy)',
            'gd_GB' => 'Scottish Gaelic (United Kingdom)',
            'sr_ME' => 'Serbian (Montenegro)',
            'sr_RS' => 'Serbian (Serbia)',
            'sid_ET' => 'Sidamo (Ethiopia)',
            'sd_IN' => 'Sindhi (India)',
            'si_LK' => 'Sinhala (Sri Lanka)',
            'sk_SK' => 'Slovak (Slovakia)',
            'sl_SI' => 'Slovenian (Slovenia)',
            'so_DJ' => 'Somali (Djibouti)',
            'so_ET' => 'Somali (Ethiopia)',
            'so_KE' => 'Somali (Kenya)',
            'so_SO' => 'Somali (Somalia)',
            'nr_ZA' => 'South Ndebele (South Africa)',
            'st_ZA' => 'Southern Sotho (South Africa)',
            'es_AR' => 'Spanish (Argentina)',
            'es_BO' => 'Spanish (Bolivia)',
            'es_CL' => 'Spanish (Chile)',
            'es_CO' => 'Spanish (Colombia)',
            'es_CR' => 'Spanish (Costa Rica)',
            'es_DO' => 'Spanish (Dominican Republic)',
            'es_EC' => 'Spanish (Ecuador)',
            'es_SV' => 'Spanish (El Salvador)',
            'es_GT' => 'Spanish (Guatemala)',
            'es_HN' => 'Spanish (Honduras)',
            'es_MX' => 'Spanish (Mexico)',
            'es_NI' => 'Spanish (Nicaragua)',
            'es_PA' => 'Spanish (Panama)',
            'es_PY' => 'Spanish (Paraguay)',
            'es_PE' => 'Spanish (Peru)',
            'es_ES' => 'Spanish (Spain)',
            'es_US' => 'Spanish (United States)',
            'es_UY' => 'Spanish (Uruguay)',
            'es_VE' => 'Spanish (Venezuela)',
            'sw_KE' => 'Swahili (Kenya)',
            'sw_TZ' => 'Swahili (Tanzania)',
            'ss_ZA' => 'Swati (South Africa)',
            'sv_FI' => 'Swedish (Finland)',
            'sv_SE' => 'Swedish (Sweden)',
            'tl_PH' => 'Tagalog (Philippines)',
            'tg_TJ' => 'Tajik (Tajikistan)',
            'ta_IN' => 'Tamil (India)',
            'tt_RU' => 'Tatar (Russia)',
            'te_IN' => 'Telugu (India)',
            'th_TH' => 'Thai (Thailand)',
            'bo_CN' => 'Tibetan (China)',
            'bo_IN' => 'Tibetan (India)',
            'tig_ER' => 'Tigre (Eritrea)',
            'ti_ER' => 'Tigrinya (Eritrea)',
            'ti_ET' => 'Tigrinya (Ethiopia)',
            'ts_ZA' => 'Tsonga (South Africa)',
            'tn_ZA' => 'Tswana (South Africa)',
            'tr_CY' => 'Turkish (Cyprus)',
            'tr_TR' => 'Turkish (Turkey)',
            'tk_TM' => 'Turkmen (Turkmenistan)',
            'ug_CN' => 'Uighur (China)',
            'uk_UA' => 'Ukrainian (Ukraine)',
            'hsb_DE' => 'Upper Sorbian (Germany)',
            'ur_PK' => 'Urdu (Pakistan)',
            'uz_UZ' => 'Uzbek (Uzbekistan)',
            've_ZA' => 'Venda (South Africa)',
            'vi_VN' => 'Vietnamese (Vietnam)',
            'wa_BE' => 'Walloon (Belgium)',
            'cy_GB' => 'Welsh (United Kingdom)',
            'fy_DE' => 'Western Frisian (Germany)',
            'fy_NL' => 'Western Frisian (Netherlands)',
            'wo_SN' => 'Wolof (Senegal)',
            'xh_ZA' => 'Xhosa (South Africa)',
            'yi_US' => 'Yiddish (United States)',
            'yo_NG' => 'Yoruba (Nigeria)',
            'zu_ZA' => 'Zulu (South Africa)'
        ];
    }
}

if (!function_exists('GenerateComission')) {
    function GenerateComission($invoice, $pay = false)
    {
        $invoice->load(
            'user',
            'unit',
            'status',
            'courses_payments_students.course_student'
        );

        $courses_students_payment = $invoice->courses_payments_students;
        $unit = $invoice->unit;
        $student = $invoice->student;
        $saler = $invoice->user;
        $status = $invoice->status;

        $title = $invoice->title;
        $titleComission = '';
        $messageComission = '';
        $enrollment = false;

        if($title == 'Enrollment Incomming'){ // If is enrollment, difernte messages and results
            $titleComission = __('Comission Payment of Enrollment');
            $messageComission = __('Comission Payment of Enrollment of :student', [ 'student' => $student->name ]);
            $quote = 1;
            $enrollment = true;
            $total_quotes = 1;
        }else{
            $quotes = explode('/',str_replace('Installment', '', $title));
            $quote = $quotes[0];
            $total_quotes = $quotes[1];
        }

        // Generrate Groups Comission
        $groups = getGroupsComission($invoice);

        foreach ($courses_students_payment as $item) {
            $course_student = $item->course_student; // Course saved for that student/invoice
            $course = $course_student->course;

            // Messages on database
            if(empty($titleComission)){
                $titleComission = __('Comission Payment of installment');
                $messageComission = __('Comission Payment of :course from :student', [ ':course' => $course_student->title, 'student' => $student->name ]);
            }


            // Get Comission of group filtered by course total
            $comission = (isset($groups[$course->id]))? $groups[$course->id] : null;

            $comission_value = $invoice->value;
            $comission_payed = null;

            // Fixed comission on course
            if(!empty($course_student->comission_percentage)){
                $comission_payed = ($invoice->value * ($course_student->comission_percentage/100));
            }

            // If doesn't have comission from group neither comission on course, then off
            if(is_null($comission) && empty($course_student->comission_percentage)){
                return false;
            }

            $payed_at = null;
            if($pay){
                $payed_at = $invoice->getAttributes()['payed_at'];
                if(!is_null($comission) && is_null($comission_payed)){
                    if($comission->type == 'percentage'){
                        $comission_payed = ($comission_value * ($comission->value/100));
                    } else {
                        $comission_payed = $comission->value;
                    }
                }
            }

            // Array of data to insert in comissions
            $dataInsert = [
                'saler_id' => $saler->id,
                'course_payment_student_id' => $item->id,
                'unit_id' => $unit->id,
                'payed_sale' => $course_student->id,
            ];
            $dataInsert2 = [
                'title' => $titleComission,
                'description' => $messageComission,
                'payed_quote' => $quote,
                'enrollment' => $enrollment,
                'total_quotes' => $total_quotes,
                'pay_at' => $invoice->getAttributes()['expire_at'],
                'payed_at' => $payed_at,
                'value' => $comission_value,
                'value_payed' => $comission_payed
            ];

            $comission = ComissionsPayments::firstOrCreate($dataInsert, $dataInsert2);
            if(!$comission->wasRecentlyCreated){
                $comission = ComissionsPayments::updateOrCreate($dataInsert, $dataInsert2);
            }
        }
    }
}

if (!function_exists('getGroupsComission')) {
    function getGroupsComission($invoice)
    {
        \DB::enableQueryLog();
        \DB::flushQueryLog();

        // Get all sales of the invoice user
        $saler = $invoice->user->load('sales_courses_students');
        $courses_sales = [];
        foreach ($saler->sales_courses_students as $course) {
            if(!isset($courses_sales[$course->course_id])){
                $courses_sales[$course->course_id] = 0;
            }
            $courses_sales[$course->course_id] ++;
        }

        $comission = [];

        if(!empty($courses_sales)){
            foreach ($invoice->courses_payments_students as $item) {
                $item->course_student->load('course');
                $course = $item->course_student->course; // Course saved for that student/invoice

                $course_id = $course->id;
                $course->load([
                    'comission_groups' => function($query) use($courses_sales, $course_id){
                        $query
                            ->where('initial_quantity', '<=', $courses_sales[$course_id])
                            ->where('final_quantity', '>=', $courses_sales[$course_id])
                        ;
                    }
                ]);
                $comission_group = $course->comission_groups
                    ->first();

                if($comission_group){
                    $comission[$course->id] = $comission_group;
                }
            }
        }

        return $comission;
    }
}
