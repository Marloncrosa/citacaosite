<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InternalSaleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'payment_entity' => 'required',
            'payment_status' => 'required',
            'course_id' => 'required',
            'student.name' => 'required',
//            'student.email' => 'required|unique:student,email',
            'student.cellphone' => 'required|unique:student,email',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'type.required' => __("validation.required", ['attribute' => 'Type']),
            'payment_entity.required' => __("validation.required", ['attribute' => 'Type Entity']),
            'payment_status.unique' => __("validation.unique", ['attribute' => 'Status']),
            'course_id.required' => __("validation.required", ['attribute' => 'Course']),

            'title.required' => __("validation.required", ['attribute' => 'Title']),
            'description.required' => __("validation.required", ['attribute' => 'Description']),

            'student.name.required' => __("validation.required", ['attribute' => 'Nome']),
            'student.email.required' => __("validation.required", ['attribute' => 'E-mail']),
            'student.email.unique' => __("validation.unique", ['attribute' => 'E-mail']),
            'student.cellphone.required' => __("validation.required", ['attribute' => 'Telefone']),
            'student.cellphone.unique' => __("validation.unique", ['attribute' => 'Telefone']),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
