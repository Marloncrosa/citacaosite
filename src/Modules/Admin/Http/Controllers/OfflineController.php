<?php namespace Modules\Admin\Http\Controllers;


class OfflineController extends Controller
{

    /**
     * Escreve o metodo do Controller - Padrão Index
     * @return [type] [description]
     */
    public function index()
    {
        $this->template = 'offline.index';
        return $this->renderizar();
    }
}
