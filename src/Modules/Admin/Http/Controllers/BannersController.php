<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\Banners as model;

class BannersController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = 'Gerenciamento de Banners';
        $this->view_vars['page']['opcional'] = 'Banners';

        // Botões
        $this->view_vars['button']['add']['title'] = 'Cadastrar Novo Banner';
        $this->view_vars['button']['add']['class'] = 'warning open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = 'banners/create';
    }

}
