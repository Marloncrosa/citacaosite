<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\Testimony as model;

class TestimonyController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __('Gerenciar Depoimentos');
        $this->view_vars['page']['opcional'] = __('Depoimentos');

        // Botões
        $this->view_vars['button']['add']['title'] = __('Cadastrar Depoimento');
        $this->view_vars['button']['add']['class'] = 'warning open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = 'testimony/create';
    }

}
