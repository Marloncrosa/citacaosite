<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\ServiceCharacteristics as model;

class ServiceCharacteristicsController extends Controller
{

    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = 'Gerenciamento das características dos serviços';
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this->view_vars['page']['opcional'] = 'Caracteristica';

        // Botões
        $this->view_vars['button']['add']['title'] = __('Cadastrar Nova Característica');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = 'service_characteristics/create';

        $this->pagina = "service_characteristics";
        $this->view_vars['pagina'] = $this->pagina;
    }
}
