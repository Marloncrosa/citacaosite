<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\Country as model;

class CountryController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Pega a model atual
        $model = new $this->config->model;

        $this->view_vars['button']['add']['title'] = __('Novo Cadastro');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = "country/create";


        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __("Gerenciamento de Países");
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this->view_vars['page']['opcional'] = __("Países");
        $this->view_vars['page']['urlbread'] = "country/";
    }

    public function DatablesFilter($listagem)
    {
        $listagem = $listagem->filter(function ($query) { // this method disables original column search feature
            if (request()->has('created_at') && !empty(request()->created_at)) {
                $query->whereRaw(" created_at >= '" . formataDataHoraMysql(request()->created_at) . "'");
            }

            if (request()->has('origin_id') && !empty(request()->origin_id)) {
                $query->where("origin_id", request()->origin_id );
            }
            if (request()->has('course_id') && !empty(request()->course_id)) {
                $query->where("course_id", request()->course_id );
            }
            if (request()->has('operator_id') && !empty(request()->operator_id)) {
                $query->where("operator_id", request()->operator_id );
            }
            if (request()->has('last_interaction') && !empty(request()->last_interaction)) {
//                $query->where("operator_id", request()->operator_id );
            }

        }, true);

        return $listagem; // TODO: Change the autogenerated stub
    }

}
