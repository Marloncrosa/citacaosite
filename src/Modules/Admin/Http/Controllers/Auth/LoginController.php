<?php

namespace Modules\Admin\Http\Controllers\Auth;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\ValidationException;
use Modules\Admin\Entities\Users;
use Modules\Admin\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
      */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectAfterLogout;
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guestAdmin', ['except' => 'logout']);

        $this->middleware(function ($request, $next) {
            $this->handleS();
            return $next($request);
        });
    }

    /**
     * Manipula antes de executar a action que foi chamada
     */
    protected function handleS()
    {
        $this->redirectAfterLogout = route('admin.home');
        $this->redirectTo = route('admin.anyroute', [ '/' ]);

        parent::handleS();
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('admin::auth.login');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        try {
            $this->validateLogin($request);

            // If the class is using the ThrottlesLogins trait, we can automatically throttle
            // the login attempts for this application. We'll key this by the username and
            // the IP address of the client making these requests into this application.
            if ($this->hasTooManyLoginAttempts($request)) {
                $this->fireLockoutEvent($request);

                return $this->sendLockoutResponse($request);
            }

            if ($this->attemptLogin($request)) {
                return $this->sendLoginResponse($request);
            }
            // If the login attempt was unsuccessful we will increment the number of attempts
            // to login and redirect the user back to the login form. Of course, when this
            // user surpasses their maximum number of attempts they will get locked out.
            $this->incrementLoginAttempts($request);

            return $this->sendFailedLoginResponse($request);
        } catch (\Exception $e) {
            Log::alert($e);

            throw ValidationException::withMessages([
                'not' => [
                    __('Não é possível conectar')
                ],
            ]);
        }
    }

    /**
     * Get the maximum number of attempts to allow.
     *
     * @return int
     */
    public function maxAttempts()
    {
        return 30;
    }

    /**
     * Get the number of minutes to throttle for.
     *
     * @return int
     */
    public function decayMinutes()
    {
        return 1;
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate(
            $request,
            [
                'login' => 'required|string',
                'password' => 'required|string',
            ],
            [
                'login.required' => __('validation.required', ['attribute' => __("E-mail")]),
                'login.string' => __('validation.string', ['attribute' => __("E-mail")]),
                'password.required' => __('validation.required', ['attribute' => __("Senha")]),
                'password.string' => __('validation.string', ['attribute' => __("Senha")]),
            ]
        );
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return (
            $this->guard()->attempt([
                'login' => $request->login,
                'password' => $request->password,
                'active' => true
            ], $request->input('mantenhaconectado', false))
            ||
            $this->guard()->attempt([
                'email' => $request->login,
                'password' => $request->password,
                'active' => true
            ], $request->input('mantenhaconectado', false))
        );
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);

        $sessionId = \Session::getId();
//        app('db')->table('sessions')->where('id', $sessionId)
//            ->update([
//            'user_id' => $this->guard()->user()->id
//        ]);
//        app('db')->table('sessions')->where('id', $sessionId)
//            ->update([
//            'user_type' => Users::class
//        ]);

        // if it's logged
        // $user = auth('admin')->user()->load('language');
        // if(!is_null($user->language) && $user->language->shortname != session('locale')){
            // \URL::defaults(['lang' => $user->language->shortname]);
            // $this->redirectAfterLogout = route('admin.home');
            // $this->redirectTo = route('admin.anyroute', [ '/' ]);
        // }

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {        // Pega a url que ele pretende ir e valida se tem o idioma
        // $sessionIntend = session('url.intended');
        //
        // $this->redirectTo = (session()->has('url.intended') && !empty(session('url.intended')))? $sessionIntend : '/';
        //
        // session()->forget('url.intended');
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    // public function redirectPath()
    // {
    //     if (method_exists($this, 'redirectTo')) {
    //         return $this->redirectTo();
    //     }
    //
    //     return property_exists($this, 'redirectTo') ? $this->redirectTo : '/minha-conta';
    // }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        // Validate through user
        $user = Users::where('email', $request->login)
            ->orWhere('login', $request->login)
            ->first();

        // Is inactive?
        if(!$user->active){
            throw ValidationException::withMessages([
                'active' => [
                    __('User is not active')
                ],
            ]);
        }

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'nome';
    }

    /**
     * Logout, Clear Session, and Return.
     *
     * @return void
     */
    public function logout(Request $request)
    {
        $errors = $request->session()->get('errors');

        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerate();

        $request->flash('errors', $errors);

        if (!empty($errors)) {
            return redirect()->route('admin.login')->with('errors', $errors);
        }

        return redirect()->route('admin.login');
    }


    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws ValidationException
     */
    //  protected function sendFailedLoginResponse(Request $request)
    //  {
    //      // Verifica qual tipo de mensagem será enviada
    //      $user = Users::withoutGlobalScopes()->where("cemail", $request->email)->first();
    //      // Verifica se usuário está ativo
    //     if (is_null($user) || !isset($user->bativo) || $user->bativo == true) { // Se sim, erro de senha ou e-mail inválido
    //         throw ValidationException::withMessages([
    //             $this->username() => [trans('auth.failed')],
    //         ]);
    //     } else { // Caso não esteja ativo
    //         throw ValidationException::withMessages([
    //             $this->username() => [__("Usuário inativo, para maiores informações entre em contato com um administrador.")],
    //         ]);
    //     }
    // }
}
