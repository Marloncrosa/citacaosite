<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\Newsletter as model;

class NewsletterController extends Controller {
    public function init()
    {
        // Model
        $this -> config -> model = model::class;

        // Titulos do breadcrumb
        $this -> view_vars['page']['title'] = __('Gerenciamento da Newsletter');
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this -> view_vars['page']['opcional'] = __('Newsletter');

        // Botões
        // $this -> view_vars['button']['add']['title'] = 'Novo Registro';
        // $this -> view_vars['button']['add']['class'] = 'warning';
        // $this -> view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        // $this -> view_vars['button']['add']['link'] = 'dicas/create';

        $this->pagina                   = "newsletter";
        $this->view_vars['pagina']      = $this->pagina;
    }
}
