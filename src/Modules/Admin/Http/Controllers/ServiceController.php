<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\Service as model;
use Modules\Admin\Entities\ServiceCharacteristics;

class ServiceController extends Controller
{

    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = 'Gerenciamento dos serviços';
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this->view_vars['page']['opcional'] = 'Serviços';

        // Botões
        $this->view_vars['button']['add']['title'] = __('Cadastrar Novo Serviço');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = 'service/create';

        $this->view_vars['caracteristicas'] = ServiceCharacteristics::where('active', true)->get();

        $this->pagina = "service";
        $this->view_vars['pagina'] = $this->pagina;
    }
}
