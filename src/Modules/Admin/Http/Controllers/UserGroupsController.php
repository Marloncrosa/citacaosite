<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\PermissionsGroup;
use Modules\Admin\Entities\Users as model2;
use Modules\Admin\Entities\UserPermissionsGroup as model;

class UserGroupsController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Valida se foi passado o user_id
        if (!isset($this->vars_class['user_id'])) {
            return redirect()->to('users/')->withErrors(__('User not found'));
        }

        // Recupera o id
        $user_id = $this->vars_class['user_id'][0];

        // Pega a model atual
        $model = new $this->config->model;

        // Pegar model secundaria
        $model2 = new model2();

        // Pega dados atuais
        $item = $model2->where('id', $user_id)->first();

        if (auth('admin')->user()->canpermission('Delegar grupo de permissão')) {
            $this->view_vars['button']['add']['title'] = __('Novo Registro');
            $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
            $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
            $this->view_vars['button']['add']['link'] = "user_groups/create/user_id/{$user_id}";
        }

        $this->view_vars['page']['ant-title-array']['users']['url'] = "users/";
        $this->view_vars['page']['ant-title-array']['users']['pagina'] = "Users";

        $this->view_vars['page']['ant-title-array']['course']['url'] = "users/edit/id/{$user_id}";
        $this->view_vars['page']['ant-title-array']['course']['class'] = "open-form-sidebar";
        $this->view_vars['page']['ant-title-array']['course']['pagina'] = "{$item->name}";

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __("Gerenciamento dos Grupos de Permissão");
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this->view_vars['page']['opcional'] = __("Grupos de Permissão");
        $this->view_vars['page']['urlbread'] = "user_groups/index/user_id/{$user_id}";

        $this->view_vars['groups'] = PermissionsGroup::all();
    }

    public function firstOrderList($list, $model = null)
    {
        if ($this->request->has('draw') && $this->request->draw == 1) {
            $list->orderBy($model->getTable() . '.permissions_group_id', 'desc');
        }

        return $list;
    }

    //    public function beforeDatatables($list)
    //    {
    //        $user_id = $this->vars_class['user_id'][0];
    //        $this->view_vars['user_id'] = $user_id;
    //
    //        $list = $list->whereHas('users', function($query) use($user_id){
    //            $query->where('user_id', $user_id);
    //        });
    //
    //
    //        return $list;
    //    }

    public function beforeinit()
    {
        $user_id = $this->vars_class['user_id'][0];
        $this->view_vars['user_id'] = $user_id;

        // Where da listagem
        $this->config->listwhere = [
            'user_id' => [
                'user_id',
                '=',
                $user_id
            ]
        ];

        // Caso seja algum post para o store ou update
        if ($this->action == 'create' || $this->action == 'edit') {
            // Pagina
            $this->view_vars['urlform'] = route('admin.anyroute', [ "user_groups/" . (($this->action == 'create') ? 'store' : 'update') . "/user_id/{$user_id}"]);

            if ($this->action == 'edit') {
                $this->view_vars['urlform'] .= '/' . $this->config->pk . '/' . $this->vars_class[$this->config->pk][0];

            }
            $this->view_vars['pagina'] = "user_groups/index/user_id/{$user_id}";

            // Campo hidden
            $this->view_vars['campohidden'] = 'user_id';
            $this->view_vars['valorhidden'] = $user_id;
        }

        // Caso seja algum post para o store ou update
        if ($this->action == 'store' || $this->action == 'update' || $this->action == 'delete') {
            // Pagina
            $this->pagina = route('admin.anyroute', [ session('locale'), "user_groups/index/user_id/{$user_id}" ]);
            $this->view_vars['pagina'] = $this->pagina;
        }
    }

    public function deleteajax()
    {

        if (!empty($this->config->permissions)) {
            if (!$this->user->canpermission($this->config->permissions->delete)) {
                return response()->json([
                    'type' => 'error',
                    'message' => 'You do not have permission for this action.',
                ]);
            }
        }

        // Dados gerais
        $model = $this->config->model;
        $chavepm = $this->config->pk;

        // Para cada item execute um deletar
        $items = $model::where('user_id', $this->vars_class['user_id'][0])
            ->where('permissions_group_id', $this->vars_class['permissions_group_id'][0])
            ->get();

        // Se encontrou o item
        if ($items) {
            foreach ($items as $item) {
                $q = 'DELETE FROM user_permissions_group where user_id = "'.$this->vars_class['user_id'][0].'" AND permissions_group_id = "'.$this->vars_class['permissions_group_id'][0].'"';
                \DB::delete($q);
                // dd(
                //     $item->delete()
                // );
                // $item->delete();
            }
        }

        return response()->json([
            'type' => 'success',
            'message' => 'item excluído com sucesso.',
        ]);
    }

}
