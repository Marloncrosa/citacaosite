<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\Portfolio as model2;
use Modules\Admin\Entities\PortfolioGallery as model;

class PortfolioGalleryController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Valida se foi passado o product_id
        if (!isset($this->vars_class['portfolio_id'])) {
            return redirect()->to('portfolio/')->withErrors('Portfólio não encontrado.');
        }

        // Recupera o id
        $portfolio_id = $this->vars_class['portfolio_id'][0];

        // Pega a model atual
        $model = new $this->config->model;

        // Pegar model secundaria
        $model2 = new model2();

        // Pega dados atuais
        $item = $model2->where('id', $portfolio_id)->first();

        $this->view_vars['button']['import']['title'] = 'Adicionar imagens';
        $this->view_vars['button']['import']['class'] = 'warning open-form-sidebar';
        $this->view_vars['button']['import']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['import']['link'] = "portfolio_gallery/create/portfolio_id/{$portfolio_id}";

        // Evento
        $this->view_vars['page']['ant-title-array']['noticias']['url'] = "noticias/";
        $this->view_vars['page']['ant-title-array']['noticias']['pagina'] = "Portfólio";

        $this->view_vars['page']['ant-title-array']['noticia']['url'] = "noticias/edit/id/{$portfolio_id}";
        $this->view_vars['page']['ant-title-array']['noticia']['pagina'] = "{$item->title}";

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = "Gerenciamento de Fotos do Portfólio: {$item->title}";
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this->view_vars['page']['opcional'] = 'Fotos';
        $this->view_vars['page']['urlbread'] = "portfolio_gallery/index/portfolio_id/{$portfolio_id}";
    }

    public function beforeinit()
    {
        // Id do salão
        $portfolio_id = $this->vars_class['portfolio_id'][0];
        $this->view_vars['portfolio_id'] = $portfolio_id;

        // Where da listagem
        $this->config->listwhere = [
            'portfolio_id' => [
                'portfolio_id',
                '=',
                $portfolio_id
            ]
        ];

        //hack para pegar o id do pai
//        $this->config->colunas['imagem']['uploadpath'] = "noticias/{$portfolio_id}/";

        // Caso seja algum post para o store ou update
        if ($this->action == 'create' || $this->action == 'edit') {
            // Pagina
            $this->view_vars['urlform'] = "portfolio_gallery/" . (($this->action == 'create') ? 'store' : 'update') . "/portfolio_id/{$portfolio_id}";

            if ($this->action == 'edit') {
                $this->view_vars['urlform'] .= '/' . $this->config->pk . '/' . $this->vars_class[$this->config->pk][0];
            }
            $this->view_vars['pagina'] = "portfolio_gallery/index/portfolio_id/{$portfolio_id}";

            // Campo hidden
            $this->view_vars['campohidden'] = 'portfolio_id';
            $this->view_vars['valorhidden'] = $portfolio_id;
        }

        // Caso seja algum post para o store ou update
        if ($this->action == 'store' || $this->action == 'update' || $this->action == 'delete') {
            // Pagina
            $this->pagina = "portfolio_gallery/index/portfolio_id/{$portfolio_id}";
            $this->view_vars['pagina'] = $this->pagina;
        }

        // Caso seja algum post para o store ou update
        if ($this->action == 'upload') {
            // Pagina
            $this->pagina = "portfolio_gallery";
            $this->view_vars['pagina'] = $this->pagina;
        }
    }

    /**
     * Listagem de fotos e upload
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function upload()
    {
        // Extras
        $portfolio_id = $this->vars_class['portfolio_id'][0];

//        $this->config->colunas['imagem']['uploadpath'] = "noticias/{$portfolio_id}/";

        return $this->multiploupload($campopai = 'portfolio_id', $campo = 'imagem', $pagina = "admin/portfolio_gallery/upload/portfolio_id/{$portfolio_id}", $maxfilesize = '100' );
    }


}
