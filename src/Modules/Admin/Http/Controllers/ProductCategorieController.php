<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\Permissions;
use Modules\Admin\Entities\ProductsCategories as model;

class ProductCategorieController extends Controller
{

    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Botões
        $this->view_vars['button']['add']['title'] = __('Novo registro');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = '';
        $this->view_vars['button']['add']['link'] = 'product_categorie/create';

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __('Gerenciamento Categoria de Produtos');
        // $this->view_vars['page']['subtitle'] = 'Gerencie os usuários do admin';
        $this->view_vars['page']['opcional'] = __('Categoria de Produtos');
        // $this -> view_vars['page']['urlbread'] 	= "/saloesconveniencias/index/idsalao/{$idsalao}";

        $this->view_vars['page']['urlbread'] = route('admin.anyroute', ["product_categorie/index/"]);

        if (isset($this->vars_class['parent_id'])) {
            $parent_id = $this->vars_class['parent_id'][0];
            $this->view_vars['parent_id'] = $parent_id;

            $item2 = model::where('id', $parent_id)->with('recursive_parent')->first();

            $this->view_vars['page']['ant-title-array']['account']['url'] = "product_categorie/index";

            if (!is_null($item2->recursive_parent)) {
                $this->view_vars['page']['ant-title-array']['account']['url'] .= "/parent_id/{$item2->recursive_parent->id}";
            }

            $this->view_vars['page']['ant-title-array']['account']['pagina'] = __('Categoria de Produtos - Anterior');

            $this->view_vars['page']['ant-title-array']['plan']['url'] = "product_categorie/edit/id/{$item2->id}";
            $this->view_vars['page']['ant-title-array']['plan']['class'] = "open-form-sidebar";
            $this->view_vars['page']['ant-title-array']['plan']['pagina'] = "{$item2->parent_code} - {$item2->title}";

            $this->view_vars['button']['add']['link'] = "product_categorie/create/parent_id/{$parent_id}";

            $this->view_vars['page']['urlbread'] = route('admin.anyroute', ["product_categorie/index/parent_id/{$parent_id}"]);
        }
    }

    public function beforeinit()
    {
        if(isset($this->vars_class['parent_id'])){
            $parent_id = $this->vars_class['parent_id'][0];
            $this->view_vars['parent_id'] = $parent_id;

            // Where da listagem
            $this->config->listwhere = [
                'parent_id' => [
                    'parent_id',
                    '=',
                    $parent_id
                ]
            ];
        }else{
            $parent_id = null;

            // Where da listagem
            $this->config->listwhere = [
                'parent_id' => [
                    'parent_id',
                    '=',
                    null
                ]
            ];
        }
        //hack para pegar o id do pai
//        $this->config->colunas['imagem']['uploadpath'] = "noticias/{$idnoticia}/";

        // Caso seja algum post para o store ou update
        if ($this->action == 'create' || $this->action == 'edit') {
            // Pagina
            $this->view_vars['urlform'] = route('admin.anyroute', ["product_categorie/" . (($this->action == 'create') ? 'store' : 'update')]);

            if ($this->action == 'edit') {
                $this->view_vars['urlform'] .= '/' . $this->config->pk . '/' . $this->vars_class[$this->config->pk][0];

            }
            $this->view_vars['pagina'] = "product_categorie/";

            if(isset($this->vars_class['idcomentario'])){
                $this->view_vars['pagina'] .= "/parent_id/{$parent_id}";

                $this->view_vars['urlform'] .= "/parent_id/{$parent_id}";
            }

            // Campo hidden
            $this->view_vars['campohidden'] = 'parent_id';
            $this->view_vars['valorhidden'] = $parent_id;

        }

        // Caso seja algum post para o store ou update
        if ($this->action == 'store' || $this->action == 'update' || $this->action == 'delete') {
            // Pagina
            $this->pagina = "product_categorie";
            $this->view_vars['pagina'] = $this->pagina;

            if(isset($this->vars_class['parent_id'])){
                $this->view_vars['pagina'] .= "/parent_id/{$parent_id}";
            }
        }
    }

}
