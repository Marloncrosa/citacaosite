<?php namespace Modules\Admin\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request as Urlrequest;
use Illuminate\Support\Facades\Route;

use Modules\Admin\Entities\Language;
use Modules\Admin\Http\Middleware\Permission;
use Validator;

use DataTables;
use Yajra\DataTables\Html\Builder;

use Response;
use Collective\Html\FormBuilder as Form;
use Collective\Html\HtmlBuilder as Html;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Configuracao
     *
     * @var
     */
    protected $config;

    /**
     * Request
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Routes
     *
     * @var \Illuminate\Http\Route
     */
    protected $rotas;

    /**
     * Action
     *
     * @var Armazena a action atual
     */
    protected $action;

    /**
     * Controller
     *
     * @var Armazena a controller atual
     */
    protected $controller;

    /**
     * Pagina, página atual que o usuario se encontra
     *
     * @var Armazena a controller atual
     */
    protected $pagina;

    /**
     * Array de dados que será passado para o template
     *
     * @var array
     */
    protected $view_vars = [];

    /**
     * Array de dados que será passado para o template
     *
     * @var array
     */
    protected $vars_class = [];

    /**
     * Datatables Html Builder
     * @var Builder
     */
    protected $listBuilder;

    /**
     * Para setar um template dinamico
     * @var Builder
     */
    protected $template;

    protected $user;

    /**
     * Construtor
     *
     * @param Request $request
     */
    // function __construct(Request $request, Route $rotas, Builder $listBuilder)
    function __construct(Request $request, Route $rotas, Builder $listBuilder)
    {
        $this->middleware(function ($request, $next) use ($rotas, $listBuilder) {
            $this->handleS($request, $rotas, $listBuilder);
            $this->handleInernal($listBuilder);
            return $next($request);
        });
    }

    protected function handleS()
    {
        $this->request = request();
        $this->rotas = app('router');

        Auth::setDefaultDriver('admin');

        // Parametros adicionais dinamicos vindos da url
        foreach (Urlrequest::segments() as $key => $value) {
            if ($key > 2) {
                $this->vars_class[Urlrequest::segment($key)][] = Urlrequest::segment($key + 1);
            }/*else{
				$this -> vars_class[ Urlrequest::segment($key) ][] = $value;
			}*/
        }
        //         dump($this -> vars_class);

        // $totalparam = count(Urlrequest::segments());
        // // $this -> vars_class = Urlrequest::segments();
        // for ($i=0; $i < $totalparam; $i++) {
        // 	if( $i >= 4 ){
        // 		$this -> vars_class[ Urlrequest::segment($i) ][] = Urlrequest::segment($i+1);
        // 		$i++;
        // 	}
        // }
        // dump($this -> vars_class);
        // dd();

        /**
         * Pega o nome da action e controller
         */
        if (!is_null(Route::current())) {
            list($caminho, $action) = explode('@', Route::current()->getActionName());
            $caminho = explode('\\', $caminho);
            $controller = end($caminho);
            $this->action = $action;
            $this->controller = $controller;
        }

        $this->pagina = str_replace("_controller", "", snake_case($this->controller));

        $this->view_vars['action'] = $this->action;
        $this->view_vars['controller'] = $this->controller;
        $this->view_vars['pagina'] = $this->pagina;
    }

    protected function handleInernal(Builder $listBuilder)
    {
        // Ativa os macros
        $this->macros();

        // Inicia a configuração
        $this->config = new \stdClass;
        $this->config->paginate = 10;

        // Valida páginação
        if (!empty($this->vars_class['pagination'])) {
            $this->config->paginate = $this->vars_class['pagination'][0];
        } else {
            $this->config->paginate = 10;
        }

        $this->user = Auth::guard('admin')->user();
        $this->view_vars['usuario'] = $this->user;

        // List builder
        $this->listBuilder = $listBuilder;

        // Chama a função de inicialização
        $this->init();

        // Chama a função
        $this->AtualizaModelConfig();

        // Chama a função de inicialização
        $this->beforeinit();


    }

    protected function macros()
    {
        // Form macro para label com html e atributos
        Form::macro('labelHTML', function ($name, $value = null, $options = array()) {
            $label = "<label for=\"{$name}\" ";

            foreach ($options as $key => $val) {
                $label .= "{$key}=\"{$val}\" ";
            }

            $label .= ">{$value}</label>";

            return $label;
        });

        // Form macro para file
        Form::macro('fileHTML', function ($name, $value = null, $options = array()) {
            $input = '<input type="file" name="' . $name . '" value="' . $value . '"';

            foreach ($options as $key => $value) {
                $input .= ' ' . $key . '="' . $value . '"';
            }

            $input .= '>';

            return $input;
        });

        // Form macro para status
        Form::macro('status', function ($name, $valor = null, $elemento1 = null, $elemento2 = null) {
            $input = "";
            $input .= "
					<fieldset class=\"fieldset\">
				";

            // Sim
            if (!is_null($elemento1)) {
                $input .= "
								<div class=\"small-12 medium-5 large-3 column\">
									<div class=\"small-4 float-left\">
										<div class=\"switch\">
											<input type=\"radio\" value=\"1\"
												name=\"{$name}\"
												id=\"" . ((isset($elemento1[id])) ? $elemento1[id] : 'item' . $name . 'ativado') . "\"
												" . (!is_null($valor) && $valor ? 'checked=\"checked\"' : '') . "
												class=\"switch-input\"
												" . (isset($elemento1[atributos]) ? implode(' ', $elemento1[atributos]) : '') . "
											>

											<label class=\"switch-paddle\" for=\"" . ((isset($elemento1[id])) ? $elemento1[id] : 'item' . $name . 'ativado') . "\">
												<span class=\"show-for-sr\">Status?</span>
											</label>
										</div>
									</div>
									<div class=\"small-6 columns end\">
										<label class=\"text-left\" for=\"" . ((isset($elemento1[id])) ? $elemento1[id] : 'item' . $name . 'ativado') . "\">
											Ativado
										</label>
									</div>
								</div>
						";
            } else {
                $input .= "
								<div class=\"small-12 medium-5 large-3 column\">
									<div class=\"small-4 float-left\">
										<div class=\"switch\">
											<input type=\"radio\" value=\"1\" name=\"{$name}\" id=\"{$name}ativado\" " . ((!is_null($valor) && $valor) ? 'checked=\"checked\"' : '') . " class=\"switch-input\" required=\"true\">
											<label class=\"switch-paddle\" for=\"{$name}ativado\">
												<span class=\"show-for-sr\">Status?</span>
											</label>
										</div>
									</div>
									<div class=\"small-6 columns end\">
										<label class=\"text-left\" for=\"{$name}ativado\">Ativado</label>
									</div>
								</div>
						";
            }

            // Não
            if (!is_null($elemento2)) {
                $input .= "
								<div class=\"small-12 medium-5 large-3 column\">
									<div class=\"small-4 float-left\">
										<div class=\"switch\">
											<input type=\"radio\" value=\"1\"
												name=\"{$name}\"
												id=\"" . ((isset($elemento2[id])) ? $elemento2[id] : 'item' . $name . 'desativado') . "\"
												" . (!is_null($valor) && !$valor ? 'checked=\"checked\"' : '') . "
												class=\"switch-input\"
												" . (isset($elemento2[atributos]) ? implode(' ', $elemento2[atributos]) : '') . "
											>

											<label class=\"switch-paddle\" for=\"" . ((isset($elemento2[id])) ? $elemento2[id] : 'item' . $name . 'desativado') . "\">
												<span class=\"show-for-sr\">Status?</span>
											</label>
										</div>
									</div>
									<div class=\"small-6 columns end\">
										<label class=\"text-left\" for=\"" . ((isset($elemento2[id])) ? $elemento2[id] : 'item' . $name . 'desativado') . "\">
											Desativado
										</label>
									</div>
								</div>
						";
            } else {
                $input .= "
								<div class=\"small-12 medium-5 large-3 column end\">
									<div class=\"small-4 float-left\">
										<div class=\"switch\">
											<input type=\"radio\" value=\"0\" name=\"{$name}\" id=\"{$name}desativado\" " . ((!is_null($valor) && !$valor) ? 'checked=\"checked\"' : '') . " class=\"switch-input\" required=\"true\">
											<label class=\"switch-paddle\" for=\"{$name}desativado\">
												<span class=\"show-for-sr\">Status?</span>
											</label>
										</div>
									</div>
									<div class=\"small-6 columns end\">
										<label class=\"text-left\" for=\"{$name}desativado\">Desativado</label>
									</div>
								</div>
						";
            }

            $input .= "
					</fieldset>
				";
            return $input;
        });

        // Form macro para campos booleanos
        Form::macro('statuscor', function ($name, $valor = null, $elemento1 = null) {
            $input = "";
            // $input .= "
            // 	<fieldset class=\"fieldset\">
            // ";

            if (!is_null($elemento1)) {
                $input .= "
                    <span class=\"m-switch m-switch--lg m-switch--icon\">
                        <input type=\"hidden\" value=\"0\"
                            name=\"{$name}\"
                            id=\"" . ((isset($elemento1[id])) ? $elemento1[id] : 'item' . $name . 'desativado') . "\"
                            " . (isset($elemento1[atributos]) ? implode(' ', $elemento1[atributos]) : '') . "
                        >
                        <label>
                            <input type=\"checkbox\" value=\"1\"  data-switch=\"true\"  data-on-color=\"primary\"
                                name=\"{$name}\"
                                id=\"" . ((isset($elemento1[id])) ? $elemento1[id] : 'item' . $name . 'ativado') . "\"
                                " . (!is_null($valor) && $valor ? 'checked="checked"' : '') . "
                                " . (isset($elemento1[atributos]) ? implode(' ', $elemento1[atributos]) : '') . "
                            >
                            <span></span>
                        </label>
                    </span>
              ";
            } else {
                $input .= "
                    <span class=\"m-switch m-switch--lg m-switch--icon\">
                        <input type=\"hidden\" value=\"0\" name=\"{$name}\" id=\"{$name}desativado\">
                        <label>
                            <input type=\"checkbox\" value=\"1\"  data-switch=\"true\"  data-on-color=\"primary\"
                            name=\"{$name}\"
                            id=\"{$name}ativado\" " . ((!is_null($valor) && $valor) ? 'checked="checked"' : '') . ">
                            <span></span>
                        </label>
                    </span>
                ";
            }

            // $input .= "
            // 		</fieldset>
            // 	";
            return $input;
        });

        // Form macro para Botões de salvar e cancelar
        Form::macro('botoesform', function ($pagina, $cancelar = true, $salvarecontinuar = true, $salvar = true, $limpar = true, $options = array()) {
            $limparButton = '';
            $salvarButton = '';
            $salvarecontinuarButton = '';
            $cancelarButton = '';

            // Caso o cancelar seja true
            if ($cancelar) {
                $cancelarButton = "
                    <a href=\"" . route('admin.anyroute', ['slug' => $pagina]) . "\" target='_parent' class=\"m-nav__link\">
                        <i class=\"m-nav__link-icon fas fa-times\" aria-hidden=\"true\"></i>
                        <span class=\"m-nav__link-text\">" . ((isset($options['texto-cancelar'])) ? $options['texto-cancelar'] : "Cancelar") . "</span>
                    </a>
                ";

            }
            // Caso o limpar seja true
            if ($limpar) {
                $limparButton = "
                    <label class=\"m-nav__link\">
                        <input type=\"reset\" name=\"salvarecontinuar\" value=\"sim\" class='d-none'>
                        <span class=\"m-nav__link-text\">" . ((isset($options['texto-limpar'])) ? $options['texto-limpar'] : "Limpar campos") . "</span>
                    </label>
                ";
            }

            // Caso o salvar seja true
            if ($salvar) {
                $salvarButton = "
                    <button type=\"submit\" class=\"m-nav__link\">
                        <i class=\"m-nav__link-icon far fa-save\" aria-hidden=\"true\"></i>
                        " . ((isset($options['texto-salvar'])) ? $options['texto-salvar'] : "Salvar") . "
                    </button>
                ";
            }
            // Caso o salvar seja true
            if ($salvarecontinuar) {
                $salvarecontinuarButton = "
                    <label class=\"m-nav__link\">
                        <input type=\"submit\" name=\"salvarecontinuar\" value=\"sim\" class='d-none'>
                        <i class=\"m-nav__link-icon far fa-save\" aria-hidden=\"true\"></i>
                        <span class=\"m-nav__link-text\">" . ((isset($options['texto-salvar-continuar'])) ? $options['texto-salvar-continuar'] : "Salvar e Continuar Editando") . "</span>
                    </label>
                ";
            }

            $input = '
                <div class="m-portlet__foot m-portlet__foot--fit">
                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col">
                                ' . $salvarButton . '
                                <div id="dropdown-form" class="m-dropdown m-dropdown--up m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push float-right" m-dropdown-toggle="hover" aria-expanded="true">
                                    <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--outline-2x m-btn--air m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                        <i class="la la-plus m--hide"></i>
                                        <i class="la la-ellipsis-h"></i>
                                    </a>
                                    <div class="m-dropdown__wrapper" style="z-index: 101;">
                                        <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust" style="left: auto; right: 21.5px;"></span>
                                        <div class="m-dropdown__inner">
                                            <div class="m-dropdown__body">
                                                <div class="m-dropdown__content">
                                                    <ul class="m-nav">
                                                        <li class="m-nav__section m-nav__section--first m--hide">
                                                        </li>
                                                        <li class="m-nav__item">
                                                            ' . $cancelarButton . '
                                                        </li>
                                                        <li class="m-nav__item">
                                                            ' . $limparButton . '
                                                        </li>
                                                        <li class="m-nav__item">
                                                            ' . $salvarecontinuarButton . '
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                            ';
            return $input;
        });
    }

    /**
     * Apenas para não bugar
     */
    public function init()
    {
    }

    /**
     * [AtualizaModelConfig Esta função atualiza as configurações de model a ser usada;
     */
    protected function AtualizaModelConfig()
    {
        // Existe alguma model
        if (isset($this->config->model)) {
            $model = new $this->config->model;

            // Relacionamentos
            $this->config->relacionamentos = isset($model->relacionamentos) && !is_object($model->relacionamentos) ? $model->relacionamentos : '';

            // Colunas
            // $this -> config -> colunas 	= $model -> getFillable();
            $this->config->colunas = $model->atributos;

            // Chave Primária
            $this->config->pk = $model->getKeyName();

            // Colunas hidden
            $this->config->hidden = $model->getHidden();

            if (isset($model->queryadicionallist)) {
                if (!isset($this->config->queryadicionallist)) {
                    $this->config->queryadicionallist = [];
                }
                $colecao_queryadicional = collect([]);
                $colecao_queryadicional->push($this->config->queryadicionallist);
                $colecao_queryadicional->push($model->queryadicionallist);
                $this->config->queryadicionallist = $colecao_queryadicional->collapse()->toArray();
            }

            // Coluna de ação
            $this->config->tamanho['acao'] = isset($model->acao['tamanho']) ? $model->acao['tamanho'] : '60';
            //            if (isset($model->acao['tamanho'])) unset($model->acao['tamanho']);

            $this->config->habilita_acao = true;
            if (empty($model->acao) && isset($model->acao)) {
                $this->config->habilita_acao = false;
            }
            $this->config->acao = isset($model->acao) ? $model->acao : '';

            if (!isset($this->config->permissions)) {
                $this->config->permissions = isset($model->control_permissions) ? $model->control_permissions : '';
            }
        }

    }

    public function beforeinit()
    {
    }

    /**
     * Deleta os itens, função padrão
     * @return [type] [description]
     */
    public function deleteajax()
    {
        if (!empty($this->config->permissions)) {
            if (!$this->user->canpermission($this->config->permissions->delete)) {
                return response()->json([
                    'type' => 'error',
                    'message' => 'You do not have permission for this action.',
                ]);
            }
        }

        // Dados gerais
        $model = $this->config->model;
        $chavepm = $this->config->pk;

        // Caso não seja passado a(s) PK para o deletar
        if (!isset($this->vars_class[$chavepm])) {
            return response()->json([
                'type' => 'error',
                'message' => 'Você deve informar ao menos um item para remover.',
            ]);
        }

        // Para cada item execute um deletar
        foreach ($this->vars_class[$chavepm] as $id) {
            $items = $model::where($chavepm, $id)->get();

            // Se encontrou o item
            if ($items) {
                foreach ($items as $item) {
                    $item->delete();
                }
            }
        }

        return response()->json([
            'type' => 'success',
            'message' => count($this->vars_class[$chavepm]) . ' item(ns) excluído(s) com sucesso.',
        ]);
    }

    /**
     * Deleta os itens, função padrão
     * @return [type] [description]
     */
    public function deletararquivo()
    {
        // Dados gerais
        $model = $this->config->model;
        $chavepm = $this->config->pk;
        $ids = $this->vars_class[$chavepm];
        $campos = $this->vars_class['campo'];

        // Verifica se existe algum path e coloca dentro de um array
        $patharray = array_where($this->vars_class, function ($value, $key) {
            if (strpos($key, 'path') === false) {
                return false;
            } else {
                return true;
            }
        });

        // Verifica se o array está vazio ou não
        if (!empty($patharray)) {
            $patharray = array_collapse($patharray);
            $path = implode('/admin/', $patharray);
            $pathnovo = $path;
            // $pathnovo = str_replace( '/'.last($patharray), '', $path );
            // foreach ( $patharray as $key => $value ){
            // 	$patharray[$key] = $value;
            // }
        }

        // Caso não seja passado a(s) PK para o deletar
        if (!isset($ids)) {
            return response()->json([
                'type' => 'error',
                'message' => 'Você deve informar ao menos um item para remover.',
            ]);
        }

        // Caso não esteja passando o campo que vai remover o arquivo e limpar do banco
        if (!isset($campos)) {
            return response()->json([
                'type' => 'error',
                'message' => 'Você precisa informar ao menos um campo para remoção.',
            ]);
        }

        // caso seja enviado um path, faz a alteração no config -> colunas
        if (isset($pathnovo)) {
            $this->ajustauploadpath($pathnovo);
        }

        // Para cada item execute um deletar
        foreach ($this->vars_class[$chavepm] as $id) {
            $item = $model::where($chavepm, $id)->first();

            // Percorre cada campo enviado
            foreach ($campos as $key => $value) {
                if (!empty($value) && isset($item->atributos[$value]['uploadpath'])) {
                    // Pega o caminho de upload
                    $caminho = 'uploads/' . $item->atributos[$value]['uploadpath'] . '/' . $item->$value;
                    if (\File::exists($caminho)) {
                        // Remove o arquivo anterior
                        \File::delete($caminho);
                    }
                    // Limpa o campo do banco
                    $item->$value = '';
                    $item->save();
                }
            }
        }

        // ve se é para apagar o registro
        if (isset($this->vars_class['removeregistro'])) {
            $item->delete();
        }

        return response()->json([
            'type' => 'success',
            'message' => count($this->vars_class[$chavepm]) . ' item(ns) excluído(s) com sucesso.',
        ]);
    }

    /**
     * Ajusta o path de upload
     */
    public function ajustauploadpath($pathnovo)
    {
        // Pega os campos baseado no tipo
        $campos = array_where($this->config->colunas, function ($value, $key) {
            if (isset($value['uploadpath'])) {
                return true;
            } else {
                return false;
            }
        });

        if (isset($campos)) {
            // Anda por cada campo que tem caminho de upload
            foreach ($campos as $key => $value) {
                $caminho = explode('/admin/', $value['uploadpath']);
                $ultimo = last($caminho);
                if (empty($ultimo)) {
                    $ultimo = $caminho[(count($caminho) - 2)];
                }
                $caminhonovo = $pathnovo . $ultimo;
                // $caminho[0] = $pathnovo;
                $campos[$key]['uploadpath'] = $caminhonovo;
                $this->config->colunas[$key]['uploadpath'] = $campos[$key]['uploadpath'];
            }
        }
    }

    /**
     * Escreve o metodo do Controller - Padrão Index
     * @return [type] [description]
     */
    public function index()
    {
        if (!empty($this->config->permissions)) {
            if (!$this->user->canpermission($this->config->permissions->list)) {
                return redirect()->route('admin.home')->with('error', __("Você não possui permissão de acesso"));
            }
        }

        $this->view_vars['page']['extrat'] = '';

        if (!view()->exists("admin::{$this->pagina}.list")) {
            $this->template = 'layouts.list';
        } else {
            $this->template = "{$this->pagina}.list";
        }

        return $this->renderizar();
    }

    // Antes de finalizar a listagem

    /**
     * Função para renderizar view
     * @return [type] [description]
     */
    protected function renderizar()
    {
        // Se existir alguma model
        if (isset($this->config->model)) {
            $retornof = '';
            $attr = '';

            foreach ($this->vars_class as $vkey => $vvalue) {
                // Valores da chave
                $valueskey = [];

                // Pega um ou mais valores da chave e atribui no $valueskey
                foreach ($vvalue as $key => $value) {
                    $valueskey[] = $value;
                }

                // Atribui a var attr a chave
                $attr .= $vkey . '/' . (implode(',', $valueskey)) . '/';
            }

            //            dump($this->action);
            //            dump($this->controller);
            //            dump($this->pagina);
            //            dump($attr);
            //            die;

            switch ($this->action) {
                case 'create':
                case 'edit':
                    // Desabilita o botão no breadcrumb
                    unset($this->view_vars['button']['add']);
                    // Envia para a view
                    $this->view_vars['attr'] = $attr;
                    $template = "layouts.form";
                    if (view()->exists("admin::{$this->pagina}.form")) {
                        $template = "{$this->pagina}.form";
                    }
                    break;
                case 'show':
                    break;
                case 'store':
                case 'update':
                    $template = "layouts.conclusion";
                    if (view()->exists("admin::{$this->pagina}.conclusion")) {
                        $template = "{$this->pagina}.conclusion";
                    }
                    break;
                case 'list':
                case 'index':
                case 'search':
                    $retornof = $this->listPadrao();
                    break;
                default:
                    $this->view_vars['action'] = $this->action;
                    $template = $this->action;
                    break;
            }

            if (!empty($retornof)) {
                return $retornof;
            }
        }

        if (empty($this->template)) {
            $this->template = $template;
        }

        if (!isset($this->view_vars['attr'])) {
            $this->view_vars['attr'] = '';
        }

        return $this->viewshow($this->template);
    }

    // Antes de finalizar a listagem

    /**
     * Listagem de itens. Backup
     *
     * @return \Illuminate\View\View
     */
    public function listPadrao()
    {
        // Pega dados da model atual
        // $model 		= new $this -> config -> model;
        // Valida Relacionamentos
        // if( isset($model -> relacionamentos) ){ $model -> with($model -> relacionamentos); }
        $colunas = $this->config->colunas;

        $colunas = array_where($colunas, function ($value, $key) {
            if (!isset($value['listar'])) {
                return true;
            }
            return $value['listar'] == true;
        });
        // dd( $colunas );
        // $colunas 	= $modelsp->getFillable();

        if ($this->request->ajax() || request()->has('debugajax')) {
            return $this->listajax();
        }

        // Adiciona o object Builder em uma variavel
        $html = $this->listBuilder;

        $html->parameters([
            "responsive" => true,
            "select" => true,
            "serverSide" => true,
            'dom' => 'rt<"row" <"pager-nav col" p><"pager-info col" <"float-right" li>>>',
            'pagingType' => 'full_numbers',
            //			'buttons' => [
            //				// 'pdf',
            //				// [
            //				// 	'extend' => 'print',
            //				// 	'text' => 'Imprimir',
            //				// 	'autoPrint' => false,
            //				// 	'exportOptions' => [
            //				// 		'modifier' => [
            //				// 			'page' => 'all'
            //				// 		]
            //				// 	]
            //				// ],
            //			],
            'pageLength' => '50'
        ]);

        // Antes de finalizar a listagem
        $html = $this->beforeListPadrao($html);

        $htmlbuilder = new Html(url(), view());
        $formbuilder = new Form($htmlbuilder, url(), view(), csrf_token(), $this->request);

        $orderOnFirst = true;

        // Para cada coluna visivel
        foreach ($colunas as $key => $value) {
            $adicional = [];
            // Atributos dinamicos da listagem
            if (isset($value['dynamic_attributes_title'])) {
                $adicional = $value['dynamic_attributes_title'];
            }

            if (isset($value['checkbox']) && $value['checkbox'] == true) {
                $attributes = [];
                if (isset($value['attributes'])) $attributes = array_merge($value['attributes'], $attributes);

                $orderOnFirst = false;
                $colunax = $html
                    ->addColumn(
                        array_merge(
                            [
                                //                                'defaultContent' => "<label class=\"m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand\"><input type=\"checkbox\" " . $htmlbuilder->attributes($attributes + ['class' => 'm-checkable']) . "/><span></span></label>",
                                'defaultContent' => '',
                                'title' => "<label class=\"m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand\"><input type=\"checkbox\" value=\"\" " . $htmlbuilder->attributes($attributes + ['id' => 'dataTablesCheckbox', 'class' => 'm-group-checkable']) . "><span></span></label>",

                                'data' => $key,
                                'name' => $key,
                                'orderable' => (isset($value['orderable'])) ? $value['orderable'] : false,
                                'searchable' => (isset($value['searchable'])) ? $value['searchable'] : false,
                                'exportable' => (isset($value['exportable'])) ? $value['exportable'] : false,
                                'printable' => (isset($value['printable'])) ? $value['printable'] : false,
                                'width' => '52px',
                            ],
                            $adicional
                        )
                    );
                //                $colunax = $html
                //                    ->addColumn(
                //                        array_merge(
                //                            [
                //                                'defaultContent' => '<input type="checkbox" ' . $htmlbuilder->attributes($attributes) . '/>',
                //                                'title' => $formbuilder->checkbox('', '', false, ['id' => 'dataTablesCheckbox']),
                //                                'data' => 'checkbox',
                //                                'name' => 'checkbox',
                //                                'orderable' => (isset($value['orderable'])) ? $value['orderable'] : false,
                //                                'searchable' => (isset($value['searchable'])) ? $value['searchable'] : false,
                //                                'exportable' => (isset($value['exportable'])) ? $value['exportable'] : false,
                //                                'printable' => (isset($value['printable'])) ? $value['printable'] : false,
                //                                'width' => '10px',
                //                            ],
                //                            $adicional
                //                        )
                //                    );
            } else {

                $colunax = $html
                    ->addColumn(
                        array_merge(
                            [
                                'data' => $key,
                                'name' => $key,
                                'title' => (isset($value['titulo'])) ? $value['titulo'] : $key,
                                // 'defaultContent' => ' --- ',
                                'defaultContent' => '',
                                'orderable' => (isset($value['orderable'])) ? $value['orderable'] : true,
                                'searchable' => (isset($value['searchable'])) ? $value['searchable'] : true,
                                'exportable' => (isset($value['exportable'])) ? $value['exportable'] : false,
                                'printable' => (isset($value['printable'])) ? $value['printable'] : false,
                                'class' => $key
                            ],
                            $adicional
                        )
                    );
            }
        }

        $html = $this->firstOrderTable($orderOnFirst, $html);
        if (!$orderOnFirst) {
            $html->parameters([
                'order' => [
                    1, // here is the column number
                    'desc'
                ]
            ]);
        }

        //        if ((isset($this->config->acao) && !empty($this->config->acao)) || isset($this->config->acao)) {
        if ($this->config->habilita_acao) {
            /**
             * Adiciona coluna de acao
             */
            $html
                ->addColumn(
                    [
                        'data' => 'acao',
                        'name' => 'acao',
                        'title' => 'Ações',
                        'className' => 'acoes text-center',
                        'defaultContent' => '',
                        'width' => $this->config->tamanho['acao'],
                        'orderable' => false,
                        'searchable' => false,
                        'exportable' => false,
                        'printable' => false,
                    ]
                );
        }

        $html->ajax(['method' => 'POST']);

        // Antes de finalizar a listagem
        $html = $this->afterListPadrao($html);

        /**
         * Retorno o builder para o $this -> view_vars['html'] compactado
         */
        $this->view_vars['html'] = $html;

        $this->view_vars['model'] = new $this->config->model();
    }

    // Depois de finalizar a listagem

    /**
     * Listagem de itens.
     *
     * @return \Illuminate\View\View
     */
    public function listajax($where = NULL, $model = null)
    {
        // language
        // $language = session('locale');
        // app('translator')->setLocale($language);
        // Carbon::setLocale($language);

        setlocale(LC_ALL, Carbon::getLocale());

        // \URL::defaults(['lang' => $language]);
        // \View::share('language', $language);

        // \DB::enableQueryLog();
        if (is_null($model)) {
            $model = new $this->config->model;
        }

        $paginacao = $this->config->paginate;
        // Valida Relacionamentos
        if (!empty($this->config->relacionamentos)) {
            // Gera o objeto da model com os relacionamentos
            // $list = $model->with($model -> relacionamentos)->get();
            $list = $model->with($model->relacionamentos);
        } else {
            // Gera o objeto da model sem relacionamentos
            $list = $model::query();
        }
        // dump($list->get());
        // dd(\DB::getQueryLog());

        // Caso esteja sendo enviado um Where manualmente
        if (!is_null($where)) {
            $list = $list->where($where[0], $where[1]);
        } elseif (isset($model->listwhere)) {
            foreach ($model->listwhere as $key => $value) {
                $list = $list->where($value[0], $value[1], $value[2]);
            }
        } elseif (isset($this->config->listwhere)) {
            foreach ($this->config->listwhere as $key => $value) {
                $list = $list->where($value[0], $value[1], $value[2]);
            }
        }

        if (isset($this->config->listwhereraw)) {
            foreach ($this->config->listwhereraw as $key => $value) {
                $list = $list->whereRaw($value[0]);
            }
        }

        // Ajuste para sempre se basear na tabela correta principalmente pelo problema de Joins
        $list->select($model->getTable() . '.*');

        // Query adicionais
        if (isset($this->config->queryadicionallist)) {
            $list->addSelect(
                \DB::raw(
                    implode(', ', $this->config->queryadicionallist)
                )
            );
        }

        $list = $this->firstOrderList($list, $model);

        $list = $this->beforeDatatables($list);

        // $modelsp 	= $model::find(1);
        // $colunas 	= $modelsp->getFillable();
        // $chavepm 	= $modelsp->getKeyName();
        // $hiddens 	= $modelsp->getHidden();

        // dump( $model );
        // echo $model->all();
        // dd($list);
        // die();

        $listagem = DataTables::of($list);

        $colunas = $this->config->colunas;

        $colunas = array_where($colunas, function ($value, $key) {
            return $value['listar'] == true;
        });

        $colunasraw = array_merge(['action', 'acao'], array_keys($colunas));

        $listagem->rawColumns($colunasraw);

        //        dump($colunasraw);
        //        dump($listagem
        //            ->make(true));
        //        die();

        /**
         * Adiciona colunasw
         */
        if ($this->config->habilita_acao) {
            // $listagem->rawColumns([ 'action', 'acao' ]);
            $listagem
                ->addColumn('acao', function ($list) {
                    $pagina = $this->pagina;
                    $pk = $list->getKeyName();
                    $id = $list[$pk];

                    $botoes = null;
                    $acoes = null;

                    $functionName = 'action_datatables';
                    if (isset($list->$functionName)) {
                        $this->config->acao = $list->$functionName;
                    }

                    if (isset($this->config->acao) && !empty($this->config->acao)) {
                        if (is_array($this->config->acao)) {
                            foreach ($this->config->acao as $key => $value) {
                                if ($key == 'tamanho') {
                                    continue;
                                }
                                if (isset($value['validar'])) {
                                    $campo = $value['campo'];
                                    switch ($value['validar']) {
                                        case 'boolean':
                                            if (!$list[$campo]) $acoes .= $value['html']; else $acoes .= $value['html2'];
                                            $acoes = str_replace('{$campo}', $campo, $acoes);
                                            break;
                                        case 'funcao':
                                            $acoes .= call_user_func($value['funcao']);
                                            break;
                                    }
                                } else {
                                    $acoes .= $value['html'];
                                }

                                if (isset($value['dynamic_vars'])) {
                                    foreach ($value['dynamic_vars'] as $variavel) {
                                        //                                        dump('{$' . $variavel . '} = '.array_get($list, $variavel));
                                        $acoes = str_replace('{$' . $variavel . '}', array_get($list, $variavel), $acoes);
                                    }
                                }
                            }
                        } else {
                            $acoes = $this->config->acao;
                        }
                        $acoes = str_replace('{$url}', route('admin.anyroute', ['slug' => $pagina]), $acoes);
                        $acoes = str_replace('{$pk}', $pk, $acoes);
                        $acoes = str_replace('{$id}', $id, $acoes);
                    } elseif (isset($this->config->acao)) {
                        $ativar = [
                            'html' => '
								<a class="dropdown-item" title="' . __('Ativar') . '" href="javascript:void(0);"
									data-alterarbool
									data-ajax="' . route('admin.anyroute', ['slug' => $pagina . '/alterarbool/' . $pk . '/' . $id . '/campo/active']) . '"
									data-icone1="fas fa-circle"
									data-texto1="' . __('Ativar') . '"
									data-icone2="far fa-dot-circle"
									data-texto2="' . __('Desativar') . '"
								>
									<i class="fas fa-circle" aria-hidden="true"></i> <span class="text">' . __('Ativar') . '</span>
								</a>
							',
                            'html2' => '
								<a class="dropdown-item" title="' . __('Desativar') . '" href="javascript:void(0);"
									data-alterarbool
									data-ajax="' . route('admin.anyroute', ['slug' => $pagina . '/alterarbool/' . $pk . '/' . $id . '/campo/active']) . '"
									data-icone1="fas fa-circle"
									data-texto1="' . __('Ativar') . '"
									data-icone2="far fa-dot-circle"
									data-texto2="' . __('Desativar') . '"
								>
									<i class="far fa-dot-circle" aria-hidden="true"></i> <span class="text">' . __('Desativar') . '</span>
								</a>
							',
                        ];

                        if (!$list['active']) $acoes .= $ativar['html']; else $acoes .= $ativar['html2'];

                        $acoes .= '
                          <a class="dropdown-item open-form-sidebar" title="' . __('Editar') . '" href="' . route('admin.anyroute', ['slug' => $pagina . '/edit/' . $pk . '/' . $id]) . '">
                            <i class="far fa-edit" aria-hidden="true"></i> ' . __('Editar') . '
                          </a>
                          <a class="dropdown-item" title="' . __('Remover') . '" data-excluir data-ajax="' . route('admin.anyroute', ['slug' => $pagina . '/deleteajax/' . $pk . '/' . $id]) . '">
                            <i class="fas fa-times" aria-hidden="true"></i> ' . __('Remover') . '
                          </a>
                        ';
                        /*return '
                            <a class="button" title="Visualizar" href="'. route('admin.anyroute', ['slug' => $pagina.'/show/'. $pk .'/'. $id ]) .'">
                                <i class="fas fa-search" aria-hidden="true"></i>
                            </a>
                            <a class="button secondary" title="'. __('Edit') .'" href="'. route('admin.anyroute', ['slug' => $pagina.'/edit/'. $pk .'/'. $id ]) .'">
                                <i class="far fa-edit" aria-hidden="true"></i>
                            </a>
                            <a class="button alert" title="Excluir" data-excluir data-ajax="'. route('admin.anyroute', ['slug' => $pagina.'/deleteajax/'. $pk .'/'. $id ]) .'">
                                <i class="fas fa-times" aria-hidden="true"></i>
                            </a>
                        ';*/

                    }

                    $botoes = '
                        <div class="dropdown">
                            <a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown">
                                <i class="la la-ellipsis-h"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                ' . $acoes . '
                            </div>
                        </div>
                    ';

                    return $botoes;
                });
        }

        $pk = $this->config->pk;

        $htmlbuilder = new Html(url(), view());
        $formbuilder = new Form($htmlbuilder, url(), view(), csrf_token(), $this->request);

        // Colunas extras
        foreach ($colunas as $key => $coluna) {
            $naturalKey = $key;
            $key = str_slug($key, '_');

            $functionNameValidate = 'get' . studly_case($key . '_datatables_attribute');
            $functionName = $key . '_datatables';

            if (method_exists($model, $functionNameValidate)) {
                $listagem->editColumn($naturalKey, function ($list) use ($functionName) {
                    return $list->$functionName;
                });
            }

            if (isset($coluna['uploadpath']) && $coluna['listar'] == true) {
                $listagem->editColumn($naturalKey, function ($list) use ($key, $coluna, $pk) {
                    $functionName = $key . '_datatables';
                    if (isset($list->$functionName)) {
                        return $list->$functionName;
                    }
                    $coluna = $list->atributos[$key];
                    $retorno = null;

                    if (!empty($list->$key) && !isset($coluna['multiploupload'])) {
                        $path = route('thumb', [150, 0, '']) . '/' . $coluna['uploadpath'] . '/' . $list->$key;
                        $path2 = route('thumb', [0, 0, '']) . '/' . $coluna['uploadpath'] . '/' . $list->$key;
                        $retorno = "<a class=\"clearfix\" data-fancybox=\"group\" href=\"{$path2}\">
                            <img src=\"{$path}\" alt=\"\">
                          </a>
                        ";
                    }
                    if (!empty($list->$key) && (isset($coluna['multiploupload']) && $coluna['multiploupload'] == true)) {
                        $path = route('thumb', [150, 0, '']) . '/' . $coluna['uploadpath'] . '/' . $list->$key;
                        $path2 = route('thumb', [0, 0, '']) . '/' . $coluna['uploadpath'] . '/' . $list->$key;

                        if (isset($coluna['listtype']) && $coluna['listtype'] === 'button') {
                            $retorno = "<a class=\"button secondary clearfix expanded\" data-fancybox href=\"{$path2}\">
                                Clique para abrir
                              </a>
                            ";
                        } else {
                            $retorno = "<a class=\"clearfix\" data-fancybox=\"group\" href=\"{$path2}\">
                                <img src=\"{$path}\" alt=\"\">
                              </a>
                            ";
                        }
                    }

                    //                    if (!empty($list->$key) && !isset($coluna['multiploupload'])) {
                    //                        $arrayPath = explode('/', $coluna['uploadpath']);
                    //                        $path = route('thumb', [150, 0, '']) . '/' . implode('/', array_filter($arrayPath)) . '/' . $list->$pk . '/' . $list->$key;
                    //                        $path2 = route('thumb', [0, 0, '']) . '/' . implode('/', array_filter($arrayPath)) . '/' . $list->$pk . '/' . $list->$key;
                    //                        $retorno = "<a class=\"clearfix\" data-fancybox=\"group\" href=\"{$path2}\">
                    //                            <img src=\"{$path}\" alt=\"\">
                    //                          </a>
                    //                        ";
                    //                    }
                    //                    if (!empty($list->$key) && (isset($coluna['multiploupload']) && $coluna['multiploupload'] == true)) {
                    //                        $arrayPath = explode('/', $coluna['uploadpath']);
                    //                        $path = route('thumb', [150, 0, '']) . '/' . implode('/', array_filter($arrayPath)) . '/' . $list->$key;
                    //                        $path2 = route('thumb', [0, 0, '']) . '/' . implode('/', array_filter($arrayPath)) . '/' . $list->$key;
                    //
                    //                        if (isset($coluna['listtype']) && $coluna['listtype'] === 'button') {
                    //                            $retorno = "<a class=\"button secondary clearfix expanded\" data-fancybox href=\"{$path2}\">
                    //                                Clique para abrir
                    //                              </a>
                    //                            ";
                    //                        } else {
                    //                            $retorno = "<a class=\"clearfix\" data-fancybox=\"group\" href=\"{$path2}\">
                    //                                <img src=\"{$path}\" alt=\"\">
                    //                              </a>
                    //                            ";
                    //                        }
                    //                    }
                    return $retorno;
                });
            }

            if (isset($coluna['checkbox']) && $coluna['checkbox'] == true) {
                $listagem->editColumn($naturalKey, function ($list) use ($key, $coluna, $pk, $htmlbuilder) {
                    $functionName = $key . '_datatables';
                    if (isset($list->$functionName)) {
                        return $list->$functionName;
                    }
                    $attributes = [];

                    if (isset($value['attributes'])) $attributes = array_merge($coluna['attributes'], $attributes);

                    return "<label class=\"m-checkbox m-checkbox--single m-checkbox--solid m-checkbox--brand\"><input type=\"checkbox\" " . $htmlbuilder->attributes($attributes + ['name' => $key.'[]', 'class' => 'm-checkable', 'value' => $list->$key]) . "/><span></span></label>";
                });
            }
        }

        $listagem = $this->DatablesFilter($listagem, $model);

        $listagem = $this->afterDatatablesList($listagem);

        //        return ($listagem
        //            ->make(true));
        //        die();


        //        dump($colunasraw);
        //        dump($listagem
        //            ->make(true));
        //        die();

        // dd('aki');

        // dd( $listagem ->make(true) );

        /**
         * Remove Colunas
         */
        // $listagem -> removeColumn($chavepm);
        // foreach ($hiddens as $key => $value) {
        // 	$listagem -> removeColumn($value);
        // }

        // dump( $listagem ->make(true) );beforeUpdate

        return $listagem
            ->make(true);
    }

    public function firstOrderList($list, $model = null)
    {
        if ($this->request->has('draw') && $this->request->draw == 1) {
            $list->orderBy($model->getTable() . '.' . $this->config->pk, 'desc');
        }

        return $list;
    }

    public function beforeDatatables($list)
    {
        return $list;
    }

    public function defaultFilters($query, $model = null)
    {
        if (request()->has('created_at') && !is_null(request()->created_at)) {
            $dates = explode(' - ', request()->created_at);
            if (!is_null($model)) {
                $query->whereRaw(" DATE(" . $model->getTable() . ".created_at) BETWEEN  '" . formataDataMysql(trim($dates[0])) . "' AND '" . formataDataMysql(trim($dates[1])) . "'");
            } else {
                $query->whereRaw(" DATE(created_at) BETWEEN  '" . formataDataMysql(trim($dates[0])) . "' AND '" . formataDataMysql(trim($dates[1])) . "'");
            }
        }

        if (request()->has('active') && !is_null(request()->active)) {
            if (!is_null($model)) {
                $query->where($model->getTable() . ".active", request()->active);
            } else {
                $query->where("active", request()->active);
            }
        }

        if (request()->has('removed')) {
            switch (request()->removed) {
                case '1':
                    $query->onlyTrashed();
                    break;
                case '2':
                    $query->withTrashed();
                    break;
            }
        }
    }

    public function DatablesFilter($listagem, $model = null)
    {
        $listagem = $listagem->filter(function ($query) use ($model) { // this method disables original column search feature

            $this->defaultFilters($query, $model);

        }, true);

        return $listagem;
    }

    public function afterDatatablesList($listagem)
    {

        return $listagem;
    }

    public function beforeListPadrao($html)
    {
        return $html;
    }

    public function firstOrderTable($orderOnFirst = true, $html)
    {
        if (!$orderOnFirst) {
            $html->parameters([
                'order' => [
                    1, // here is the column number
                    'desc'
                ]
            ]);
        }

        return $html;
    }

    public function afterListPadrao($html)
    {
        return $html;
    }

    /**
     * Imprimi o template
     *
     * @param null $template
     *
     * @return \Illuminate\View\View
     */
    protected function viewshow($template)
    {
        $this->template = $template;

        $this->beforeView();

        // Exibe a view dinamicamentecom os parametros
        return view('admin::' . $this->template, $this->view_vars);
    }

    public function beforeView()
    {
    }

    /**
     * Página de multiploupload
     * @param  [type] $campopai    [Padrão é nulo para o caso de ser uma página para enviar varios arquivos para download por exemplo, caso seja informado está relacionado a outra tabela]
     * @param  string $campo [ campo do banco de dados que será armazenado e informado no name do input ]
     * @param  [type] $pagina      [ página que será enviado o post ]
     * @param  string $maxfilesize [ tamanho máximo para envio de arquivo, padrão é 100 (MB)]
     * @param  string $template [ caminho do template que será exibido ]
     * @return [type]              [ retorna o call para renderizar a página ]
     */
    public function multiploupload($campopai = NULL, $campo = 'file', $pagina, $maxfilesize = '100', $template = 'layouts.uploads.multupload')
    {
        if (!empty($this->config->permissions)) {
            if (isset($this->config->permissions->multiuploupload) && !$this->user->canpermission($this->config->permissions->multiuploupload)) {
                return redirect()->route('admin.home')->with('error', __("Você não possui permissão de acesso"));
            }
        }

        // Verifica se foi passado o campopai
        // Caso não seja relacionado a uma tabela "pai", exemplo admin de downlads
        if (!is_null($campopai)) {
            // Recupera o id
            $idpai = $this->vars_class[$campopai][0];
        }

        // Se for um post (upload)
        if ($this->request->isMethod('post')) {
            $modelItem = new $this->config->model;

            // Validar campos
            $this->validar('multiploupload');

            // Pega todos o campos do tipo file
            if (!empty($this->request->allFiles())) {
                // Loop que passa por cada arquivo enviado
                foreach ($this->request->allFiles() as $key => $value) {
                    // Valida o campo para ver se existe arquivo
                    if ($this->request->file($key)->isValid()) {

                        // Atribui o file a um arquivo
                        $arquivo = $this->request->file($key);
                        // Pega a extenão
                        $extensao = $arquivo->extension();
                        // Gera o novo nome do arquivo
                        $nome = date('dmYHis') . '_' . str_slug(str_replace("." . $extensao, '', $arquivo->getClientOriginalName())) . '.' . $extensao;

                        // Salva o campo do banco
                        // Verifica se foi passado o campopai
                        // Caso não seja relacionado a uma tabela "pai", exemplo admin de downlads
                        if (!is_null($campopai)) {
                            $modelItem->$campopai = $idpai;
                        }
                        // Armazena baseado no campo que existe (Insert)
                        $modelItem->$key = $nome;

                        // Caso conseguiu salvar retorna um success
                        if ($modelItem->save()) {

                            try {
                                // Pega o caminho de upload
                                $caminho = 'uploads/' . $modelItem->atributos[$key]['uploadpath'];

                                // Salva o arquivo novo
                                $arquivo->move($caminho, $nome);

                            } catch (\Exception $e) {
                                \Log::error($e);
                                \Log::error('Erro no multiplo upload');

                                return response()->json(['error' => "Error"], 500);
                            }

                            return response()->json(['success' => 'Item cadastrado com sucesso']);
                        } else { //Caso não tenha conseguido salvar retorna os erros
                            return response()->json(['error' => $modelItem->erros()], 500);
                        }
                    }
                }
            }

            die;
        }

        // Subtitulo
        $this->view_vars['page']['subtitle'] = 'Clique no botão para selecionar os arquivos ou arraste e solte os arquivos nesta tela';

        // Botões
        // Remove todos os botões
        unset($this->view_vars['button']);
        // Selecionar arquivos
        $this->view_vars['button']['voltar']['title'] = 'Voltar para a Listagem de Fotos';
        $this->view_vars['button']['voltar']['class'] = 'btn-warning';
        $this->view_vars['button']['voltar']['icone'] = 'fa fa-chevron-left';
        $this->view_vars['button']['voltar']['link'] = $this->pagina . "/index/{$campopai}/{$idpai}";

        // Selecionar arquivos
        $this->view_vars['button']['sel']['title'] = 'Selecionar arquivos...';
        $this->view_vars['button']['sel']['class'] = 'seleciona-arquivos btn-primary';
        $this->view_vars['button']['sel']['icone'] = 'fas fa-plus-circle';
        $this->view_vars['button']['sel']['link'] = '#';

        // Começa todos os uploads
        $this->view_vars['button']['send']['title'] = 'Enviar arquivos';
        $this->view_vars['button']['send']['class'] = 'btn-success enviar-arquivo';
        $this->view_vars['button']['send']['icone'] = 'fa fa-cloud-upload';
        $this->view_vars['button']['send']['link'] = '#';

        // Cancelar uploads
        $this->view_vars['button']['cancel']['title'] = 'Cancelar uploads';
        $this->view_vars['button']['cancel']['class'] = 'btn-danger cancelar-arquivos';
        $this->view_vars['button']['cancel']['icone'] = 'fa fa-trash';
        $this->view_vars['button']['cancel']['link'] = '#';

        // Verifica se foi passado o campopai
        if (!is_null($campopai)) {
            // Url form
            $this->view_vars['urlmult'] = route('admin.anyroute', [ "/{$pagina}/{$campopai}/{$idpai}" ]);
        } else {
            // Url form
            $this->view_vars['urlmult'] = route('admin.anyroute', [ "/{$pagina}/" ]);
        }

        // Tamanho maximo de arquivos enviados
        $this->view_vars['maxfilesize'] = $maxfilesize;
        // Nome do campo
        $this->view_vars['campo'] = $campo;

        // Template
        $this->template = $template;

        return $this->renderizar();
    }

    /**
     * Validar campos no adicionar e editar
     * @param  [type] $tipo [description]
     * @return [type]       [description]
     */
    public function validar($tipo, $model = null, $request = null, $rules = null)
    {
        // $this -> config -> tipoform = $tipo;

        // Pega a model
        if (is_null($model)) {
            $model = new $this->config->model;
        }

        // Pega os campos baseado no tipo
        //        $camposOff = array_where($model->atributos, function ($value, $key) use ($tipo) {
        //            if (isset($value[$tipo]) && $value[$tipo] == true) {
        //                return true;
        //            } else {
        //                return false;
        //            }
        //        });

        if (is_null($rules)) {
            $camposOff = null;
            if (is_array($model->atributos)) {
                $camposOff = array_where($model->atributos, function ($value, $key) use ($tipo) {
                    if (isset($value[$tipo]) && $value[$tipo] == false) {
                        return true;
                    } else {
                        return false;
                    }
                });
            }

            $campos = array_where($model->getFillable(), function ($value, $key) use ($camposOff, $tipo) {
                if (isset($camposOff[$value]) && !$camposOff[$value][$tipo]) {
                    return false;
                } else {
                    return true;
                }
            });
        } else {
            $campos = $rules;
        }

        $validar = [];

        // Loop para gerar o array de validação
        foreach ($campos as $key => $value) {
            if (is_numeric($key)) {
                $key = $value;
            }

            if (isset($model->rules) && isset($model->rules[$key])) { // Verifica se existe na model a variavel de regras
                if (is_array($model->rules[$key])) { // é um array
                    $regra = $model->rules[$key]['regra'];
                    if (isset($model->rules[$key]['dynamic_vars'])) {
                        foreach ($model->rules[$key]['dynamic_vars'] as $variavel) {
                            $regra = str_replace('{$' . $variavel . '}', array_get($this->vars_class, $variavel), $regra);
                        }
                    }
                    $validar[$key] = $regra;
                } else {// não é array
                    $validar[$key] = $model->rules[$key];
                }
            } elseif (is_array($value)) {
                if (array_key_exists('validacao', $value)) {
                    if (is_array($value['validacao'])) { // é um array
                        $regra = $value['validacao']['regra'];
                        if (isset($value['validacao']['dynamic_vars'])) {

                            foreach ($value['validacao']['dynamic_vars'] as $variavel) {
                                $regra = str_replace('{$' . $variavel . '}', array_get($this->vars_class, $variavel), $regra);
                            }
                        }
                        $validar[$key] = $regra;
                    } else {// não é array
                        $validar[$key] = $value['validacao'];
                    }
                }
            }
        }

        $request = $this->request;

        $messages = [];
        if( method_exists($model, 'messages' ) ){
            $messages = $model->messages();
        }else if(isset($model->messages)){
            $messages = $model->messages;
        }

        // $validator = Validator::make($request->all(), $validar, $messages);
        // dump(
        //     $validar
        // );
        // dump(
        //     $validator
        // );
        // dump(
        //     $validator->messages()
        // );
        //
        // die();

        $erros = $this->validate($request, $validar, $messages);
        // if(is_null($request)){
        // Valida os campos
        // }else{
        // 	Valida os campos
        // $erros = Validator::make($request->all(), $validar)->validate();
        // }

        // unset($this -> config -> tipoform);
    }

    /**
     * Visualização do item.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show()
    {
        if (!empty($this->config->permissions)) {
            if (!$this->user->canpermission($this->config->permissions->show)) {
                return redirect()->route('admin.home')->with('error', __("Você não possui permissão de acesso"));
            }
        }

        // Breadcrumb
        $this->view_vars['page']['extrat'] = 'Visualização de ' . $this->view_vars['page']['opcional'];

        if (empty($this->template)) {
            // Template
            $this->template = strtolower(studly_case($this->pagina)) . '.show';
        }

        // Valida se foi passado o id
        if (!isset($this->vars_class[$this->config->pk])) {
            return redirect()->route('admin.anyroute', ['slug' => $this->pagina])->withErrors('Item não informado.');
        }

        // Recupera o id
        $id = $this->vars_class[$this->config->pk][0];

        // Pega a model atual
        $model = new $this->config->model;

        // Procura o item baseado no id
        $traits = class_uses($model);
        $usesSoftDeletes = in_array('Illuminate\Database\Eloquent\SoftDeletes', $traits);
        if ($usesSoftDeletes) {
            // dd( method_exists(new $model, 'withTrashed') );
            $item = $model::withTrashed()->find($id);
        } else {
            $item = $model::find($id);
        }
        // Se não encontrou o item
        if (!$item) {
            request()->session()->flash('error', 'Item não informado.');
            return $this->conclusionRedirect();
        }

        // Valida Relacionamentos
        if (!empty($model->relacionamentos)) {
            foreach ($model->relacionamentos as $key => $value) {
                // Executa o relacionamento
                $nmodel = $model->$value()->getRelated();
                // $this -> view_vars['relacionamentos'][$nmodel -> getTable()] = $nmodel;
                $this->view_vars['relacionamentos'][$value] = $nmodel;
            }
        }

        // dd( $item -> get() -> lists('idcategoria') );

        // Passa a model com o usuario
        $this->view_vars['item'] = $item;

        unset($this->view_vars['button']['categorias']);

        return $this->renderizar();
    }

    /**
     * Form de cadastro do item.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        if (!empty($this->config->permissions)) {
            if (!$this->user->canpermission($this->config->permissions->create)) {
                return redirect()->route('admin.home')->with('error', __("Você não possui permissão de acesso"));
            }
        }

        // Breadcrumb
        $this->view_vars['page']['title'] = __('Novo registro de') . ' ' . $this->view_vars['page']['opcional'];

        // Template
        $this->template = strtolower(studly_case($this->pagina)) . '.form';
        // Url form
        $this->view_vars['url'] = route('admin.anyroute', ['slug' => $this->pagina . '/store']);

        // Passa a model com o usuario
        $model = new $this->config->model;
        // Valida Relacionamentos
        if (!empty($model->relacionamentos)) {
            foreach ($model->relacionamentos as $key => $value) {
                // Executa o relacionamento
                $nmodel = $model->$value()->getRelated();
                // $this -> view_vars['relacionamentos'][$nmodel -> getTable()] = $nmodel;
                $this->view_vars['relacionamentos'][$value] = $nmodel;
            }
        }

        // dump( $model -> relacionamentos );
        // dd( $this -> view_vars['relacionamentos']['bannercategorias']->all() );
        // die();

        $this->view_vars['FormModel'] = $model;

        // dd( $model -> get() -> lists('bannercategorias') -> lists('categoria','id') -> all() );

        unset($this->view_vars['button']['categorias']);

        return $this->renderizar();
    }

    /**
     * Cria o item.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        if (!empty($this->config->permissions)) {
            if (!$this->user->canpermission($this->config->permissions->create)) {
                return redirect()->route('admin.home')->with('error', __("Você não possui permissão de acesso"));
            }
        }

        \DB::enableQueryLog();

        // Validar campos
        $this->validar('adicionar');

        // Preenche o model com o post
        $item = new $this->config->model($this->request->all());

        // Chave
        $key = $this->config->pk;

        // criar uma função para usar antes do insert/salvar
        $item = $this->beforeInsert($item);

        // Se salvar o item
        if ($item->save()) {
            // criar uma função para usar depois do insert/salvar e antes de salvar os arquivos
            $item = $this->afterInsert($item);

            // Uploads
            $this->salvarArquivos($item);

            // criar uma função para usar depois do salvar os arquivos
            $item = $this->afterInsertSalvarArqivos($item);

            request()->session()->flash('sucesso', __("Item adicionado com sucesso"));

            if ($this->request->salvarecontinuar == 'sim') {
                // dd(route('admin.anyroute', ['slug' => $this->pagina . '/edit/' . $key . '/' . $item->$key]));
                request()->session()->flash('reload', route('admin.anyroute', ['slug' => $this->pagina . '/edit/' . $key . '/' . $item->$key]));
            }
        } else {
            request()->session()->flash('error', $item->erros());
        }

        return $this->renderizar();
    }

    /**
     * Essa funçao é executada antes de executar o insert no banco de dados
     * @param $item
     * @return mixed
     * ou usar o evento updating, updated (https://laravel.com/docs/5.1/eloquent#events)
     */
    public function beforeInsert($item)
    {
        return $item;
    }

    /**
     * Esta função é executada após executar o insert no banco de dados e antes de acessar a função de salvar a imagem
     * @param $item
     * @return mixed
     * ou usar o evento updating, updated (https://laravel.com/docs/5.1/eloquent#events)
     */
    public function afterInsert($item)
    {
        return $item;
    }

    /**
     * Função salvarArquivos
     * Executa upload de todos os campos
     * @param  [model] $modelItem [ Envia a model/item que será feito o upload/atualizado ]
     * @return [type]       [description]
     */
    public function salvarArquivos($modelItem)
    {
        $arrayteste = collect($modelItem->toArray());
        // Pega todos o campos do tipo file
        if (!empty($this->request->allFiles())) {
            foreach ($this->request->allFiles() as $key => $value) {
                // Pega o caminho de upload
                $caminho = 'uploads/' . $modelItem->atributos[$key]['uploadpath'];
                // Verifica se é uma atualização ou inclusão!
                if (!empty($arrayteste)) {
                    // Valida se o campo exite no banco
                    if ($arrayteste->has($key)) {
                        // Valida se o campo está preenchido
                        if (!empty($modelItem->$key) && !is_null($modelItem->$key)) {
                            // Remove o arquivo anterior
                            \File::delete($caminho . $modelItem->$key);

                            // Limpa o campo do banco
                            $modelItem->$key = '';
                            $modelItem->save();
                        }
                        // Valida o campo para ver se existe arquivo
                        if ($this->request->file($key)->isValid()) {
                            // Atribui o file a um arquivo
                            $arquivo = $this->request->file($key);
                            // Pega a extenão
                            $extensao = $arquivo->extension();
                            // Gera o novo nome do arquivo
                            $nome = date('dmYHis') . '_' . str_slug(str_replace("." . $extensao, '', $arquivo->getClientOriginalName())) . '.' . $extensao;
                            // Salva o arquivo novo
                            $arquivo->move($caminho, $nome);

                            // Salva o campo do banco
                            $modelItem->$key = $nome;
                            $modelItem->save();
                        }
                    }
                }
            }
        }
    }

    /**
     * Esta função é executada depois de acessar a função de salvar arquivos do insert
     * @param $item
     * @return mixed
     */
    public function afterInsertSalvarArqivos($item)
    {
        return $item;
    }

    /**
     * Form de edição do item.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit()
    {
        if (!empty($this->config->permissions)) {
            if (!$this->user->canpermission($this->config->permissions->edit)) {
                return redirect()->route('admin.home')->with('error', __("Você não possui permissão de acesso"));
            }
        }

        // Breadcrumb
        $this->view_vars['page']['extrat'] = __('Edição de') . ' ' . $this->view_vars['page']['opcional'];

        // Template
        $this->template = strtolower(studly_case($this->pagina)) . '.form';

        // Valida se foi passado o id
        if (!isset($this->vars_class[$this->config->pk])) {
            request()->session()->flash('error', 'Item não informado.');
            return $this->conclusionRedirect();
            //            return redirect()->route('admin.anyroute', ['slug' => $this->pagina ])->withErrors('Item não informado.');
        }

        // Recupera o id
        $id = $this->vars_class[$this->config->pk][0];

        // Url form
        $this->view_vars['url'] = route('admin.anyroute', ['slug' => $this->pagina . '/update/' . $this->config->pk . '/' . $id]);

        // Pega a model atual
        $model = new $this->config->model;

        // Procura o item baseado no id
        $traits = class_uses($model);
        $usesSoftDeletes = in_array('Illuminate\Database\Eloquent\SoftDeletes', $traits);
        if ($usesSoftDeletes) {
            // dd( method_exists(new $model, 'withTrashed') );
            $item = $model::withTrashed()->find($id);
        } else {
            $item = $model::find($id);
        }
        // Se não encontrou o item
        if (!$item) {
            request()->session()->flash('error', 'Item não informado.');
            return $this->conclusionRedirect();
        }

        // Valida Relacionamentos
        if (!empty($model->relacionamentos)) {
            foreach ($model->relacionamentos as $key => $value) {
                // Executa o relacionamento
                $nmodel = $model->$value()->getRelated();
                // $this -> view_vars['relacionamentos'][$nmodel -> getTable()] = $nmodel;
                $this->view_vars['relacionamentos'][$value] = $nmodel;
            }
        }

        // dd( $item -> get() -> lists('idcategoria') );

        // Passa a model com o usuario
        $this->view_vars['FormModel'] = $item;

        unset($this->view_vars['button']['categorias']);

        return $this->renderizar();
    }

    private function conclusionRedirect()
    {
        $this->template = 'layouts.conclusion';
        return $this->renderizar();
    }

    /**
     * Atualiza o item.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        if (!empty($this->config->permissions)) {
            if (!$this->user->canpermission($this->config->permissions->edit)) {
                return redirect()->route('admin.home')->with('error', __("Você não possui permissão de acesso"));
            }
        }

        // Validar campos
        $this->validar('editar');

        // Valida se foi passado o id
        if (!isset($this->vars_class[$this->config->pk])) {
            request()->session()->flash('error', 'Item não informado.');
            return $this->conclusionRedirect();
        }
        // Verifica se a ação está ativa
        // if( $this->config->acoes->editar !== true )
        // {
        // 	return redirect()->to($this->config->componente_url)->withErrors('Ação não disponível.');
        // }

        // dd( $this->request->all() );

        // dd( $this->request->allFiles() );

        // Recupera o id
        $id = $this->vars_class[$this->config->pk][0];

        // Pega a model atual
        $model = $this->config->model;

        // Procura o item baseado no id
        $traits = class_uses($model);
        $usesSoftDeletes = in_array('Illuminate\Database\Eloquent\SoftDeletes', $traits);
        if ($usesSoftDeletes) {
            $item = $model::withTrashed()->find($id);
        } else {
            $item = $model::find($id);
        }

        // Se não encontrou o item
        if (!$item) {
            request()->session()->flash('error', 'Item não informado.');
            return $this->conclusionRedirect();
        }

        // Preenche o model com o post
        $item->fill($this->request->all());

        // Chave
        $key = $this->config->pk;

        // criar uma função para usar antes do update/salvar
        $item = $this->beforeUpdate($item);

        // Se salvar o item
        if ($item->save()) {
            // criar uma função para usar depois do update/salvar e antes de salvar os arquivos
            $item = $this->afterUpdate($item);

            // Função de upload
            $this->salvarArquivos($item);

            // criar uma função para usar depois do salvar os arquivos
            $item = $this->afterUpdateSalvarArqivos($item);

            request()->session()->flash('sucesso', __("Item editado com sucesso!"));

            if ($this->request->salvarecontinuar == 'sim') {
                request()->session()->flash('reload', route('admin.anyroute', ['slug' => $this->pagina . '/edit/' . $key . '/' . $item->$key]));
            }
        } else {
            return back()->withErrors($item->erros())->withInput();
        }

        return $this->renderizar();
    }

    /**
     * Esta função é executada antes de executar o update do banco de dados
     * @param $item
     * @return mixed
     * ou usar o evento updating, updated (https://laravel.com/docs/5.1/eloquent#events)
     */
    public function beforeUpdate($item)
    {
        return $item;
    }

    /**
     * Esta função é executada depois de executar o update no banco de dados
     * @param $item
     * @return mixed
     * ou usar o evento updating, updated (https://laravel.com/docs/5.1/eloquent#events)
     */
    public function afterUpdate($item)
    {
        return $item;
    }

    /**
     * Esta função é executada depois de acessar a função de salvar arquivos do update
     * @param $item
     * @return mixed
     */
    public function afterUpdateSalvarArqivos($item)
    {
        return $item;
    }

    /**
     * Deleta itens.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy()
    {
        if (!empty($this->config->permissions)) {
            if (!$this->user->canpermission($this->config->permissions->delete)) {
                return redirect()->route('admin.home')->with('error', __("Você não possui permissão de acesso"));
            }
        }

        return $this->delete();
    }

    /**
     * Deleta os itens, função padrão
     * @return [type] [description]
     */
    public function delete()
    {
        if (!empty($this->config->permissions)) {
            if (!$this->user->canpermission($this->config->permissions->delete)) {
                return redirect()->route('admin.home')->with('error', __("Você não possui permissão de acesso"));
            }
        }

        // Dados gerais
        $model = $this->config->model;
        $chavepm = $this->config->pk;
        $pagina = $this->pagina;

        // Caso não seja passado a(s) PK para o deletar
        if (!isset($this->vars_class[$chavepm])) {
            return redirect()->route('admin.anyroute', ['slug' => $pagina])->with('erro', 'Você deve informar ao menos um item para remover.');
        }

        // Para cada item execute um deletar
        foreach ($this->vars_class[$chavepm] as $id) {
            $item = $model::where($chavepm, $id);

            // Se encontrou o item
            if ($item) {
                $item->delete();
            }
        }

        return redirect()->route('admin.anyroute', ['slug' => $pagina])->with('sucesso', count($this->vars_class[$chavepm]) . ' itens excluídos com sucesso.');
    }

    /**
     * Função para alterar boolean
     * Ajax
     * @return \Illuminate\Http\RedirectResponse
     */
    public function alterarbool()
    {
        // Campo booleano
        $campo = $this->vars_class['campo'][0];
        // Os ids que serão alterados
        $id = $this->vars_class[$this->config->pk][0];

        // Model atual
        $model = new $this->config->model;

        // Se não veio o array dos id
        if (empty($id)) {
            return response()->json([
                'type' => 'error',
                'message' => 'Nenhum item informado.'
            ]);
            // return redirect()->to($this->config->componente_url)->withErrors('Você precisa selecionar ao menos 1 item.');
        }

        // Baseado no id pega os dados
        $item = $model::find($id);

        // Se encontrou o item
        if ($item) {
            if ($item->$campo) { // Se for true altera para falso
                $item->$campo = false;
            } else { //Se for false altera para true
                $item->$campo = true;
            }
            $item->save();
        }

        return response()->json([
            'type' => 'success',
            'message' => 'true'
        ]);
    }

    /**
     * Exportar os itens.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function exportar()
    {
        // Verifica se a ação está ativa
        // if( $this->config->acoes->exportar !== true )
        // {
        // 	return redirect()->to($this->config->componente_url)->withErrors('Ação não disponível.');
        // }

        // Se não definiu os itens
        if (!isset($this->config->linhas)) {
            return 'Necessário definir as linhas da exportação em $this->config->linhas = []';
        }

        // Cria o arquivo do excel
        $excel = new XLSXWriter();
        $excel->writeSheet($this->config->linhas, 'Produtos', isset($this->config->colunas) ? $this->config->colunas : ['asas']);

        return response($excel->writeToString(), 200, [
            'Content-type' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'Cache-Control' => 'max-age=0',
            'Content-Disposition' => 'attachment;filename=exportação_' . $this->config->componente_alias . '_' . Carbon::now()->format('Y-m-d-H-i-s') . '.xlsx',
        ]);
    }
}
