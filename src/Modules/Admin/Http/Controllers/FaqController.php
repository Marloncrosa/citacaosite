<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\FrequentlyAskedQuestions as model;

class FaqController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Pega a model atual
        $model = new $this->config->model;

        $this->view_vars['button']['add']['title'] = __('Cadastrar Nova Pergunta');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = "faq/create";

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __("Gerenciador das Perguntas Frequentes");
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this->view_vars['page']['opcional'] = __("Perguntas Frequentes");
        $this->view_vars['page']['urlbread'] = "faq/index";

    }

    public function beforeinit()
    {
        // Caso seja algum post para o store ou update
        if ($this->action == 'create' || $this->action == 'edit') {
            // Pagina
            $this->view_vars['urlform'] = route('admin.anyroute', [ "faq/" . (($this->action == 'create') ? 'store' : 'update') ]);

            if ($this->action == 'edit') {
                $this->view_vars['urlform'] .= '/' . $this->config->pk . '/' . $this->vars_class[$this->config->pk][0];

            }
            $this->view_vars['pagina'] = "faq/index";
        }

        // Caso seja algum post para o store ou update
        if ($this->action == 'store' || $this->action == 'update' || $this->action == 'delete') {
            // Pagina
            $this->pagina = route('admin.anyroute', [ session('locale'), "faq/index" ]);
            $this->view_vars['pagina'] = $this->pagina;
        }
    }

}
