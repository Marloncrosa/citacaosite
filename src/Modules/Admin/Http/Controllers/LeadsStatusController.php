<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\LeadsStatus as model;

class LeadsStatusController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Pega a model atual
        $model = new $this->config->model;

        $this->view_vars['button']['add']['title'] = __('Novo Registro');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = "leads_status/create";


        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __("Gerenciamento dos Status dos Leads");
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this->view_vars['page']['opcional'] = __("Status dos Leads");
        $this->view_vars['page']['urlbread'] = "leads_status/";

    }

}
