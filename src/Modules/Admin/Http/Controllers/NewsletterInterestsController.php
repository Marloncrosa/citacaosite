<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\NewsletterInterests as model;

class NewsletterInterestsController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Pega a model atual
        $model = new $this->config->model;

        $this->view_vars['button']['add']['title'] = __('Novo Cadastro');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = "newsletter_interests/create";


        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __("Gerenciamento dos Interesses da Newsletter");
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this->view_vars['page']['opcional'] = __("Interesses da Newsletter");
        $this->view_vars['page']['urlbread'] = "newsletter_interests/";

    }

}
