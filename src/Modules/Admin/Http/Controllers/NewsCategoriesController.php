<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\NewsCategories as model;

class NewsCategoriesController extends Controller
{

    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __('Gerenciador de novas categorias');
        // $this -> view_vars['page']['subtitle'] 	= 'Preencha todos os campos para melhor exibição dos posts do blog.';
        $this->view_vars['page']['opcional'] = __('Novas categorias');

        // Botões
        $this->view_vars['button']['add']['title'] = __('Novo Cadastro');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = 'news_categories/create';

        $this->view_vars['pagina'] = $this->pagina;

    }
}
