<?php namespace Modules\Admin\Http\Controllers;

use Barryvdh\Reflection\DocBlock\Type\Collection;
use Modules\Admin\Entities\PortfolioGallery;
use Modules\Admin\Entities\PortfolioTypes;
use Modules\Admin\Entities\Portfolio as model;

class PortfolioController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Pega a model atual
        $model = new $this->config->model;

        $this->view_vars['button']['add']['title'] = __('Novo Portfólio');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = "portfolio/create";

        $this->view_vars['portfolio_types'] = PortfolioTypes::where('active', true)->get();
        // dd( $this->view_vars['portfolio_types']);

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __("Gerenciamento de Portfólios");
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this->view_vars['page']['opcional'] = __("Portfólios");
        $this->view_vars['page']['urlbread'] = "portfolio/";
    }

}
