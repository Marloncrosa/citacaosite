<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\News as model2;
use Modules\Admin\Entities\NewsComments as model;

class NewsCommentsController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Valida se foi passado o news_id
        if (!isset($this->vars_class['news_id'])) {
            return redirect()->to('news/')->withErrors(__(':text not found', [ 'text' => __('News') ]));
        }

        // Recupera o id
        $news_id = $this->vars_class['news_id'][0];

        // Pega a model atual
        $model = new $this->config->model;

        // Pegar model secundaria
        $model2 = new model2();

        // Pega dados atuais
        $item = $model2->where('id', $news_id)->first();

        $this->view_vars['button']['add']['title'] = __('New Record');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = "news_comments/create/news_id/{$news_id}";

        // Evento
        $this->view_vars['page']['ant-title-array']['news']['url'] = "news/";
        $this->view_vars['page']['ant-title-array']['news']['pagina'] = __("News");

        $this->view_vars['page']['ant-title-array']['noticia']['url'] = "news/edit/id/{$news_id}";
        $this->view_vars['page']['ant-title-array']['noticia']['class'] = "open-form-sidebar";
        $this->view_vars['page']['ant-title-array']['noticia']['pagina'] = "{$item->title}";

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __('News Comments Management');
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this->view_vars['page']['opcional'] = __('News Comments');
        $this->view_vars['page']['urlbread'] = route('admin.anyroute', ["news_comments/index/news_id/{$news_id}"]);

        if(isset($this->vars_class['news_comments_id'])){
            $news_comments_id = $this->vars_class['news_comments_id'][0];
            $this->view_vars['news_comments_id'] = $news_comments_id;

            $item2 = model::where('id', $news_comments_id)->first();

            $this->view_vars['page']['ant-title-array']['comentarios']['url'] = "news_comments/index/news_id/{$news_id}";
            $this->view_vars['page']['ant-title-array']['comentarios']['pagina'] = __('News Comments');

            $this->view_vars['page']['ant-title-array']['comentario']['url'] = "news_comments/edit/id/{$news_id}/news_id/{$news_id}";
            $this->view_vars['page']['ant-title-array']['comentario']['class'] = "open-form-sidebar";
            $this->view_vars['page']['ant-title-array']['comentario']['pagina'] = "{$item2->name}";

            $this->view_vars['button']['add']['link'] = "news_comments/create/news_id/{$news_id}/news_comments_id/{$news_comments_id}";

            $this->view_vars['button']['add']['link'] = "news_comments/create/news_id/{$news_id}/news_comments_id/{$news_comments_id}";

            $this->view_vars['page']['urlbread'] = "news_comments/index/news_id/{$news_id}/news_comments_id/{$news_comments_id}";
        }
    }

    public function beforeinit()
    {
        // Id do salão
        $news_id = $this->vars_class['news_id'][0];
        $this->view_vars['news_id'] = $news_id;

        // Where da listagem
        $this->config->listwhere = [
            'news_id' => [
                'news_id',
                '=',
                $news_id
            ]
        ];

        if(isset($this->vars_class['news_comments_id'])){
            $news_comments_id = $this->vars_class['news_comments_id'][0];
            $this->view_vars['news_comments_id'] = $news_comments_id;

            // Where da listagem
            $this->config->listwhere = [
                'news_comments_id' => [
                    'news_comments_id',
                    '=',
                    $news_comments_id
                ]
            ];

            unset($this->config->acao['comments']);
        }else{
            $news_comments_id = null;

            // Where da listagem
            $this->config->listwhere = [
                'news_comments_id' => [
                    'news_comments_id',
                    '=',
                    null
                ]
            ];
        }
        //hack para pegar o id do pai
//        $this->config->colunas['imagem']['uploadpath'] = "news/{$news_id}/";

        // Caso seja algum post para o store ou update
        if ($this->action == 'create' || $this->action == 'edit') {
            // Pagina
            $this->view_vars['urlform'] = route('admin.anyroute', ["news_comments/" . (($this->action == 'create') ? 'store' : 'update') . "/news_id/{$news_id}"]);

            if ($this->action == 'edit') {
                $this->view_vars['urlform'] .= '/' . $this->config->pk . '/' . $this->vars_class[$this->config->pk][0];

            }
            $this->view_vars['pagina'] = "news_comments/index/news_id/{$news_id}";

            if(isset($this->vars_class['news_comments_id'])){
                $this->view_vars['pagina'] .= "/news_comments_id/{$news_comments_id}";

                $this->view_vars['urlform'] .= "/news_comments_id/{$news_comments_id}";
            }

            // Campo hidden
            $this->view_vars['campohidden'] = 'news_id';
            $this->view_vars['valorhidden'] = $news_id;

            // Campo hidden
            $this->view_vars['campohidden2'] = 'news_comments_id';
            $this->view_vars['valorhidden2'] = $news_comments_id;
        }

        // Caso seja algum post para o store ou update
        if ($this->action == 'store' || $this->action == 'update' || $this->action == 'delete') {
            // Pagina
            $this->pagina = route('admin.anyroute', ["news_comments/index/news_id/{$news_id}"]);
            $this->view_vars['pagina'] = $this->pagina;

            if(isset($this->vars_class['news_comments_id'])){
                $this->view_vars['pagina'] .= "/news_comments_id/{$news_comments_id}";
            }
        }
    }

}
