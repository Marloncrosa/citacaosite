<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\Cart;
use Modules\Admin\Entities\Clients;
use Modules\Admin\Entities\Contact;
use Modules\Admin\Entities\HasProductInteress;
use Modules\Admin\Entities\Products;

class AdminController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = 'Página Inicial';

    }

    /**
     * Escreve o metodo do Controller - Padrão Index
     * @return [type] [description]
     */
    public function index()
    {
        // Basico
        // $this->view_vars['page']['extrat'] = '';
        // unset($this->view_vars['pagina']);
        $this->template = 'home.index';
        //
        // $totalPedidos = Cart::all();
        // $this->view_vars['totalPedidos'] = $totalPedidos;
        //
        // $totalVendasEfetivadas = Cart::where(function($query){
        //         $query->where('order_status_id', 3)
        //             ->orWhere('order_status_id', 4)
        //         ;
        //     })
        //     ->get();
        // $this->view_vars['totalVendasEfetivadas'] = $totalVendasEfetivadas;
        //
        // $totalClietnes = Clients::get();
        // $this->view_vars['totalClietnes'] = $totalClietnes;
        //
        // $produtosAtivos = Products::where('active', true)->get();
        // $this->view_vars['produtosAtivos'] = $produtosAtivos;
        //
        // $ultimosPedidos = Cart::with([
        //         'client',
        //         'order_status',
        //     ])
        //     ->limit(11)
        //     ->get();
        // $this->view_vars['ultimosPedidos'] = $ultimosPedidos;
        //
        // $ultimosInteressados = HasProductInteress::with([
        //     'product',
        // ])
        //     ->limit(4)
        //     ->get();
        // $this->view_vars['ultimosInteressados'] = $ultimosInteressados;


        return $this->renderizar();
    }
}
