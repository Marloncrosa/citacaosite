<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\ContactSector as model;

class ContactSectorController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __('Gerenciamento dos Setores dos Contatos');
//				$this -> view_vars['page']['subtitle'] 	= __('Gerencie Todos os e-mails de Contato');
        $this->view_vars['page']['opcional'] = __('Contatos Setores');

        // Botões
        $this->view_vars['button']['add']['title'] = __('Novo cadastro');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = 'contact_sector/create';


        $this->pagina = "contactsector";
        $this->view_vars['pagina'] = $this->pagina;
    }
}
