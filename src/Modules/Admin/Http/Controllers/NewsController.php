<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\News as model;

class NewsController extends Controller
{

    public function init()
    {
        // dd("aki");
        // Model
        $this->config->model = model::class;

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = __('Gerenciamento do Blog');
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this->view_vars['page']['opcional'] = __('Blog');

        // Botões
        $this->view_vars['button']['add']['title'] = __('Novo cadastro');
        $this->view_vars['button']['add']['class'] = 'open-form-sidebar';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = 'news/create';

        $this->view_vars['button']['categorias']['title'] = __('Gerenciar Novas categorias');
        $this->view_vars['button']['categorias']['class'] = 'warning';
        $this->view_vars['button']['categorias']['icone'] = 'fa fa-th-list';
        $this->view_vars['button']['categorias']['link'] = 'news_categories';
        $this->pagina = "news";
        $this->view_vars['pagina'] = $this->pagina;

        /**
         * Querys adicionais no list
         * @var [type]
         */
//        $this->config->queryadicionallist = [
//            'capaLista' => "
//                Concat(
//                '<a class=\"clearfix\" data-fancybox=\"group\" href=\"" . route('thumb', [0, 0, 'noticias']) . "/', id ,'/',capa,'\">',
//                    '<img src=\"" . route('thumb', [100, 50, 'noticias']) . "/', id ,'/',capa,'\" alt=\"\">',
//                '</a>'
//            ) as `capaLista`
//          "
//        ];
    }
}
