<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\Configuration as model;

class ConfiguracoesController extends Controller
{

    /**
     * Função Inicial
     * @return [type] [description]
     */
    public function init()
    {
        // Model
        $this->config->model = model::class;

        // Titulos do breadcrumb
        $this->view_vars['page']['title'] = 'Gerenciamento de Configurações';
        $this->view_vars['page']['opcional'] = 'Configurações';

        // Botões
        $this->view_vars['button']['add']['title'] = 'Criar novo registro';
        $this->view_vars['button']['add']['class'] = 'warning';
        $this->view_vars['button']['add']['icone'] = 'fa fa-plus-circle';
        $this->view_vars['button']['add']['link'] = 'configuracoes/create';
    }

}
