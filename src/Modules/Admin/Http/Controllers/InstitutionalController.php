<?php namespace Modules\Admin\Http\Controllers;

use Modules\Admin\Entities\Institutional as model;

class InstitutionalController extends Controller {


    public function init(){
        // Model
        $this -> config -> model = model::class;

        // Titulos do breadcrumb
        $this -> view_vars['page']['title'] 	= 'Gerenciamento de conteúdo Institucional';
        // $this -> view_vars['page']['subtitle'] 	= 'Gerencie Todos os Posts do blog';
        $this -> view_vars['page']['opcional'] 	= 'Institucional';

        // Botões
        $this -> view_vars['button']['add']['title'] 	= __('Novo Registro');
        $this -> view_vars['button']['add']['class'] 	= 'open-form-sidebar';
        $this -> view_vars['button']['add']['icone'] 	= 'fa fa-plus-circle';
        $this -> view_vars['button']['add']['link'] 	= 'institutional/create';

        $this->pagina                   = "institutional";
        $this->view_vars['pagina']      = $this->pagina;

    }

}
