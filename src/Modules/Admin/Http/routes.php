<?php

Route::group(['domain' => 'manager.'.env('APP_DOMAIN'), 'middleware' => 'admin', 'prefix' => '', 'namespace' => 'Modules\Admin\Http\Controllers'], function ($route) {

    $route->get('offline', 'OfflineController@index')->name('admin.offline');

    // Rotas de Autenticação
    $route->get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    $route->post('login', 'Auth\LoginController@login')->name('admin.login.post');

    $route->get('logout', 'Auth\LoginController@logout')->name('admin.logout');
    $route->post('logout', 'Auth\LoginController@logout')->name('admin.logout.post');

    $route->get('keep-alive', 'Auth\LoginController@keepalive')->name('admin.keepalive');

    // Rotas para cadastro e login via midias sociais
    //	$route->get('login/{provider}', 'Auth\LoginController@redirectToProvider')->name('admin.login.provider');
    //	$route->get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback')->name('admin.login.callback');

    // Password Reset Routes...
    $route->get('resetar-senha/', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.resetar-senha.request');
    $route->post('resetar-senha/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.resetar-senha.email');

    $route->get('resetar-senha/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.resetar-senha.reset');
    $route->post('resetar-senha/reset', 'Auth\ResetPasswordController@reset')->name('admin.resetar-senha.reset.post');

    $route->get('nova-senha/{token}', 'Auth\NewPasswordController@showForm')->name('admin.nova-senha.reset');
    $route->post('nova-senha/save', 'Auth\NewPasswordController@savePassword')->name('admin.nova-senha.save.post');


    // Rotas Privadas que precisa estar autenticado e ativo
    $route->group(['middleware' => ['authAdmin']], function ($route) {
        //  Homepage Route - Redirect based on user role is in controller.
        $route->get('/', 'AdminController@index')->name('admin.home');

        if(Request::segment(1) != 'webservice') {
            $controller = !is_null(Request::segment(1)) ? Request::segment(1) : 'admin';
            $controllerp = studly_case($controller);

            $action = !is_null(Request::segment(2)) ? Request::segment(2) : 'index';
            $method = strtolower(Request::method());

            $route->any('{slug}',
                [
                    //				'as' 	=> 	"site.{$controller}.{$action}.{$method}",
                    'uses' => "{$controllerp}Controller@{$action}"
                ]
            )->where('slug', '([A-Za-z0-9\-\_\/]+)')->name('admin.anyroute');
        }
    });
});
