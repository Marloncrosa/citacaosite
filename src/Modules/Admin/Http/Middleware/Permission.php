<?php

namespace Modules\Admin\Http\Middleware;

use Auth;
use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class Permission
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $user;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */

    public function __construct(Guard $auth)
    {
        $this->user = $auth->user();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param $role
     * @return mixed
     */

    public function handle($request, Closure $next, $parametros)
    {
        // Valida as permissões
        $permissions = $parametros;

//         dump($permissions);
//         dump($this->user->canpermission($permissions));
//         die;

        if(!$this->user->canpermission($permissions)){
        return redirect()->route('admin.home',[$request->segment(1)])->with('error', __("Você não tem permissão de acesso"));
    }

        return $next($request);
    }
}
