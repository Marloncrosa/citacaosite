<?php

namespace Modules\Admin\Http\Middleware;

use Closure;
use URL;
use Carbon\Carbon;
use View;
use Modules\Admin\Entities\Language;
use Auth;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param  string|null $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        if ($request->method() === 'GET') {
        $segment = $request->segment(1);

        $languages = Language::where('active', 1)->get()->pluck('shortname')->toArray();


        if (!in_array($segment, $languages)) {
            $segments = $request->segments();
            $fallback = session('locale') ?: Language::where('shortname', 'ar')->where('active', 1)->first()->shortname;
            unset($segments[0]);
            $segments = array_prepend($segments, $fallback);
            session(['locale' => $fallback]);


            return redirect()->to(implode('/', $segments));
        } elseif (is_null($segment)) {
            $segments = $request->segments();
            $fallback = Language::all()->first()->shortname;
            unset($segments[0]);
            $segments = array_prepend($segments, $fallback);
            session(['locale' => $fallback]);

            return redirect()->to(implode('/', $segments));
        }

        session(['locale' => $segment]);
        app()->setLocale($segment);

        // language
        $language = $segment;

        $idioma = Language::where('active', 1)->where('shortname', $language)->with('currency')->first();
        session(['localeObject' => $idioma]);

        app('translator')->setLocale($language);
        Carbon::setLocale($idioma->locale);

        $timezone = (empty($idioma->timezone))? env('APP_TIMEZONE', 'UTC') : $idioma->timezone;

        date_default_timezone_set($timezone);
        setlocale(LC_ALL, $idioma->locale);

        URL::defaults(['lang' => $language]);

        View::share('idioma_atual', $idioma);
        View::share('idiomas', Language::where('active', 1)->get());
        View::share('language', $language);
//        }

        return $next($request);
    }
}
