
require('popper.js');
require('bootstrap/dist/js/bootstrap.bundle.min');
require('js-cookie');

window.moment = require('moment');
// window.moment.locale('en');

require('tooltip.js');
import PerfectScrollbar from 'perfect-scrollbar';
window.PerfectScrollbar = PerfectScrollbar;
require('wnumb');

require('jquery.repeater');
require('jquery-form');
require('block-ui');
window.datepicker = require('bootstrap-datepicker/dist/js/bootstrap-datepicker');
require('bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min');
require('../metronic2/vendors/js/framework/components/plugins/forms/bootstrap-datepicker.init.js');

require('bootstrap-datetime-picker');
require('bootstrap-datetime-picker/js/locales/bootstrap-datetimepicker.pt-BR');
require('bootstrap-timepicker');
require('../metronic2/vendors/js/framework/components/plugins/forms/bootstrap-timepicker.init.js');

require('bootstrap-daterangepicker');

require('../metronic2/vendors/js/framework/components/plugins/forms/bootstrap-daterangepicker.init.js');
require('bootstrap-touchspin');
require('bootstrap-maxlength');
require('bootstrap-switch');
require('../metronic2/vendors/js/framework/components/plugins/forms/bootstrap-switch.init.js');
require('../metronic2/vendors/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter');
require('bootstrap-select');
require('select2/dist/js/select2.full.js');

require('typeahead.js');
require('handlebars');

require('nouislider');
require('owl.carousel');
require('autosize');
require('clipboard');
require('ion-rangeslider');
// require('summernote');
require('markdown');
require('bootstrap-markdown/js/bootstrap-markdown.js');
require('../metronic2/vendors/js/framework/components/plugins/forms/bootstrap-markdown.init.js');
require('jquery-validation');
require('jquery-validation/dist/additional-methods.js');
require('../metronic2/vendors/js/framework/components/plugins/forms/jquery-validation.init.js');
require('bootstrap-notify');
require('../metronic2/vendors/js/framework/components/plugins/base/bootstrap-notify.init.js');
require('toastr');
require('jstree');
require('raphael');
require('morris.js/morris');
require('chartist');
require('chart.js');
require('../metronic2/vendors/js/framework/components/plugins/charts/chart.init.js');
require('../metronic2/vendors/bootstrap-session-timeout/dist/bootstrap-session-timeout');
require('../metronic2/vendors/jquery-idletimer/idle-timer');
require('waypoints/lib/jquery.waypoints');
require('counterup/jquery.counterup');
require('es6-promise-polyfill');

import fullCalendar from 'fullcalendar';
window.fullCalendar = fullCalendar;
require('fullcalendar/dist/locale-all');

require("./js/framework/base/util.js");
require("./js/framework/base/app.js");
require("./js/framework/components/general/datatable/datatable.js");
require("./js/framework/components/plugins/misc/mdatatable.init.js");

function requireAll(r) {r.keys().forEach(r);}
requireAll(require.context('./js/framework/components/general/', true, /\.js$/));
// requireAll(require.context('./js/demo/default/base/', true, /\.js$/));
requireAll(require.context('./js/demo/demo7/base/', true, /\.js$/));
requireAll(require.context('./js/app/base/', true, /\.js$/));
requireAll(require.context('./js/snippets/base/', true, /\.js$/));
// requireAll(require.context('./js/demo/default/custom/', true, /\.js$/));

// // require("../metronic2/js/framework/components/general/**/*.js");
// // require("../metronic2/js/demo/default/base/**/*.js");
// // require("../metronic2/js/app/base/**/*.js");
// // require("../metronic2/js/snippets/base/**/*.js");
//
// require("./js/demo/demo7/base/layout");
// require('./js/snippets/custom/pages/user/login');

// require('./classic/base/scripts.bundle.js');

require('./js/snippets/custom/pages/user/login');
