var staticCacheName = "manager-pwa-v" + new Date().getTime();
// var staticCacheName = "manager-pwa-v1";
var filesToCache = [
    // '/login',
    '/offline',
    '/modules/admin/css/admin.css',
    '/modules/admin/js/admin.js',
    '/modules/admin/img/favicon/favicon-16x16.png',
    '/modules/admin/img/favicon/favicon-194x194.png',

    '/modules/admin/img/favicon/android-chrome-36x36.png',
    '/modules/admin/img/favicon/android-chrome-48x48.png',
    '/modules/admin/img/favicon/android-chrome-72x72.png',
    '/modules/admin/img/favicon/android-chrome-96x96.png',
    // '/modules/admin/manifest.json',
];


self.addEventListener('install', function(event) {
    this.skipWaiting();
    event.waitUntil(
        caches.open(staticCacheName)
            .then(cache => {
                return cache.addAll(filesToCache);
            })
    );
});


// Clear cache on activate
self.addEventListener('activate', event => {
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames
                    .filter(cacheName => (cacheName.startsWith("manager-pwa-")))
                    .filter(cacheName => (cacheName !== staticCacheName))
                    .map(cacheName => caches.delete(cacheName))
            );
        })
    );
});

// Serve from Cache
self.addEventListener("fetch", event => {
    event.respondWith(
        caches.match(event.request)
            .then(response => {
                return response || fetch(event.request);
            })
            .catch(() => {
                // return caches.match('offline');

                return fetch(event.request).then(function(res) {
                    return caches.open('dynamic').then(function(cache) {
                        cache.put(event.request.url, res.clone());
                        return res;
                    });
                });
            })
    );
});
