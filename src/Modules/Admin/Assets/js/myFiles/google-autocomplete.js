var GoogleMapsAutoComplete = GoogleMapsAutoComplete || {};
window.GoogleMapsAutoComplete = GoogleMapsAutoComplete;

GoogleMapsAutoComplete.Utilities = (function () {
    var _getUserLocation = function (successCallback, failureCallback) {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                successCallback(position);
            }, function () {
                failureCallback(true);
            });
        } else {
            failureCallback(false);
        }
    };

    return {
        GetUserLocation: _getUserLocation
    }
})();

GoogleMapsAutoComplete.Application = (function () {
    var _geocoder;

    var _init = function () {
        _geocoder = new google.maps.Geocoder;
        _initAutocompletes();
    };

    var _initwithuserlocation = function () {
        _geocoder = new google.maps.Geocoder;

        GoogleMapsAutoComplete.Utilities.GetUserLocation(function (position) {
            var latLng = {
                lat: position.coords.latitude,
                lng: position.coords.longitude
            };
            _autofillFromUserLocation(latLng);
            _initAutocompletes(latLng);
        }, function (browserHasGeolocation) {
            _initAutocompletes();
        });
    };

    var _initAutocompletes = function (latLng) {
        var keydownTimeout = setTimeout(function () {
        }, 100);
        $('.places-autocomplete').each(function () {
            var input = this;
            var isPostalCode = (typeof $(input).data('postal-code') !== 'undefined');
            var hasLatLng = (typeof $(input).data('latlng') !== 'undefined');
            var autocomplete = new google.maps.places.Autocomplete(input, {
                types: [isPostalCode ? '(regions)' : 'address']
            });
            if (latLng) {
                _setBoundsFromLatLng(autocomplete, latLng);
            }
            if (hasLatLng) {
                _setBoundsFromLatLng(autocomplete, $(input).data('latlng'));
            }

            autocomplete.addListener('place_changed', function () {
                _placeChanged(autocomplete, input);
            });

            $(input).on('keydown', function (e) {
                // Prevent form submit when selecting from autocomplete dropdown with enter key
                if (e.keyCode === 13 && $('.pac-container:visible').length > 0) {
                    e.preventDefault();
                }
            });
        });
    };

    var _autofillFromUserLocation = function (latLng) {
        _reverseGeocode(latLng, function (result) {
            $('.address').each(function (i, fieldset) {
                _updateAddress({
                    fieldset: fieldset,
                    address_components: result.address_components
                });
            });
        });
    };

    var _reverseGeocode = function (latLng, successCallback, failureCallback) {
        _geocoder.geocode({'location': latLng}, function (results, status) {
            if (status === 'OK') {
                if (results[1]) {
                    successCallback(results[1]);
                } else {
                    if (failureCallback)
                        failureCallback(status);
                }
            } else {
                if (failureCallback)
                    failureCallback(status);
            }
        });
    };

    var _setBoundsFromLatLng = function (autocomplete, latLng) {
        var circle = new google.maps.Circle({
            radius: 40233.6, // 25 mi radius
            center: latLng
        });
        autocomplete.setBounds(circle.getBounds());
    };

    var _placeChanged = function (autocomplete, input) {
        var place = autocomplete.getPlace();
        _updateAddress({
            input: input,
            address_components: place.address_components
        });
    };


    var _countryValue = function (element, value) {
        // element.val(value.short_name).trigger('change');
        if (element.find('option:contains(' + value.long_name + ')').length > 0) { //se for um select e contem o texto como uf
            element.find('option:contains(' + value.long_name + '):first').prop("selected", true).trigger("change"); // aciona o change para trazer as cidades
        } else {
            if ($.fn.select2) {
                element.parents('form').prepend('<input type="hidden" name="new_country[initials]" value="' + value.short_name + '">');
                element.parents('form').prepend('<input type="hidden" name="new_country[country]" value="' + value.long_name + '">');
                var newOption = new Option(value.short_name, 'new_country', true, true);
                // Append it to the select
                element.append(newOption).trigger('change');
            } else {
                element.val(value.short_name);
            }
        }
    };

    var _stateValue = function (element, value) {
        if (element.find('option:contains(' + value.short_name + ')').length > 0) { //se for um select e contem o texto como uf
            element.find('option:contains(' + value.short_name + ')').prop("selected", true).trigger("change"); // aciona o change para trazer as cidades
        } else if (element.find('option:contains(' + estadoJson[value.short_name] + ')')) { //se for um select e contem o texto como o estado descrito
            element.find('option:contains(' + estadoJson[value.short_name] + ')').prop("selected", true).trigger("change"); // aciona o change para trazer as cidades
        } else {
            if ($.fn.select2) {
                element.parents('form').prepend('<input type="hidden" name="new_state[initials]" value="' + value.short_name + '">');
                element.parents('form').prepend('<input type="hidden" name="new_state[state]" value="' + value.long_name + '">');
                var newOption = new Option(value.short_name, 'new_state', true, true);
                // Append it to the select
                element.append(newOption).trigger('change');
            } else {
                element.val(value.short_name);
            }
        }
    };

    var _cityValue = function (element, value) {
        if (element.find('option:contains(' + value.long_name + ')').length > 0) { //se for um select e contem o texto como uf
            element.find('option:contains(' + value.long_name + '):first').prop("selected", true).trigger("change"); // aciona o change para trazer as cidades
        } else {
            if ($.fn.select2) {
                element.parents('form').prepend('<input type="hidden" name="new_city" value="' + value.short_name + '">');
                var newOption = new Option(value.short_name, 'new_city', true, true);
                // Append it to the select
                element.append(newOption).trigger('change');
            } else {
                element.val(value.short_name);
            }
        }
    };

    var _updateAddress = function (args) {
        var isPostalCode = false;
        if (args.input) {
            // $fieldset = $(args.input).closest('fieldset');
            isPostalCode = (typeof $(args.input).data('postal-code') !== 'undefined');
            // console.log(isPostalCode);
        } /*else {
            $fieldset = $(args.fieldset);
        }*/

        $eInput = $(args.input);

        var $country = $($eInput.data('country'));
        var $state = $($eInput.data('state'));
        var $city = $($eInput.data('city'));
        var $address = $($eInput.data('address'));
        var $neighborhood = $($eInput.data('neighborhood'));
        var $streetNumber = $($eInput.data('number'));
        var $focusField = $($eInput.data('focus'));

        var $postalCode = '';
        if (isPostalCode) {
            $postalCode = $(args.input);
        } else if ((typeof $(args.input).data('postalcode') !== 'undefined')) {
            $postalCode = $($eInput.data('postalcode'));
        }
        // $postalCode.val('');
        // $city.val('');
        // $country.val('');
        // $state.val('');

        components = (args.address_components).reverse();

        for (var i = 0; i < components.length; i++) {
            var component = components[i];
            var addressType = component.types[0];

            switch (addressType) {
                case 'postal_code':
                    if ($postalCode.length > 0) {
                        $postalCode.val(component.long_name);
                    }
                    break;
                case 'country':
                    _countryValue($country, component);
                    break;
                case 'administrative_area_level_1':
                    _stateValue($state, component);
                    break;
                case 'administrative_area_level_2':
                    _cityValue($city, component);
                    break;
                case 'locality':
                    $city.val('');
                    _cityValue($city, component);
                    break;
                case 'sublocality_level_1':
                    $neighborhood.val(component.long_name);
                    break;
                case 'route':
                    $address.val(component.long_name);
                    break;
                case 'street_number':
                    $streetNumber.val(component.long_name);
                    break;
            }
        }

        $focusField.focus();
    };

    return {
        Init: _init,
        InitWithUserLocation: _initwithuserlocation,
    };
})();
