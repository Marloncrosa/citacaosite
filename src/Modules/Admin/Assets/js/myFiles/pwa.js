if ('serviceWorker' in navigator) {
    navigator.serviceWorker
        .register('/modules/admin/js/service-worker.js', {
            scope: '../../../'
        })
        /*.register('/modules/admin/js/service-worker.js')*/
        .then(function (registration) {
            console.log(
                'Service Worker registrado com sucesso com scope: ',
                registration.scope
            );
        })
        .catch(function (err) {
            console.log('Service Worker falhou em registrar-se: ', err);
        });
}
