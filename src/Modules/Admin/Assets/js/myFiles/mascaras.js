window.mascaras = function () {
    $('input').inputmask("remove");

    /* Mascaras */
    $('input[data-inputmask]').inputmask();

    /* E-mail */
    $email_mask = $('.mascara-email');
    if ($email_mask.length > 0) {
        $email_mask.inputmask("email");
    }
    /* E-mail */

    /* Link"s */
    $linkSite = $('.mascara-link');
    if ($linkSite.length > 0) {
        $linkSite.inputmask("url");
    }
    /* Link"s */

    /* Telefone */
    $telefone = $('.mascara-telefone')
    $ramal_tel = $('.mascara-ramal')

    if ($telefone.length > 0) {
        $telefone.inputmask("(99) 9999-9999 [9]");
        $telefone.on('focusout', function () {
            var elemento_at, element;

            element = $(this);

            telefone = element.val().replace(/\D/g, "");

            // console.log('aki_telefone')

            if (telefone.length > 10) {
                element.inputmask("(99) 99999-9999");
            } else {
                element.inputmask("(99) 9999-9999 [9]");
            }

        });

        if ($telefone.val() != '') {
            $telefone.trigger("focusout");
        }
    }

    if ($ramal_tel.length > 0) {
        $ramal_tel.inputmask("(999) ")
    }
    /* Telefone */

    /* Documento */
    $documento = $('.mascara-documento');
    if ($documento.length > 0) {
        /*
                Quem encontrar problemas com o "target", utilize esta alternativa
            */
        $documento.on('focusout', function () {

            var elemento_at, element;

            element = $(this);

            elemento_at = element.val().replace(/\D/g, "");

            if (elemento_at.length > 12) {
                element.inputmask("99.999.999/9999-99");
                return false;
            }

            if (elemento_at.length > 11 && elemento_at.length <= 12) {
                element.inputmask("99.999.99999/99 [99]");
                return false;
            }

            if (elemento_at.length <= 11) {
                element.inputmask("999.999.999-99 [999]");
                return false;
            }
        });

        // Documento = $documento.val();
        $documento.inputmask("999.999.999-99 [999]");

        if ($documento.val() != '') {
            $documento.trigger("focusout");
        }
    }
    /* Documento */

    /* CPF */
    cpf = $('.mascara-cpf');
    if (cpf.length > 0) {
        cpf.inputmask("999.999.999.99");
    }
    /* CPF */

    /* RG */
    $rg = $('.mascara-rg');
    if ($rg.length > 0) {
        $rg.inputmask("999.999.999-9");
    }
    /* RG */

    /* Cep */
    $cep = $('input').filter('.mascara-cep');
    if ($cep.length > 0) {
        $cep.inputmask("99.999-999");
        if ($cep.hasClass('cep-preenche')) {
            $estado = $($cep.data('estado')); // colocar qual elemento é o estado
            $cidade = $($cep.data('cidade')); // colocar qual elemento é a cidade
            $endereco = $($cep.data('endereco')); // colocar qual elemento é o endereço
            $bairro = $($cep.data('bairro')); // colocar qual elemento é o bairro
            $numero = $($cep.data('numero')); // colocar qual elemento é o numero
            $pais = $($cep.data('pais')); // colocar qual elemento é o pais

            $cep.on('keyup', function () {

                clearTimeout(cepTime);

                // Valor do cep
                valorCep = $cep.val().replace(/\D/g, '');

                //Expressão regular para validar o CEP.
                var validacep = /^[0-9]{8}$/;

                //Valida o formato do CEP.
                if (validacep.test(valorCep)) {
                    cepTime = setTimeout(function () {

                        cepPromise(valorCep)
                            .then((json) => {
                                // Coloca o logradouro no campo
                                if ($endereco) $endereco.val(json.street);
                                // Coloca o bairro no campo
                                if ($bairro) $bairro.val(json.neighborhood);

                                if ($pais) $pais.find("option:containsIn(Brasil)").prop("selected", true).trigger('change');

                                // Verifica se o estao é uma select
                                if ($estado) {
                                    if ($estado.is('select')) {
                                        if ($estado.find('option:contains(' + json.state + ')').length > 0) { //se for um select e contem o texto como uf
                                            $estado.find('option:contains(' + json.state + ')').prop("selected", true).trigger("change"); // aciona o change para trazer as cidades
                                        } else if ($estado.find('option:contains(' + (json.state).toUpperCase() + ')').length > 0) { //se for um select e contem o texto como o estado descrito
                                            $estado.find('option:contains(' + (json.state).toUpperCase() + ')').prop("selected", true).trigger("change"); // aciona o change para trazer as cidades
                                        } else if ($estado.find('option:contains(' + estadoJson[json.state] + ')').length > 0) { //se for um select e contem o texto como o estado descrito
                                            $estado.find('option:contains(' + estadoJson[json.state] + ')').prop("selected", true).trigger("change"); // aciona o change para trazer as cidades
                                        } else if ($estado.find('option:contains(' + (estadoJson[json.state]).toUppercase() + ')').length > 0) { //se for um select e contem o texto como o estado descrito
                                            $estado.find('option:contains(' + (estadoJson[json.state]).toUppercase() + ')').prop("selected", true).trigger("change"); // aciona o change para trazer as cidades
                                        } else {
                                            swal('Estado não encontrado!', '', 'error');
                                        }
                                    } else { // se for um campo de texto
                                        $estado.val(json.state);
                                    }
                                }

                                // mesmo processo do estado
                                if ($cidade) {
                                    if ($cidade.is('select')) {
                                        cidadex = json.city.replace(/[^\w\s]/gi, '');
                                        cidadeTime = setTimeout(function () {
                                            if ($cidade.find('option:contains(' + cidadex + '):first').length > 0) {
                                                $cidade.find('option:contains(' + cidadex + '):first').prop("selected", true).trigger("change");
                                            } else {
                                                $cidade.find('option:contains(' + (cidadex).toUpperCase() + '):first').prop("selected", true).trigger("change");
                                            }
                                        }, 1000);
                                    } else {
                                        $cidade.val(json.city).trigger('blur');
                                    }
                                }

                                if ($numero) $numero.trigger("focus");
                            })
                            .catch((rvalue) => {
                                console.log(rvalue);
                                swal('CEP inválido ou não encontrado!', null, "error");
                            });

                        // $.ajax({
                        //     url: "//viacep.com.br/ws/" + valorCep + "/json/?callback=?",
                        //     type: "get",
                        //     dataType: "json",
                        //     timeout: 3000
                        // })
                        //     .done(function (json) {
                        //     })
                        //     .fail(function () {
                        //         swal('CEP não encontrado!', null, "error");
                        //     })
                        // ;

                    }, 1500);
                }

            });
        }
    }
    /* Cep */

    /* Preço */
    $preco = $('.mascara-preco');

    if ($preco.length > 0) {
        $preco.each(function () {
            campo = $(this);
            decimais = (campo.data('decimais') != undefined && campo.data('decimais') != '' ? campo.data('decimais') != undefined : 2);

            campo.inputmask('decimal', {
                'alias': 'numeric',
                'groupSeparator': '.',
                'autoGroup': true,
                'digits': decimais,
                'radixPoint': ",",
                'digitsOptional': false,
                'allowMinus': false,
                'prefix': 'R$ ',
                'placeholder': ''
            });
        })
    }
    $preco = $('.mascara-preco-dolar');

    if ($preco.length > 0) {
        $preco.each(function () {
            campo = $(this);
            decimais = (campo.data('decimais') != undefined && campo.data('decimais') != '' ? campo.data('decimais') != undefined : 2);

            campo.inputmask('decimal', {
                'alias': 'numeric',
                'groupSeparator': ',',
                'autoGroup': true,
                'digits': decimais,
                'radixPoint': ".",
                'digitsOptional': false,
                'allowMinus': false,
                'prefix': 'US$ ',
                'placeholder': ''
            });
        })
    }
    /* Preço */

    /* Data */
    $data_mask = $('.mascara-data');
    if ($data_mask.length > 0) {
        $data_mask.inputmask("99/99/9999");

        if ($.fn.datepicker) {
            $data_mask.datepicker({
                language: 'pt-BR',
                showAnim: "slideDown",
                changeMonth: true,
                changeYear: true,
                beforeShow: function (input, inst) {
                    /*datepickerVar = inst.dpDiv;
                              campo_pre = $(input).parent();

                              campo_pre.append(datepickerVar);
                              // $(datepickerVar).appendTo( formulario_pagina );
                              setTimeout(function(){
                                  $(datepickerVar).position({
                                      my: "right top",
                                      at: "left top",
                                      of: input
                                  });
                              },10)*/
                }
            });
        }
    }
    /* Data */

    /* Data e Hora */
    $data_hora_mask = $('.mascara-datahora');
    if ($data_hora_mask.length > 0) {
        $data_hora_mask.inputmask("99/99/9999 99:99");
        if ($.fn.datetimepicker) {
            $data_hora_mask.datetimepicker({
                language: 'pt-BR',

                todayHighlight: true,
                autoclose: true,

                showAnim: "slideDown",

                changeMonth: true,
                changeYear: true,

                todayBtn: true,

                format: 'dd.mm.yyyy hh:ii',

                beforeShow: function (input, inst) {
                    /*datepickerVar = inst.dpDiv;
                              campo_pre = $(input).parent();

                              campo_pre.append(datepickerVar);
                              // $(datepickerVar).appendTo( formulario_pagina );

                              setTimeout(function(){
                                  $(datepickerVar).position({
                                      my: "right top",
                                      at: "left top",
                                      of: input
                                  });
                              },10)*/
                }
            });
        }
    }
    /* Data e Hora */

    /* Hora */
    // $horario_mask = $('.mascara-horario');
    // if ($horario_mask.length > 0) {
    //   mask = "99:99";
    //   $horario_mask.inputmask("99:99");
    // }
    /* Hora */

    /* PESO */
    $peso = $('.mascara-peso');
    if ($peso.length > 0) {
        $peso.inputmask("99,99");
    }
    /* PESO */
    /* Mascaras */
};
