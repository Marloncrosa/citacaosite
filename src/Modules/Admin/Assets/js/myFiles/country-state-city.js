window.isJson = function (str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
};

$(function () {

    country = $('form [name*="country"]');
    state = $('form [name*="state"]');
    city = $('form [name*="city"]');
    unit = $('form [name*="unit"]');
    course = $('form [name*="course"]');

    if ($.fn.intlTelInput) {
        var countryData = window.intlTelInputGlobals.getCountryData();
        window.intltelinput = $(".mask-intl-phone");
        $(".mask-intl-phone").intlTelInput({
            utilsScript: window.utilsIntlTel,
            autoPlaceholder: 'aggressive',
            initialCountry: 'AR',
            preferredCountries: ['AR', 'BR'],
            formatOnDisplay: false,
        });

        $(".mask-intl-phone").off('countrychange').on("countrychange", function () {
            var elemento = $(this),
                countrySelected = $(this).intlTelInput("getSelectedCountryData"),
                mask = '+' + countrySelected.dialCode + ' ' + elemento.attr('placeholder').replace(/[0-9]/g, 9);

            if (countrySelected.iso2 == 'ar') {
                // mask = '+' + countrySelected.dialCode + ' 999 999-9999';
                mask = [
                    '+' + countrySelected.dialCode + ' 999 999-9999',
                    '+' + countrySelected.dialCode + ' 99 999 999-9999',
                ];
            }

            elemento.inputmask("remove").inputmask({
                mask: mask
            });

            // elemento.trigger('focusout');
            // setTimeout(function () {
            //   elemento.trigger('focusin');
            // }, 200);
        });
        // // setTimeout(function () {}, 500)
        intltelinput.trigger('countrychange');
    }

    // If exist city select
    if (country.length > 0) {
        stateValue = '';
        $(document).on('change', 'form [name*="country"]', function (event) {
            event.preventDefault();
            var element = $(event.target),
                value = element.val(),
                state = (element[0].hasAttribute("data-state"))? $(element.data('data-state')) : element.parents('form').find('form [name*="state"]'),
                stateValue = state.val(),
                stateDataValue = state.data('value'),
                requestState;

            if(typeof element.data('stateinput') != undefined){

                state = $(element.data('stateinput'));
                stateValue = state.val();
                stateDataValue = state.data('value');

                if ((value == '' && value == undefined) || element.parents('form').find(element.data('stateinput')).length <= 0 ) {
                    return false;

                }
            }else {
                if ((value == '' && value == undefined) || element.parents('form').find('[name*="state"]').length <= 0 ) {
                    return false;

                }
            }


            if ($.fn.intlTelInput) {
                $(this).parents('form').find(".mask-intl-phone").intlTelInput("setCountry", this.value);
            }

            requestState = $.ajax({
                url: apiurl + 'states',
                crossDomain: true,
                type: 'POST',
                async: false,
                data: {
                    country_id: value
                }
            });

            requestState.done(function (data) {
                if (isJson(data)) {
                    state.html('');
                    data = JSON.parse(data);
                    $.each(data, function (index, data) {
                        state.append('<option value="' + data.id + '">' + data.state + '</option>');
                    });
                    if (!!stateValue) {
                        state.find('option[value="' + stateValue + '"]').prop('selected', true);
                    }
                    if (!!stateDataValue) {
                        state.find('option[value="' + stateDataValue + '"]').prop('selected', true);
                    }
                } else {
                    state.html('<option value="">' + data + '</option>');
                    // swal(data, null, "error")
                }
            });

        });
    }

    // City, state and countr
    if (city.length > 0) {
        cityValue = '';
        $(document).on('change', 'select[id*="state"],select[name*="state"],select[class*="state"]', function (event) {
            event.preventDefault();
            var element = $(event.target),
                value = element.val(),
                city = (element[0].hasAttribute("data-city"))? $(element.data('data-city')) : element.parents('form').find('select[id*="city"],select[name*="city"],select[class*="city"]'),
                cityValue = city.val(),
                requestCity;

            if (value == '' && value == undefined) {
                return false;
            }

            requestCity = $.ajax({
                url: '/webservice/cities',
                type: 'POST',
                async: false,
                data: {
                    state_id: value
                }
            });

            requestCity.done(function (data) {
                if (isJson(data)) {
                    city.html('');
                    data = JSON.parse(data);
                    $.each(data, function (index, data) {
                        city.append('<option value="' + data.id + '">' + data.city + '</option>');
                    });
                    if (!!cityValue) {
                        city.find('option[value="' + cityValue + '"]').prop('selected', true);
                    }
                } else {
                    city.html('<option value="">' + data + '</option>');
                    // swal(data, null, "error")
                }
            });
        });
    }

    country.each(function () {
        if ($(this).val() != '' && $(this).val() != undefined) {
            $(this).trigger('change');
        }
    });

    setTimeout(function () {
        state.each(function () {
            if ($(this).val() != '' && $(this).val() != undefined) {
                $(this).trigger('change');
            }
        });
    }, 500);
});
