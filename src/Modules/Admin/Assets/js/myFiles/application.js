$(function () {
    // Expressao contains in
    $.expr[":"].containsIn = function (el, i, m) {
        var search = m[3];
        if (!search) {
            return false;
        }
        return eval("/" + search + "/i").test($(el).text());
    };

    // NEW selector
    $.expr[':'].Contains = function (a, i, m) {
        return $(a).text().toUpperCase()
            .indexOf(m[3].toUpperCase()) >= 0;
    };

    // OVERWRITES old selecor
    $.expr[':'].contains = function (a, i, m) {
        return $(a).text().toUpperCase()
            .indexOf(m[3].toUpperCase()) >= 0;
    };

    $.fn.serializeFormJSON = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    // Foundation
    if ($.fn.foundation) {
        Foundation.Interchange.SPECIAL_QUERIES['small-portrait'] = 'screen and (min-width: 0em) and (orientation: portrait)';
        Foundation.Interchange.SPECIAL_QUERIES['small-landscape'] = 'screen and (min-width: 0em) and (orientation: landscape)';
        Foundation.Interchange.SPECIAL_QUERIES['medium-portrait'] = 'only screen and (min-width: 40em) and (orientation: portrait)';
        Foundation.Interchange.SPECIAL_QUERIES['medium-landscape'] = 'only screen and (min-width: 40em) and (orientation: landscape)';
        $(document).foundation();
    }

    // Datatables
    if (window.LaravelDataTables != undefined) {
        $.fn.dataTable.ext.errMode = 'none';
        datatables = window.LaravelDataTables["dataTableBuilder"];


        $(document).on("change", ".m-group-checkable", function () {
            window.e = LaravelDataTables["dataTableBuilder"];

            var a = $(this).closest("table").find("td:first-child .m-checkable"),
                t = $(this).is(":checked");
            $(a).each(function () {
                t ? ($(this).prop("checked", !0), e.rows($(this).closest("tr")).select()) : ($(this).prop("checked", !1), e.rows($(this).closest("tr")).deselect())
            })
        });

        $(document).on("change", "tbody tr .m-checkbox", function () {
            $(this).parents("tr").toggleClass("active")
        });

        // $(".m-group-checkable").on("change",  function () {
        //     console.log('aki')
        //     var a = $(this).closest("table").find("td:first-child .m-checkable"), t = $(this).is(":checked");
        //     $(a).each(function () {
        //         t ? ($(this).prop("checked", !0), e.rows($(this).closest("tr")).select()) : ($(this).prop("checked", !1), e.rows($(this).closest("tr")).deselect())
        //     })
        // })
    }

    // Ajax de bõtões
    // $(document).on('click', '[data-ajax]', function(){
    // 	elemento = $(this)
    // 	swal(
    // 		{
    // 			title: elemento.data('titulo'),
    // 			type: elemento.data('classeswal'),
    // 			showCancelButton: true,
    // 			closeOnConfirm: false,
    // 			showLoaderOnConfirm: true,
    // 		},
    // 		function(){
    // 			location.href = elemento.data('ajax')
    // 			// setTimeout(function(){
    // 			// 	swal("Ajax request finished!");
    // 			// }, 2000);
    // 		}
    // 	);
    // 	return false;
    // })

    // Ajax
    $(document).on('click', '[data-callajax]', function () {
        var elemento = $(this),
            texto = ((elemento.data('texto') != undefined) ? elemento.data('texto') : 'Será feita uma alteração?'),
            remover = ((elemento.data('remover') != undefined) ? $(elemento.data('remover')) : '')
        ;

        $.ajax({
            url: elemento.data('ajax'),
            type: 'GET',
        })
            .always(function (data) {
                let title = (data.title != undefined) ? data.title : (data.message != undefined) ? data.message : '',
                    message = (title != data.message && data.message != undefined) ? data.message : '',
                    type = (data.type != undefined && data.type != 'error') ? 'success' : 'error';
                swal(
                    {
                        title: title,
                        text: message,
                        type: type,
                    }
                );
                if (data.type != undefined && data.type != 'error') {
                    LaravelDataTables.dataTableBuilder.draw();
                }
            });

        return false;
    });

    $(document).on('click', '[data-callajaxh]', function () {
        var elemento = $(this),
            texto = ((elemento.data('texto') != undefined) ? elemento.data('texto') : 'Será feita uma alteração, tem certeza?'),
            remover = ((elemento.data('remover') != undefined) ? $(elemento.data('remover')) : '')
        ;

        swal({
            title: "Informação",
            text: texto,
            icon: 'info',
            buttons: {
                cancel: true,
                confirm: true,
            },
        }).then((value) => {
            if (value) {
                $.ajax({
                    url: elemento.prop('href'),
                    type: 'GET',
                })
                    .done(function (data) {
                        swal({title: data.message, type: data.type, icon: "success"});
                        if (data.type == 'success') {
                            if (remover != '') {
                                remover.remove();
                            } else {
                                // if (elemento.parents('tr').length > 0) {
                                //     elemento.parents('tr').remove();
                                parent.LaravelDataTables.dataTableBuilder.draw();
                                // }
                            }
                        }
                    })
                    .fail(function (data) {
                        swal({title: 'Houve um problema!', type: 'error', icon: "info"});
                    });

            }
        });

        return false;
    });

    // Deletar
    $(document).on('click', '[data-excluir]', function () {
        var elemento = $(this),
            texto = ((elemento.data('texto') != undefined) ? elemento.data('texto') : 'Deseja excluir o registro atual?'),
            remover = ((elemento.data('remover') != undefined) ? $(elemento.data('remover')) : '')
        ;

        swal({
            text: texto,
            icon: "error",
            buttons: ['Não', 'Sim'],
            dangerMode: true,
        })
            .then((VaiDeletar) => {
                    if (VaiDeletar) {
                        $.ajax({
                            url: elemento.data('ajax'),
                            type: 'GET',
                        })
                            .always(function (data) {
                                swal({title: data.message, type: data.type, icon: "success"});
                                if (data.type == 'success') {
                                    if (remover != '') {
                                        remover.remove();
                                    } else {
                                        if (parent.LaravelDataTables) {
                                            if (elemento.parents('tr').length > 0) {
                                                elemento.parents('tr').remove();
                                                // datatables.ajax.reload();
                                                parent.LaravelDataTables.dataTableBuilder.draw();
                                            }
                                        }
                                        if (parent.Calendar) {
                                            parent.Calendar.fullCalendar('refetchEvents')
                                        }
                                    }
                                }
                            });
                    }
                }
            )
        ;

        return false;
    });

    // Ativar e Desativar
    $(document).on('click', '[data-alterarbool]', function () {
        var elemento = $(this),
            icone = elemento.find('> i'),
            icone1 = elemento.data('icone1'),
            text1 = elemento.data('texto1'),
            icone2 = elemento.data('icone2'),
            text2 = elemento.data('texto2')
        ;
        $.ajax({
            url: elemento.data('ajax'),
            type: 'GET',
        })
            .always(function (data) {
                if (data.type == 'error') {
                    swal(
                        {
                            title: data.message,
                            type: 'error',
                        }
                    );
                } else if (data.type == 'success') {
                    if (icone.length <= 0) {
                        swal(
                            {
                                title: 'Campo alterado com sucesso.',
                                type: 'success',
                            }
                        );
                    } else {
                        if (icone.hasClass(icone1)) {
                            icone.removeClass(icone1).addClass(icone2);
                            elemento.find('span.text').html(text2);
                            // console.log(icone2)
                            // console.log(text2)
                        } else if (icone.hasClass(icone2)) {
                            icone.removeClass(icone2).addClass(icone1);
                            elemento.find('span.text').html(text1);
                        }
                    }
                }
            });
        return false;
    });

    // Editor
    if (tinymce) {
//	tinymce.baseURL = 'js/tinymce';
        /*
        tinymce.init({
            selector: "[data-editor-simples]",
            language: 'pt_BR',
            relative_urls: false,
            remove_script_host: false,
            cleanup_on_startup: false,
            trim_span_elements: false,
            verify_html: false,
            cleanup: false,
            convert_urls: false,
            force_br_newlines: false,
            force_p_newlines: false,
            forced_root_block: '',
            extended_valid_elements: '+*[*]',
            // theme: "modern",
            // width: 680,
            height: 300,
            menubar: false,
            plugins: [
                "link charmap",
                "searchreplace wordcount",
                "contextmenu paste textcolor code"
            ],
            toolbar1: "undo redo | bold italic | outdent indent forecolor code",
            image_advtab: true,

            filemanager_access_key: window.app_key,
            external_filemanager_path: "tinymce/plugins/responsivefilemanager/",
            filemanager_title: "Gerenciador de arquivos" ,
            external_plugins: {
                "responsivefilemanager": "tinymce/plugins/responsivefilemanager/plugin.min.js"
            }
        });
        */

        tinymce.init({
            selector: "[data-editor-completo]",
            language: 'pt_BR',
            relative_urls: false,
            remove_script_host: false,
            cleanup_on_startup: false,
            trim_span_elements: false,
            verify_html: false,
            cleanup: false,
            convert_urls: false,
            force_br_newlines: false,
            force_p_newlines: false,
            forced_root_block: '',
            extended_valid_elements: '+*[*]',
            statusbar: false,
            valid_elements: '+*[*]',
            valid_children: '+*[*]',
            // theme: "modern",
            // width: 680,
            height: 300,
            plugins: [
                "advlist autolink link image lists charmap print hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars insertdatetime media nonbreaking",
                "table contextmenu directionality emoticons paste textcolor code"
            ],
            toolbar1: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect",
            toolbar2: "| link unlink anchor | image media | forecolor backcolor  | print code ",
            image_advtab: true,

            filemanager_access_key: window.app_key,
            external_filemanager_path: "js/tinymce/plugins/responsivefilemanager/",
            filemanager_title: "Gerenciador de arquivos",
            external_plugins: {
                "responsivefilemanager": "tinymce/plugins/responsivefilemanager/plugin.min.js"
            }
        });
    }

    // Fancybox
    if ($.fn.fancybox) {
        $(".fancy").fancybox();
        $('a[href*=".jpg"],a[href*=".jpeg"],a[href*=".gif"],a[href*=".png"]').fancybox();
        $("a:not(.youtubechannel)[href*=youtu],a:not(.youtubechannel)[href*=vimeo],a:not(.youtubechannel)[href*=vevo]").fancybox({
            autoScale: true,
            transitionIn: "fade",
            transitionOut: "fade",
            titlePosition: "inside",
            type: "iframe",
            openEffect: "elastic",
            openSpeed: 150,
            closeEffect: "elastic",
            closeSpeed: 150,
            swf: {
                wmode: "transparent",
                allowfullscreen: "true",
                allowscriptaccess: "always"
            },
            iframe: {
                scrolling: "auto",
                preload: true
            },
            helpers: {
                media: true,
                comments: {
                    type: "overlay",
                    position: "top",
                    commentsUrl: function (a) {
                        return $(a.element).data("fancybox-comments");
                    },
                    commentsWidth: 330,
                    numberPosts: 5,
                    colorScheme: "light",
                    disableOnEmptyUrl: true
                }
            }
        });
    }

    // Se houver um select de cidades
    cidade = $('select[id*="cidade"],select[name*="cidade"],select[class*="cidade"]');
    cidadeValor = cidade.val();
    if (cidade.length > 0) {
        $(document).on('change', 'select[id*="estado"],select[name*="estado"],select[class*="estado"]', function (event) {
            elemento = $(event.target);
            valor = elemento.val();

            var request = '';
            request = $.ajax({
                url: '/webservice/cidades',
                type: 'POST',
                data: {
                    idestado: valor
                }
            });

            request.done(function (retorno) {
                if (Object.keys(retorno).length > 0) {
                    cidade.html('<option selected="selected" value="">Selecione uma cidade</option>');
                    $.each(retorno, function (index, data) {
                        cidade.append('<option value="' + data.id + '">' + data.cidade + '</option>');
                    });
                    if (!!cidadeValor) {
                        cidade.find('option[value="' + cidadeValor + '"]').prop('selected', true);
                    }
                } else {
                    swal('City not found!', null, "error")
                }
            });
        });
        $('select[id*="estado"],select[name*="estado"],select[class*="estado"]').find('option:selected[value!=""]').trigger('change');
    }

    // Mascaras
    if ($.fn.inputmask) {
        mascaras();
    }

    if ($.fn.select2) {
        $.fn.select2.defaults.set("width", '100%');
        $.fn.select2.defaults.set("debug", true);
        $.fn.select2.defaults.set("language", "pt-BR");
        $.fn.select2.defaults.set("templateResult", function (item) {
            var elemento = $(item.element);

            if (!!elemento.data('help')) {
                var template = $('#' + elemento.data('template'));

                // setTimeout(function(){
                //     $(document).foundation();
                // }, 500);

                return item.text + ' '
                    + '<i class="far fa-question-circle helper swal" style="margin-left: 10px;" data-template="' + elemento.data('template') + '"></i>'
                    ;
            }
            return item.text;
        });
        $.fn.select2.defaults.set("escapeMarkup", function (m) {
            // Do not escape HTML in the select options text
            return m;
        });

        $('select').each(function (e) {
            if ($(this).is('[data-close-on-select]')) {
                $(this).select2({
                    closeOnSelect: false
                });
            } else {
                $(this).select2()
            }
        });

        // $('select[data-close-on-select]').on('select2:closing', function (e) {
        //     $('.select2-results i.helper:not(.swal)').foundation('hide');
        // });

        $('select').on('select2:open', function (e) {
            window.openedSelect2 = $(this);
            window.openedSelect2Value = openedSelect2.val();
        });

        $(document).on('click', '.select2-results i.helper:not(.swal)', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on('click', '.select2-results .select2-results__option i.helper.swal', function (e) {
            e.preventDefault();
            e.stopPropagation();

            if (!openedSelect2.val()) {
                openedSelect2.val('');
            } else {
                openedSelect2.val(openedSelect2Value);
            }

            openedSelect2.trigger('change.select2');

            swal({
                title: "Informação",
                content: {
                    element: "div",
                    attributes: {
                        innerHTML: $('#' + $(this).data('template')).html()
                    },
                },
                icon: 'success'
            }).then((value) => {
                openedSelect2.select2('open');
            });
        });

        $(document).on('click', '.select2-results .select2-results__option', function (e) {
            e.preventDefault();
            e.stopPropagation();
            openedSelect2.select2('close');
        });
    }

    $('.preview-image, .preview-images').on('change', function () {
        $(this).parent().parent().find('.preview-images-box').fadeOut().remove();
        readURL(this);
    });

    let floatLabel = ':input.form-control:not(.not-float), .form-group label:not(.not-float)';
    $(document).on('focusin', floatLabel, function () {
        $(this).parent('.column').find('label').addClass('focusfield');
    }).on('focusout', floatLabel, function () {
        input = $(this).parent('.column').find('input');
        if (!input.val() && !input.is(':focus')) {
            $(this).parent('.column').find('label').removeClass('focusfield');
        }
    }).on('mouseenter', floatLabel, function () {
        $(this).parent('.column').find('label').addClass('focusfield');
    }).on('mouseout', floatLabel, function () {
        input = $(this).parent('.column').find('input');
        if (!input.val() && !input.is(':focus')) {
            $(this).parent('.column').find('label').removeClass('focusfield');
        }
    }).on('change', floatLabel, function () {
        input = $(this).parent('.column').find('input');
        if (!!input.val()) {
            $(this).parent('.column').find('label').addClass('focusfield');
        }
    });
    $(floatLabel).trigger('change');

    $(':input.slugify').on('keyup', function () {
        if (!!$(this).val()) {
            slug = slugify($(this).val());
            $($(this).data('slugged')).val(slug.toLowerCase()).trigger('focusout');
        }
    });

    if ($.fn.repeater) {
        $('[data-make-repeater]').each(function () {
            var elemento = $(this);
            elemento.repeater({
                initEmpty: false,
                isFirstItemUndeletable: true,

                /*defaultValues: {
                    'text-input': 'foo'
                },*/

                show: function () {
                    el = $(this);
                    if ($.fn.select2 && $(this).find('select').length > 0) {
                        el.find('select').removeClass('select2-hidden-accessible');
                        el.find('.select2-container').remove();
                        el.slideDown().promise().done(function () {
                            el.find('select').each(function (e) {
                                $(this).val('');
                                if ($(this).is('[data-close-on-select]')) {
                                    $(this).select2({
                                        closeOnSelect: false
                                    });
                                } else {
                                    $(this).select2()
                                }
                            });
                        })
                    } else {
                        el.slideDown();
                    }

                    // Fixing the issue of the labels
                    // var params = [this];

                    var element = $('[data-make-repeater]');

                    element.find("label[for]").each(function(index, element) {
                        var currentRepeater = $(this).parents('[data-repeater-item]'),
                            originalFieldId = $(element).attr("for"),
                            newField = $(currentRepeater).find("input[id='"+originalFieldId+"']");
                        if($(newField).length > 0)
                        {
                            var newFieldName = $(newField).attr('name');
                            $(newField).attr('id', newFieldName);
                            $(currentRepeater).find("label[for='"+originalFieldId+"']").attr('for', newFieldName);
                            $(newField).attr('attr', newFieldName);
                        }
                    });

                    element.find(":input[data-original-name]").each(function(index, element) {
                        var name = $(this).prop('name'),
                            originalName = $(this).data('original-name');

                        if(name != originalName){
                            $(this).attr('name', originalName);
                        }
                        $(this).attr('id', name);
                    });

                    $(this).removeAttr('attr');
                },

                hide: function (deleteElement) {
                    var thisline = $(this);

                    swal({
                        text: elemento.data('removetext'),
                        icon: "error",
                        buttons: [elemento.data('removetext0'), elemento.data('removetext1')],
                        dangerMode: true,
                    })
                        .then((VaiDeletar) => {
                                if (VaiDeletar) {
                                    thisline.slideUp(deleteElement, function () {
                                        $(this).remove();
                                    });
                                }
                            }
                        )
                    ;
                },

                ready: function (setIndexes) {

                    var element = $('[data-make-repeater]');

                    element.find("label[for]").each(function(index, element) {
                        var currentRepeater = $(this).parents('[data-repeater-item]'),
                            originalFieldId = $(element).attr("for"),
                            newField = $(currentRepeater).find("input[id='"+originalFieldId+"']");
                        if($(newField).length > 0)
                        {
                            var newFieldName = $(newField).attr('name');
                            $(newField).attr('id', newFieldName);
                            $(currentRepeater).find("label[for='"+originalFieldId+"']").attr('for', newFieldName);
                            $(newField).attr('attr', newFieldName);
                        }
                    });

                    element.find(":input[data-original-name]").each(function(index, element) {
                        var name = $(this).prop('name'),
                            originalName = $(this).data('original-name');

                        if(name != originalName){
                            $(this).attr('name', originalName);
                        }
                        $(this).attr('id', name);
                    });

                    // $dragAndDrop.on('drop', setIndexes);
                },
            });
        });
    }

    // predefined ranges
    var start = moment().subtract(29, 'days');
    var end = moment();

    if ($.fn.daterangepicker) {
        if ($('.dataranger-picker').length > 0 && moment != undefined) {
            $('.dataranger-picker').each(function () {

                var element = $(this),
                    start = moment().subtract(29, 'days'),
                    end = moment();

                element.daterangepicker({
                    buttonClasses: 'm-btn btn',
                    applyClass: 'btn-primary',
                    cancelClass: 'btn-secondary',

                    autoApply: true,
                    autoUpdateInput: true,
                    startDate: start,
                    endDate: end,
                    ranges: {
                        [window.dateToday]: [moment(), moment()],
                        [window.dateYesterday]: [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        [window.dateLast7Days]: [moment().subtract(6, 'days'), moment()],
                        [window.dateLast30Days]: [moment().subtract(29, 'days'), moment()],
                        [window.dateThisMonth]: [moment().startOf('month'), moment().endOf('month')],
                        [window.dateLastMonth]: [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }
                }, function (start, end, label) {
                    if (!element.is(':input')) {
                        element.find(':input:first').val(start.format(dateFormatMoment) + ' - ' + end.format(dateFormatMoment));
                    } else {
                        element.val(start.format(dateFormatMoment) + ' - ' + end.format(dateFormatMoment));
                    }
                });
            })
        }
    }

});
