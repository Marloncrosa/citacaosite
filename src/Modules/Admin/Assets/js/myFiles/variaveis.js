// Inicializador de timeout
window.pegaitem = setTimeout(function () {
}, 100);
window.cepTime = setTimeout(function () {
}, 0);
window.cidadeTime = setTimeout(function () {
}, 0);
window.timeoutsearch = setTimeout(function () {
}, 10);

// Create Base64 Object
window.Base64 = {
  _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", encode: function (e) {
    var t = "";
    var n, r, i, s, o, u, a;
    var f = 0;
    e = Base64._utf8_encode(e);
    while (f < e.length) {
      n = e.charCodeAt(f++);
      r = e.charCodeAt(f++);
      i = e.charCodeAt(f++);
      s = n >> 2;
      o = (n & 3) << 4 | r >> 4;
      u = (r & 15) << 2 | i >> 6;
      a = i & 63;
      if (isNaN(r)) {
        u = a = 64
      } else if (isNaN(i)) {
        a = 64
      }
      t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
    }
    return t
  }, decode: function (e) {
    var t = "";
    var n, r, i;
    var s, o, u, a;
    var f = 0;
    e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    while (f < e.length) {
      s = this._keyStr.indexOf(e.charAt(f++));
      o = this._keyStr.indexOf(e.charAt(f++));
      u = this._keyStr.indexOf(e.charAt(f++));
      a = this._keyStr.indexOf(e.charAt(f++));
      n = s << 2 | o >> 4;
      r = (o & 15) << 4 | u >> 2;
      i = (u & 3) << 6 | a;
      t = t + String.fromCharCode(n);
      if (u != 64) {
        t = t + String.fromCharCode(r)
      }
      if (a != 64) {
        t = t + String.fromCharCode(i)
      }
    }
    t = Base64._utf8_decode(t);
    return t
  }, _utf8_encode: function (e) {
    e = e.replace(/\r\n/g, "\n");
    var t = "";
    for (var n = 0; n < e.length; n++) {
      var r = e.charCodeAt(n);
      if (r < 128) {
        t += String.fromCharCode(r)
      } else if (r > 127 && r < 2048) {
        t += String.fromCharCode(r >> 6 | 192);
        t += String.fromCharCode(r & 63 | 128)
      } else {
        t += String.fromCharCode(r >> 12 | 224);
        t += String.fromCharCode(r >> 6 & 63 | 128);
        t += String.fromCharCode(r & 63 | 128)
      }
    }
    return t
  }, _utf8_decode: function (e) {
    var t = "";
    var n = 0;
    var r = c1 = c2 = 0;
    while (n < e.length) {
      r = e.charCodeAt(n);
      if (r < 128) {
        t += String.fromCharCode(r);
        n++
      } else if (r > 191 && r < 224) {
        c2 = e.charCodeAt(n + 1);
        t += String.fromCharCode((r & 31) << 6 | c2 & 63);
        n += 2
      } else {
        c2 = e.charCodeAt(n + 1);
        c3 = e.charCodeAt(n + 2);
        t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
        n += 3
      }
    }
    return t
  }
};

// Json de estados
window.estadoJson = {
  "AC": "Acre",
  "AL": "Alagoas",
  "AM": "Amazonas",
  "AP": "Amapá",
  "BA": "Bahia",
  "CE": "Ceará",
  "DF": "Distrito Federal",
  "ES": "Espírito Santo",
  "GO": "Goiás",
  "MA": "Maranhão",
  "MG": "Minas Gerais",
  "MS": "Mato Grosso do Sul",
  "MT": "Mato Grosso",
  "PA": "Pará",
  "PB": "Paraíba",
  "PE": "Pernambuco",
  "PI": "Piauí",
  "PR": "Paraná",
  "RJ": "Rio de Janeiro",
  "RN": "Rio Grande do Norte",
  "RO": "Rondônia",
  "RR": "Roraima",
  "RS": "Rio Grande do Sul",
  "SC": "Santa Catarina",
  "SE": "Sergipe",
  "SP": "São Paulo",
  "TO": "Tocantins"
};

window.xs = window.matchMedia("(max-width: 575.98px)");
window.sm = window.matchMedia("(max-width: 767.98px)");
window.md = window.matchMedia("(max-width: 991.98px)");
window.lg = window.matchMedia("(max-width: 1199.98px)");
window.xl = window.matchMedia("(max-width: 1359.98px)");
window.xxl = window.matchMedia("(max-width: 1439.98px)");

// Tradução dropzone
if (Dropzone) {
    // Tradução dropzone
    Dropzone.dictDefaultMessage = 'Solte os arquivos aqui';
    Dropzone.dictFallbackMessage = 'Seu navegador não suporta o sistema de arrastar e soltar';
    Dropzone.dictFallbackText = 'Por favor use os botões para realizar o envio de arquivos';
    Dropzone.dictInvalidFileType = 'Tipo de arquivos não permitidos';
    Dropzone.dictFileTooBig = 'O tamanho {{filesize}} não é permitido. Limite máximo é de {{maxFilesize}}';
    Dropzone.dictResponseError = 'Erro ao enviar o arquivo, codigo: {{statusCode}}';
    Dropzone.dictCancelUpload = 'cancelar arquivo';
    Dropzone.dictCancelUploadConfirmation = 'Tem certeza que deseja remover?';
    Dropzone.dictRemoveFile = 'Arquivo removido.';
    Dropzone.dictMaxFilesExceeded = 'Quantidade maxima de arquivos excedido.';

    Dropzone.createElement = function (string) {
        var el = $(string);
        return el[0];
    };
}


$(function () {
  if($.fn.datetimepicker && datetimepickerDates != undefined) {
    $.fn.datetimepicker.dates['en'] = datetimepickerDates;
  }

  if($.fn.datepicker && datepickerDates != undefined) {
    $.fn.datepicker.dates['en'] = datepickerDates;
  }

  if($.fn.daterangepicker && daterangepickerOptions != undefined) {
    $.fn.daterangepicker.defaultOptions = daterangepickerOptions;
  }
});
