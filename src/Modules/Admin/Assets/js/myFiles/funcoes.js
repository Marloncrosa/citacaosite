// Seta o csrf nos ajax
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

// função de multiplos Uploads
window.multiuploads = function multiuploads() {

    // Pega o template e remove da tela
    var previewNode = document.querySelector("#template");

    // Remove o id
    previewNode.id = "";

    // pega apenas o html
    var previewTemplate = previewNode.parentNode.innerHTML;

    // Remove o html do documento
    previewNode.parentNode.removeChild(previewNode);

    // Barra de progresso
    var barraprogresso = document.querySelector(".total-progress"),
        barraprogressoenchimento = barraprogresso.querySelector('.progress-meter'),
        barraprogressoenchimentotexto = barraprogressoenchimento.querySelector('.progress-meter-text')
    ;

    // Tabela de upload
    var uploadtable = document.querySelector('#table-preview');

    // Inicia o multiplo upload
    // Dropzone.autoDiscover = false;
    var myDropzone = new Dropzone(
        document.body,
        {
            url: uploadtable.getAttribute('data-url'), // url de disparo
            paramName: uploadtable.getAttribute('data-campo'), // nome do campo que será usado no upload
            maxFilesize: uploadtable.getAttribute('data-maxfilesize'), // tamanho emMB
//			headers : { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },

            thumbnailWidth: 80, // largura da thumb
            thumbnailHeight: 80, // altura da thumb

            parallelUploads: 1, // uploads em paralelo
            uploadMultiple: false, //Whether Dropzone should send multiple files in one request

            previewTemplate: previewTemplate, // template

            autoQueue: false, // Para ter certeza que os arquivos não serão enviados automaticamente

            previewsContainer: "#previews", // Define o container que será exibido os previews
            clickable: ".seleciona-arquivos" // Define o elemento que deve ser clicado para abrir o seletor de arquivos
        }
    );

    // Quando adicionar um arquivo
    myDropzone.on("addedfile", function (file) {
        if (file.previewElement.querySelector(".start") != null) {
            // Hookup para o botão de iniciar o upload
            file.previewElement.querySelector(".start").onclick = function () {
                myDropzone.enqueueFile(file);
            };
        }
    });

    // Quando completar o envio de um arquivo:
    myDropzone.on("complete", function (file) {
        if (file.status == 'success') {
            setTimeout(function () {
                myDropzone.removeFile(file);
            }, 5000);
        } else {
            var json = JSON.parse(file.xhr.response);
            var item = '';
            for (var key in json) {
                if (json.hasOwnProperty(key)) {
                    if (item == '')
                        item = json[key];
                    else
                        item += '<br>' + json[key];
                }
            }
            var error = file.previewElement.querySelectorAll(".error.callout.alert")[0];
            error.style.opacity = 1;
            error.innerHTML = item;
        }
    });

    // Quando completar o envio de um arquivo:
    myDropzone.on("successmultiple", function (file, responseText) {
        json = JSON.parse(responseText.success);
        console.log(file);
        console.log(responseText);
        console.log(json);
        if (json.success != undefined) {
            swal('Item(ns) cadastrado(s) com sucesso!', null, "success")
        }
        if (json.error != undefined) {
            swal('Houve problemas ao enviar o(s) arquivo(s)', null, "error")
        }
    });

    // Atualiza a barra de progresso total
    myDropzone.on("totaluploadprogress", function (progress) {
        // Zera a pa
        barraprogressoenchimento.style.width = progress + "%";
        // coloca o texto
        barraprogressoenchimentotexto.innerHTML = Math.round(progress) + "%";
        //  limpa a fila
    });

    // Quando estiver enviando a barra de progresso
    myDropzone.on("sending", function (file) {
        // Mostra a barra de progresso
        barraprogresso.style.opacity = "1";

        if (file.previewElement.querySelector(".start") != null) {
            // Desabilita o botão de enviar da linha
            file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
        }
    });

    // Esconde a barra de upload caso não haja mais nada para enviar
    myDropzone.on("queuecomplete", function (progress) {
        setTimeout(function () {
            // esconde a barra de progresso
            barraprogresso.style.opacity = "0";
            // Zera a pa
            barraprogressoenchimento.style.width = 0 + "%";
            // coloca o texto
            barraprogressoenchimentotexto.innerHTML = '';
            //  limpa a fila
            // myDropzone.removeAllFiles(true);
        }, 2000)
    });


    // Click de seleciona-arquivos
    document.querySelector(".seleciona-arquivos").onclick = function (e) {
        e.preventDefault();
    };
    // Adiciona função de click para enviar os arquivos da fila
    document.querySelector(".enviar-arquivo").onclick = function (e) {
        e.preventDefault();
        myDropzone.enqueueFiles(
            myDropzone.getFilesWithStatus(Dropzone.ADDED)
        );
    };
    // Adiciona função de click para cancelar os arquivos da fila
    document.querySelector(".cancelar-arquivos").onclick = function (e) {
        e.preventDefault();
        myDropzone.removeAllFiles(true);
    };
};

// Datatables
/*if ($.fn.dataTable != undefined) {
    $.extend(true, $.fn.dataTable.defaults, {
        responsive: !0,
        dom: "<'row'<'col-sm-12'tr>>\n\t\t\t<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>",
        pageLength: 10,
        searchDelay: 500,
        processing: !0,
        serverSide: !0,
        // pagingType: "simple_numbers",
        lengthMenu: [
            [10, 25, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, -1],
            [10, 25, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500, "Todos"]
        ],
        language: {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": '<strong>Carregando</strong> <br><div class="sk-cube-grid"><div class="sk-cube sk-cube1"></div><div class="sk-cube sk-cube2"></div><div class="sk-cube sk-cube3"></div><div class="sk-cube sk-cube4"></div><div class="sk-cube sk-cube5"></div><div class="sk-cube sk-cube6"></div><div class="sk-cube sk-cube7"></div><div class="sk-cube sk-cube8"></div><div class="sk-cube sk-cube9"></div></div>',
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },
            processing: '<strong>Carregando</strong> <br><div class="sk-cube-grid"><div class="sk-cube sk-cube1"></div><div class="sk-cube sk-cube2"></div><div class="sk-cube sk-cube3"></div><div class="sk-cube sk-cube4"></div><div class="sk-cube sk-cube5"></div><div class="sk-cube sk-cube6"></div><div class="sk-cube sk-cube7"></div><div class="sk-cube sk-cube8"></div><div class="sk-cube sk-cube9"></div></div>'
        },
        initComplete: function () {
            if (!$(this).hasClass('nosearchHeader')) {
                Linhanova = $('<tr role="row"></tr>').appendTo($(this).find('thead'));

                this.api().columns().every(function (i) {
                    var column = this;
                    cabecalho = $(column.header());
                    ColunaNova = $('<th rowspan="1" colspan="1"></th>').appendTo(Linhanova);
                    console.log(column.settings()[0].aoColumns[i]);
                    if (cabecalho.text() != 'Ações' && column.settings()[0].aoColumns[i].bSearchable) {

                        var input = '<input type="text" class="form-control form-control-sm form-filter m-input" data-col-index="' + this.index() + '"  placeholder="Procurar ' + cabecalho.text() + '"/>';

                        ColunaNova.append('<div class="clearfix"></div>');
                        var elemento = $(input).appendTo(ColunaNova);
                        elemento.on('change keyup', function () {
                            elemento = $(this);
                            clearTimeout(timeoutsearch);
                            timeoutsearch = setTimeout(function () {
                                column.search(elemento.val(), false, false, true).draw();
                            }, 1000);

                        });

                    }
                });
            }
        },
    });
}*/

// Função que converte em moeda
window.moeda = function moeda(num) {
    x = 0;

    if (num < 0) {
        num = Math.abs(num);
        x = 1;
    }
    if (isNaN(num)) num = "0";
    cents = Math.floor((num * 100 + 0.5) % 100);

    num = Math.floor((num * 100 + 0.5) / 100).toString();

    if (cents < 10) cents = "0" + cents;
    for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
        num = num.substring(0, num.length - (4 * i + 3)) + "."
            + num.substring(num.length - (4 * i + 3));
    ret = num + "," + cents;
    if (x == 1) ret = " - " + ret;
    return ret;
};

// Função que converte em float
window.floatN = function (num) {
    var str = num.toString();
    str = str.replace(".", "");
    str = str.replace(",", ".");
    return parseFloat(str);
};

window.utf8_encode = function (string) {
    return unescape(encodeURIComponent(string));
};

window.utf8_decode = function (string) {
    return decodeURIComponent(escape(string));
};

String.prototype.replaceAll = function (find, replace) {
    return this.replace(new RegExp(find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1"), "g"), replace);
};

window.mySwal = function(option){
    v = {}; for (var key in option) { v[key] = option[key]; };
    swal(v);
};

window.isJson = function (str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
};
