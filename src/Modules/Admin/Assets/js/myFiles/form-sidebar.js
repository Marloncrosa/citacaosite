$(function () {
    sidebarHide = $('.sidebar__hide');
    resizerTimer = setTimeout(function () {}, 100);

    toogleFormSideBar = function (param) {
        if (param == true) {
            $('.form-sidebar:first', window.parent.document).addClass('sidebar--on')
                .css('left', 'calc(100% - ' + $('.form-sidebar:first', window.parent.document).width() + 'px)');
        } else {
            $('.form-sidebar:first', window.parent.document).removeClass('sidebar--on').removeAttr('style');
        }
    };

    $(document).on('click', '.open-form-sidebar', function () {
        // if(!mUtil.isInResponsiveRange("tablet-and-mobile")){
        $('.form-sidebar-preload').addClass('open');
        $('.form-sidebar-overlay').addClass('open');

        elemento = $('.form-sidebar:first');

        // $('html, body').css('overflow', 'hidden');
        $('.sidebar__hide').removeClass('closed').removeClass('animate');
        $('.sidebar__hide i').toggleClass('la-angle-double-left').toggleClass('la-angle-double-right');
        $('.sidebar__hide').css('left', '-35px');

        elemento.find('iframe:first').prop('src', $(this).prop('href')).on('load', function () {
            $('.form-sidebar-preload').removeClass('open');
            toogleFormSideBar(true);
        });

        return false;
        // }
    });

    $(window).resize(function () {
        resizerTimer = setTimeout(function () {
            if ($('.form-sidebar:first', window.parent.document).hasClass('sidebar--on')) {
                toogleFormSideBar(true);
            }
        }, 1000);
    });

    $(document).on('click', '.sidebar__close', function () {
        $('.form-sidebar:first', window.parent.document).removeClass('sidebar--on').removeAttr('style');
        $('.form-sidebar-overlay', window.parent.document).removeClass('open');
        $('html, body').css('overflow', 'auto');

        sidebarHide.css('left', '-35px');
        parent.$('#loading').modal('hide');
        return false;
    });

    $(document).on('click', '.form-sidebar-overlay', function () {
        toogleFormSideBar(false);

        $('.form-sidebar-overlay', window.parent.document).removeClass('open');
        $('html, body').css('overflow', 'auto');

        sidebarHide.addClass('closed').addClass('animate');
        sidebarHide.find('i').toggleClass('la-angle-double-right').toggleClass('la-angle-double-left');
        sidebarHide.css('left', '-135px');

        return false;
    });

    $(document).on('click', '.sidebar__hide', function () {
        $(this).toggleClass('closed').toggleClass('animate');

        $('.form-sidebar-overlay', window.parent.document).toggleClass('open');

        if (!$(this).hasClass('closed')) {
            toogleFormSideBar(true);
            $('.sidebar__hide').css('left', '-35px');
            $('.sidebar__hide i').toggleClass('la-angle-double-left').toggleClass('la-angle-double-right');
        } else {
            $(this).addClass('animate');
            toogleFormSideBar(false);
            $('.sidebar__hide').css('left', '-135px');
            $('.sidebar__hide i').toggleClass('la-angle-double-right').toggleClass('la-angle-double-left');
        }
        return false;
    });

    $(document).on('click', '.sidebar__update', function () {
        // $('.form-sidebar:first', window.parent.document).removeClass('sidebar--on');
        $('.form-sidebar iframe', window.parent.document)[0].contentDocument.location.reload(true);
        return false;
    });
});
