require('datatables.net-bs4/js/dataTables.bootstrap4.min');
require('datatables.net-autofill/js/dataTables.autoFill.min');
require('datatables.net-autofill-bs4/js/autoFill.bootstrap4.min');
require('jszip/dist/jszip.min');
require('pdfmake/build/pdfmake.min');
require('pdfmake/build/vfs_fonts');
require('datatables.net-buttons/js/dataTables.buttons.min');
require('datatables.net-buttons-bs4/js/buttons.bootstrap4.min');
require('datatables.net-buttons/js/buttons.colVis');
require('datatables.net-buttons/js/buttons.flash');
require('datatables.net-buttons/js/buttons.html5');
require('datatables.net-buttons/js/buttons.print');
require('datatables.net-colreorder/js/dataTables.colReorder.min');
require('datatables.net-fixedcolumns/js/dataTables.fixedColumns.min');
require('datatables.net-fixedheader/js/dataTables.fixedHeader.min');
require('datatables.net-keytable/js/dataTables.keyTable.min');
require('datatables.net-responsive/js/dataTables.responsive.min');
require('datatables.net-responsive-bs4/js/responsive.bootstrap4.min');
require('datatables.net-rowgroup/js/dataTables.rowGroup.min');
require('datatables.net-rowreorder/js/dataTables.rowReorder.min');
require('datatables.net-scroller/js/dataTables.scroller.min');
require('datatables.net-select/js/dataTables.select.min');


require('Vendor/yajra/laravel-datatables-buttons/src/resources/assets/buttons.server-side');
