// Sudações
console.log('Desenvolvido com ♥ por aireset.com.br');

window.xs = window.matchMedia("(max-width: 575.98px)");
window.sm = window.matchMedia("(max-width: 767.98px)");
window.md = window.matchMedia("(max-width: 991.98px)");
window.lg = window.matchMedia("(max-width: 1199.98px)");
window.xl = window.matchMedia("(max-width: 1359.98px)");
window.xxl = window.matchMedia("(max-width: 1439.98px)");

require('./myFiles/pwa');

import jquery from 'jquery';
window.jQuery = window.jquery = window.$ = jquery;

/**
 * Metronicar
 * */
require('../metronic2/metronic');
/**
 * Metronicar
 * */

import swaljs from 'sweetalert';
window.swaljs = swaljs;

import Dropzone from 'dropzone';
window.Dropzone = Dropzone;

// Tinymce
require('./plugins/tinymce');

// Datatables
import datatables from 'datatables.net';
window.datatables = datatables;

import datatables_select from 'datatables.net-select';
window.datatables_select = datatables_select;

import datatables_responsive from 'datatables.net-responsive';
window.datatables_responsive = datatables_responsive;

require('Vendor/yajra/laravel-datatables-buttons/src/resources/assets/buttons.server-side');

// Mascaras
import inputmask from 'inputmask';
window.inputmask = inputmask;
require('inputmask/dist/jquery.inputmask.bundle');
// require('inputmask/dist/inputmask/inputmask.numeric.extensions');
// require('inputmask/dist/inputmask/inputmask.phone.extensions.js');

require('@fancyapps/fancybox/dist/jquery.fancybox.min');

require('select2');
// require('select2/dist/js/i18n/pt-BR');

window.slugify = require('slugify');

require('intl-tel-input/build/js/intlTelInput-jquery.min');
window.utilsIntlTel = require('intl-tel-input/build/js/utils');

import jstree from 'jstree';
window.jstree = jstree;

import cepPromise from "cep-promise";
window.cepPromise = cepPromise;

// Bibliotecas
require('./myFiles/variaveis');
require('./myFiles/funcoes');
require('./myFiles/mascaras');
require('./myFiles/country-state-city');
require('./myFiles/google-autocomplete');
require('./myFiles/application');
require('./myFiles/form-sidebar');
