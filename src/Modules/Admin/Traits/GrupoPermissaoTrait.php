<?php

namespace Modules\Sistema\Traits;

use Modules\Sistema\Entities\ClienteUsuarioGrupoPermissaoCliente;
use Modules\Sistema\Entities\GrupoPermissaoCliente;

trait GrupoPermissaoTrait
{
    
    // Pega os grupos de permissões que esse usuário tem nesse cliente
    public function grupospermissoesclientes()
    {
        /**
         * Exemplo da select gerada
         *  select `grupopermissaocliente`.*, `clienteusuariogrupopermissaocliente`.`nidclienteusuario`
         *  from `grupopermissaocliente`
         *  inner join `clienteusuariogrupopermissaocliente` on `clienteusuariogrupopermissaocliente`.`nidgrupopermissaocliente` = `grupopermissaocliente`.`nid`
         *  where `clienteusuariogrupopermissaocliente`.`nidclienteusuario` in (1) and `grupopermissaocliente`.`deleted_at` is null
         */
        return $this->hasManyThrough(
            GrupoPermissaoCliente::class,
            ClienteUsuarioGrupoPermissaoCliente::class,
            'nidclienteusuario',
            'nid', // PK da tabela final que você deseja os dados
            'nid',
            // Traz o dado baseado no relacionamento que está usando exemplo essa trait está sendo chamada na ClienteUsuario (pega o nid dela)
            'nidgrupopermissaocliente' // Chave da tabela ClienteUsuarioGrupoPermissaoCliente
        );
        // )->withTrashed();
    }
    
    public function getGruposEloquent()
    {
        // dd("getGruposPorID_{$this->getKey()}");
        
        $grupos = \Cache::remember(
            "getGruposEloquent_".auth()->user()->nid,
            60,
            function () {
                return $this->grupospermissoesclientes()->get();
            }
        );
        // $grupos = $this->grupospermissoesclientes()->get();
        
        return $grupos;
    }
    
    public function getGrupos()
    {
        $grupos = $this->getGruposEloquent();
        
        return $this->mapearGrupos($grupos);
    }
    
    // Pega a relação do usuario que está em um cliente com um
    public function usuarioclientepermissao()
    {
        return $this->with('grupospermissoesclientes.permissoes')->get();
    }
    
    /**
     * Get sorted map from roles.
     *
     * @return array
     */
    private function mapearGrupos($grupos)
    {
        if (empty($grupos)) {
            return [];
        }
        
        $grupos = $grupos->pluck('cnome', 'nid');
        
        // $grupos = $this->collectionAsArray($grupos);
        
        // foreach ($map as $role_id => $grupos) {
        //     sort($grupos);
        //     $map[$role_id] = $grupos;
        // }
        
        return $grupos;
    }
    
    
}
