<?php

namespace Modules\Admin\Traits;

use Modules\Admin\Entities\PermissionsGroup;
use Modules\Admin\Entities\UserPermissionsGroup;

/**
 * Trait PermissionTrait
 *
 * Essa trait foi desenvolvida para gerenciar o sistema de permissões desde permissions dos grupos, baseado no cliente com usuario
 *
 * Exemplos:
 * Sempre chamar pela Model do usuario ($usuario = auth('admin)->user())
 *
 * $usuario->getMeusGruposEloquent(); // Traz os grupos como uma collection de models
 * $usuario->getMeusGrupos(); // Traz os grupos em array
 * $usuario->getMyGroupsPermissionsEloquent(); // Traz os grupos com permissões como uma collection de models
 * $usuario->getMyGroupsPermissions(); // Traz os grupos com permissões em array
 * $usuario->getMyPermissionsEloquent(); // Traz todas as permissões do usuario (Grupos e Equipes) como uma collection de models
 * $usuario->getMyPermissions(); // Traz todas as permissões do usuario (Grupos e Equipes) em array
 *
 * @package Modules\Sistema\Traits
 */
trait GroupPermissionsTrait
{
    /**
     * Pega os grupos
     * @return array
     */
    public function getMeusGrupos()
    {
        $grupos = $this->getMyGroupsPermissions();

        return $this->mapearGrupos($grupos);
    }

    /**
     * Mapea os grupos
     *
     * @return array
     */
    private function mapearGrupos($grupos)
    {
        if (empty($grupos)) {
            return [];
        }

        $grupos = $grupos->pluck('name', 'id');

        // $grupos = $this->collectionAsArray($grupos);

        // foreach ($map as $role_id => $grupos) {
        //     sort($grupos);
        //     $map[$role_id] = $grupos;
        // }

        return $grupos;
    }

    /**
     * @return array
     */
    public function getMyGroupsPermissions()
    {
//        $grupospermissions = \Cache::remember(
//            "getMyGroupsPermissions_".auth('admin')->user()->id,
//            60,
//            function () {
//                return $this->getMyGroupsPermissionsEloquent();
//            }
//        );
        $grupospermissions = $this->getMyGroupsPermissionsEloquent();

        return $this->mapGroupPermissions($grupospermissions);
    }

    /**
     * O Usuário pertence a cliente
     * @return mixed
     */
    public function getMyGroupsPermissionsEloquent()
    {
        // generate all groups
        if(
            is_null(cache()->get('groups'))
        ){
            cache()->forget('groups');
        }
        cache()->forget('groups');

        $groups = cache()->remember('groups', 60, function () {
            return PermissionsGroup::with('permissions')->get();
        });

        foreach ($groups as $group) {
            if(
                is_null("group_{$group->id}")
            ){
                cache()->forget("group_{$group->id}");
            }
            cache()->remember("group_{$group->id}", 60, function () use($group){
                return $group;
            });
        }

        // Get my groups id
        $mygroups = UserPermissionsGroup::where('user_id', auth('admin')->user()->id)->get();
        $meusgrupos = collect();
        foreach ($mygroups as $group) {
           // dump(
           //     "group_{$group->permissions_group_id}"
           // );
            $meusgrupos->push(
                cache()->get("group_{$group->permissions_group_id}")
            );
        }
//        dd(
//            $meusgrupos
//        );

        // Carrega as permissões assincrona no grupo
        return $meusgrupos;
    }

    /**
     * Mapea os grupos com as permissões
     *
     * @return array
     */
    private function mapGroupPermissions($grupos)
    {
        if (empty($grupos)) {
            return [];
        }

        $grupos = $grupos->map(
            function ($r) {
                return [
                    'id' => $r->id,
                    'name' => $r->name,
                    'permissions' => $r->permissions->toArray(),
                ];
            }
        );
        return $grupos;
    }

    /**
     * Pega as permissões
     * @return um objeto de permissões
     */
    public function getMyPermissionsEloquent()
    {
        // Pega o ClienteUsuario
        $meusgrupos = $this->getMyGroupsPermissionsEloquent();

        $collection_permissions = collect();

        // Anda por cada grupo e por cada permissão
        foreach ($meusgrupos as $key => $value) {
            foreach ($value->permissions as $keypermission => $valuepermission) {
                $collection_permissions->put('permission_' . $valuepermission->id, $valuepermission);
            }
        }

        // Integra as permissões de equipes
//        $equipespermissions = $this->integraEquipes();
//
//        $nova_collection = $collection_permissions->merge($equipespermissions);

        // retorna minhas permissões
        return $collection_permissions;
    }

    public function mygroupspermission()
    {
        return $this->hasManyThrough(PermissionsGroup::class, UserPermissionsGroup::class, 'user_id', 'id', 'id', 'permissions_group_id');
    }
}
