<?php

namespace Modules\Sistema\Traits;

use Modules\Sistema\Entities\ClienteUsuario;

/**
 * Trait PermissaoTrait
 *
 * Essa trait foi desenvolvida para gerenciar o sistema de permissões desde permissoes dos grupos, baseado no cliente com usuario
 *
 * Exemplos:
 * Sempre chamar pela Model do usuario ($usuario = auth()->user())
 *
 * $usuario->getMeusGruposEloquent(); // Traz os grupos como uma collection de models
 * $usuario->getMeusGrupos(); // Traz os grupos em array
 * $usuario->getMeusGruposePermissoesEloquent(); // Traz os grupos com permissões como uma collection de models
 * $usuario->getMeusGruposePermissoes(); // Traz os grupos com permissões em array
 * $usuario->getMinhasPermissoesEloquent(); // Traz todas as permissões do usuario (Grupos e Equipes) como uma collection de models
 * $usuario->getMinhasPermissoes(); // Traz todas as permissões do usuario (Grupos e Equipes) em array
 *
 * @package Modules\Sistema\Traits
 */
trait PermissaoTrait
{
    use GruposPermissoesTrait;

    /**
     * @return mixed
     */
    public function getClienteUsuario()
    {
        // verifica se existe algum cliente selecionado, caso não haja, selecionar o primeiro disponivel
        if ( ! session()->exists('cliente_atual')) {
            session()->put('cliente_atual', $this->clientes->first()->nid);
        }
        $nidcliente     = session('cliente_atual');
        $nidusuario     = $this->nid;

        // Consume menos recurso
        $clienteusuario = ClienteUsuario::where('nidcliente', $nidcliente)->where('nidusuario', $nidusuario)->with(
            'grupospermissoesclientes'
        );

        return $clienteusuario;
    }

    /**
     * Pega as permissões
     * @return um objeto de permissões
     */
    public function getMinhasPermissoesEloquent()
    {
        // Pega o ClienteUsuario
        $meusgrupos = $this->getMeusGruposePermissoesEloquent();

        $collection_permissoes = collect();

        // Anda por cada grupo e por cada permissão
        foreach ($meusgrupos as $key => $value) {
            foreach ($value->permissoes as $keypermissao => $valuepermissao) {
                $collection_permissoes->put('permissao_'.$valuepermissao->nid, $valuepermissao);
            }
        }

        // Integra as permissões de equipes
        $equipespermissoes = $this->integraEquipes();

        $nova_collection = $collection_permissoes->merge($equipespermissoes);

        // retorna minhas permissões
        return $nova_collection;
    }

    /**
     * Pega as permissões
     * @return um array com as permissões
     */
    public function getMinhasPermissoes()
    {
        $permissoes = $this->getMinhasPermissoesEloquent();

        return $this->mapearPermissoes($permissoes)->unique()->values()->all();
    }

    /**
     * Faz integração da trait de equipes com a de permissoes
     * @return mixed
     */
    public function integraEquipes(){
        $clienteusuario = $this->getClienteUsuario()->first()->load('equipes');
        return $clienteusuario->minhasEquipesePermissoesEloquent();
    }

    /**
     * Faz integração da trait de equipes com a de permissoes
     * @return mixed
     */
    public function minhasEquipes(){
        $clienteusuario = $this->getClienteUsuario()->first()->load('equipes');
        return $clienteusuario->equipes;
    }

    /**
     * Mapea as permissões
     *
     * @return array
     */
    private function mapearPermissoes($permissoes)
    {
        // verifica se existe permissão
        if (empty($permissoes)) {
            return [];
        }

        // Mapea a coleção do laravel para retornar como um array
        // utiliza uma função de mapeamento, executa como se fosse um loop,
        // passando por cada posição e traz dados conver informo, mapeando o retornoa
        $permissoes = $permissoes->map(
            function ($r) {
                return [
                    'nid'       => $r->nid,
                    'cnome'     => $r->cnome,
                    'cslug'     => $r->cslug,
                ];
            }
        );
        return $permissoes; // retorna as permissões mapeadas
    }
    
    /**
     * @param null $permissao
     * @param null $operador
     *
     * @return bool
     */
    public function podepermissao($permissao = null, $operador = null){
        // caso não seja passado um operador (AND ou OR), ele tenta encontrar um na string de permissão
        $operador = is_null($operador) ? $this->parseOperador($permissao) : $operador;

        $permissao = $this->hasDelimiterToArray($permissao);
        if(is_string($permissao)){
            $permissao = array($permissao);
            $operador = 'and';
        }

        // convert em slug a permissão atual
        $permissao = $this->paraSlug($permissao);
        $permissoes = auth()->user()->getMinhasPermissoes();

        // validate permissions array
        if ( is_array($permissao) ) {

            if ( ! in_array($operador, ['and', 'or']) ) {
                $e = __("Operador inválido, operadores disponveis \"and\", \"or\".");
                throw new \InvalidArgumentException($e);
            }

            $call = 'podeCom' . ucwords($operador);

            return $this->$call($permissao, $permissoes) || auth()->user()->badministrador;
        }
    }

    /**
     * Função que verifica as permissões com o operador AND
     * Fazendo com que o usuário só pode ter uma permissão se a(s) form válida também
     *
     * @param string $permission
     * @param array  $permissions
     * @param string $model
     * @param int    $reference_id
     *
     * @return bool
     */
    protected function podeComAnd($permissao, $permissoes)
    {
        $permissoes = collect($permissoes); // gera uma coleção
        $permissoes = $permissoes->pluck('cslug', 'nid')->toArray(); // caso exista mais campos na coleção, retornar apenas o cslug e nid
        $permission_ok = false; // seta o retorno inicial
        foreach ($permissao as $check) { // percorre cada permissão
            if(in_array($check, $permissoes)){ // verifica se a permissão enconra-se no array de permissões do usuário
                $permission_ok        = true;
            }else{
                $permission_ok        = false;
            }
        }
        return $permission_ok;
    }

    /**
     * Função que verifica as permissões com o operador OR
     * Fazendo com que o usuário pode ter uma ou outra permissão
     *
     * @param string $permission
     * @param array  $permissions
     * @param string $model
     * @param int    $reference_id
     *
     * @return bool
     */
    protected function podeComOr($permissao, $permissoes)
    {
        $permissoes = collect($permissoes); // gera uma coleção
        $permissoes = $permissoes->pluck('cslug', 'nid')->toArray(); // caso exista mais campos na coleção, retornar apenas o cslug e nid
        $permission_ok = false; // seta o retorno inicial
        foreach ($permissao as $check) {
            if(in_array($check, $permissoes)){
                $permission_ok        = true;
            }
        }
        return $permission_ok;
    }

    /*
    |----------------------------------------------------------------------
    | Slug Permission Related Protected Methods
    |----------------------------------------------------------------------
    |
    */
    
    /**
     * Transforma os textos de cada posição do array em um slug, para facilitar a validação
     *
     * @param array $permissao
     *
     * @return array
     */
    protected function paraSlug(array $permissao)
    {
        $data = [];
        foreach ($permissao as $alias => $perm) { // Percore cada item do array
            $data[] = str_slug($perm); // Converte o texto em uma url amigavel
        }
        return $data;
    }
    
    
    /**
     * Descobre qual o tipo de operador que está sendo usado (baseia-se apenas no primeiro)
     * @param $str
     *
     * @return bool|string
     */
    protected function parseOperador($str)
    {
       // if its an array lets use
       // and operator by default
       if ( is_array($str) ) {
           $str = implode(',', $str);
       }

       if ( preg_match('/((&&)|[|])(?:\s+)?/', $str, $m) ) {
           return $m[0].$m[1] == '||' ? 'or' : 'and';
       }

       return false;
    }


    /**
     * Converte strings que tem virgula ou pipe para um array em minuscula
     *
     * @param $str
     * @return array
     */
    protected function hasDelimiterToArray($str)
    {
        if ( is_string($str) && preg_match('/(&&)|[|]/is', $str) ) {
            return preg_split('/ ?(&&)|[|] ?/', mb_strtolower($str, 'UTF-8'));
        }

        return is_array($str) ?
            array_map(function ($value){ return mb_strtolower($value, 'UTF-8');}, $str) :
            (is_object($str) ? $str : mb_strtolower($str, 'UTF-8'));
    }
}
