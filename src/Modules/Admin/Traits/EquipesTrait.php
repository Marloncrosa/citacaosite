<?php

namespace Modules\Sistema\Traits;

use Modules\Sistema\Entities\ClienteUsuarioGrupoPermissaoCliente;
use Modules\Sistema\Entities\GrupoPermissaoCliente;

trait EquipesTrait
{

    public function minhasEquipesEloquent(){
        return $this->equipes;
    }

    public function minhasEquipes(){
        // dd("getGruposPorID_{$this->getKey()}");

        $equipes = \Cache::remember(
            "minhasEquipes_".auth()->user()->id,
            60,
            function () {
                return $this->minhasEquipesEloquent();
            }
        );

        // $equipes = $this->minhasEquipesEloquent();

        return $this->mapearEquipes($equipes);
    }

    public function minhasEquipeseGruposPermissoesEloquent(){
        return $this->minhasEquipesEloquent()->load('grupospermissions', 'grupospermissions.permissions');
    }

    public function minhasEquipeseGruposPermissoes(){
        return $this->mapearEquipeseGruposPermissoes($this->minhasEquipeseGruposPermissoesEloquent());
    }

    public function minhasEquipesePermissoesEloquent(){
        $equipes = $this->minhasEquipesEloquent()->load('grupospermissions', 'grupospermissions.permissions');

        $permissions = collect();
        foreach ($equipes as $key => $value) {
            foreach ($value->grupospermissions as $keygrupospermissions => $valuegrupospermissions) {
                foreach ($valuegrupospermissions->permissions as $keypermission => $valuepermission) {
                    $permissions->put('permission_'.$valuepermission->id, $valuepermission);
                }
            }
        }

        return $permissions;
    }

    /**
     * Get sorted map from roles.
     *
     * @return array
     */
    private function mapearEquipeseGruposPermissoes($equipes)
    {
        if (empty($equipes)) {
            return [];
        }

        $equipes = $equipes->map(
            function ($r) {
                unset($r['pivot']);

                $grupo = [];
                foreach ($r->grupospermissions as $keygrupo => $valuegrupo) {
                    $grupox = $valuegrupo->toArray();
                    unset($grupox['pivot']);
                    $grupo[] = $grupox;
                }

                return [
                    'id'        => $r->id,
                    'name'      => $r->name,
                    'grupospermissions' => $grupo
                ];
            }
        );
        return $equipes;
    }

    /**
     * Get sorted map from roles.
     *
     * @return array
     */
    private function mapearEquipes($equipes)
    {
        if (empty($equipes)) {
            return [];
        }
        $equipes = $equipes->pluck('name', 'id');
        return $equipes;
    }


}
