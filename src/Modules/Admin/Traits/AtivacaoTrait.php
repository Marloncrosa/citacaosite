<?php

namespace Modules\Sistema\Traits;

use Modules\Sistema\Repositories\AtivacaoRepository;
use Modules\Sistema\Entities\UsuarioTemp;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;

trait AtivacaoTrait
{

    public function initiateEmailAtivacao(UsuarioTemp $user)
    {
        if ( !config('settings.activation')  || !$this->validateEmail($user)) {
            return true;
        }
        
        $activationRepostory = new AtivacaoRepository();
        $activationRepostory->createTokenAndSendEmail($user);
    }

    protected function validateEmail(UsuarioTemp $user)
    {
        $validator = Validator::make(['email' => $user->cemail], ['email' => 'required']);
        if ($validator->fails()) {
            return false;
        }
        return true;
    }

}
