<?php

namespace Modules\Sistema\Traits;
use Modules\Sistema\Events\CodClienteObserver;
use Modules\Sistema\Scopes\CodClienteScope;

trait CodClienteTrait
{

    public static function bootCodClienteTrait()
    {
        static::addGlobalScope(new CodClienteScope);
        static::observe(new CodClienteObserver);
    }
}
