<?php

namespace Modules\Sistema\Traits;

use Modules\Sistema\Entities\ClienteUsuario;

/**
 * Trait PermissaoTrait
 *
 * Essa trait foi desenvolvida para gerenciar o sistema de permissões desde permissoes dos grupos, baseado no cliente com usuario
 *
 * Exemplos:
 * Sempre chamar pela Model do usuario ($usuario = auth()->user())
 *
 * $usuario->getMeusGruposEloquent(); // Traz os grupos como uma collection de models
 * $usuario->getMeusGrupos(); // Traz os grupos em array
 * $usuario->getMeusGruposePermissoesEloquent(); // Traz os grupos com permissões como uma collection de models
 * $usuario->getMeusGruposePermissoes(); // Traz os grupos com permissões em array
 * $usuario->getMinhasPermissoesEloquent(); // Traz todas as permissões do usuario (Grupos e Equipes) como uma collection de models
 * $usuario->getMinhasPermissoes(); // Traz todas as permissões do usuario (Grupos e Equipes) em array
 *
 * @package Modules\Sistema\Traits
 */
trait GruposPermissoesTrait
{
    /**
     * Pega os grupos com retorno em object
     * @return mixed
     */
    public function getMeusGruposEloquent()
    {
        // Pega o ClienteUsuario
        $meusgrupos = $this->getClienteUsuario()->first();
        // Pega os Grupos que estão relacionados ao ClienteUsuario
        return $meusgrupos->getGruposEloquent();
    }
    
    /**
     * Pega os grupos
     * @return array
     */
    public function getMeusGrupos()
    {
        $grupos = $this->getMeusGruposEloquent();
        
        return $this->mapearGrupos($grupos);
    }
    
    /**
     * O Usuário pertence a cliente
     * @return mixed
     */
    public function getMeusGruposePermissoesEloquent()
    {
        // Pega o ClienteUsuario
        $meusgrupos = $this->getClienteUsuario()->first();
        // Carrega as permissões assincrona no grupo
        return $meusgrupos->grupospermissoesclientes->load('permissoes');
    }
    
    /**
     * @return array
     */
    public function getMeusGruposePermissoes()
    {
        $grupospermissoes = \Cache::remember(
            "getMeusGruposePermissoes_".auth()->user()->nid,
            60,
            function () {
                return $this->getMeusGruposePermissoesEloquent();
            }
        );
        // $grupospermissoes = $this->getMeusGruposePermissoesEloquent();
        
        return $this->mapearGruposPermissoes($grupospermissoes);
    }
    
    /**
     * Pega as permissões
     * @return um objeto de permissões
     */
    public function getMinhasPermissoesEloquent()
    {
        // Pega o ClienteUsuario
        $meusgrupos = $this->getMeusGruposePermissoesEloquent();
        
        $collection_permissoes = collect();
        
        // Anda por cada grupo e por cada permissão
        foreach ($meusgrupos as $key => $value) {
            foreach ($value->permissoes as $keypermissao => $valuepermissao) {
                $collection_permissoes->put('permissao_'.$valuepermissao->nid, $valuepermissao);
            }
        }
        
        // Integra as permissões de equipes
        $equipespermissoes = $this->integraEquipes();
        
        $nova_collection = $collection_permissoes->merge($equipespermissoes);
        
        // retorna minhas permissões
        return $nova_collection;
    }
    
    /**
     * Mapea os grupos
     *
     * @return array
     */
    private function mapearGrupos($grupos)
    {
        if (empty($grupos)) {
            return [];
        }
        
        $grupos = $grupos->pluck('cnome', 'nid');
        
        // $grupos = $this->collectionAsArray($grupos);
        
        // foreach ($map as $role_id => $grupos) {
        //     sort($grupos);
        //     $map[$role_id] = $grupos;
        // }
        
        return $grupos;
    }
    
    /**
     * Mapea os grupos com as permissões
     *
     * @return array
     */
    private function mapearGruposPermissoes($grupos)
    {
        if (empty($grupos)) {
            return [];
        }
        
        $grupos = $grupos->map(
            function ($r) {
                return [
                    'nid'        => $r->nid,
                    'cnome'      => $r->cnome,
                    'permissoes' => $r->permissoes->toArray(),
                ];
            }
        );
        return $grupos;
    }
}
