<?php

namespace Modules\Admin\Traits;

/**
 * Trait PermissionTrait
 *
 * Essa trait foi desenvolvida para gerenciar o sistema de permissões desde permissions dos grupos, baseado no cliente com usuario
 *
 * Exemplos:
 * Sempre chamar pela Model do usuario ($usuario = auth()->user())
 *
 * $usuario->getMeusGruposEloquent(); // Traz os grupos como uma collection de models
 * $usuario->getMeusGrupos(); // Traz os grupos em array
 * $usuario->getMyGroupsPermissionsEloquent(); // Traz os grupos com permissões como uma collection de models
 * $usuario->getMeusGruposePermissoes(); // Traz os grupos com permissões em array
 * $usuario->getMyPermissionsEloquent(); // Traz todas as permissões do usuario (Grupos e Equipes) como uma collection de models
 * $usuario->getMyPermissions(); // Traz todas as permissões do usuario (Grupos e Equipes) em array
 *
 * @package Modules\Sistema\Traits
 */
trait PermissionTrait
{
    use GroupPermissionsTrait;

    /**
     * Pega as permissões
     * @return um objeto de permissões
     */
    public function getMyPermissionsEloquent()
    {
        // Pega o ClienteUsuario
        $meusgrupos = $this->getMyGroupsPermissionsEloquent();

        $collection_permissions = collect();

        // Anda por cada grupo e por cada permissão
        foreach ($meusgrupos as $key => $value) {
            foreach ($value->permissions as $keypermission => $valuepermission) {
                $collection_permissions->put('permission_'.$valuepermission->id, $valuepermission);
            }
        }

        return $collection_permissions;
    }

    /**
     * Pega as permissões
     * @return um array com as permissões
     */
    public function getMyPermissions()
    {
        $permissions = $this->getMyPermissionsEloquent();

        $mapedPermissions = $this->mapPermissions($permissions);

        return $mapedPermissions->unique()->values()->all();
    }

    /**
     * Mapea as permissões
     *
     * @return array
     */
    private function mapPermissions($permissions)
    {
        // verifica se existe permissão
        if (empty($permissions)) {
            return [];
        }

        // Mapea a coleção do laravel para retornar como um array
        // utiliza uma função de mapeamento, executa como se fosse um loop,
        // passando por cada posição e traz dados conver informo, mapeando o retornoa
        $permissions = $permissions->map(
            function ($r) {
                return [
                    'id'       => $r->id,
                    'name'     => $r->name,
                    'slug'     => $r->slug,
                    'allowed'     => (isset($r->allowed))? $r->allowed : true,
                ];
            }
        );

        $myPermissionsOnUser = $this->permissions;

        $permissions = $permissions->merge($myPermissionsOnUser);

        return $permissions; // retorna as permissões mapeadas
    }

    /**
     * @param null $permission
     * @param null $operador
     *
     * @return bool
     */
    public function canpermission($permission = null, $operador = null){
        // caso não seja passado um operador (AND ou OR), ele tenta encontrar um na string de permissão
        $operador = is_null($operador) ? $this->parseOperador($permission) : $operador;

        $permission = $this->hasDelimiterToArray($permission);
        if(is_string($permission)){
            $permission = array($permission);
            $operador = 'and';
        }

        // convert em slug a permissão atual
        $permission = $this->toSlug($permission);

        $permissions = $this->getMyPermissions();

        // validate permissions array
        if ( is_array($permission) ) {

            if ( ! in_array($operador, ['and', 'or']) ) {
                $e = __("Operador inválido, operadores disponveis \"and\", \"or\".");
                throw new \InvalidArgumentException($e);
            }

            $call = 'canWith' . ucwords($operador);

            return $this->$call($permission, $permissions) || auth('admin')->user()->webmaster;
        }
    }

    /**
     * Função que verifica as permissões com o operador AND
     * Fazendo com que o usuário só pode ter uma permissão se a(s) form válida também
     *
     * @param string $permission
     * @param array  $permissions
     * @param string $model
     * @param int    $reference_id
     *
     * @return bool
     */
    protected function canWithAnd($permission, $permissions)
    {
        $permissions = collect($permissions); // gera uma coleção
//        $permissions = $permissions->pluck('slug', 'id')->toArray(); // caso exista mais campos na coleção, retornar apenas o slug e nid

        $permission_ok = false; // seta o retorno inicial
        foreach ($permission as $check) { // percorre cada permissão
            if($permissions->count() > 0){
                // Find id on array
                $finded_id = $permissions->search(function ($item, $key) use($check){
                    $item = (object) $item;
                    return $item->slug == $check;
                });
                if($finded_id != false){
                    $finded = (object) $permissions[$finded_id];

                    if(!is_null($finded)){
                        if(isset($finded->allowed) && $finded->allowed){ // verifica se a permissão enconra-se no array de permissões do usuário
                            $permission_ok        = true;
                        }else{
                            $permission_ok        = false;
                        }
                    }
                }
            }
        }


        return $permission_ok;
    }

    /**
     * Função que verifica as permissões com o operador OR
     * Fazendo com que o usuário pode ter uma ou outra permissão
     *
     * @param string $permission
     * @param array  $permissions
     * @param string $model
     * @param int    $reference_id
     *
     * @return bool
     */
    protected function canWithOr($permission, $permissions)
    {
        $permissions = collect($permissions); // gera uma coleção
        $permissions = $permissions->pluck('slug', 'id')->toArray(); // caso exista mais campos na coleção, retornar apenas o slug e nid
        $permission_ok = false; // seta o retorno inicial
        foreach ($permission as $check) {
            if(in_array($check, $permissions)){
                $permission_ok        = true;
            }
        }
        return $permission_ok;
    }

    /**
     * Transforma os textos de cada posição do array em um slug, para facilitar a validação
     *
     * @param array $permission
     *
     * @return array
     */
    protected function toSlug(array $permission)
    {
        $data = [];
        foreach ($permission as $alias => $perm) { // Percore cada item do array
            $data[] = str_slug($perm); // Converte o texto em uma url amigavel
        }
        return $data;
    }

    /**
     * Descobre qual o tipo de operador que está sendo usado (baseia-se apenas no primeiro)
     * @param $str
     *
     * @return bool|string
     */
    protected function parseOperador($str)
    {
       // if its an array lets use
       // and operator by default
       if ( is_array($str) ) {
           $str = implode(',', $str);
       }

       if ( preg_match('/((&&)|[|])(?:\s+)?/', $str, $m) ) {
           return $m[0].$m[1] == '||' ? 'or' : 'and';
       }

       return false;
    }

    /**
     * Converte strings que tem virgula ou pipe para um array em minuscula
     *
     * @param $str
     * @return array
     */
    protected function hasDelimiterToArray($str)
    {
        if ( is_string($str) && preg_match('/(&&)|[|]/is', $str) ) {
            return preg_split('/ ?(&&)|[|] ?/', mb_strtolower($str, 'UTF-8'));
        }

        return is_array($str) ?
            array_map(function ($value){ return mb_strtolower($value, 'UTF-8');}, $str) :
            (is_object($str) ? $str : mb_strtolower($str, 'UTF-8'));
    }
}
