<?php

namespace Modules\Admin\Traits;

use Modules\Sistema\Entities\ClienteUsuarioGrupoPermissaoCliente;
use Modules\Sistema\Entities\GrupoPermissaoCliente;

trait GroupPermissionTrait
{

    public function getGruposEloquent()
    {
        // dd("getGruposPorID_{$this->getKey()}");

        $grupos = \Cache::remember(
            "getGruposEloquent_".auth()->user()->id,
            60,
            function () {
                return $this->grupospermissionsclientes()->get();
            }
        );
        // $grupos = $this->grupospermissionsclientes()->get();

        return $grupos;
    }

    public function getGrupos()
    {
        $grupos = $this->getGruposEloquent();

        return $this->mapearGrupos($grupos);
    }

    // Pega a relação do usuario que está em um cliente com um
    public function usuarioclientepermission()
    {
        return $this->with('grupospermissionsclientes.permissions')->get();
    }

    /**
     * Get sorted map from roles.
     *
     * @return array
     */
    private function mapearGrupos($grupos)
    {
        if (empty($grupos)) {
            return [];
        }

        $grupos = $grupos->pluck('name', 'id');

        // $grupos = $this->collectionAsArray($grupos);

        // foreach ($map as $role_id => $grupos) {
        //     sort($grupos);
        //     $map[$role_id] = $grupos;
        // }

        return $grupos;
    }


}
