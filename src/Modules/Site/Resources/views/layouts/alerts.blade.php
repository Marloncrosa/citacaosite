@push('line-js')
  @if (count($errors) > 0)
    @php
      $message = '';
    @endphp
    @foreach ($errors->all() as $key => $value)
      @php
        $message .= $value;
        $message .= '<br>';
      @endphp
    @endforeach
    <script>
      $(function(){
        var content = document.createElement('div');
        content.innerHTML = "{!! $message !!}";
        swal({
          content: content,
          icon: "error",
        });
      })
    </script>
  @endif

  @if (session('error'))
    <script>
      $(function(){
        var content = document.createElement('div');
        content.innerHTML = "{!! session('error') !!}";

        swal({
          content: content,
          icon: "error",
        });
      })
    </script>
  @endif

  @if (session('success'))
    <script>
      $(function(){
        var content = document.createElement('div');
        content.innerHTML = "{!! session('success') !!}";

        swal({
          content: content,
          icon: "success",
        });
      })
    </script>
  @endif

  @if (session('status'))
    <script>
      $(function(){
        var content = document.createElement('div');
        content.innerHTML = "{!! session('status') !!}";

        swal({
          content: content,
          icon: "success",
        });
      })
    </script>
  @endif
@endpush