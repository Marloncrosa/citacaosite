@php
    // Página atual
    $page = $paginator->currentPage();

    // Última página
    $lastPage 	= $paginator->lastPage();
    $lasturl 	= route( app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => $lastPage ]) );

    // Penúltima página
    $beforelastPage = $lastPage - 1;
    $beforelasturl 	= route( app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => $beforelastPage ]) );

    // Urls página um e dois
    $firsturl 	= route( app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => 1 ]) );
    $secondurl 	= route( app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => 2 ]) );

    // Páginas adjacentes
    $adjacentes = (isset($adjacentes))? $adjacentes : 1;

    // Se a página atual for menor-igual que 1 pega a página um como página anterior
    if($page <= 1){
      $prevPage = route( app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => 1 ]) );
    }else{
      $prevPage = route( app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => $page-1 ]) );
    }

    // Se a página atual for maior-igual que 1 pega a página um como página anterior
    if($page <= $lastPage){
      $nextPage = route( app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => $page+1 ]) );
    }else{
      $nextPage = route( app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => $lastPage ]) );
    }
@endphp

@if ($paginator->lastPage() > 1)
    <nav >
        <ul class="pagination justify-content-center" role="navigation" aria-label="Paginação">
            {{-- Caso a página atual seja maior que 1 mostra o anterior --}}
            @if($page > 1)
                <li class="page-item {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
                    <a href="{{ $prevPage }}" title="Página anterior" aria-label="Página anterior"><i
                            class="page-link"  aria-hidden="true">Anterior</i></a>
                </li>
            @endif

            {{-- Caso a ultima página seja maior que 5 --}}
            @if($lastPage > 5 )
                {{--Se a página atual menor que a soma (2+(2 * $adjacentes)), página atual esta no começo--}}
                @if($page < 2 + (2 * $adjacentes))
                    {{--Loop dos primeiros itens--}}
                    @for ($i = 1; $i < 3 + (2 * $adjacentes); $i++)
                        @php
                            $url = route(app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => $i ]));
                        @endphp
                        <li class="{{ ($paginator->currentPage() == $i) ? 'active' : '' }}">
                            @if($paginator->currentPage() == $i)
                                <a href="javascript:;" class="page-link" title="Página {{ $i }}"
                                   aria-label="Página {{ $i }}">
                                    {{ $i }}
                                </a>
                            @else
                                <a href="{{ $url }}" class="page-link" title="Página {{ $i }}"
                                   aria-label="Página {{ $i }}">{{ $i }}</a>
                            @endif
                        </li>
                    @endfor
                    <li class="ellipsis">
                        <span>
                            ...
                        </span>
                    </li>
                    <li>
                        <a href="{{ $beforelasturl }}" class="page-link" title="Penúltima página"
                           aria-label="Penúltima página">{{ $beforelastPage }}</a>
                    </li>
                    <li>
                        <a href="{{ $lasturl }}" class="page-link" title="Última página"
                           aria-label="Última página">{{ $lastPage }}</a>
                    </li>
                    {{--Se a página atual for maior que a soma (2+(2 * $adjacentes)) e menor que a página final--}}
                @elseif($page > (2 * $adjacentes) && $page < $lastPage - 2)
                    <li>
                        <a href="{{ $firsturl }}" class="page-link" title="Primeira página" aria-label="Primeira página">1</a>
                    </li>
                    <li>
                        <a href="{{ $secondurl }}" class="page-link" title="Segunda página" aria-label="Segunda página">2</a>
                    </li>
                    <li class="ellipsis">
                        <span>
                            ...
                        </span>
                    </li>
                    {{--Loop das páginas do meio--}}
                    @for ($i = $page - $adjacentes; $i <= $page + $adjacentes; $i++)
                        @php
                            $url = route(app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => $i ]));
                        @endphp
                        <li class="{{ ($paginator->currentPage() == $i) ? 'active' : '' }}">
                            @if($paginator->currentPage() == $i)
                                <a href="javascript:;" class="page-link" class="active" title="Página {{ $i }}"
                                   aria-label="Página {{ $i }}">
                                    {{ $i }}
                                </a>
                            @else
                                <a href="{{ $url }}" title="Página {{ $i }}"
                                   aria-label="Página {{ $i }}">{{ $i }}</a>
                            @endif
                        </li>
                    @endfor
                    <li class="ellipsis">
                        <span>
                            ...
                        </span>
                    </li>
                    <li>
                        <a href="{{ $beforelasturl }}" title="Penúltima página"
                           aria-label="Penúltima página">{{ $beforelastPage }}</a>
                    </li>
                    <li>
                        <a href="{{ $lasturl }}" title="Última página"
                           aria-label="Última página">{{ $lastPage }}</a>
                    </li>
                @else
                    <li>
                        <a href="{{ $firsturl }}" title="Primeira página" aria-label="Primeira página">1</a>
                    </li>
                    <li>
                        <a href="{{ $secondurl }}" title="Segunda página" aria-label="Segunda página">2</a>
                    </li>
                    <li class="ellipsis">
                        <span>
                            ...
                        </span>
                    </li>
                    {{--Loop das ultmas páginas--}}
                    @for ($i = $lastPage - (1 + (2 * $adjacentes)); $i <= $lastPage; $i++)
                        @php
                            $url = route(app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => $i ]));
                        @endphp
                        <li class="{{ ($paginator->currentPage() == $i) ? 'active' : '' }}">
                            @if($paginator->currentPage() == $i)

                                <a href="javascript:;" class="active" title="Página {{ $i }}"
                                   aria-label="Página {{ $i }}">
                                    {{ $i }}
                                </a>
                            @else
                                <a href="{{ $url }}" title="Página {{ $i }}"
                                   aria-label="Página {{ $i }}">{{ $i }}</a>
                            @endif
                        </li>
                    @endfor
                @endif
            @else
                {{--Caso a páginação seja maior que 1 e menor que 5--}}
                {{--elseif( $total >= 1 && $lastPage <= 5 )--}}
                @for ($i = 1; $i <= $lastPage; $i++)
                    @php
                        $url = route(app('request')->route()->getAction()['as'], array_merge($args, [ 'pagina' => $i ]));
                    @endphp
                    <li class="{{ ($paginator->currentPage() == $i) ? 'active' : '' }}">
                        @if($paginator->currentPage() == $i)

                            <a href="javascript:;" class="active" title="Página {{ $i }}"
                               aria-label="Página {{ $i }}">
                                {{ $i }}
                            </a>
                        @else
                            <a href="{{ $url }}" title="Página {{ $i }}" aria-label="Página {{ $i }}">{{ $i }}</a>
                        @endif
                    </li>
                @endfor
            @endif

            {{--show if only we're not at the last page--}}
            @if($page<$lastPage)
                <li class="page-item" {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}">
                    <a class="page-link" href="{{ $nextPage }}">Próximo<i></i></a>
                </li>
            @endif
        </ul>
    </nav>
@endif
