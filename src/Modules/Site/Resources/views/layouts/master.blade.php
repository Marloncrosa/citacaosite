<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta name="robots" content="index, follow">
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title>{{ env('APP_SITE_TITLE') }} {{ (isset($subtitle))?$subtitle:'' }}</title>

        <meta name="description" content="">
        <meta name="author" content="Felipe Almeman">
        <meta name="generator" content="Aireset - Agência Digital">
        <meta name="keywords" content="">
        <meta name="theme-color" content="#000">

        <meta name="Language" content="Português">
        <meta http-equiv="cache-control" content="public">
        <meta name="distribution" content="Global">
        <meta name="city" content="Londrina">
        <meta name="country" content="Brasil">
        <meta name="Copyright" content="{{ env('APP_SITE_TITLE') }} {{ (isset($subtitle))?$subtitle:'' }}">

        {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <base href="{{ Request::root() }}/modules/site/">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="apple-mobile-web-app-capable" content="yes">

        <link rel="apple-touch-icon" sizes="180x180" href="images/favicon/apple-touch-icon.png?v=jwEegMd0v4">
        <link rel="icon" type="image/png" sizes="32x32" href="images/favicon/favicon-32x32.png?v=jwEegMd0v4">
        <link rel="icon" type="image/png" sizes="194x194" href="images/favicon/favicon-194x194.png?v=jwEegMd0v4">
        <link rel="icon" type="image/png" sizes="192x192" href="images/favicon/android-chrome-192x192.png?v=jwEegMd0v4">
        <link rel="icon" type="image/png" sizes="16x16" href="images/favicon/favicon-16x16.png?v=jwEegMd0v4">

        <link rel="mask-icon" href="images/favicon/safari-pinned-tab.svg?v=jwEegMd0v4" color="#5bbad5">
        <link rel="shortcut icon" href="images/favicon/favicon.ico?v=jwEegMd0v4">
        <meta name="apple-mobile-web-app-title" content="{{ env('APP_SITE_TITLE') }}">
        <meta name="application-name" content="{{ env('APP_SITE_TITLE') }}">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png?v=jwEegMd0v4">
        <meta name="theme-color" content="#ffffff">

        <link rel="manifest" href="manifest.json">

        <meta property="og:type" content="website">
        <meta property="og:title" content="{{ env('APP_SITE_TITLE') }} {{ (isset($subtitle))?$subtitle:'' }}">
        <meta property="og:site_name" content="{{ env('APP_SITE_TITLE') }} {{ (isset($subtitle))?$subtitle:'' }}"/>
        <meta property="og:url" content="{{ env('APP_URL') }}">
        <meta property="og:image" content="{{ env('APP_URL') }}/social.jpg">
        <meta property="og:description" content="{!! (isset($metadescription)? $metadescription : '') !!}">

        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:title" content="{{ env('APP_SITE_TITLE') }} {{ (isset($subtitle))?$subtitle:'' }}">
        <meta name="twitter:url" content="{{ env('APP_URL') }}">
        <meta name="twitter:image:src" content="{{ env('APP_URL') }}/social.jpg">
        <meta name="twitter:description" content="{!! (isset($metadescription)? $metadescription : '') !!}">

        <meta itemprop="name" content="{{ env('APP_SITE_TITLE') }} {{ (isset($subtitle))?$subtitle:'' }}">
        <meta itemprop="description" content="{!! (isset($metadescription)? $metadescription : '') !!}">
        <meta itemprop="image" content="{{ env('APP_URL') }}/social.jpg">

        <link
            href="//fonts.googleapis.com/css?family=Raleway:300,400,500,600,700,800,900|Roboto+Condensed:300,400,700|Roboto:300,400,500,700,900&display=swap"
            rel="stylesheet">

        <link href="css/site.css" rel="stylesheet">

        @stack('line-css')
    </head>
    <body class="{!! (isset($bodyclass))? $bodyclass : 'interna' !!}">
        <!--[if lt IE 8]>
        <p class="browserupgrade">Você está usando um navegador
            <strong>antigo</strong>
            . Por favor
            <a
                href="http://browsehappy.com/">atualize
            </a>
            para melhorar sua experiência.
        </p>
        <![endif]-->

        <div class="page-loader">
            <div class="loader">Loading...</div>
        </div>

        @if(\Route::getCurrentRoute()->getName() == 'site.home')
            @include('site::layouts.header')
        @else
            @include('site::layouts.header-interno')
        @endif

        @yield('beforeContent')

        <main id="{!! (isset($idMain))? $idMain : Route::currentRouteName() !!}"
              class="{{ Route::currentRouteName() }}">
            @yield('content')
        </main>

        @yield('afterContent')

        @include('site::layouts.footer')


        @include('site::layouts.alerts')

        {{-- Scripts --}}
        <div class="js">
            <script src="js/site.js"></script>
            <script>
                // if ('serviceWorker' in navigator ) {
                //     window.addEventListener('load', function() {
                //         navigator.serviceWorker.register('js/service-worker.js').then(function(registration) {
                //             // Registration was successful
                //             console.log('ServiceWorker registration successful with scope: ', registration.scope);
                //         }, function(err) {
                //             // registration failed :(
                //             console.log('ServiceWorker registration failed: ', err);
                //         });
                //     });
                // }
            </script>
            @stack('line-js')
        </div>

        @if(app('env') == 'production')
        <!-- Global site tag (gtag.js) - Google Analytics -->
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-29351171-5"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-29351171-5');
            </script>


            <!--Start of Tawk.to Script-->
            <script type="text/javascript">
                var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
                (function () {
                    var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
                    s1.async = true;
                    s1.src = 'https://embed.tawk.to/58512898de6cd808f331a5f8/default';
                    s1.charset = 'UTF-8';
                    s1.setAttribute('crossorigin', '*');
                    s0.parentNode.insertBefore(s1, s0);
                })();
            </script>

            <!--End of Tawk.to Script-->

        @endif

    </body>
</html>
