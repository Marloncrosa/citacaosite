<div class="off-canvas position-left" id="offCanvas" data-off-canvas>

    <!-- Close button -->
    <button class="close-button" aria-label="Close menu" type="button" data-close>
        <span aria-hidden="true">&times;</span>
    </button>

    <!-- Menu -->
    <ul data-drilldown data-back-button="&lt;li class&#x3D;&quot;js-drilldown-back&quot;&gt;&lt;a tabindex&#x3D;&quot;0&quot;&gt;Voltar&lt;/a&gt;&lt;/li&gt;">
        <li class="close-menu"><span><i class="icon-cancel"></i></span></li>
        @includeIf('site::layouts.menu-header')
    </ul>
</div>
