<header class="header header-visible">
    <div class="container">
        <div class="nav-wrapper clearfix">
            <!-- LOGO -->
            <h1>
                <a data-link-outer="true" href="{!! route('site.home') !!}" title="logo">Muller Diesel</a>
            </h1>

            <!-- SOCIAL ICON LINK -->
            <ol class="social-icon">
                {{--<li>
                    <a href="#" title="Twitter" target="_blank">
                        <i class="icon-096"></i>
                    </a>
                </li>--}}
                <li>
                    <a href="https://www.facebook.com/muller.diesel.2/" title="Facebook" target="_blank">
                        <i class="icon-093" style="color: rgb(231,30,20);"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/mullerdiesel/" title="Dribbble" target="_blank">
                        <svg viewBox="0 0 512 512" width="25px" xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="m305 256c0 27.0625-21.9375 49-49 49s-49-21.9375-49-49 21.9375-49 49-49 49 21.9375 49 49zm0 0"/>
                            <path
                                d="m370.59375 169.304688c-2.355469-6.382813-6.113281-12.160157-10.996094-16.902344-4.742187-4.882813-10.515625-8.640625-16.902344-10.996094-5.179687-2.011719-12.960937-4.40625-27.292968-5.058594-15.503906-.707031-20.152344-.859375-59.402344-.859375-39.253906 0-43.902344.148438-59.402344.855469-14.332031.65625-22.117187 3.050781-27.292968 5.0625-6.386719 2.355469-12.164063 6.113281-16.902344 10.996094-4.882813 4.742187-8.640625 10.515625-11 16.902344-2.011719 5.179687-4.40625 12.964843-5.058594 27.296874-.707031 15.5-.859375 20.148438-.859375 59.402344 0 39.25.152344 43.898438.859375 59.402344.652344 14.332031 3.046875 22.113281 5.058594 27.292969 2.359375 6.386719 6.113281 12.160156 10.996094 16.902343 4.742187 4.882813 10.515624 8.640626 16.902343 10.996094 5.179688 2.015625 12.964844 4.410156 27.296875 5.0625 15.5.707032 20.144532.855469 59.398438.855469 39.257812 0 43.90625-.148437 59.402344-.855469 14.332031-.652344 22.117187-3.046875 27.296874-5.0625 12.820313-4.945312 22.953126-15.078125 27.898438-27.898437 2.011719-5.179688 4.40625-12.960938 5.0625-27.292969.707031-15.503906.855469-20.152344.855469-59.402344 0-39.253906-.148438-43.902344-.855469-59.402344-.652344-14.332031-3.046875-22.117187-5.0625-27.296874zm-114.59375 162.179687c-41.691406 0-75.488281-33.792969-75.488281-75.484375s33.796875-75.484375 75.488281-75.484375c41.6875 0 75.484375 33.792969 75.484375 75.484375s-33.796875 75.484375-75.484375 75.484375zm78.46875-136.3125c-9.742188 0-17.640625-7.898437-17.640625-17.640625s7.898437-17.640625 17.640625-17.640625 17.640625 7.898437 17.640625 17.640625c-.003906 9.742188-7.898437 17.640625-17.640625 17.640625zm0 0"/>
                            <path
                                d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm146.113281 316.605469c-.710937 15.648437-3.199219 26.332031-6.832031 35.683593-7.636719 19.746094-23.246094 35.355469-42.992188 42.992188-9.347656 3.632812-20.035156 6.117188-35.679687 6.832031-15.675781.714844-20.683594.886719-60.605469.886719-39.925781 0-44.929687-.171875-60.609375-.886719-15.644531-.714843-26.332031-3.199219-35.679687-6.832031-9.8125-3.691406-18.695313-9.476562-26.039063-16.957031-7.476562-7.339844-13.261719-16.226563-16.953125-26.035157-3.632812-9.347656-6.121094-20.035156-6.832031-35.679687-.722656-15.679687-.890625-20.6875-.890625-60.609375s.167969-44.929688.886719-60.605469c.710937-15.648437 3.195312-26.332031 6.828125-35.683593 3.691406-9.808594 9.480468-18.695313 16.960937-26.035157 7.339844-7.480469 16.226563-13.265625 26.035157-16.957031 9.351562-3.632812 20.035156-6.117188 35.683593-6.832031 15.675781-.714844 20.683594-.886719 60.605469-.886719s44.929688.171875 60.605469.890625c15.648437.710937 26.332031 3.195313 35.683593 6.824219 9.808594 3.691406 18.695313 9.480468 26.039063 16.960937 7.476563 7.34375 13.265625 16.226563 16.953125 26.035157 3.636719 9.351562 6.121094 20.035156 6.835938 35.683593.714843 15.675781.882812 20.683594.882812 60.605469s-.167969 44.929688-.886719 60.605469zm0 0"/>
                        </svg>
                    </a>
                </li>
                <li><span class="icon-search"></span></li>
            </ol>

            <!-- MAIN MENU -->
            <nav class="menu">
                <ul class="local-scroll">
                    <li class="is-active">
                        <a href="{!! route('site.home') !!}" title="Home">Home</a>
                    </li>
                    <li>
                        <a href="{!! route('site.produtos') !!}" data-nav-target="projects"
                           title="Produtos">Produtos</a>
                    </li>
                    <li>
                        <a href="{!! route('site.blog') !!}" data-nav-target="projects" title="Produtos">Blog</a>
                    </li>
                    {{--                    <li>--}}
                    {{--                        <a href="#services" data-nav-target="services" title="Serviços">Serviços</a>--}}
                    {{--                    </li>--}}
                    <li>
                        <div class="dropdown">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Institucionais
                            </a>

                            <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                @if(!empty($footerinstitucionais))
                                    @foreach ($footerinstitucionais as $institucional)
                                        <a class="dropdown-item" href="{!! route('site.institucional', [ 'slug' => str_slug($institucional->title) ]) !!}">{!! $institucional->title !!}</a>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </li>
                    {{--                    <li>--}}
                    {{--                        <a href="#contact" data-nav-target="contact" title="Contato">Contato</a>--}}
                    {{--                    </li>--}}
                </ul>
            </nav>

            <!-- MOBILE MENU -->
            <button id="sidebarCollapse">
                <svg version="1.1" id="Capa_4" xmlns="http://www.w3.org/2000/svg"
                     xmlns:xlink="http://www.w3.org/1999/xlink"
                     x="0px" y="0px" width="32px" height="32px" viewBox="-3.15 -2.715 32 32"
                     enable-background="new -3.15 -2.715 32 32"
                     xml:space="preserve">
                            <ellipse transform="matrix(-0.523 0.8523 -0.8523 -0.523 2.9298 0.8213)" cx="1.235" cy="1.23"
                                     rx="3.994"
                                     ry="3.809"/>
                    <ellipse transform="matrix(-0.5232 0.8522 -0.8522 -0.5232 13.2042 19.1827)" cx="1.236"
                             cy="13.285" rx="3.994"
                             ry="3.809"/>
                    <ellipse transform="matrix(-0.5232 0.8522 -0.8522 -0.5232 23.4789 37.5437)" cx="1.237"
                             cy="25.34" rx="3.994"
                             ry="3.809"/>
                    <polygon points="9.265,-1.447 9.265,2.944 28.325,2.944 28.325,-2.449 "/>
                    <polygon points="9.265,14.986 28.325,14.986 28.325,9.593 9.265,10.596 "/>
                    <polygon points="9.265,27.029 28.325,27.029 28.325,21.635 9.265,22.639 "/>
                </svg>
            </button>
        </div>
    </div>
    <form action="{!! route('site.produtos', ['pagina' => 1] + $_GET) !!}" method="get" name="busca-geral"
          class="busca-geral">
        <div>
            <input type="text" name="busca" id="palavra-chave" placeholder="O que você procura">
            <button type="submit" name="buscar" class="icon-search"></button>
        </div>
    </form>
</header>


<nav id="sidebar">
    <div id="dismiss">
        <i class="fas fa-arrow-left"></i>
    </div>

    <ul class="list-unstyled components">
        <p>Menu</p>

        <li class="active">
            <a href="{!! route('site.home') !!}" title="Home">Home</a>
        </li>
        <li>
            <a href="{!! route('site.produtos') !!}" data-nav-target="projects" title="Produtos">Produtos</a>
        </li>
        <li>
            <a href="{!! route('site.institucional') !!}" title="Quem Somos">Quem Somos</a>
        </li>
        {{--        <li>--}}
        {{--            <a href="#contact" data-nav-target="contact" title="Contato">Contato</a>--}}
        {{--        </li>--}}
    </ul>
</nav>
