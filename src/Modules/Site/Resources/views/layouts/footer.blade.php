<section data-inpage-section="true" id="contact">
    <section class="page-section bg-color overlay-color-90 pt-140 pt-sm-100 pb-140 pb-sm-100"
             data-background="images/image_6.jpg">
        <div class="container">

            <!-- CONTACT INTRO -->
            <div class="row">
                <div class="page-section pb-140 pb-sm-100">
                    <div class="col-sm-12 col-md-8">
                        <h2 class="heading-big fw700 text-white mb-30 mb-sm-20">
                            Vamos trabalhar
                            <span class="newline">Juntos</span>
                        </h2>
                        <h3 class="heading-big-subhead fw400 text-gray pt-20">
                            Vamos fazer a diferença, vamos trabalhar juntos, trazendo novas ideias e perspectivas.
                            Com os melhores produtos, melhor qualidade e eficiência garantida.
                        </h3>
                    </div>
                </div>
            </div>

            <!-- CONTACT DETAIL -->
            <div class="hero-section bg-white last-para-no-margin pt-70 pb-70 pl-50 pr-50 pl-sm-30 pr-sm-30">
                <div class="row equalize sm-equalize-auto alt-contact-box">
                    <div class="col-sm-4 mb-sm-30 mt-70">
                        <div class="contact-no">
                            <h4 class="heading-medium  mb-10 fw700">Entre em contato</h4>
                            <p class=" heading-small mb-0">
                                <a href="tel:4333720200">Central: 43. 3372 0200</a>
                                <br/>
                                <a href="tel:08004000200">0800 400 0200</a>
                                <br/>
                                <a href="mailto:contato@muller.com.br">Email: contato@mullerdiesel.com.br</a>
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-4 mb-sm-30 mt-70 ">
                        <div class="contact-address">
                            <h4 class="heading-medium  mb-10 fw600">Endereço</h4>
                            <p class=" heading-small">
                                Muller Diesel <br/>
                                Av. Waldemar Spranger, 820 - Colonial, Londrina - PR, 86047-285
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-4 mt-70">
                        <div class="contact-social text-center">
                            <h4 class="heading-medium  mb-10 fw600">Siga-nos</h4>
                            <!-- SOCIAL ICON LINK -->
                            <ol>
                                {{--<li>
                                    <a href="#" title="Twitter" target="_blank">
                                        <i class="icon-096"></i>
                                    </a>
                                </li>--}}
                                <li>
                                    <a href="https://www.facebook.com/muller.diesel.2/" title="Facebook"
                                       target="_blank">
                                        <i class="icon-093"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/mullerdiesel/" title="Dribbble"
                                       target="_blank">
                                        <svg viewBox="0 0 512 512" width="25px" xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="m305 256c0 27.0625-21.9375 49-49 49s-49-21.9375-49-49 21.9375-49 49-49 49 21.9375 49 49zm0 0"/>
                                            <path
                                                d="m370.59375 169.304688c-2.355469-6.382813-6.113281-12.160157-10.996094-16.902344-4.742187-4.882813-10.515625-8.640625-16.902344-10.996094-5.179687-2.011719-12.960937-4.40625-27.292968-5.058594-15.503906-.707031-20.152344-.859375-59.402344-.859375-39.253906 0-43.902344.148438-59.402344.855469-14.332031.65625-22.117187 3.050781-27.292968 5.0625-6.386719 2.355469-12.164063 6.113281-16.902344 10.996094-4.882813 4.742187-8.640625 10.515625-11 16.902344-2.011719 5.179687-4.40625 12.964843-5.058594 27.296874-.707031 15.5-.859375 20.148438-.859375 59.402344 0 39.25.152344 43.898438.859375 59.402344.652344 14.332031 3.046875 22.113281 5.058594 27.292969 2.359375 6.386719 6.113281 12.160156 10.996094 16.902343 4.742187 4.882813 10.515624 8.640626 16.902343 10.996094 5.179688 2.015625 12.964844 4.410156 27.296875 5.0625 15.5.707032 20.144532.855469 59.398438.855469 39.257812 0 43.90625-.148437 59.402344-.855469 14.332031-.652344 22.117187-3.046875 27.296874-5.0625 12.820313-4.945312 22.953126-15.078125 27.898438-27.898437 2.011719-5.179688 4.40625-12.960938 5.0625-27.292969.707031-15.503906.855469-20.152344.855469-59.402344 0-39.253906-.148438-43.902344-.855469-59.402344-.652344-14.332031-3.046875-22.117187-5.0625-27.296874zm-114.59375 162.179687c-41.691406 0-75.488281-33.792969-75.488281-75.484375s33.796875-75.484375 75.488281-75.484375c41.6875 0 75.484375 33.792969 75.484375 75.484375s-33.796875 75.484375-75.484375 75.484375zm78.46875-136.3125c-9.742188 0-17.640625-7.898437-17.640625-17.640625s7.898437-17.640625 17.640625-17.640625 17.640625 7.898437 17.640625 17.640625c-.003906 9.742188-7.898437 17.640625-17.640625 17.640625zm0 0"/>
                                            <path
                                                d="m256 0c-141.363281 0-256 114.636719-256 256s114.636719 256 256 256 256-114.636719 256-256-114.636719-256-256-256zm146.113281 316.605469c-.710937 15.648437-3.199219 26.332031-6.832031 35.683593-7.636719 19.746094-23.246094 35.355469-42.992188 42.992188-9.347656 3.632812-20.035156 6.117188-35.679687 6.832031-15.675781.714844-20.683594.886719-60.605469.886719-39.925781 0-44.929687-.171875-60.609375-.886719-15.644531-.714843-26.332031-3.199219-35.679687-6.832031-9.8125-3.691406-18.695313-9.476562-26.039063-16.957031-7.476562-7.339844-13.261719-16.226563-16.953125-26.035157-3.632812-9.347656-6.121094-20.035156-6.832031-35.679687-.722656-15.679687-.890625-20.6875-.890625-60.609375s.167969-44.929688.886719-60.605469c.710937-15.648437 3.195312-26.332031 6.828125-35.683593 3.691406-9.808594 9.480468-18.695313 16.960937-26.035157 7.339844-7.480469 16.226563-13.265625 26.035157-16.957031 9.351562-3.632812 20.035156-6.117188 35.683593-6.832031 15.675781-.714844 20.683594-.886719 60.605469-.886719s44.929688.171875 60.605469.890625c15.648437.710937 26.332031 3.195313 35.683593 6.824219 9.808594 3.691406 18.695313 9.480468 26.039063 16.960937 7.476563 7.34375 13.265625 16.226563 16.953125 26.035157 3.636719 9.351562 6.121094 20.035156 6.835938 35.683593.714843 15.675781.882812 20.683594.882812 60.605469s-.167969 44.929688-.886719 60.605469zm0 0"/>
                                        </svg>
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <!-- CONTACT FORM -->
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2  text-center">
                    <div class="hero-section pt-70">
                        <h6 class="heading-big-subhead line-height-1 fw700 mt-0 mb-20 text-white">
                            <span class="newline heading-hero">Entrar em contato</span>
                        </h6>
                        <p class="text-gray">
                            Acesse nosso formulário abaixo para poder entrar conosco e iniciarmos um parceria juntos
                        </p>
                        <a href="javascript:void(0);" class="btn-contact-popup btn btn-black mt-10 mt-xs-20">
                            Abrir formulário
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>

<footer class="page-section footer pb-40 pt-sm-100 pb-sm-100">
        <nav>
            <ul>
                <li class="is-active">
                    <a href="{!! route('site.home') !!}" title="Home">Home</a>
                </li>
                <li>
                    <a href="{!! route('site.produtos') !!}" data-nav-target="projects" title="Produtos">Produtos</a>
                </li>
                {{--                <li>--}}
                {{--                    <a href="#services" data-nav-target="services" title="Serviços">Serviços</a>--}}
                {{--                </li>--}}
                @if(!empty($footerinstitucionais))
                    @foreach ($footerinstitucionais as $institucional)

                        <li>
                            <a href="{!! route('site.institucional', [ 'slug' => str_slug($institucional->title) ]) !!}"
                               data-nav-target="about" title="Quem Somos">{!! $institucional->title !!}</a>
                        </li>
                    @endforeach
                @endif
                <li>
                    <a href="/#contact" data-nav-target="contact" title="Contato">Contato</a>
                </li>
            </ul>
        </nav>
        <aside>
            <p class="mb-0">© {!! date('Y') !!} Todos os Direitos Reservados a Muller Diesel </p>
            <p class="mb-0">
                Desenvolvido por
                <a href="https://aireset.com.br" target="_blank">aireset.com.br</a>
            </p>
        </aside>
</footer>

<a href="/" class="back-to-top"><i class="fa fa-arrow-up"></i></a>

<div class="popup is-popup-contact">
    <!-- popup background grid -->
    <div class="bg-grid">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>

    <!-- popup close button -->
    <button class="popup-close">
        <svg viewBox="0 0 512 512">
            <path
                d="M492.5 410L338.8 256l153.7-154c22.9-22.9 22.9-60 0-82.9-22.9-22.9-59.9-22.9-82.8 0L256 173.1l-153.7-154c-22.9-22.9-59.9-22.9-82.8 0s-22.9 60 0 82.9l153.7 154L19.5 410c-22.9 22.9-22.9 60 0 82.9 11.4 11.5 26.4 17.2 41.4 17.2s30-5.7 41.4-17.2l153.7-154 153.7 154c11.4 11.5 26.4 17.2 41.4 17.2s29.9-5.7 41.4-17.2c22.9-22.9 22.9-60 0-82.9z"/>
        </svg>
    </button>

    <!-- popup content area -->
    <div class="popup-content">

        <section class="page-section pt-100 pb-100 pt-sm-80 pb-sm-80">
            <div class="container">
                <div class="row">
                    <div class="project-detail-body mb-100 clearfix">
                        <div class="col-md-8 col-md-offset-2 text-center">
                            <h4 class="heading-big fw700 text-color mb-30 mb-sm-20">
                                Vamos trabalhar juntos
                            </h4>
                            <p class="heading-big-subhead fw400 pt-20">
                                At Cedia we don’t simply brand companies, build websites and run
                                digital marketing campaigns. We create innovative solutions to help
                                businesses work at their full potential.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="row alt-contact-box">
                    <div class="col-sm-8 col-sm-offset-2 text-center">
                        <div class="row">
                            <!-- CONTACT FORM -->
                            {!! Form::model($model, ['route' => 'site.contato-post', 'method' => 'post', 'class' => 'form-wrapper']) !!}
                            {{--                                                        <form autocomplete="off" id="contact_form" class="form-wrapper clearfix">--}}
                            <div class="col-sm-6 mt-30">
                                <div class="field-group">
                                    <div class="form-field">
                                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Nome']) !!}
                                        {{--                                        <input type="text" required="" pattern=".{3,100}" placeholder="Nome"--}}
                                        {{--                                               id="name" name="name"/>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-30">
                                <div class="field-group">
                                    <div class="form-field">
                                        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'E-mail']) !!}
                                        {{--                                        <input type="email" required="" pattern=".{5,100}"--}}
                                        {{--                                               placeholder="Email" id="email" name="email">--}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 mt-30">
                                <div class="field-group">
                                    <div class="form-field">
                                        <input type="text" required="" placeholder="Assunto" id="subject"
                                               name="subject">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 mt-30">
                                <div class="form-field is-block textarea-field">
                                    {!! Form::textarea('message', null, ['class' => 'form-control', 'required' => 'true', 'placeholder' => 'Conte-nos sobre o seu projeto. *', 'maxlength' => '5000']) !!}
                                    {{--                                        <textarea name="message" id="message"--}}
                                    {{--                                                  placeholder="Conte-nos sobre o seu projeto. *"--}}
                                    {{--                                                  required=""></textarea>--}}
                                </div>
                                <button type="submit" id="submit_btn" class="btn btn-color">Enviar Mensagem</button>
                            </div>
                            {!! Form::close() !!}
                            <div id="submit_output"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>
</div>
