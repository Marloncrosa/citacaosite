@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            <img src="{{ asset('/modules/admin/img/logos/logo.png') }}" alt="{{ env('APP_SITE_TITLE', 'Site Padrão') }}" data-auto-embed style="max-width: 570px; padding: 0 35px;">
        @endcomponent
    @endslot

    {{-- Body --}}
    {{ $slot }}

    {{-- Subcopy --}}
    @isset($subcopy)
        @slot('subcopy')
            @component('mail::subcopy')
                {{ $subcopy }}
            @endcomponent
        @endslot
    @endisset

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            &copy; {{ date('Y') }} {{ config('app.name') }}. {{__("Todos os direitos reservados")}}.
        @endcomponent
    @endslot
@endcomponent
