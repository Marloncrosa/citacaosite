@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'index', 'subtitle' => ''])

@section('content')
    <section data-inpage-section="true" id="home">
        <section class="page-section">
            <div id="homepage-content-slider" style="margin-bottom: 0">

                <!-- Não remover este item -->
                <div class="item">
                    <section data-inpage-section="true" id="home">
                        <section class="page-section fullscreen">

                            <div class="display-table width-100 height-100">
                                <div class="display-table-cell vertical-align-middle">
                                    <!-- HOME PAGE INTRO -->
                                    <div class="container relative">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2 relative text-center">
                                                <h2 class="heading-big fw700 text-color mb-0 relative text-center">
                                                    <img src="./images/logo.png" alt="" width="450"
                                                         style="display: inline-block;">
                                                </h2>
                                                <h3 class="heading-big-subhead fw400 pt-20 pb-0">
                                                    {!! __('Solução em Injeção a Diesel') !!}
                                                </h3>
                                            </div>
                                        </div>

                                        <!-- SCROLL BOTTOM -->
                                        <div class="scroll-bottom hidden-xs">
                                            <svg viewBox="0 0 512 512">
                                                <g>
                                                    <path
                                                        d="M256,1.6c-87.8,0-159,71.2-159,159v190.7c0,87.8,71.2,158.9,159,158.9c87.8,0,158.9-71.2,158.9-158.9V160.5C414.9,72.7,343.8,1.6,256,1.6z M383.2,351.2c0,70.1-57,127.2-127.1,127.2c-70.1,0-127.2-57-127.2-127.2V160.5c0-70.1,57-127.2,127.2-127.2c70.1,0,127.1,57,127.1,127.2V351.2z"></path>
                                                    <path
                                                        d="M256,128.7c-11.7,0-21.2,9.5-21.2,21.2v63.6c0,11.7,9.5,21.2,21.2,21.2c11.7,0,21.2-9.5,21.2-21.2v-63.6C277.2,138.2,267.7,128.7,256,128.7z"></path>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- BACKGROUND ANIMATION -->
                            <div class="bg-animation hidden-xs">
                                <div class="bg-animation-inner">
                                    <div class="bg-animation-svg">
                                        <svg class="icon-shape">
                                            <svg id="icon-shape" viewBox="0 0 504.2 492.2" width="100%" height="100%">
                                                <path stroke-miterlimit="10"
                                                      d="M252.1.6L471 123.3v245.5L252.1 491.6 33.2 368.8V123.3L252.1.6z"></path>
                                            </svg>
                                        </svg>
                                    </div>
                                    <div class="bg-animation-svg">
                                        <svg class="icon-shape">
                                            <svg id="icon-shape2" viewBox="0 0 504.2 492.2" width="100%" height="100%">
                                                <path stroke-miterlimit="10"
                                                      d="M252.1.6L471 123.3v245.5L252.1 491.6 33.2 368.8V123.3L252.1.6z"></path>
                                            </svg>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>
                </div>

                @if(!empty($banners) && $banners->count() > 0)
                    @foreach ($banners as $banner)
                        <div class="item">
                            <section class="page-section fullscreen">
                                <picture>
                                    @if((!empty($banner->image_mobile_cover) && file_exists(public_path('uploads/'.$banner->image_mobile_cover))))
                                        <source
                                            srcset="{{ route('thumbfit',[ 414, 896, $banner->image_mobile_cover, 'ffffff', true]) }}"
                                            media="(max-width: 768px)">
                                        <source
                                            srcset="{{ route('thumbfit',[ 414, 896, $banner->image_mobile_cover, 'ffffff', false]) }}"
                                            media="(max-width: 768px)"
                                            type="image/webp">
                                    @endif

                                    <source
                                        srcset="{{ route('thumb',[ 3840, 0, $banner->image_cover, 'ffffff', true]) }}"
                                        media="(min-width: 1928px)">
                                    <source
                                        srcset="{{ route('thumb',[ 3840, 0, $banner->image_cover, 'ffffff', false]) }}"
                                        media="(min-width: 1928px)"
                                        type="image/webp">

                                    <source
                                        srcset="{{ route('thumb',[ 1920, 0, $banner->image_cover, 'ffffff', false]) }}"
                                        media="(max-width: 1928px)"
                                        type="image/webp">

                                    <img class="img-responsive"
                                         src="{{ route('thumb', [ 1920, 0, $banner->image_cover, 'ffffff', true]) }}"
                                         alt="{{$banner->title}}">
                                </picture>
                                <div class="container height-parent">
                                    <div class="display-table width-100 height-100">
                                        <div class="display-table-cell vertical-align-middle">
                                            <div class="row">
                                                <div class="col-md-8 col-md-offset-2 relative text-center">
                                                    <h2 class="heading-big fw700 text-white mb-0 relative">
                                                        {!! $banner->title !!}
                                                    </h2>
                                                    <h3 class="heading-big-subhead text-gray fw400 pt-20 pb-0">
                                                        {!! $banner->subtitle !!}
                                                    </h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    @endforeach
                @endif
            </div>
        </section>
    </section>

    {{--    @if (!empty($portfolios) && $portfolios->count() > 0)--}}
    {{--        <section data-inpage-section="true" id="projects">--}}
    {{--            <section class="page-section work-section pb-140 pb-sm-100">--}}
    {{--                <div class="container">--}}
    {{--                    <ul id="work-grid" class="works-grid work-grid-3 work-grid-gut clearfix">--}}
    {{--                        @foreach ($portfolios as $portfolio)--}}
    {{--                            @php--}}
    {{--                                $categorias = $portifolio_categorias->whereIn('id', (object) $portfolio->portfolio_types);--}}
    {{--                            @endphp--}}
    {{--                            <li class="work-item mix">--}}
    {{--                                <a class="item-grid-style1" data-url="projects/project-1.html"--}}
    {{--                                   href="javascript:;">--}}
    {{--                                    <div class="work-img">--}}
    {{--                                        @if((!empty($portfolio->image_cover) && file_exists(public_path('uploads/'.$portfolio->image_cover))))--}}
    {{--                                            <img class="img-responsive"--}}
    {{--                                                 src="{{ route('thumbfit', [ 371, 278, $portfolio->image_cover, 'ffffff', true]) }}"--}}
    {{--                                                 alt="{{$portfolio->title}}">--}}
    {{--                                        @else--}}
    {{--                                            <img src="images/portfolio/project1.jpg" alt="Work" class="width-100">--}}
    {{--                                        @endif--}}
    {{--                                    </div>--}}
    {{--                                    <div class="grid-inner">--}}
    {{--                                        <div class="grid-inner-content">--}}
    {{--                                            <h3 class="project-name heading-medium fw700 text-white">{!! $portfolio->title !!}</h3>--}}
    {{--                                            @foreach($categorias as $categoria)--}}
    {{--                                                <p class="heading-small text-white mb-0 fw500">{!! $categoria->title !!}</p>--}}
    {{--                                            @endforeach--}}
    {{--                                        </div>--}}
    {{--                                    </div>--}}
    {{--                                </a>--}}
    {{--                            </li>--}}
    {{--                        @endforeach--}}
    {{--                    </ul>--}}
    {{--                </div>--}}
    {{--            </section>--}}
    {{--        </section>--}}
    {{--    @endif--}}

    @if (!empty($depoimentos) && $depoimentos->count() > 0)
        <section class="w-100 page-section bg-black testimonial overlay-dark-80"
                 data-background="images/home/img-3.png">
            <div class="container relative">
                <div class="hero-section overflow-auto testimonial-wrap pt-100 pb-100 pt-sm-70 pb-sm-70">
                    <div id="testimonial-slider">
                        @foreach ($depoimentos as $depoimento)
                            <div class="slick-slider">
                                <blockquote>
                                    <i>
                                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                             x="0px"
                                             y="0px" viewBox="0 0 32 25.236" enable-background="new 0 0 32 25.236"
                                             xml:space="preserve"><path
                                                d="M16.461,12.236C14.357,8.973,9.951,7.98,6.098,9.688L6.101,9.68c0,0,0.69-4.938,5.094-9.681H7.468c0,0-10.387,11.216-6.661,20.396v-0.001c0.18,0.505,0.417,0.994,0.717,1.461c2.357,3.66,7.612,4.475,11.736,1.816C17.387,21.018,18.818,15.897,16.461,12.236z"></path>
                                            <path
                                                d="M30.865,12.236C28.762,8.972,24.354,7.979,20.5,9.688l0.006-0.009c0,0,1.367-6.181,5.094-9.681h-4.178c0,0-3.666,3.921-5.722,8.855c1.196,0.614,2.233,1.514,2.991,2.688c2.252,3.497,1.308,8.225-2.012,11.267c2.598,2.821,7.252,3.271,10.985,0.863C31.791,21.018,33.223,15.897,30.865,12.236z"></path></svg>
                                    </i>
                                    {!! $depoimento->description !!}

                                    <figure class="clearfix mt-30 mt-sm-20">
                                        <div class="review-wrap">
                                            <div class="leftreview">
                                                @if((!empty($depoimento->image_cover) && file_exists(public_path('uploads/'.$depoimento->image_cover))))
                                                    <picture>
                                                        @if((!empty($depoimento->image_mobile_cover) && file_exists(public_path('uploads/'.$depoimento->image_mobile_cover))))
                                                            <source
                                                                srcset="{{ route('thumbfit',[ 120, 120, $depoimento->image_mobile_cover, 'ffffff', true]) }}"
                                                                media="(max-width: 768px)">
                                                            <source
                                                                srcset="{{ route('thumbfit',[ 120, 120, $depoimento->image_mobile_cover, 'ffffff', false]) }}"
                                                                media="(max-width: 768px)"
                                                                type="image/webp">
                                                        @endif
                                                        <source
                                                            srcset="{{ route('thumbfit',[ 120, 120, $depoimento->image_cover, 'ffffff', false]) }}"
                                                            media="(min-width: 769px)"
                                                            type="image/webp">

                                                        <img class="img-responsive"
                                                             src="{{ route('thumb', [ 120, 120, $depoimento->image_cover, 'ffffff', true]) }}">
                                                    </picture>
                                                @else
                                                    <img src="images/faces/face-img1.jpg" alt=""/>
                                                @endif
                                            </div>
                                            <div class="rightreview">
                                                <div class="review_name">{!! $depoimento->name !!}</div>
                                                <div class="review_detail">
                                                    {!! $depoimento->function !!}
                                                </div>
                                            </div>
                                        </div>
                                    </figure>
                                </blockquote>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif

    <section class="w-100 page-section brands pt-70 pb-70">
        @if (!empty($logos && $logos->count() > 0))
            <div class="container-fluid">
                <div id="brands-slider">
                    @foreach ($logos as $logo)
                        <div class="item">
                            @if((!empty($logo->image_cover) && file_exists(public_path('uploads/'.$logo->image_cover))))
                                <picture>
                                    @if((!empty($logo->image_mobile_cover) && file_exists(public_path('uploads/'.$logo->image_mobile_cover))))
                                        <source
                                            srcset="{{ route('thumbfit',[ 220, 120, $logo->image_mobile_cover, 'ffffff', true]) }}"
                                            media="(max-width: 768px)">
                                        <source
                                            srcset="{{ route('thumbfit',[ 220, 120, $logo->image_mobile_cover, 'ffffff', false]) }}"
                                            media="(max-width: 768px)"
                                            type="image/webp">
                                    @endif
                                    <source
                                        srcset="{{ route('thumbfit',[ 220, 120, $logo->image_cover, 'ffffff', false]) }}"
                                        media="(min-width: 769px)"
                                        type="image/webp">

                                    <img class="img-responsive"
                                         src="{{ route('thumb', [ 220, 120, $logo->image_cover, 'ffffff', true]) }}"
                                         alt="{{$logo->title}}">
                                </picture>
                            @else
                                <img src="images/clients/client-logo-2.png" alt="c1" title="clients"/>
                            @endif
                        </div>

                    @endforeach
                </div>
            </div>
        @endif
    </section>

    <section data-inpage-section="true" id="about">

    @if (!empty($institucionais && $institucionais->count() > 0))
        <!-- Intitucional -->
        {{--            <section class="page-section bg-black pt-140 pb-140 pt-sm-100 pb-sm-100">--}}
        {{--                <div class="container">--}}
        {{--                    <div class="row">--}}
        {{--                        <div class="col-sm-12">--}}
        {{--                            <div class="row">--}}
        {{--                                <div class="col-lg-6 col-md-8 col-xs-11 ">--}}
        {{--                                    <h2 class="heading-big fw700 text-color mb-20">--}}
        {{--                                        {!! $institucionais->title !!}--}}
        {{--                                    </h2>--}}
        {{--                                </div>--}}
        {{--                            </div>--}}
        {{--                            <div class="row ">--}}
        {{--                                <div class="col-md-6 col-sm-6 col-xs-12">--}}
        {{--                                    {!! $institucionais->description1 !!}--}}

        {{--                                </div>--}}
        {{--                            </div>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </section>--}}

        {{--            <!-- Galeria do Institucional -->--}}
        {{--            <section class="page-section bg-black">--}}
        {{--                <div class="container">--}}
        {{--                    --}}
        {{--                    <!-- GALLERY SLIDER -->--}}
        {{--                    <div id="about-gallery">--}}
        {{--                        @foreach ($institucionais as $institucional)--}}
        {{--                            <div class="item">--}}
        {{--                                <a href="{{ route('thumbfit', [ 1800, 1200, $institucional->image_cover, 'ffffff', true]) }}"--}}
        {{--                                   title="{!! $institucional->title !!}."--}}
        {{--                                   class="lightbox-gallery mfp-image">--}}
        {{--                                    @if((!empty($institucional->image_cover) && file_exists(public_path('uploads/'.$institucional->image_cover))))--}}
        {{--                                        <img class="img-responsive"--}}
        {{--                                             src="{{ route('thumbfit', [ 570, 380, $institucional->image_cover, 'ffffff', true]) }}"--}}
        {{--                                             alt="{{$institucional->title}}">--}}
        {{--                                    @else--}}
        {{--                                        <img src="images/gallery/image_2.jpg" alt=""/>--}}
        {{--                                    @endif--}}
        {{--                                </a>--}}
        {{--                            </div>--}}
        {{--                        @endforeach--}}
        {{--                        <div class="item">--}}
        {{--                            <a href="images/gallery/image_3.jpg" title="Nulla for arcu lacinia."--}}
        {{--                               class="lightbox-gallery mfp-image">--}}
        {{--                                <img src="images/gallery/image_3.jpg" alt=""/>--}}
        {{--                            </a>--}}
        {{--                        </div>--}}
        {{--                        <div class="item">--}}
        {{--                            <a href="images/gallery/image_5.jpg" title="Prcu nulla for arcu lacinia."--}}
        {{--                               class="lightbox-gallery mfp-image">--}}
        {{--                                <img src="images/gallery/image_5.jpg" alt=""/>--}}
        {{--                            </a>--}}
        {{--                        </div>--}}
        {{--                        <div class="item">--}}
        {{--                            <a href="images/gallery/image_6.jpg" title="Praesent dapibus purus erat at vitae arcu."--}}
        {{--                               class="lightbox-gallery mfp-image">--}}
        {{--                                <img src="images/gallery/image_6.jpg" alt=""/>--}}
        {{--                            </a>--}}
        {{--                        </div>--}}
        {{--                        <div class="item">--}}
        {{--                            <a href="images/gallery/image_2.jpg"--}}
        {{--                               title="Purus erat at vitae arcu nulla for arcu lacinia."--}}
        {{--                               class="lightbox-gallery mfp-image">--}}
        {{--                                <img src="images/gallery/image_2.jpg" alt=""/>--}}
        {{--                            </a>--}}
        {{--                        </div>--}}
        {{--                        <div class="item">--}}
        {{--                            <a href="images/gallery/image_3.jpg" title="Praesent dapibus purus erat lacinia."--}}
        {{--                               class="lightbox-gallery mfp-image">--}}
        {{--                                <img src="images/gallery/image_3.jpg" alt=""/>--}}
        {{--                            </a>--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </section>--}}
    @endif

    <!-- Deixar temporariamente -->
        <section class="page-section bg-black pt-70 pb-70 ">
            <div class="container relative ">
                <div class="row equalize sm-equalize-auto alt-number-box">
                    {{--                    --}}
                    {{--                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mt-70 last-para-no-margin">--}}
                    {{--                        <div class="feature-box-2 ">--}}
                    {{--                            <div class="feature-content">--}}
                    {{--                                <h3 class="heading-medium text-gray mt-0 mb-10 fw700">--}}
                    {{--                                    Launched--}}
                    {{--                                </h3>--}}
                    {{--                                <div class="heading-hero mb-10 text-color fw700">2008</div>--}}
                    {{--                                <p class="heading-small">--}}
                    {{--                                    Praesent dapibus purus erat at diam purusnec vitae arcu nulla--}}
                    {{--                                    for arcu lacinia.--}}
                    {{--                                </p>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    --}}
                    {{--                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mt-70 last-para-no-margin">--}}
                    {{--                        <div class="feature-box-2 ">--}}
                    {{--                            <div class="feature-content">--}}
                    {{--                                <h3 class="heading-medium text-gray mt-0 mb-10 fw700">--}}
                    {{--                                    Headcount--}}
                    {{--                                </h3>--}}
                    {{--                                <div class="heading-hero mb-10 text-color fw700">150+</div>--}}
                    {{--                                <p class="heading-small">--}}
                    {{--                                    Praesent dapibus purus erat at diam purusnec vitae arcu nulla--}}
                    {{--                                    for arcu lacinia.--}}
                    {{--                                </p>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    --}}
                    {{--                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mt-70 last-para-no-margin">--}}
                    {{--                        <div class="feature-box-2 ">--}}
                    {{--                            <div class="feature-content">--}}
                    {{--                                <h3 class="heading-medium text-gray mt-0 mb-10 fw700">--}}
                    {{--                                    HQ--}}
                    {{--                                </h3>--}}
                    {{--                                <div class="heading-hero mb-10 text-color fw700">Europe</div>--}}
                    {{--                                <p class="heading-small">--}}
                    {{--                                    Praesent dapibus purus erat at diam purusnec vitae arcu nulla--}}
                    {{--                                    for arcu lacinia.--}}
                    {{--                                </p>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                    --}}
                    {{--                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 mt-70 last-para-no-margin">--}}
                    {{--                        <div class="feature-box-2">--}}
                    {{--                            <div class="feature-content">--}}
                    {{--                                <h3 class="heading-medium text-gray mt-0 mb-10 fw700">--}}
                    {{--                                    Clients--}}
                    {{--                                </h3>--}}
                    {{--                                <div class="heading-hero mb-10 text-color fw700">Global</div>--}}
                    {{--                                <p class="heading-small">--}}
                    {{--                                    Praesent dapibus purus erat at diam purusnec vitae arcu nulla--}}
                    {{--                                    for arcu lacinia.--}}
                    {{--                                </p>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}

                </div>
            </div>
        </section>

        <!-- Produtos -->
        @if (!empty($produtos) && $produtos->count() > 0)
            <section class="page-section bg-black pt-140 pb-140 pt-sm-100 pb-sm-100">
                <div class="container">
                    <div class="row">
                        <!-- CONTENT SLIDER -->
                        <div id="slick-slider4">

                            <!-- Slide 1 -->
                            @foreach ($produtos as $produto)
                                <div class="item equalize sm-equalize-auto">
                                    <div class="col-md-6 ">
                                        <div class="product-photo">
                                            <div class="product-photo-wrap">
                                                @if((!empty($produto->image_cover) && file_exists(public_path('uploads/'.$produto->image_cover))))
                                                    <picture>
                                                        @if((!empty($produto->image_mobile_cover) && file_exists(public_path('uploads/'.$produto->image_mobile_cover))))
                                                            <source
                                                                srcset="{{ route('thumb',[ 600, 905, $produto->image_mobile_cover, 'ffffff', true]) }}"
                                                                media="(max-width: 768px)">
                                                            <source
                                                                srcset="{{ route('thumb',[ 600, 905, $produto->image_mobile_cover, 'ffffff', false]) }}"
                                                                media="(max-width: 768px)"
                                                                type="image/webp">
                                                        @endif
                                                        <source
                                                            srcset="{{ route('thumb',[ 600, 905, $produto->image_cover, 'ffffff', false]) }}"
                                                            media="(max-width: 769px)"
                                                            type="image/webp">

                                                        <img class="img-responsive"
                                                             src="{{ route('thumb', [ 600, 905, $produto->image_cover, 'ffffff', true]) }}"
                                                             alt="{{$produto->title}}">
                                                    </picture>
                                                @else
                                                    <img class="width-100" src="images/gallery/img_update.png" alt="">
                                                @endif
                                                <div class="product-code">Código do
                                                    Produto: {!! $produto->code !!}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 display-table">
                                        <div class="display-table-cell vertical-align-middle">
                                            <h4 class="heading-hero text-white fw700 mt-0 mb-30">
                                                {!! $produto->title !!}
                                            </h4>
                                            <p>{!! $produto->sinopsys !!}</p>

                                            <a href="{!! route('site.produto', [ 'slug' => $produto->slug ]) !!}"
                                               style="padding-bottom: 50px;">Saiba mais...
                                            </a>
                                            <div class="content-slider-nav">
                                                <!-- Previous/Next controls -->
                                                <a class="content-slider-left">
                                                </a>
                                                <a class="content-slider-right">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
        @endif
    </section>

    <section data-inpage-section="true" id="pricing">

        <!-- Lista de serviços -->
        <section class="page-section service-section bg-light-gray pb-140 pb-sm-100">
            <div class="container">

                <!-- PRICING HERO -->
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 text-center">
                        <div class="hero-section pt-70">
                            <h6 class="heading-big-subhead text-black fw700 mb-20 mb-sm-20">
                                Reconhecido por empresas nacionalmente e internacionalmente
                            </h6>
                            <p>
                                Ao oferecer os melhores produtos e serviços para as mais diversas empresas,
                                ganhamos notoriedade e reconhecimento nacionalmente e internacionalmente.
                            </p>
                            <a href="javascript:;" class="btn btn-color">
                                Vamos começar juntos
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <section data-inpage-section="true" id="thoughts">

        <!-- Nossas Idéias & perspectivas -->
        <section class="page-section pt-140 pb-140 pt-sm-100 pb-sm-100">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-xs-8">
                        <h2 class="heading-big fw700 text-color mb-30 mb-sm-20">
                            Nossas Ideias &
                            <span class="newline">perspectivas</span>
                        </h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h3 class="heading-big-subhead fw400 pt-20">
                            Abaixo você irá ler notícias passadas, atuais e futuras, mostrando nossas ideias e <br>
                            perspectivas atuais e sobre o futuro.
                        </h3>
                    </div>
                </div>
            </div>
        </section>

        @if(!empty($blog) && $blog->count() > 0)
            <section class="page-section blog-section pb-140 pb-md-100 pb-sm-70">
                <div class="container">
                    <div class="row equalize sm-equalize-auto alt-blog-box">
                        @foreach ($blog as $post)
                            <div class="col-lg-4 col-md-4 col-sm-6 mt-70">
                                <a href="{{route("site.post",[ 'slug' => $post->slug ])}}" data-link-outer="true"
                                   class="blog-item">
                                    <div class="blog-item-thumbnail mb-40">
                                        @if((!empty($post->image_cover) && file_exists(public_path('uploads/'.$post->image_cover))))
                                            <picture>
                                                @if((!empty($post->image_mobile_cover) && file_exists(public_path('uploads/'.$post->image_mobile_cover))))
                                                    <source
                                                        srcset="{{ route('thumb',[ 371, 278, $post->image_mobile_cover, 'ffffff', true]) }}"
                                                        media="(max-width: 768px)">
                                                    <source
                                                        srcset="{{ route('thumb',[ 371, 278, $post->image_mobile_cover, 'ffffff', false]) }}"
                                                        media="(max-width: 768px)"
                                                        type="image/webp">
                                                @endif
                                                <source
                                                    srcset="{{ route('thumb',[ 371, 278, $post->image_cover, 'ffffff', false]) }}"
                                                    media="(max-width: 769px)"
                                                    type="image/webp">

                                                <img class="img-responsive"
                                                     src="{{ route('thumb', [ 371, 278, $post->image_cover, 'ffffff', true]) }}"
                                                     alt="{{$post->title}}">
                                            </picture>
                                        @else
                                            <img src="images/blog/blog1.jpg" alt=""/>
                                        @endif
                                    </div>
                                    <div class="blog-title mb-10 pr-30 pr-sm-0">
                                        <h3 class="heading-big-subhead fw700">{!! $post->title !!}</h3>
                                    </div>
                                    <div class="blog-title-info">
                                        <div class="fw400 heading-small ">
                                            <span class="fw500">{!! $post->date_time !!}</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach

                        <div class="row mt-70">
                            <div class="col-sm-8 col-sm-offset-2  text-center">
                                <div class="hero-section">
                                    <a href="{!! route('site.blog') !!}" data-link-outer="true"
                                       class="btn btn-border-color">
                                        Veja mais notícias
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </section>

            <!-- SUBSCRIBE SECTION-->
            <section class="page-section overflow-auto pb-140 pb-sm-100">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            <div
                                class="relative bg-light-gray alt-subscribe-box pt-70 pb-70 pl-50 pr-50 pl-sm-30 pr-sm-30">
                                <div class="newsletter-info">
                                    <h6 class="heading-hero mt-0 mb-0 fw700 text-color">
                                        Inscreva-se
                                    </h6>
                                    <p>
                                        Novidades incríveis, direto no seu e-mail!
                                    </p>
                                </div>
                                <div class="newsletter-block">
                                    <form id="mailchimp" action="{!! route('site.newslleter') !!}"
                                          method="post">
                                        <input type="email" name="email" placeholder="SEU MELHOR E-MAIL"
                                               class="newsletter-input">
                                        <button type="submit" class="newsletter-submit"></button>
                                    </form>
                                </div>
                                <div id="subscribe-result"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
    </section>
@stop
