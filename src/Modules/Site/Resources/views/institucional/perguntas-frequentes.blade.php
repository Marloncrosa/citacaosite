@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'index', 'subtitle' => ''])

@section('content')
    <main id="duvidas-frequentes">
        <section class="carrinho">
            <div class="row">
                <div class="columns small-12 medium-12 large-12">
                    <h1>Dúvidas Frequentes</h1>
                    
                    @if($faq->count() > 0)
                        <ul class="accordion list-duvidas" data-accordion>
                            @foreach ($faq as $item)
                                <li class="accordion-item" data-accordion-item>
                                    <a href="javascript:;" class="accordion-title">
                                        {!! $item->title !!}
                                    </a>
                                    <div class="accordion-content" data-tab-content>
                                        {!! $item->description !!}
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <div class="columns small-12 medium-12 large-12">
                            <h2>Nenhum resultado encontrado</h2>
                        </div>
                    @endif
                
                </div>
            </div>
        </section>
    </main>
@stop
