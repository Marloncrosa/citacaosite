@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'institucional', 'subtitle' => ' - institucional'])

@section('content')
    <!-- SECTION HOME -->
    <section data-inpage-section="true" id="home">
{{--        <section class="page-section">--}}
{{--            <div id="homepage-content-slider">--}}

{{--                <!-- Não remover este item -->--}}
{{--                <div class="item">--}}
{{--                    <section data-inpage-section="true" id="home">--}}
{{--                        <section class="page-section fullscreen">--}}

{{--                            <div class="display-table width-100 height-100">--}}
{{--                                <div class="display-table-cell vertical-align-middle">--}}
{{--                                    <!-- HOME PAGE INTRO -->--}}
{{--                                    <div class="container relative">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-md-8 col-md-offset-2 relative text-center">--}}
{{--                                                <h2 class="heading-big fw700 text-color mb-0 relative text-center">--}}
{{--                                                    <img src="./images/logo.png" alt="" width="450"--}}
{{--                                                         style="display: inline-block;">--}}
{{--                                                </h2>--}}
{{--                                                <h3 class="heading-big-subhead fw400 pt-20 pb-0">--}}
{{--                                                    {!! __('Solução em Injeção a Diesel') !!}--}}
{{--                                                </h3>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}

{{--                                        <!-- SCROLL BOTTOM -->--}}
{{--                                        <div class="scroll-bottom hidden-xs">--}}
{{--                                            <svg viewBox="0 0 512 512">--}}
{{--                                                <g>--}}
{{--                                                    <path--}}
{{--                                                        d="M256,1.6c-87.8,0-159,71.2-159,159v190.7c0,87.8,71.2,158.9,159,158.9c87.8,0,158.9-71.2,158.9-158.9V160.5C414.9,72.7,343.8,1.6,256,1.6z M383.2,351.2c0,70.1-57,127.2-127.1,127.2c-70.1,0-127.2-57-127.2-127.2V160.5c0-70.1,57-127.2,127.2-127.2c70.1,0,127.1,57,127.1,127.2V351.2z"></path>--}}
{{--                                                    <path--}}
{{--                                                        d="M256,128.7c-11.7,0-21.2,9.5-21.2,21.2v63.6c0,11.7,9.5,21.2,21.2,21.2c11.7,0,21.2-9.5,21.2-21.2v-63.6C277.2,138.2,267.7,128.7,256,128.7z"></path>--}}
{{--                                                </g>--}}
{{--                                            </svg>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <!-- BACKGROUND ANIMATION -->--}}
{{--                            <div class="bg-animation hidden-xs">--}}
{{--                                <div class="bg-animation-inner">--}}
{{--                                    <div class="bg-animation-svg">--}}
{{--                                        <svg class="icon-shape">--}}
{{--                                            <svg id="icon-shape" viewBox="0 0 504.2 492.2" width="100%" height="100%">--}}
{{--                                                <path stroke-miterlimit="10"--}}
{{--                                                      d="M252.1.6L471 123.3v245.5L252.1 491.6 33.2 368.8V123.3L252.1.6z"></path>--}}
{{--                                            </svg>--}}
{{--                                        </svg>--}}
{{--                                    </div>--}}
{{--                                    <div class="bg-animation-svg">--}}
{{--                                        <svg class="icon-shape">--}}
{{--                                            <svg id="icon-shape2" viewBox="0 0 504.2 492.2" width="100%" height="100%">--}}
{{--                                                <path stroke-miterlimit="10"--}}
{{--                                                      d="M252.1.6L471 123.3v245.5L252.1 491.6 33.2 368.8V123.3L252.1.6z"></path>--}}
{{--                                            </svg>--}}
{{--                                        </svg>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                        </section>--}}
{{--                    </section>--}}
{{--                </div>--}}

{{--                @if(!empty($institucional->image_cover) )--}}
{{--                    <div class="item">--}}
{{--                        <section class="page-section fullscreen"--}}
{{--                                 data-background="{{ route('thumbfit', [ 1920, 1080, $institucional->image_cover, 'ffffff', true]) }}"--}}
{{--                                 alt="{!! $institucional->title !!}">--}}
{{--                            <div class="container height-parent">--}}
{{--                                <div class="display-table width-100 height-100">--}}
{{--                                    <div class="display-table-cell vertical-align-middle">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-md-8 col-md-offset-2 relative text-center">--}}
{{--                                                <h2 class="heading-big fw700 text-white mb-0 relative">--}}
{{--                                                    {!! $institucional->title !!}--}}
{{--                                                </h2>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </section>--}}
{{--                    </div>--}}
{{--                @endif--}}
{{--            </div>--}}
{{--        </section>--}}

        <!-- BLOG LIST -->
        <div class="container">
            <div class="project-detail-body mb-100 clearfix">
                <div class="col-md-12">
                    <h4 class="heading-big fw700 text-color mb-30 mb-sm-20">
                        {!! $institucional->title !!}
                    </h4>
                </div>
            </div>
        </div>

        <section id="blogs" class="page-section blog-section pb-140 pb-md-100 pb-sm-70">
            @if(!empty($institucional_menu))
                <div class="col-md-3">
                    <div class="display-block relative">
                        <ul class="project-detail-content-list">
                            @foreach($institucional_menu as $item)
                                <li>
                                    <a href="{!! route('site.institucional', [ 'slug' => str_slug($item->title) ]) !!}">
                                        <strong>{!! $item->title !!}</strong>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
        @endif
        <!-- blog item 3 column grid -->
            <div class="col-md-8 ">

                {!! $institucional->description1 !!}

            </div>
        </section>
    </section>
@stop
