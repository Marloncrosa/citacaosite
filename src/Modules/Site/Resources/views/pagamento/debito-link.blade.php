@extends('site::layouts.master')

@section('content')
    <main id="checkout">
        <section class="carrinho">
            <div class="row">
                
                <div class="columns small-12 medium-7 large-8">
                    <h1>Selecione o método de pagamento</h1>
                    
                    <ul class="modo-pagamento">
                        <li class="card">
                            <a href="{!! route('site-pagamento-cartao') !!}">
                                <i class="icon-card"></i>
                                CARTÃO DE CRÉDITO
                            </a>
                        </li>
                        
                        <li class="card">
                            <a href="{!! route('site-pagamento-debito') !!}">
                                <i class="icon-card"></i>
                                CARTÃO DE DÉBITO
                            </a>
                        </li>
                        
                        <li class="boleto">
                            <a href="{!! route('site-pagamento-boleto') !!}">
                                <i class="icon-barcode"></i>
                                BOLETO BANCÁRIO
                            </a>
                        </li>
                    </ul>
                    
                    <div class="box-boleto">
                        @if(!empty($link))
                            <a href="{!! $link !!}" class="button full" data-fancybox data-type="iframe">
                                Abrir tela de pagamento
                            </a>
                        @else
                            <strong>Houve problemas para gerar o link</strong>
                        @endif
                    </div>
                </div>
                
                @includeIf('site::pagamento.resumo')
            
            </div>
        </section>
    </main>
@stop
