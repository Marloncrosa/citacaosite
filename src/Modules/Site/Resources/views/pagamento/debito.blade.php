@extends('site::layouts.master')

@section('content')
    <main id="checkout">
        <section class="carrinho">
            <div class="row">

                <div class="columns small-12 medium-7 large-8">
                    <h1>Selecione o método de pagamento</h1>

                    <ul class="modo-pagamento">
                        <li class="card">
                            <a href="{!! route('site-pagamento-cartao') !!}">
                                <i class="icon-card"></i>
                                CARTÃO DE CRÉDITO
                            </a>
                        </li>

                        <li class="card">
                            <a href="{!! route('site-pagamento-debito') !!}">
                                <i class="icon-card"></i>
                                CARTÃO DE DÉBITO
                            </a>
                        </li>

                        <li class="boleto">
                            <a href="{!! route('site-pagamento-boleto') !!}">
                                <i class="icon-barcode"></i>
                                BOLETO BANCÁRIO
                            </a>
                        </li>
                    </ul>

                    {{ Form::open([ 'route' => 'site-pagamento-debito-post', 'class' => 'form-geral form-cartao', 'method' => 'post', 'data-abide novalidate']) }}

                        {!! Form::hidden('ip', request()->ip(), ['id' => 'ip']) !!}
                        {!! Form::hidden('bandeira', null, ['id' => 'bandeira']) !!}
                        {!! Form::hidden('senderHash', null, ['id' => 'senderHash']) !!}

                    <div class="row">

                        <div class="columns small-12 medium-12 large-12 columns">
                            <label for="bankName">Em qual banco deseja pagar?</label>
                            <select name="bankName" id="bankName" required>
                                <option value="bradesco">Bradesco</option>
                                <option value="itau">Itaú</option>
                                <option value="bancodobrasil">Banco do Brasil</option>
                                <option value="banrisul">Banrisul</option>
                                <option value="hsbc">HSBC</option>
                            </select>
                        </div>

                    </div>


                    @include('site::pagamento.user-data')

                    <div class="row">
                        <div class="columns small-12 medium-12 large-12">
                            <button type="submit" name="pagar" class="button full">
                                Realizar Pagamento
                                <i class="icon-next-1"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

                @includeIf('site::pagamento.resumo')

            </div>
        </section>

        <div class="reveal" id="app-card" data-reveal>
            <img src="images/cards/creditCard.jpg" alt="Apresentação de dados do Cartão">
            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </main>
@stop
