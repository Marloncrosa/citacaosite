@extends('site::layouts.master')

@section('content')
    <main id="checkout">
        <section class="carrinho">
            @php
                // dump('Juros'.$maximaParcelaComJuros);
                // dump('Sem Juros'.$maximaParcelaSemJuros);
            @endphp
            <div class="row">

                <div class="columns small-12 medium-7 large-8">
                    <h1>Selecione o método de pagamento</h1>

                    <ul class="modo-pagamento">
                        <li class="card">
                            <a href="{!! route('site-pagamento-cartao') !!}">
                                <i class="icon-card"></i>
                                CARTÃO DE CRÉDITO
                            </a>
                        </li>

                        <li class="card">
                            <a href="{!! route('site-pagamento-debito') !!}">
                                <i class="icon-card"></i>
                                CARTÃO DE DÉBITO
                            </a>
                        </li>

                        <li class="boleto">
                            <a href="{!! route('site-pagamento-boleto') !!}">
                                <i class="icon-barcode"></i>
                                BOLETO BANCÁRIO
                            </a>
                        </li>
                    </ul>

                    {{ Form::open([ 'route' => 'site-pagamento-cartao-post', 'class' => 'form-geral form-cartao', 'method' => 'post', 'data-abide novalidate']) }}

                    {!! Form::hidden('ip', request()->ip(), ['id' => 'ip']) !!}
                    {!! Form::hidden('bandeira', null, ['id' => 'bandeira']) !!}
                    {!! Form::hidden('cardToken', null, ['id' => 'cardToken']) !!}
                    {!! Form::hidden('senderHash', null, ['id' => 'senderHash']) !!}
                    {!! Form::hidden('installmentQuantity', null, ['id' => 'installmentQuantity']) !!}
                    {!! Form::hidden('installmentValue', null, ['id' => 'installmentValue']) !!}

                    <div class="row">

                        <div class="columns small-12 medium-12 large-12 number">
                            {!! Form::label('phone', 'Telefone do dono do Cartão', [ 'class' => '' ]) !!}
                            {!! Form::text('phone', (!empty(auth()->user()->cellphone))? auth()->user()->cellphone : null , ['class' => 'form-control mascara-telefone', 'required']) !!}
                        </div>

                        <div class="columns small-12 medium-12 large-12 number">
                            {!! Form::label('cpf', 'CPF do dono do Cartão', [ 'class' => '' ]) !!}
                            {!! Form::text('cpf', (!empty(auth()->user()->document))? auth()->user()->document : null, ['class' => 'form-control', 'required']) !!}
                        </div>

                        <div class="columns small-12 medium-12 large-12 number">
                            {!! Form::label('data_nascimento', 'Nascimento do Dono do cartão', [], false) !!}
                            {!! Form::text('data_nascimento', (!empty(auth()->user()->birthdate))? auth()->user()->birthdate : null, ['class' => 'mascara-data', 'required']) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="columns small-12 medium-12 large-12 number">
                            {!! Form::label('number', 'Número do Cartão <span data-open="app-card">(?)</span>', [], false) !!}
                            {!! Form::text('number', null, ['required']) !!}
                            <span class="bandeira"></span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="columns small-12 medium-12 large-12">
                            {!! Form::label('nome', 'Nome como esta no cartão  <span data-open="app-card">(?)</span>', [], false) !!}
                            {!! Form::text('nome', null, ['required']) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="columns small-12 medium-6 large-6 vencimento">
                            {!! Form::label('date', 'Vencimento  <span data-open="app-card">(?)</span>', [], false) !!}
                            {!! Form::text('date', null, [ 'placeholer' => 'MM/AA', 'required' ]) !!}
                        </div>

                        <div class="columns small-12 medium-6 large-6 cvv">
                            {!! Form::label('cvv', 'CVV  <span data-open="app-card">(?)</span>', [], false) !!}
                            {!! Form::text('cvv', null, ['required']) !!}
                        </div>
                    </div>

                    <div class="row">

                        <div class="columns small-12 medium-12 large-12 columns">
                            <label for="installments">Em quantas parcelas deseja pagar?</label>
                            <select name="installments" id="installments" required>
                                <option value="">Primeiro informe os dados do cartão</option>
                            </select>
                        </div>

                    </div>

                    @include('site::pagamento.user-data')

                    <div class="row">
                        <div class="columns small-12 medium-12 large-12">
                            <button type="submit" name="pagar" class="button full">
                                Realizar Pagamento
                                <i class="icon-next-1"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>

                @includeIf('site::pagamento.resumo')

            </div>
        </section>

        <div class="reveal" id="app-card" data-reveal>
            <img src="images/cards/creditCard.jpg" alt="Apresentação de dados do Cartão">
            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </main>
@stop


@push('line-js')
    <script type="text/javascript" src="{{ PagSeguro::getUrl()['javascript'] }}"></script>
    <script>
        PagSeguroDirectPayment.setSessionId('{{ PagSeguro::startSession() }}');
        //PagSeguroRecorrente tem um método identico, use o que preferir neste caso, não tem diferença.
    </script>
    <script>
        $(function () {
            window.brand = '';
            window.isntallments = '';
            cartaoBandeira = setTimeout(function () {
            }, 100);

            $("#number").inputmask("9999 9999 [9999 9999]", {'autoUnmask': true});
            $("#cvv").inputmask("999[9]");
            $("#data").inputmask("99/99/9999");
            $("#date").inputmask("99/99");
            $("#cpf").inputmask("999.999.999-99");

            $('#pagamento input:not([type="submit"]):not([type="hidden"])').trigger('focusout');

            $('.form-cartao').off('submit').on('submit', function () {
                return true;
            });

            $('.form-cartao .button').on('click', function () {

                data = ($('input#date').val()).split('/');
                if (data[1] < 100) {
                    year = '20' + data[1];
                } else {
                    year = '2' + data[1];
                }

                if (
                    brand != ''
                    && $('input#date').val() != ''
                ) {
                    window.param = {
                        cardNumber: $("input#number").val(),
                        brand: brand.name,
                        cvv: $("input#cvv").val(),
                        expirationMonth: data[0],
                        expirationYear: year,
                        success: function (response) {
                            $('#cardToken').val(response.card.token);
                            $('#senderHash').val(PagSeguroDirectPayment.getSenderHash());
                            $('.form-cartao').trigger('submit');
                        },
                        error: function (response) {
                            // console.log('error');
                            // console.log(response);
                            var array = new Array();
                            key = 0;
                            $.each(response.errors, function (e, i) {
                                switch (e) {
                                    case '30405':
                                        message = 'Data inválida';
                                        break;
                                    case '10001':
                                        message = 'Cartão de crédito inválido';
                                        break;
                                    default:
                                        message = 'Cartão de crédito inválido';
                                }
                                array[key] = message;
                                key++;
                            });
                            console.log(array);

                            var juntar = array.join(', ');
                            swal(juntar, '', 'error');
                        }
                    };
                    cardToken = PagSeguroDirectPayment.createCardToken(param);
                }
                return false;
            });

            // $("#number").on('keydown', function () {
            //     clearTimeout(cartaoBandeira);
            // });

            $("#number").on('keydown', function () {
                value = (this.value).replace(/ +/g, '');
                clearTimeout(cartaoBandeira);
                cartaoBandeira = setTimeout(function () {
                    PagSeguroDirectPayment.getBrand({
                        cardBin: value,
                        success: function (response) {
                            brand = response.brand;
                            $('[name="bandeira"]').val(brand.name);
                            $('.bandeira').html('<img src="//stc.pagseguro.uol.com.br/public/img/payment-methods-flags/42x20/' + brand.name + '.png">');

                            PagSeguroDirectPayment.getInstallments({
                                amount: floatN('{!! $total !!}'),
                                maxInstallmentNoInterest: {!! $maximaParcelaSemJuros !!},
                                brand: brand.name,
                                success: function (response) {
                                    installments = response.installments[brand.name];
                                    $('[name="installments"]').html('');
                                    $.each(installments, function (key, value) {
                                        if (key == 0) {
                                            $('[name="installments"]').append('<option value="1">1 parcela de R$ ' + moeda(value.installmentAmount) + ' á vista</option>');
                                        } else {
                                            if(key <= {!! ($maximaParcelaComJuros > $maximaParcelaSemJuros)? ($maximaParcelaComJuros - 1) : ($maximaParcelaSemJuros - 1) !!}){
                                                if (value.interestFree) {
                                                    $('[name="installments"]').append('<option value="' + value.quantity + '">' + value.quantity + ' parcela de R$ ' + moeda(value.installmentAmount) + ' sem juros</option>');
                                                } else {
                                                    $('[name="installments"]').append('<option value="' + value.quantity + '">' + value.quantity + ' parcela de R$ ' + moeda(value.installmentAmount) + '</option>');
                                                }
                                            }
                                        }
                                    });
                                    $('[name="installments"]').trigger('change');
                                },
                                error: function (response) {
                                    // callback para chamadas que falharam.
                                },
                                complete: function (response) {
                                    // Callback para todas chamadas.
                                }
                            });
                        },
                        error: function (response) {
                            // $("input#number").val(' ');
                            // $("input#cvv").val(' ');
                            // $("input#date").val(' ');
                        }
                    });
                }, 1500);
            })
                .trigger('keydown');

            $('#pagamento input:not([type="submit"]):not([type="hidden"])').each(function () {
                if ($(this).val() != "") {
                    $(this).prev().addClass('active');
                }
            });

            $('[name="installments"]').on('change', function () {
                qtde = this.value - 1;
                $('[name="installmentQuantity"]').val(installments[qtde].quantity);
                $('[name="installmentValue"]').val(installments[qtde].installmentAmount);
            });

            // $("#cvv").on('keyup', function () {
            //     value = (this.value).replace(/ +/g, '');
            //     clearTimeout(cartaoBandeira);
            //     cartaoBandeira = setTimeout(function () {
            //     }, 1500);
            // });
        })


    </script>
@endpush


