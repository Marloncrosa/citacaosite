@extends('site::layouts.master')

@section('content')
    <main id="resposta">
        <section>
            <div class="row">
                @if(session('type') == 'success')
                    <div class="resposta sucesso">
                        <i class="icon-happiness-1"></i>
                        <h1>Solicitação de pagamento, realizada com sucesso!</h1>
                        <p>Estamos aguardando a confirmação do pagamento, assim que estiver tudo certo, te informamos
                            por
                            e-mail.
                        </p>
                        
                        <a href="{!! route('site-produtos') !!}" class="button thema branco">Continuar comprando</a>
                        <br>
                        ou ir para<br><br>
                        <a href="{!! route('usuario-meus-pedidos') !!}" class="button thema branco">Meus pedidos</a>
                    </div>
                @else
                    <div class="resposta erro">
                        <i class="icon-dead"></i>
                        <h1>Opss!</h1>
                        <p>Houve um problema, tente novamente mais tarde.</p>
            
                        <a href="{!! route('site-produtos') !!}" class="button thema branco">Continuar comprando</a>
                        <br>
                        ou ir para<br><br>
                        <a href="{!! route('usuario-meus-pedidos') !!}" class="button thema branco">Meus pedidos</a>
                    </div>
                @endif
            </div>
        </section>
    
    </main>
@stop
