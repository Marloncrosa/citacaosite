@extends('site::layouts.master')

@section('content')
    <main id="checkout">
        <section class="carrinho">
            <div class="row">

                <div class="columns small-12 medium-7 large-8">
                    <h1>Selecione o método de pagamento</h1>

                    <ul class="modo-pagamento">
                        <li class="card">
                            <a href="{!! route('site-pagamento-cartao') !!}">
                                <i class="icon-card"></i>
                                CARTÃO DE CRÉDITO
                            </a>
                        </li>

                        <li class="card">
                            <a href="{!! route('site-pagamento-debito') !!}">
                                <i class="icon-card"></i>
                                CARTÃO DE DÉBITO
                            </a>
                        </li>

                        <li class="boleto">
                            <a href="{!! route('site-pagamento-boleto') !!}">
                                <i class="icon-barcode"></i>
                                BOLETO BANCÁRIO
                            </a>
                        </li>
                    </ul>
                </div>

                @includeIf('site::pagamento.resumo')

            </div>
        </section>

        <div class="reveal" id="add-cupom" data-reveal>
            <img src="images/cards/creditCard.jpg" alt="Apresentação de dados do Cartão">
            <span class="close-modal icon-x-b" data-close></span>
        </div>
    </main>
@stop
