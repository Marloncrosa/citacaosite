{!! Form::hidden('country_id', 'BR') !!}

@if(empty($user->name))
    <div class="row">
        <div class="columns small-12 medium-9 large-9">
            {!! Form::label('user[name]', 'Nome') !!}
            {!! Form::text('user[name]', (request()->has('nome'))? request()->nome : null, ['class' => '', 'placeholder' => 'Nome completo', 'required']) !!}
        </div>
    </div>
@endif

@if(empty($user->birthdate))
    <div class="row">
        <div class="columns small-12">
            {!! Form::label('user[birthdate]', 'Data de Nasc.') !!}
            {!! Form::text('user[birthdate]', null, ['class' => 'mascara-data', 'required']) !!}
        </div>
    </div>
@endif

@if(empty($user->document))
    <div class="row">
        <div class="columns small-12 medium-6 large-6">
            {!! Form::label('user[document]', 'CPF') !!}
            {!! Form::text('user[document]', (request()->has('cpf'))? request()->cpf : null, ['class' => 'mascara-cpf', 'required']) !!}
        </div>
    </div>
@endif


@if(empty($user->phone))
    <div class="row">
        <div class="columns small-12">
            {!! Form::label('user[phone]', 'Tel. Residencial.') !!}
            {!! Form::text('user[phone]', null, ['class' => 'mascara-telefone']) !!}
        </div>
    </div>
@endif

@if(empty($user->cellphone))
    <div class="row">
        <div class="columns small-12">
            {!! Form::label('user[cellphone]', 'Tel. Celular.') !!}
            {!! Form::text('user[cellphone]', null, ['class' => 'mascara-telefone']) !!}
        </div>
    </div>
@endif

@if(empty($user->email))
    <div class="row">
        <div class="columns small-12 medium-12 large-12">
            {!! Form::label('user[email]', 'E-mail') !!}
            {!! Form::email('user[email]', (request()->has('email'))? request()->email : null, ['class' => '', 'required']) !!}
        </div>
    </div>
@endif

@if(empty($user->postal_code))
    <div class="row">
        <div class="columns small-12 medium-12 large-12">
            <h2>Endereço para Entrega</h2>
        </div>
    </div>
    
    <div class="row">
        <div class="columns small-12">
            {!! Form::label('user[postal_code]', 'Cep') !!}
            {!!
                Form::text('user[postal_code]', null, [
                    'class' => 'm-input mascara-cep cep-preenche',
                    'id' => 'postal_code',
                    'data-estado' => '#state_id',
                    'data-cidade' => '#city_id',
                    'data-endereco' => '#address',
                    'data-bairro' => '#neighborhood',
                    'data-numero' => '#number',
                    'data-pais' => '#country_id',
                    'required',
                ])
            !!}
        </div>
    </div>
@endif

<span class="clear"></span>

@if(empty($user->address))
    <div class="row">
        <div class="columns small-12 medium-9 large-9">
            {!! Form::label('uesr[address]', __('Endereço'), [ 'class' => '' ]) !!}
            {!! Form::text('uesr[address]', null, ['class' => '', 'required']) !!}
        </div>
    </div>
@endif

@if(empty($user->number))
    <div class="row">
        <div class="columns small-12">
            {!! Form::label('user[number]', __('Número'), [ 'class' => '' ]) !!}
            {!! Form::text('user[number]', null, ['class' => '', 'required']) !!}
        </div>
    </div>
@endif

@if(empty($user->neighborhood))
    <div class="row">
        <div class="columns small-12">
            {!! Form::label('user[neighborhood]', __('Bairro'), [ 'class' => '' ]) !!}
            {!! Form::text('user[neighborhood]', null, ['class' => '', 'required']) !!}
        </div>
    </div>
@endif

{{--@if(empty($user->address_complement))
div.row

    <div class="columns small-12">
        {!! Form::label('user[address_complement]', __('Complemento'), [ 'class' => '' ]) !!}
        {!! Form::text('user[address_complement]', null, ['class' => '']) !!}
    </div>
@endif--}}

@if(empty($user->state_id))
    <div class="row">
        <div class="columns small-12">
            {!! Form::label('user[state_id]', __('Estado'), [ 'class' => '' ]) !!}
            {!! Form::select(
                  'user[state_id]',
                  ['' => __('Seleccionar')] + $estado->pluck('state', 'id')->toArray(),
                  null,
                  ['class'=>'not-select2', 'required'])
              !!}
        </div>
    </div>
@endif

@if(empty($user->city_id))
    <div class="row">
        <div class="columns small-12">
            {!! Form::label('user[city_id]', __('Cidade'), [ 'class' => '' ]) !!}
            {!! Form::select(
                  'user[city_id]',
                  ['' => __('Selecionar')],
                  null,
                  ['class'=>'not-select2', 'id' => 'required'])
              !!}
        </div>
    </div>
@endif
