@if(!empty($carrinho))
    <div class="columns small-12 medium-5 large-4">
        <table class="resumo-compra">
            <thead>
                <tr>
                    <th>RESUMO DA COMPRA</th>
                </tr>
            </thead>
            <tbody>
                
                @foreach ($carrinho as $item)
                    @php
                        $itemSession = session('item_'.$item->id);
                        $product = $itemSession['product'];
                        $color = isset($itemSession['color'])? $itemSession['color'] : null;
                        $additionals = isset($itemSession['adicionais'])? collect($itemSession['adicionais'])->recursive() : null;
                        $valideImage = !empty($color)? $color->image_cover : $product->image_cover
    
                    //dump(!empty($product->image_cover));
                    //dump(file_exists(public_path('uploads/'.$product->image_cover)));
                    //dump('uploads/'.$product->image_cover);
                    //dump($product);
                    //die;
                    @endphp
                    <tr>
                        <td>
                            @if((!empty($valideImage) && file_exists(public_path('uploads/'.$valideImage))))
                                <picture>
                                    <source
                                        srcset="{{ route('thumbfit',[ 70, 70, $valideImage, 'ffffff', false]) }}"
                                        type="image/webp">
                                    
                                    
                                    <img class="img-responsive modelo-color active" data-image="1"
                                         src="{{ route('thumbfit', [ 70, 70, $valideImage, 'ffffff', true]) }}"
                                         alt="{{$item->name}}">
                                </picture>
                            @else
                                <picture>
                                    <img src="https://picsum.photos/70/70/?random" alt="{{$item->name}}">
                                </picture>
                            @endif
                            <h2>
                                {!! $item->name !!}
                                <br>
                                @if(!empty($color))
                                    {!! $color->title !!}@if(!empty($additionals)), @endif
                                @endif
                                @if(!empty($additionals))
                                    {!! $additionals->pluck('title')->implode(', ') !!}
                                @endif
                            </h2>
                            <small>{!! $item->quantity !!} unidades</small>
                        </td>
                    </tr>
                @endforeach
                
                <tr>
                    <td class="subtotal">
                        Subtotal
                        <span>{!! formataNumero(Cart::getSubTotal()) !!}</span>
                    </td>
                </tr>
                
                @if ($shipping > 0)
                    <tr>
                        <td class="entrega">
                            Entrega
                            <span>{!! formataNumero($shipping) !!}</span>
                        </td>
                    </tr>
                @endif
                
                @if($discount != 0)
                    <tr>
                        <td class="desconto">
                            Desconto
                            <span>{!! formataNumero($discount) !!}</span>
                        </td>
                    </tr>
                @endif
                
                <tr>
                    <td class="total">
                        Total
                        <span class="valor-total">{!! formataNumero($total) !!}</span>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
@endif
