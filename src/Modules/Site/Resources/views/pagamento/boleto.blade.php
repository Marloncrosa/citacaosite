@extends('site::layouts.master')

@section('content')
    <main id="checkout">
        <section class="carrinho">
            <div class="row">

                <div class="columns small-12 medium-7 large-8">
                    <h1>Selecione o método de pagamento</h1>

                    <ul class="modo-pagamento">
                        <li class="card">
                            <a href="{!! route('site-pagamento-cartao') !!}">
                                <i class="icon-card"></i>
                                CARTÃO DE CRÉDITO
                            </a>
                        </li>

                        <li class="card">
                            <a href="{!! route('site-pagamento-debito') !!}">
                                <i class="icon-card"></i>
                                CARTÃO DE DÉBITO
                            </a>
                        </li>

                        <li class="boleto">
                            <a href="{!! route('site-pagamento-boleto') !!}">
                                <i class="icon-barcode"></i>
                                BOLETO BANCÁRIO
                            </a>
                        </li>
                    </ul>

                    <div class="box-boleto">
                        <a href="{!! $link !!}" class="cod_barra" data-fancybox data-type="iframe">
                            <img src="images/cards/barra.png" alt="">
                        </a>

                        <a href="{!! $link !!}" class="button full" data-fancybox data-type="iframe">Imprimir Boleto</a>
                    </div>
                </div>

                @includeIf('site::pagamento.resumo')

            </div>
        </section>
    </main>
@stop
