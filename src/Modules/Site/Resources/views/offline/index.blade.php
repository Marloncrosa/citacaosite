@extends('site::offline.illustrated-layout')

@section('code', 'Offline')
@section('title', 'Offline')

@section('image')
    <div style="background-image: url('img/logo.png'); -webkit-background-size: 199px 67px;" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message')
    Conecte-se em um rede para poder usar o sistema
@stop

