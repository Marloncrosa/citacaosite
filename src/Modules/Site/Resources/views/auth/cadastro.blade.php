@extends('site::layouts.master', [ 'idMain' => 'blog', 'subtitle' => '', 'bodyclass' => 'interna'])

@section('content')
    <main id="cadastro">
        <section>
            <div class="row">
                <form action="minha-conta.php" method="post" name="cadastro" class="cadastro">
                    <div class="row">

                        <div class="columns small-12 medium-12 large-12">
                            <h2>Cadastre-se e aproveite todas as ofertas it.boss</h2>
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <input type="text" name="nome" id="nome" placeholder="Nome Completo">
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <input type="text" name="email" id="email" placeholder="E-mail">
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <input type="text" name="cpf" id="cpf" placeholder="CPF">
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <input type="text" name="telefone" id="telefone" placeholder="Telefone">
                        </div>

                        <div class="columns small-12 medium-12 large-12">
                            <h2>Endereço para entrega</h2>
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <input type="text" name="cep" id="cep" placeholder="CEP">
                        </div>

                        <span class="clear"></span>

                        <div class="columns small-12 medium-6 large-6">
                            <input type="text" name="endereco" id="endereco" placeholder="Endereço">
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <input type="text" name="numero" id="numero" placeholder="Número">
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <input type="text" name="complemento" id="complemento" placeholder="Complemento">
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <input type="text" name="bairro" id="bairro" placeholder="Bairro">
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <select class="select2" name="estado">
                                <option value="">Selecionar estado</option>
                            </select>
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <select class="select2" name="cidade">
                                <option value="">Selecione o estado primeiro</option>
                            </select>
                        </div>

                        <div class="columns small-12 medium-12 large-12">
                            <h2>Segurança</h2>
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <input type="password" name="senha" id="senha" placeholder="Senha">
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <input type="password" name="cof_senha" id="cof_senha" placeholder="Confirmar Senha">
                        </div>

                        <div class="columns small-12 medium-6 large-6">
						<span class="termos">
							Ao se cadastrar, estará aceitando todos os <a href="#" class="link thema">termor de uso</a>.
						</span>
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <button type="submit" name="logar" class="button full thema">Realizar Cadastro</button>
                        </div>

                    </div>
                </form>
            </div>
        </section>
    </main>

    <script type="text/javascript">
	$(function () {
		$("#password").complexify({}, function (valid, complexity) { document.getElementById("PassValue").value = complexity; });
	});

    </script>
@stop

