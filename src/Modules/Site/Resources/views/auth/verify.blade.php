@extends('site::layouts.master', [ 'idMain' => 'home', 'subtitle' => '', 'bodyclass' => 'interna'])

@section('content')
    <main id="index">
        <section>
            <article id="login">

                <div class="row">
                    <div class="card">
                        <div class="card-header">{{ __('Verify Your Email Address') }}</div>

                        <div class="card-body">
                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('A fresh verification link has been sent to your email address.') }}
                                </div>
                            @endif

                            {{ __('Before proceeding, please check your email for a verification link.') }}
                            {{ __('If you did not receive the email') }}, <a
                                    href="{{ route('site.verification.resend') }}">{{ __('click here to request another') }}</a>.
                        </div>
                    </div>
                </div>
            </article>
        </section>
    </main>
@stop
