@extends('site::layouts.master-login')

@section('content')

    <main id="login">
        <h1 class="ocultar">Londrinando APP</h1>

        <div class="aling">
            {!! Form::model(null, ['route' => 'site.resetar-senha.reset.post', 'class' => 'restart', 'files' => false, 'name' => 'restart', 'style' => 'display: inline-block;']) !!}

            <input type="hidden" name="token" value="{{ $token }}">
            <figure><img src="images/icons/password.png"></figure>
            <h2>Recuperar Senha</h2>
            <p>Informe seu e-mail e a nova senha abaixo para realizar a atualização do seu cadastro.</p>
            <div>
                <input type="email" name="email" placeholder="Seu E-mail">
            </div>
            <div>
                <input type="password" name="password" placeholder="Nova senha">
            </div>
            <div>
                <input type="password" name="password_confirmation" placeholder="Repetir senha">
            </div>

            <div>
                <button type="submit" name="submit" class="button thema branco full">Continuar</button>
            </div>
            <a href="{!! route('site.cadastro') !!}" class="link branco">
                <small>Novo por aqui? Cadastre-se</small>
            </a>

            <span class="btn-voltar icon-arrow-pointing-to-left"></span>
            {{ Form::close() }}
        </div>
    </main>
@stop
