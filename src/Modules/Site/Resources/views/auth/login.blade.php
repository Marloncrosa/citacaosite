@extends('site::layouts.master', [ 'idMain' => 'home', 'subtitle' => '', 'bodyclass' => 'interna'])

@section('content')
    <main id="login">
        <section>
            <div class="row">
                <div class="columns small-12 medium-6 large-6">
                    {{ Form::open([ 'route' => 'site.loginpost', 'class' => 'login', 'method' => 'post']) }}
                    <h2>Realizar login</h2>

                    <div class="user">
                        <input type="text" name="usuario" placeholder="Seu usuário">
                    </div>

                    <div class="password">
                        <input type="password" name="password" placeholder="Sua Senha">
                    </div>

                    <div>
                        <button type="submit" name="logar" class="button full thema">Realizar Login</button>
                    </div>

                    <p class="text-center">Ou</p>

                    <div class="text-center">
                        <a href="#" class="button full facebook">Acesse via Facebook</a>
                        <a href="#" class="link branco">
                            <small>Esqueci minha senha</small>
                        </a>
                    </div>
                    {{ Form::close() }}
                </div>

                <div class="columns small-12 medium-6 large-6">

                    <form action="minha-conta.php" method="post" name="login" class="pre-cadastro">
                        <h2>Ainda não tem uma conta? Cadastre-se</h2>

                        <div>
                            <input type="text" name="nome" placeholder="Seu nome completo">
                        </div>

                        <div class="password">
                            <input type="text" name="email" placeholder="Seu e-mail">
                        </div>

                        <div>
                            <button type="submit" name="logar" class="button full thema">Continuar cadastro</button>
                        </div>

                        <p class="text-center">Ou</p>

                        <div class="text-center">
                            <a href="#" class="button full facebook">Cadastre-se via Facebook</a>
                            <small>Ao se cadastrar, estará aceitando os nossos <a href="#" class="link thema">termor de
                                    uso</a>.
                            </small>
                        </div>

                    </form>
                </div>
            </div>
        </section>
    </main>
@stop

@push('line-js')
    <script type="text/javascript">
        $(function () {
            $("#password").complexify({}, function (valid, complexity) {
                document.getElementById("PassValue").value = complexity;
            });
        });
    </script>
@endpush
