@component('site::mail.html.message')
{{-- Saudação --}}
@isset($saudacao)
# {{ $saudacao }}
@endisset

{{-- Linhas de Introdução --}}
@isset($introLines)
@if(is_array($introLines))
@foreach ($introLines as $line)
{{ $line }}

@endforeach
@else
{{ $introLines }}
@endif
@endisset

@php
  $pedido = $dados['pedido'];
@endphp

# Dados do Pedido
* Pedido solicitado em: {{ $pedido->created_at }}
* CEP: {{ $pedido->cep }}
* Endereço: {{ $pedido->endereco }}
* Bairro: {{ $pedido->bairro }}
* Cidade/Estado: {{ $pedido->cidades->cidade }} - {{ $pedido->estados->uf }}

# Dados do Cliente

* Nome: {{ $pedido->clientes->nome }}
* CPF/CNPJ: {{ $pedido->clientes->documento }}
* E-mail: {{ $pedido->clientes->email }}

# Itens do Pedido
@foreach ($pedido->itenspedido as $item)
 * Produto: {{ $item->produto->titulo }}
 * Cód. de venda do Produto: {{ $item->produto->codigo_venda }}
 * Quantidade: {{ $item->quantidade }}

@endforeach

{{-- Agradecimento --}}
@if (! empty($agradecimento))
{{ $agradecimento }}
@else
{{__("Atenciosamente")}},
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('site::mail.html.subcopy')
{{__("Se você está tendo problemas para clicar no botão")}} "{{ $actionText }}", {{__("copie e cole a url abaixo")}}
{{__("no seu navegador")}}: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent
