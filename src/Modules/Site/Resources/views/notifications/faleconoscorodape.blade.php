@component('site::mail.html.message')
{{-- Saudação --}}
@isset($saudacao)
# {{ $saudacao }}
@endisset

{{-- Linhas de Introdução --}}
@isset($introLines)
@if(is_array($introLines))
@foreach ($introLines as $line)
{{ $line }}

@endforeach
@else
{{ $introLines }}
@endif
@endisset

* E-mail: {{ $dados['email'] }}
* Mensagem: {!! $dados['mensagem'] !!}

{{-- Agradecimento --}}
@if (! empty($agradecimento))
{{ $agradecimento }}
@else
{{__("Atenciosamente")}},
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('site::mail.html.subcopy')
{{__("Se você está tendo problemas para clicar no botão")}} "{{ $actionText }}", {{__("copie e cole a url abaixo")}}
{{__("no seu navegador")}}: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent
