@component('site::mail.html.message')
{{-- Saudação --}}
@isset($saudacao)
# {{ $saudacao }}
@endisset

{{-- Linhas de Introdução --}}
@isset($introLines)
@if(is_array($introLines))
@foreach ($introLines as $line)
{{ $line }}

@endforeach
@else
{{ $introLines }}
@endif
@endisset

@if(!empty($dados['nome']))
* Nome: {{ $dados['nome'] }}
@endif
@if(!empty($dados['empresa']))
* Empresa, Produtos ou Negócios: {{ $dados['empresa'] }}
@endif
@if(!empty($dados['email']))
* E-mail: {{ $dados['email'] }}
@endif
@if(!empty($dados['telefone']))
* Telefone: {{ $dados['telefone'] }}
@endif
@if(!empty($dados['celular']))
* Celular: {{ $dados['celular'] }}
@endif

{{-- Agradecimento --}}
@if (! empty($agradecimento))
{{ $agradecimento }}
@else
{{__("Atenciosamente")}},
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@isset($actionText)
@component('site::mail.html.subcopy')
{{__("Se você está tendo problemas para clicar no botão")}} "{{ $actionText }}", {{__("copie e cole a url abaixo")}}
{{__("no seu navegador")}}: [{{ $actionUrl }}]({{ $actionUrl }})
@endcomponent
@endisset
@endcomponent
