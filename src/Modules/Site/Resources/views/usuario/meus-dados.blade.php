@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'index', 'subtitle' => ''])

@section('content')
    <main id="painel-user">
        <section>
            <div class="row">
                <div class="columns small-12 medium-3 large-3">
                    <ul class="menu-painel">
                        <li><a href="{!! route('site.meuspedidos') !!}">Meus pedidos</a></li>
                        <li><a href="{!! route('site.minhaconta') !!}" class="active">Meus dados</a></li>
                        <li><a href="{!! route('site.meusenderecos') !!}">Endereços de entrega</a></li>
                        <li><a href="{!! route('site.alterarsenha') !!}">Segurança</a></li>
                    </ul>
                </div>

                <div class="columns small-12 medium-9 large-9 padding-off conteudo-painel">
                    {{ Form::model($FormModel, [ 'route' => 'site.minhaconta.post', 'class' => 'form-painel', 'method' => 'post', 'data-abide', 'novalidate']) }}
                    <div class="row">
                        <div class="columns small-12 medium-12 large-12">
                            <h2>Meus Dados</h2>
                        </div>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('name', 'Nome completo') !!}
                            {!! Form::text('name', null, ['class' => '']) !!}
                        </div>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('document', 'CPF') !!}
                            {!! Form::text('document', null, ['class' => 'mascara-cpf']) !!}
                        </div>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('birthdate', 'Data de Nasc.') !!}
                            {!! Form::text('birthdate', null, ['class' => 'mascara-data']) !!}
                        </div>

                        {{--<div class="columns small-6 medium-6 large-6">
                            <label for="sexo">Sexo</label>
                            <input type="text" name="sexo" value="Masculino" disabled>
                        </div>--}}

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('email', 'E-mail') !!}
                            {!! Form::email('email', null, ['class' => '']) !!}
                        </div>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('phone', 'Tel. Residencial.') !!}
                            {!! Form::text('phone', null, ['class' => 'mascara-telefone']) !!}
                        </div>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('cellphone', 'Tel. Celular.') !!}
                            {!! Form::text('cellphone', null, ['class' => 'mascara-telefone']) !!}
                        </div>

                        <div class="columns small-12 medium-12 large-12">
                            <h2>Endereço padrão</h2>
                        </div>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('postal_code', 'Cep') !!}
                            {!!
                                Form::text('postal_code', null, [
                                    'class' => 'm-input mascara-cep cep-preenche',
                                    'id' => 'postal_code',
                                    'data-estado' => '#state_id_example',
                                    'data-cidade' => '#city_id_example',
                                    'data-endereco' => '#address_example',
                                    'data-bairro' => '#neighborhood_example',
                                    'data-numero' => '#number_example',
                                    'data-pais' => '#country_id_example',
                                    'disabled',
                                ])
                            !!}
                        </div>

                        <span class="clear"></span>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('address_example', __('Endereço'), [ 'class' => '' ]) !!}
                            {!! Form::text('address', null, ['class' => '']) !!}
                        </div>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('number_example', __('Número'), [ 'class' => '' ]) !!}
                            {!! Form::text('number', null, ['class' => '']) !!}
                        </div>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('neighborhood_example', __('Bairro'), [ 'class' => '' ]) !!}
                            {!! Form::text('neighborhood', null, ['class' => '']) !!}
                        </div>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('address_complement_example', __('Complemento'), [ 'class' => '' ]) !!}
                            {!! Form::text('address_complement', null, ['class' => '']) !!}
                        </div>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('state_id_example', __('Estado'), [ 'class' => '' ]) !!}
                            {!! Form::select(
                                  'state_id',
                                  ['' => __('Seleccionar')] + $estado->pluck('state', 'id')->toArray(),
                                  null,
                                  [
                                    'class' => 'not-select2',
                                    'data-value' => (!empty($FormModel->state_id))? $FormModel->state_id : old('state_id'),
                                    'disabled'
                                  ])
                              !!}
                        </div>

                        <div class="columns small-6 medium-6 large-6">
                            {!! Form::label('city_id_example', __('Cidade'), [ 'class' => '' ]) !!}
                            {!! Form::select(
                                  'city_id',
                                  ['' => __('Selecionar')],
                                  null,
                                  [
                                    'class'=>'not-select2',
                                    'data-value' => (!empty($FormModel->city_id))? $FormModel->city_id : old('city_id'),
                                    'disabled'
                                  ])
                              !!}
                        </div>
                    </div>
                    <div class="columns small-12 medium-12 large-12 text-right">
                        <button type="submit" name="submit" class="button thema">Salvar informações</button>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </section>
    </main>
@stop
