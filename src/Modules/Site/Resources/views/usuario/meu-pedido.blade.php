@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'index', 'subtitle' => ''])

@section('content')
    <main id="painel-user">
        <section>
            <div class="row">
                <div class="columns small-12 medium-3 large-3">
                    <ul class="menu-painel">
                        <li><a href="{!! route('site.meuspedidos') !!}" class="active">Meus pedidos</a></li>
                        <li><a href="{!! route('site.minhaconta') !!}">Meus dados</a></li>
                        <li><a href="{!! route('site.meusenderecos') !!}">Endereços de entrega</a></li>
                        <li><a href="{!! route('site.alterarsenha') !!}">Segurança</a></li>
                    </ul>
                </div>

                <div class="columns small-12 medium-9 large-9 padding-off conteudo-painel">
                    @php
                        $address = (object) $pedido->delivery_address_json;
                    @endphp
                    <div class="row pedido">
                        <div class="columns small-12 medium-12 large-12">
                            <h2>Pedido {!! $pedido->id !!}</h2>
                        </div>

                        <div class="columns small-12 medium-12 large-12">
                            <div class="item">
                                <div class="linha-a">
                                    <h3>{!! $pedido->created_at !!}</h3>
                                    <span class="status">Status:
                                        <strong class="_001">
                                     @if (!empty($pedido->order_status))
                                                {!! $pedido->order_status->name !!}
                                            @endif
                                        </strong>
                                    </span>
                                </div>
                                @if (!empty($cart_item))
                                    <div class="linha-b">
                                        @foreach ($cart_item as  $item)
                                            <div class="produto">
                                                @php
                                                    $produto = (object) $item->product;
                                                    $produtoE = $item->product()->withTrashed()->first();
                                                    $brand = (object) $produto->brand;
                                                @endphp
                                                @if((!empty($produtoE->image_cover) && file_exists(public_path('uploads/'.$produtoE->image_cover))))
                                                    <figure>
                                                        <img class="img-responsive modelo-color active" data-image="1"
                                                             src="{{ route('thumbfit', [ 70, 70, $produtoE->image_cover, 'ffffff', true]) }}"
                                                             alt="{{$produto->title}}">
                                                    </figure>
                                                @else
                                                    <figure>
                                                        <img src="public/assets/img/temp/produto01.jpg" alt="">
                                                    </figure>
                                                @endif
                                                <div class="aling">
                                                    <h5>{!! $brand->title !!}</h5>
                                                    <h4>{!! $produto->title !!}</h4>
                                                    <span class="cor">Cor: <strong>Preto</strong></span>
                                                    <span
                                                        class="quantidade">Quantidade: <strong>{!! $produto->amount !!}</strong></span>
                                                    <span
                                                        class="valor_unidade">Valor unitário: <strong>{!! formataNumero($produto->final_value) !!}</strong></span>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @endif

                                <div class="linha-c info-pedido">
                                    <div class="row">
                                        <div class="columns small-12 medium-4 large-4">
                                            <div class="entrega">
                                                <h3>Endereço de entrega</h3>
                                                <p>
                                                    {!! $address->address !!}, {!! $address->number !!}
                                                    , {!! $address->address_complement !!}<br>
                                                    {!! $address->neighborhood !!}, {!! $address->city !!}
                                                    - {!! $address->UF !!}<br>
                                                    CEP: {!! $address->postal_code !!}
                                                </p>
                                                <a href="#" class="link verde">Código de Rastreamento</a>
                                            </div>
                                        </div>

                                        <div class="columns small-12 medium-4 large-4">
                                            <div class="pagamento">
                                                <h3>Dados do pagamento</h3>
                                                @if ($pedido->payment_method_id == 1 )
                                                    <p>
                                                        <span class="_004">
                                                            Status: {!! $pedido->order_status->name !!}
                                                        </span>
                                                        <br>
                                                        Pagamento via Cartão de Crédito
                                                    </p>
                                                    <span class="card">
                                                        <span class="bandeira">
                                                            <img src="//stc.pagseguro.uol.com.br/public/img/payment-methods-flags/42x20/{!! $pedido->brand !!}.jpg" alt="">
                                                        </span>
                                                        <span class="numero">
                                                            **** **** **** {!! $pedido->cred_card_number !!}
                                                        </span>
                                                    </span>
                                                @elseif ($pedido->payment_method_id == 2)
                                                    <p>
                                                        <span class="_004">
                                                            Status: {!! $pedido->order_status->name !!}
                                                        </span>
                                                        <br>

                                                        Pagamento via Boleto Bancário
                                                    </p>
                                                @elseif ($pedido->payment_method_id == 3)
                                                    <p>
                                                        <span class="_004">
                                                            Status: {!! $pedido->order_status->name !!}
                                                        </span>
                                                        <br>
                                                        Pagamento via Débito
                                                    </p>
                                                    <span class="card">
                                                        {{--<span class="bandeira">
                                                            <img src="//stc.pagseguro.uol.com.br/public/img/payment-methods-flags/42x20/master-card.jpg" alt="">
                                                        </span>--}}
                                                        {{--<span class="numero">
                                                            **** **** **** {!! $pedido->cred_card_number !!}
                                                        </span>--}}
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="columns small-12 medium-4 large-4">
                                            <div class="valores">
                                                <span class="l_valor">Frete: <strong
                                                        class="float-right">{!! formataNumero($pedido->shipping) !!}</strong></span>
                                                <span class="l_valor">Desconto: <strong
                                                        class="float-right">{!! formataNumero($pedido->discount) !!}</strong></span>
                                                <span class="l_valor">Valor total: <strong
                                                        class="float-right">{!! formataNumero($pedido->total) !!}</strong></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </main>
@stop
