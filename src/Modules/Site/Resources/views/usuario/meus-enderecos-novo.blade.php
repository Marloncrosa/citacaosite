@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'index', 'subtitle' => ''])

@section('content')
    <main id="painel-user">
        <section>
            <div class="row">
                <div class="columns small-12 medium-3 large-3">
                    <ul class="menu-painel">
                        <li><a href="meus-pedidos.php">Meus pedidos</a></li>
                        <li><a href="meus-dados.php">Meus dados</a></li>
                        <li><a href="meus-enderecos.php" class="active">Endereços de entrega</a></li>
                        <li><a href="minha-senha.php">Segurança</a></li>
                    </ul>
                </div>

                <div class="columns small-12 medium-9 large-9 padding-off conteudo-painel">

                    {{ Form::model($FormModel, [ 'url' => $url, 'class' => 'form-painel', 'method' => 'post', 'data-abide', 'novalidate']) }}
                    <div class="row">
                        <div class="columns small-12 medium-12 large-12">
                            <h2>Novo endereço</h2>
                        </div>

                        <div class="columns small-12 medium-12 large-12">
                            {!! Form::text('title', null, ['class' => '', 'placeholder' => 'Titulo: (exemplo: Meus trabalho)']) !!}
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            {!!
                           Form::text('postal_code', null, [
                               'class' => 'm-input mascara-cep cep-preenche',
                               'id' => 'postal_code',
                               'data-estado' => '#state_id_example',
                               'data-cidade' => '#city_id_example',
                               'data-endereco' => '#address_example',
                               'data-bairro' => '#neighborhood_example',
                               'data-numero' => '#number_example',
                               'data-pais' => '#country_id_example',
                               'placeholder' => 'CEP',
                           ])
                       !!}
                        </div>

                        <span class="clear"></span>

                        <div class="columns small-12 medium-6 large-6">
                            {!! Form::text('address', null, ['class' => '', 'placeholder' => 'Endereço']) !!}
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            {!! Form::text('number', null, ['class' => '', 'placeholder' => 'Número']) !!}
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            {!! Form::text('address_complement', null, ['class' => '', 'placeholder' => 'Complemento']) !!}
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            {!! Form::text('neighborhood', null, ['class' => '', 'placeholder' => 'Bairro']) !!}
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            {!! Form::select(
                             'state_id',
                             ['' => __('Selecionar o estado')] + $estado->pluck('state', 'id')->toArray(),
                             null,
                             [
                               'class' => 'select2',
                               'data-value' => (!empty($FormModel->state_id))? $FormModel->state_id : old('state_id'),
                               'disabled'
                             ])
                         !!}
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            {!! Form::select(
                             'city_id',
                             ['' => __('Selecionar')],
                             null,
                             [
                               'class'=>'select2',
                               'data-value' => (!empty($FormModel->city_id))? $FormModel->city_id : old('city_id'),
                               'disabled'
                             ])
                         !!}
                        </div>

                        <div class="columns small-12 medium-12 large-12 text-right">
                            <button type="submit" name="logar" class="button thema">Salvar novo endereço</button>
                        </div>
                    </div>
                    {{ Form::close() }}
                </div>
            </div>
        </section>
    </main>
@stop
