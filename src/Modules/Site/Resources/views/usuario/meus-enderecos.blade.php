@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'index', 'subtitle' => ''])

@section('content')
    <main id="painel-user">
        <section>
            <div class="row">
                <div class="columns small-12 medium-3 large-3">
                    <ul class="menu-painel">
                        <li><a href="{!! route('site.meuspedidos') !!}">Meus pedidos</a></li>
                        <li><a href="{!! route('site.minhaconta') !!}">Meus dados</a></li>
                        <li><a href="{!! route('site.meusenderecos') !!}" class="active">Endereços de entrega</a></li>
                        <li><a href="{!! route('site.alterarsenha') !!}">Segurança</a></li>
                    </ul>
                </div>

                <div class="columns small-12 medium-9 large-9 padding-off conteudo-painel">

                    <div class="row">
                        <div class="columns small-12 medium-12 large-12">
                            <h2>Endereços de entrega</h2>
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            <div class="endereco">
                                <h3>Endereço principal</h3>
                                <p>
                                    {!! $principal_address->address !!}, {!! $principal_address->number !!}
                                    , {!! $principal_address->address_complement !!}<br>
                                    {!! $principal_address->neighborhood !!}. {!! $principal_address->city->city !!}
                                    - {!! $principal_address->state->initials !!}<br>
                                    CEP: {!! $principal_address->postal_code !!}
                                </p>
                                <a href="meus-endereco-editar.php">Editar</a>
                            </div>
                        </div>

                        @if (!empty($other_address))
                            @foreach ($other_address as $address)
                                <div class="columns small-12 medium-6 large-6">
                                    <div class="endereco">
                                        <h3>{!! $address->title !!}</h3>
                                        <p>
                                            {!! $address->address !!}, {!! $address->number !!}
                                            , {!! $address->address_complement !!}<br>
                                            {!! $address->neighborhood !!}. @if (!empty($address->city)) {!! $address->city->city !!} @endif
                                            -  @if (!empty($address->state)) {!! $address->state->initials !!} @endif<br>
                                            CEP: {!! $address->postal_code !!}
                                        </p>
                                        <a href="{!! route('site.meusenderecoseditar', ['id' => $address->id]) !!}">Editar</a> I <a href="{!! route('usuario-address-remove', ['id' => $address->id]) !!}">Excluir</a>
                                    </div>
                                </div>
                            @endforeach
                        @endif

                        <div class="columns small-12 medium-6 large-6">
                            <a href="{!! route('site.meusenderecosnovo') !!}" class="button thema full">Adicionar novo endereço</a>
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </main>
@stop
