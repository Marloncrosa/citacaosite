@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'index', 'subtitle' => ''])

@section('content')
    <main id="painel-user">
        <section>
            <div class="row">
                <div class="columns small-12 medium-3 large-3">
                    <ul class="menu-painel">
                        <li><a href="{!! route('site.meuspedidos') !!}" class="active">Meus pedidos</a></li>
                        <li><a href="{!! route('site.minhaconta') !!}">Meus dados</a></li>
                        <li><a href="{!! route('site.meusenderecos') !!}">Endereços de entrega</a></li>
                        <li><a href="{!! route('site.alterarsenha') !!}">Segurança</a></li>
                    </ul>
                </div>

                <div class="columns small-12 medium-9 large-9 padding-off conteudo-painel">

                    <div class="row pedidos">
                        <div class="columns small-12 medium-12 large-12">
                            <h2>Meus Pedidos</h2>
                        </div>

                        @if (!empty($pedidos))
                            <div class="columns small-12 medium-12 large-12">

                                @foreach ($pedidos as $pedido)
                                    <div class="item">
                                        <div class="linha-a">
                                            <h3>Pedido {!! $pedido->id !!}</h3> <a href="{{route("site.meupedido",[ 'id' => $pedido->id ])}}"
                                                                                   class="btn-detalhar">Ver
                                                detalhes</a>
                                            <span class="status">Status:
                                                <strong class="_001">
                                                    @if (!empty($pedido->order_status))
                                                        {!! $pedido->order_status->name !!}
                                                    @endif
                                                </strong>
                                            </span>
                                        </div>
                                        <div class="linha-b">
                                            <span
                                                class="data">Solicitado <strong>{!! $pedido->created_at !!}</strong></span>
                                            <span
                                                class="valor">Valor total: <strong> {!! formataNumero($pedido->total) !!}</strong></span>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif

                    </div>
                </div>
            </div>
        </section>
    </main>
@stop
