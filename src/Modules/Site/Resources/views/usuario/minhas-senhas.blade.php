@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'index', 'subtitle' => ''])

@section('content')
    <main id="painel-user">
        <section>
            <div class="row">
                <div class="columns small-12 medium-3 large-3">
                    <ul class="menu-painel">
                        <li><a href="{!! route('site.meuspedidos') !!}">Meus pedidos</a></li>
                        <li><a href="{!! route('site.minhaconta') !!}">Meus dados</a></li>
                        <li><a href="{!! route('site.meusenderecos') !!}">Endereços de entrega</a></li>
                        <li><a href="{!! route('site.alterarsenha') !!}" class="active">Segurança</a></li>
                    </ul>
                </div>

                <div class="columns small-12 medium-9 large-9 padding-off conteudo-painel">
                    {!! Form::model($model,['url' => route('site.alterarsenhapost'), 'class' => 'form-painel', 'method' => 'post']) !!}
                    <div class="row">
                        <div class="columns small-12 medium-12 large-12">
                            <h2>Alterar Senha</h2>
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            {{--                                <input type="password" name="senha" id="senha" placeholder="Nova senha">--}}
                            {!! Form::password('password', array('class' => 'form-control m-input', 'required' => 'true', 'placeholder' => 'Nova senha')) !!}
                        </div>

                        <div class="columns small-12 medium-6 large-6">
                            {{--                                <input type="password" name="cof_senha" id="cof_senha" placeholder="Confirmar nova senha">--}}
                            {!! Form::password('password_confirmation', array('class' => 'form-control m-input', 'required' => 'true', 'placeholder' => 'Confirmar nova senha')) !!}
                        </div>

                        <div class="columns small-12 medium-12 large-12 text-right">
                            <button type="submit" name="submit" class="button thema">Salvar nova senha</button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </section>
    </main>
@stop
