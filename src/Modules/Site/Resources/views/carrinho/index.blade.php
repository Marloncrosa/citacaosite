@extends('site::layouts.master', [ 'bodyclass' => 'carrinho', 'idMain' => 'carrinho', 'subtitle' => ''])

@section('content')
    <section>
        <div class="row">
            <div class="column small-12">
                <h1>SACOLA DE COMPRAS</h1>
            </div>
        </div>
        <div class="row">
            <div class="column small-12">
                <div class="produtos">
                    @foreach ($carrinho as $item)
                        @php
                            $itemSession = session('item_'.$item->id);
                            $product = $itemSession['product'];
                            $color = isset($itemSession['color'])? $itemSession['color'] : null;
                            $additionals = isset($itemSession['adicionais'])? collect($itemSession['adicionais'])->recursive() : null;
                        //dd($product)
                        @endphp
                        <div class="produto">
                            <div class="row">
                                <div class="columns small-12 medium-2 large-1 figure">
                                    @if((!empty($product->image_cover) && file_exists(public_path('uploads/'.$product->image_cover))))
                                        <figure>
                                            <source
                                                srcset="{{ route('thumbfit',[ 70, 70, $product->image_cover, 'ffffff', false]) }}"
                                                type="image/webp">

                                            <img class="img-responsive modelo-color active" data-image="1"
                                                 src="{{ route('thumbfit', [ 94, 94, $product->image_cover, 'ffffff', true]) }}"
                                                 alt="{{$item->name}}">
                                        </figure>
                                    @else
                                        <figure><img src="public/assets/img/temp/produto01.jpg" alt=""></figure>
                                    @endif
                                    {{--                                    <figure><img src="public/assets/img/temp/produto01.jpg" alt=""></figure>--}}
                                </div>
                                <div class="columns small-12 medium-3 large-3">
                                    @if (!empty($product->brand))
                                        <h5>{!! $product->brand->title !!}</h5>
                                    @endif
                                    <h4>{!! $item->name !!}</h4>
                                    <span
                                        class="cor">Cor: <strong> @if(!empty($color)){!! $color->title !!}@endif</strong></span>
                                </div>
                                <div class="columns small-12 medium-3 large-3 unitario">
                                    Valor unitário:
                                    <strong>{!! formataNumero($item->getPriceWithConditions()) !!}</strong>
                                </div>
                                <div class="columns small-12 medium-3 large-3 quantidade">
                                    <div>
                                        Quantidade:
                                        <div class="qtd">
                                            <input type="text" name="quantidade1" class="quantidade"
                                                   data-id="{{$item->id}}"
                                                   value="{!! $item->quantity !!}" autocomplete="off">
                                            <span>
                                                <span class="mais">+</span>
                                                <span class="menos">-</span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="columns small-12 medium-1 large-2 subtotal">
                                    Subtotal: <strong>{!! formataNumero($item->getPriceSumWithConditions()) !!}</strong>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row">
            <div class="columns small-12 medium-6 large-6">
                <div class="entrega">
                    @if(auth()->check() || !empty($delivery_address))
                        <div class="endereco">
                            @if(empty($delivery_address->id))
                                <h2>Endereço Padrão</h2>
                            @else
                                <h2>Endereço de Entrega</h2>
                            @endif
                            <p>
                                {!! $delivery_address->address !!}
                                <br>
                                @if(!empty($delivery_address->city))
                                    {!! $delivery_address->city !!}
                                @endif
                                @if(!empty($delivery_address->city) && !empty($delivery_address->state))
                                    -
                                @endif
                                @if(!empty($delivery_address->state))
                                    {!! $delivery_address->state !!}
                                @endif
                                |
                                @if(!empty($delivery_address->postal_code))
                                    <span class="cep">CEP: {!! $delivery_address->postal_code !!}</span>
                                @endif
                            </p>
                            <span style="cursor: pointer;" data-open="alterar-endereco"><i
                                    class="icon-circular"></i> Alterar endereço</span>
                        </div>
                    @else
                        <form action="{!! route('site-cart-custom-cep') !!}" method="post" name="form-cep"
                              class="form-cep">
                            @csrf
                            <h3 for="cep">Adicionar CEP</h3>
                            <div>

                                <input type="text" name="postal_code" id="postal_code" class="mascara-cep"
                                       placeholder="CEP" required>
                                <input type="text" name="number" id="number" placeholder="Número" required>
                                <input type="text" name="address_complement" id="address_complement"
                                       placeholder="Complemento">
                                <button type="submit" name="calcular" class="button thema pagar">Adicionar</button>
                            </div>
                            <a href="http://www.buscacep.correios.com.br/sistemas/buscacep/" target="_blank">Não sei
                                meu CEP
                            </a>
                        </form>
                    @endif
                </div>
            </div>

            <div class="columns small-12 medium-6 large-6">
                {{--                    <div class="valores">--}}
                {{--                        <span class="l_valor">Frete: <strong class="float-right">R$ 20,00</strong></span>--}}
                {{--                        <span class="l_valor">Desconto: <strong class="float-right">R$ 10,00</strong></span>--}}
                {{--                        <span class="l_valor">Valor total: <strong class="float-right">R$ 228,00</strong></span>--}}
                {{--                    </div>--}}
                <div class="valores">
                        <span class="l_valor">Subtotal:  <strong
                                class="float-right">{!! formataNumero(Cart::getSubTotal()) !!}</strong></span>

                    @if(!$acombinar && $shipping > 0)
                        <strong>Frete</strong>
                        <span class="l_valor">Frete: <strong
                                class="float-right">{!! formataNumero($shipping) !!}</strong></span>
                    @elseif($acombinar)
                        <span class="l_valor">Frete: <strong class="float-right">A Combinar</strong></span>
                    @endif

                    @if($discount == 0)
                        <span class="l_valor" data-open="add-cupom">Desconto: <strong class="float-right">adicionar cupom</strong></span>
                    @else
                        <span class="add-cupom" data-open="add-cupom">Desconto: <strong
                                class="float-right">{!! formataNumero($discount) !!}</strong></span>
                    @endif

                    <span class="l_valor">Total:  <strong
                            class="float-right">{!! formataNumero($total) !!} </strong></span>
                </div>

                <div class="columns small-12 medium-12 large-12 text-right padding-off">
                    <a href="{{route('site.produtos')}}" class="button thema branco continuar">Continuar
                        comprando</a>
                    <a href="{!! route('site-pagamento') !!}" class="button thema pagar">Realizar pagamento</a>
                </div>
            </div>
        </div>
    </section>
@stop


@push('line-js')
    <script>
            @if(isset($addresses) && !empty($addresses))
        var addresses = JSON.parse('{!! $address->toJson() !!}');
        @endif
        $(function () {
            var cartUpdate = setTimeout(() => {
            }, 500);

            $('#add-cupom form').on('submit', function () {
                requestVar = $.ajax({
                    url: '{!! route('site-cupom') !!}',
                    type: 'POST',
                    async: false,
                    data: $('#add-cupom form').serializeFormJSON()
                });

                requestVar.done(function (data) {
                    if (data.error != undefined) {
                        swal('{!! __('Erro') !!}', data.error, 'error')
                    } else if (data.warning != undefined) {
                        swal('{!! __('Atenção') !!}', data.warning, 'warning').then((hasGone) => {
                            window.location.reload();
                        })
                    } else if (data.success != undefined) {
                        swal('{!! __('Sucesso') !!}', data.success, 'success').then((hasGone) => {
                            window.location.reload();
                        })
                    }
                });
                return false;
            });

            $(".carrinho .mais").on('click', function () {
                $field = $(this).parents('.qtd').find('input.quantidade');
                value = parseInt($field.val()) + 1;
                $field.val(value);

                clearTimeout(cartUpdate);

                cartUpdate = setTimeout(() => {
                    requestVar = $.ajax({
                        url: '{!! route('site-carrinho-update') !!}',
                        type: 'POST',
                        async: false,
                        data: {
                            'id': $field.data('id'),
                            'qtde': $field.val(),
                        }
                    });

                    requestVar.done(function (data) {
                        if (data.error != undefined) {
                            swal('{!! __('Erro') !!}', data.error, 'error')
                        } else if (data.warning != undefined) {
                            swal('{!! __('Atenção') !!}', data.warning, 'warning').then((hasGone) => {
                                window.location.reload();
                            })
                        } else if (data.success != undefined) {
                            window.location.reload();
                        }
                    });
                }, 1500);

                return false;
            });

            $(".carrinho .menos").on('click', function () {
                $field = $(this).parents('.qtd').find('input.quantidade');

                clearTimeout(cartUpdate);

                if ($field.val() > 1) {
                    value = parseInt($field.val()) - 1;
                    $field.val(value);
                }

                cartUpdate = setTimeout(() => {
                    requestVar = $.ajax({
                        url: '{!! route('site-carrinho-update') !!}',
                        type: 'POST',
                        async: false,
                        data: {
                            'id': $field.data('id'),
                            'qtde': $field.val(),
                        }
                    });

                    requestVar.done(function (data) {
                        if (data.error != undefined) {
                            swal('{!! __('Erro') !!}', data.error, 'error')
                        } else if (data.warning != undefined) {
                            swal('{!! __('Atenção') !!}', data.warning, 'warning').then((hasGone) => {
                                window.location.reload();
                            })
                        } else if (data.success != undefined) {
                            window.location.reload();
                        }
                    });
                }, 1500);

                return false;
            });

            $('#cartupdate').on('submit', function () {

                if ($(this).find('input:invalid').length > 0) {
                    swal("{!! __('Por favor, cheque su carrito, existen cursos que no se seleccionaron las cuotas') !!}", "", "error");
                } else {
                    return true;
                }

                return false;
            });

            $('#alterar-endereco #address_id').on('change', function () {
                var input = $(this);

                if (input.val() == 'novo') {
                    $('.novo-endereco').slideDown('slow');
                } else if (input.val() == '') {
                    $('.novo-endereco').slideUp('fast');
                } else {
                    var address = addresses.filter((data) => {
                        return data['id'] == input.val();
                    })[0];
                    $('.novo-endereco').slideDown('slow');


                    $('#alterar-endereco #types_address_id').val(address.types_address_id).trigger('change');
                    $('#alterar-endereco #postal_code').val(address.postal_code);
                    $('#alterar-endereco #address').val(address.address);
                    $('#alterar-endereco #number').val(address.number);
                    $('#alterar-endereco #neighborhood').val(address.neighborhood);
                    $('#alterar-endereco #address_complement').val(address.address_complement);
                    $('#alterar-endereco #state_id').val(address.state_id).prop('value', address.state_id).trigger('change');
                    $('#alterar-endereco #city_id').val(address.city_id).prop('value', address.city_id).trigger('change');
                }
            })

        });
    </script>
@endpush
