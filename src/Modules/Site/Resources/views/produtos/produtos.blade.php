@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'produtos', 'subtitle' => ''])

@section('content')
    <section data-inpage-section="true" id="home">
        <section class="page-section bg-gray pt-100 pt-sm-100">
            
            <div class="container">
                
                <div class="row">
                    <div class="project-detail-body mt-100 mb-100 clearfix">
                        <div class="col-sm-12 text-center">
                            <h4 class="heading-big fw700 text-color ">
                                Produtos
                            </h4>
                        </div>
                    </div>
                </div>
            </div>
        
        </section>
    </section>
   
    <section class="page-section bg-white pt-100 pb-100">
      
        <!-- PROJECTS-->
        <section class="page-section work-section">
            <div class="container">
                <div class="row">
                    @if(!empty($categorias))
                        <div class="col-md-3">
                            <div class="display-block relative">
                                <ul class="project-detail-content-list">
                                    @foreach($categorias as $categoria)
                                        <li>
                                            <a href="{!! route('site.produtos', [ 'pagina' => 1, 'category' => $categoria->id ] + $_GET) !!}">
                                                <strong>{!! $categoria->title !!}</strong>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    @endif
                    <div class="col-md-9">
                        <!-- Work 3 column grid -->
                        
                        <ul id="work-grid" class="works-grid work-grid-3 work-grid-gut clearfix">
                            <!-- item -->
                            @if(!empty($produtos))
                                @foreach($produtos as $item)
                                    <li class="work-item mix">
                                        <figure class="item-grid-style2">
                                            <div class="work-img">
                                                <a href="{!! route('site.produto', [ 'slug' => $item->slug ]) !!}">
                                                    @if((!empty($item->image_cover) && file_exists(public_path('uploads/'.$item->image_cover))))
                                                        <picture>
                                                            @if((!empty($item->image_mobile_cover) && file_exists(public_path('uploads/'.$item->image_mobile_cover))))
                                                                <source
                                                                    srcset="{{ route('thumb',[ 880, 636, $item->image_mobile_cover, 'ffffff', true]) }}"
                                                                    media="(max-width: 768px)">
                                                            @endif
                                                            
                                                            <img class="img-responsive"
                                                                 src="{{ route('thumb', [ 371, 278, $item->image_cover, 'ffffff', true]) }}"
                                                                 alt="{{$item->title}}">
                                                        </picture>
                                                    @else
                                                        <img src="images/portfolio/project2.png" alt="Work"
                                                             class="width-100">
                                                    @endif
                                                </a>
                                            </div>
                                            <figcaption class="bg-white">
                                                <div class="portfolio-hover-main text-center">
                                                    <div class="portfolio-hover-box vertical-align-middle">
                                                        <div class="portfolio-hover-content position-relative">
                                                            <h3 class="project-name heading-medium fw700 text-color">
                                                                {!! $item->title !!}
                                                            </h3>
                                                            <p class="heading-small mb-0 fw500">
                                                                Código: {!! $item->code !!}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </figcaption>
                                        </figure>
                                    </li>
                                @endforeach
                            @endif
                        </ul>
                        
                        @if(!empty($produtos))
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    @include('site::layouts.paginacao', ['paginator' => $produtos, 'args' => $_GET ])
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </section>
@stop
