@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'index', 'subtitle' => ''])

@section('content')
    <section data-inpage-section="true" id="home">
        <section class="page-section bg-gray pt-100 pt-sm-100">
            <div class="container">
                <div class="row">
                    <div class="project-detail-body mt-100 mb-100 clearfix">
                        <div class="col-sm-12 text-center">
                            <h4 class="heading-big fw700 text-color mb-30 mb-sm-20">
                                {!! $produto->title !!}
                            </h4>
                            <div class="heading-big-subhead fw400 pt-20">
                                {!! $produto->sinopsys !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <section data-inpage-section="true" id="projects">
        <section class="page-section bg-gray">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="case-study-img">
                            <div class="head">
                                <div class="ptn"></div>
                                <div class="ptn"></div>
                                <div class="ptn"></div>
                            </div>
                            <div class="container-screen-slider">
                                <div id="project-detail-screens">

                                    @if((!empty($produto->image_cover) && file_exists(public_path('uploads/'.$produto->image_cover))))
                                        <div class="item">
                                            <picture>
                                                @if((!empty($produto->image_mobile_cover) && file_exists(public_path('uploads/'.$produto->image_mobile_cover))))
                                                    <source
                                                        srcset="{{ route('thumb',[ 880, 636, $produto->image_mobile_cover, 'ffffff', true]) }}"
                                                        media="(max-width: 768px)">
                                                @endif
                                                <img class="img-responsive" alt=""
                                                     src="{{ route('thumb', [ 1910, 0, $produto->image_cover, 'ffffff', true]) }}">
                                            </picture>
                                        </div>
                                    @else
                                        <div class="item">
                                            <img src="images/portfolio/p2-screen1.jpg" alt="" class="width-100"/>
                                        </div>
                                    @endif

                                    @if(!empty($galeria))
                                        @foreach($galeria as $item)
                                            @if((!empty($item->image_cover) && file_exists(public_path('uploads/'.$item->image_cover))))
                                                <div class="item">
                                                    <picture>
                                                        <source
                                                            srcset="{{ route('thumb',[ 880, 636, $item->image_mobile_cover, 'ffffff', true]) }}"
                                                            media="(max-width: 768px)">
                                                        <img class="zoom-gallery lightbox-gallery mfp-image" alt=""
                                                             src="{{ route('thumb', [ 234, 131, $item->image_cover, 'ffffff', true]) }}">
                                                    </picture>
                                                </div>
                                            @else
                                                <div class="item">
                                                    <img src="images/portfolio/p2-screen1.jpg" alt=""/>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="page-section bg-black">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 project-mission">
                        <div class="content" style="background: #212121; border-color: #212121; -webkit-box-shadow: none;-moz-box-shadow: none;box-shadow: none;">
                            <div class="cdr cdr-type">
                                <div class="text text-white">
                                    <h6 class="fw700">Código do Produto</h6>
                                    <p>{!! $produto->code !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @if (!empty($produto->description))
            <section
                class="page-section service-section bg-gray pt-100 pb-100 pt-sm-80 pb-sm-80">
                <div class="container">
                    <div class="row mb-70">
                        <div class="col-sm-8 col-sm-offset-2  text-center">
                            <div class="hero-section last-para-no-margin">
                                <h6 class="heading-big-subhead text-black fw700 mt-0 mb-30 mb-sm-20">
                                    Descrição
                                </h6>
                                <div>
                                    {!! $produto->description !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        @endif
    </section>
@stop

@push('line-js')

    <script type="text/javascript">
        $(function () {

            $(".cores ul li").on('click', function () {
                var cor = $(this).data("cor");
                var cor_valor = $(this).data("valor_cor");
                // $(".imagens .modelo-color").removeClass('active');
                // $("[data-image='" + cor + "']").addClass('active');
                $(".cores ul li:not(:nth-child(" + ($(this).index() + 1) + "))").removeClass('active');
                $(this).addClass('active');
            });

            $(".adicionais ul li").on('click', function () {
                var motor = $(this).data("motor");
                var motor_valor = $(this).data("valor_motor");
                $(this).parents('ul').find('li').removeClass('active');
                $(this).addClass('active');
            });

            $('#compraProduto').off('click').on('click', function () {
                if ($(".cores ul li").length > 0 && $(".cores ul li.active").length <= 0) {
                    swal("Atenção", 'Seleciona uma cor para poder comprar', 'error')
                    return false;
                }
            });

            $(".cores ul li, .adicionais ul li").on('click', function () {
                var soma_cor = parseFloat($(".cores input:checked").parents('li:first').data('value'));
                if (soma_cor == undefined || soma_cor == "") {
                    soma_cor = 0;
                }

                var soma_aditional = 0;
                $(".adicionais ul li input:checked").each(function () {
                    var li = $(this).parents('li:first');
                    if (li.data('value') != undefined) {
                        soma_aditional += parseFloat(li.data('value'));
                    }
                });

                var product_value = parseFloat($('[name="value"]').val());
                var total = product_value + soma_cor + soma_aditional;
                $(".por .valor").html("R$ " + moeda(total));
            });
        });
    </script>

@endpush
