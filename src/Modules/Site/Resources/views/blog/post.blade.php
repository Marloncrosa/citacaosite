@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'index', 'subtitle' => ''])

@section('content')
    
    
    <!-- SECTION HOME -->
    <section id="home" class="page-section pb-140 pb-sm-100 overlay-dark-70">
        <picture>
            @if((!empty($post->image_mobile_cover) && file_exists(public_path('uploads/'.$post->image_mobile_cover))))
                <source
                    srcset="{{ route('thumb',[ 880, 636, $post->image_mobile_cover, 'ffffff', true]) }}"
                    media="(max-width: 768px)">
            @endif
            
            <img class="img-responsive"
                 data-background="{{ route('thumb', [ 371, 278, $post->image_cover, 'ffffff', true]) }}"
                 alt="{{$post->title}}">
        </picture>
        <div class="container relative">
            <!-- INTRO -->
            <div class="row">
                <div class="col-lg-8 col-md-10 col-xs-12 relative pt-140  pt-sm-100">
                    <h2 class="heading-hero fw700 text-white">
                        {!! $post->title !!}
                    </h2>
                    <h3 class="heading-big-subhead fw400 pt-20 text-gray">
                        {!! $post->synopsis !!}
                    </h3>
                    <div class="blog-single-header pt-70 alt-blog-single-header clearfix">
                        <div class="row">
                            <div class="col-sm-4 col-xs-6">
                                <div class="blog-single-info text-left mt-30">
                                    <header>Publicado</header>
                                    <aside class="text-gray">{!! $post->date_time !!}</aside>
                                </div>
                            </div>
                            <div class="col-sm-4 col-xs-6">
                                <div class="blog-single-info text-left mt-30">
                                    <header>Author</header>
                                    <aside class="text-gray">Cedia Theme</aside>
                                
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- SCROLL BOTTOM -->
            <div class="scroll-bottom hidden-xs">
                <svg viewBox="0 0 512 512">
                    <g>
                        <path
                            d="M256,1.6c-87.8,0-159,71.2-159,159v190.7c0,87.8,71.2,158.9,159,158.9c87.8,0,158.9-71.2,158.9-158.9V160.5C414.9,72.7,343.8,1.6,256,1.6z M383.2,351.2c0,70.1-57,127.2-127.1,127.2c-70.1,0-127.2-57-127.2-127.2V160.5c0-70.1,57-127.2,127.2-127.2c70.1,0,127.1,57,127.1,127.2V351.2z"></path>
                        <path
                            d="M256,128.7c-11.7,0-21.2,9.5-21.2,21.2v63.6c0,11.7,9.5,21.2,21.2,21.2c11.7,0,21.2-9.5,21.2-21.2v-63.6C277.2,138.2,267.7,128.7,256,128.7z"></path>
                    </g>
                </svg>
            </div>
        </div>
    </section>
    
    <!-- BLOG SINGLE  -->
    <section id="blogs" class="page-section blog-section pt-140 pb-140 pt-md-100 pb-md-100 pt-sm-70 pb-sm-70">
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        {!! $post->description !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="page-section blog-section pt-140 pt-md-100 pt-sm-70 pb-140 pb-md-100 pb-sm-70">
        @if (!empty($news))
            <div class="container">
                <div class="row mb-40">
                    <div class="col-sm-8 col-sm-offset-2  text-center">
                        <div class="hero-section last-para-no-margin">
                            <h6 class="heading-big-subhead text-black fw700 mt-0 mb-0">
                                Related post
                            </h6>
                        </div>
                    </div>
                </div>
                <div class="row equalize sm-equalize-auto alt-blog-box">
                    @foreach ($news as $post)
                        <div class="col-lg-4 col-md-4 col-sm-6 mt-70">
                            <a href="{{route("site.post",[ 'slug' => $post->slug ])}}" data-link-outer="true"
                               class="blog-item">
                                <div class="blog-item-thumbnail mb-40">
                                    @if((!empty($post->image_cover) && file_exists(public_path('uploads/'.$post->image_cover))))
                                        <picture>
                                            @if((!empty($post->image_mobile_cover) && file_exists(public_path('uploads/'.$post->image_mobile_cover))))
                                                <source
                                                    srcset="{{ route('thumb',[ 880, 636, $post->image_mobile_cover, 'ffffff', true]) }}"
                                                    media="(max-width: 768px)">
                                            @endif
                                            
                                            <img class="img-responsive"
                                                 data-background="{{ route('thumb', [ 371, 278, $post->image_cover, 'ffffff', true]) }}"
                                                 alt="{{$post->title}}">
                                        </picture>
                                    @else
                                        <img src="images/blog/blog1.jpg" alt="Kirsty Intro"/>
                                    @endif
                                </div>
                                <div class="blog-title mb-10 pr-30 pr-sm-0">
                                    <h3 class="heading-big-subhead fw700">{!! $post->title !!}</h3>
                                </div>
                                <div class="blog-title-info">
                                    <div class="fw400 heading-small ">
                                        <span class="fw500">{!! $post->date_time !!}</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </section>
@stop
