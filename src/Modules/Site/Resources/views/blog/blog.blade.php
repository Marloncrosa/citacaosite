@extends('site::layouts.master', [ 'bodyclass' => 'home', 'idMain' => 'blog', 'subtitle' => ' - Blog'])

@section('content')

    <section data-inpage-section="true" id="home">
        <section class="page-section bg-gray pt-100 pt-sm-100">

            <div class="container">
                <div class="row">
                    <div class="project-detail-body mt-100 mb-100 clearfix">
                        <div class="col-sm-12 text-center">
                            <h4 class="heading-big fw700 text-color ">
                                Blog
                            </h4>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </section>

    <section id="blogs" class="page-section blog-section pb-140 pb-md-100 pb-sm-70 pt-100">
        <div class="container">
            <div class="row">
                @if(!empty($categorias))
                    <div class="col-md-3">
                        <div class="display-block relative">
                            <ul class="project-detail-content-list">
                                @foreach($categorias as $categoria)
                                    <li>
                                        <a href="{!! route('site.blog', [ 'pagina' => 1, 'categorieid'=> $categoria->id, 'slugcategoria'=>$categoria->slug ] + $_GET) !!}">
                                            <strong>{!! $categoria->title !!}</strong>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                @endif
                <div class="col-md-9">
                    <ul id="item-grid" class="item-grid item-grid-3 item-grid-gut masonry clearfix">
                    @if (!empty($news))
                        @foreach ($news as $post)
                            <!-- item -->
                                <li class="grid-item alt-blog-box">
                                    <a href="{!! route("site.post",[ 'slug' => $post->slug ]) !!}"
                                       data-link-outer="true"
                                       class="blog-item mt-50 pt-0 pr-0 pl-0 pb-0">
                                        <div class="blog-item-thumbnail mb-40">
                                            @if((!empty($post->image_cover) && file_exists(public_path('uploads/'.$post->image_cover))))

                                                <picture>
                                                    @if((!empty($logo->image_mobile_cover) && file_exists(public_path('uploads/'.$logo->image_mobile_cover))))
                                                        <source
                                                            srcset="{{ route('thumb',[ 880, 636, $post->image_mobile_cover, 'ffffff', true]) }}"
                                                            media="(max-width: 768px)">
                                                    @endif

                                                    <img class="img-responsive"
                                                         src="{{ route('thumb', [ 371, 278, $post->image_cover, 'ffffff', true]) }}"
                                                         alt="{{$post->title}}">
                                                </picture>
                                            @else
                                                <img src="images/blog/blog1.jpg" alt="Kirsty Intro"/>
                                            @endif
                                        </div>
                                        <div class="blog-title mb-10 pr-30 pr-sm-0">
                                            <h3 class="heading-big-subhead fw700">{!! $post->title !!}</h3>
                                        </div>
                                        <div class="blog-title-info">
                                            <div class="fw400 heading-small ">
                                                <span class="fw500">{!! $post->date_time !!}</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        @endif
                    </ul>
                    @if(!empty($news))
                        <div class="row">
                            <div class="col-md-12 text-center">
                                @include('site::layouts.paginacao', ['paginator' => $news, 'args' => $_GET ])
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
@stop
