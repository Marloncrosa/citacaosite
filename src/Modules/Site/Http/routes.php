<?php

Route::group(['middleware' => 'site', 'prefix' => '', 'namespace' => 'Modules\Site\Http\Controllers'], function ($route) {

    $route->get('offline', 'OfflineController@index')->name('site.offline');

    $route->get('', 'SiteController@index')->name("site.home");

    # Institucional
    $route->get('institucional/{slug?}', 'SiteController@institucional')->name("site.institucional");

    # Produtos
    $route->get('produtos/{pagina?}', 'ProdutosController@index')->name("site.produtos");
    $route->get('produto/{slug?}', 'ProdutosController@produto')->name("site.produto");

    # Interesse em produto
    $route->post('tenho-interesse', 'ProdutosController@interesse')->name("site-produto-interesse");

    # Perguntas e Resposta (FAQ)
    $route->get('duvidas-frequentes', 'FaqController@duvidasfrequentes')->name("site.duvidasfrequentes");

    # Mapa do site
    $route->get('notificacao', 'NotificationController@notificacao')->name("site.notificacao");

    # Página de retorno de erro ou sucesso
    $route->get('retorno/{pagina?}/{tipo?}/{titulo?}/{mensagem?}', 'RetornoController@retorno')->name("site.retorno");

    # Contato
    $route->get('contato', 'ContatoController@contato')->name("site.contato");
    $route->post('contato', 'ContatoController@contatopost')->name("site.contato-post");

    $route->post('newslleter', 'SiteController@newslleter')->name("site.newslleter");
    # Dicas
    $route->get('tips/{pagina?}', 'BlogController@tips')->name("site.tips");

    # Cases
    $route->get('cases/{pagina?}', 'BlogController@cases')->name("site.cases");

    # Blog
    $route->get('blog/{pagina?}/{categorieid?}/{slugcategoria?}', 'BlogController@index')->name("site.blog");
    $route->get('post/{slug?}', 'BlogController@post')->name("site.post");

    # Cadastrar o usuário
    $route->get('cadastro', 'Auth\UserController@cadastro')->name("site.cadastro");
    $route->post('cadastro-post', 'Auth\UserController@cadastropost')->name("site.cadastropost");

    # Login
    $route->get('login', 'Auth\LoginController@showLoginForm')->name("site.login");
    $route->post('login', 'Auth\LoginController@login')->name("site.loginpost");

    # Logout
    $route->get('logout', 'Auth\LoginController@logout')->name('site.logout');

    # Resetar senha
    $route->get('resetar-senha/', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('site.resetar-senha.request');
    $route->post('resetar-senha/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('site.resetar-senha.email');
    $route->get('resetar-senha/{token}', 'Auth\ResetPasswordController@showResetForm')->name('site.resetar-senha.reset');
    $route->post('resetar-senha/reset', 'Auth\ResetPasswordController@reset')->name('site.resetar-senha.reset.post');

    # E-mail verificação
    $route->get('email/verificacao', 'Auth\VerificationController@show')->name('site.verification.notice');
    $route->get('email/verificacao/{id}', 'Auth\VerificationController@verify')->name('site.verification.verify');
    $route->get('email/re-enviar', 'Auth\VerificationController@resend')->name('site.verification.resend');

    # Carrinho
    $route->get('carrinho', 'CarrinhoController@carrinho')->name('site-carrinho');

    $route->post('carrinho/add', 'CarrinhoController@add')->name('site-carrinho-add');
    $route->post('carrinho/update', 'CarrinhoController@update')->name('site-carrinho-update');
    $route->get('carrinho/remove/{id}', 'CarrinhoController@remove')->name('site-carrinho-remove');

    # Procurar cep
    $route->post('carrinho/custom-cep', 'CarrinhoController@customcep')->name("site-cart-custom-cep");

    # Cupon
    $route->post('carrinho/cupom', 'CarrinhoController@cupom')->name("site-cupom");

    # Pagseguro
    $route->any('notification/pagseguro', "NotificationController@pagseguro")->name("site-pagseguro-notificacao");

    # Rotas somente logado
    $route->group(['middleware' => ['authSite', 'verifiedSite']], function ($route) {

        $route->get('alterar-email', 'Auth\UserController\UserController@alteraremail')->name("site.alteraremail");
        $route->get('alterar-senha', 'Auth\UserController@alterarsenha')->name("site.alterarsenha");
        $route->post('alterar-senha', 'Auth\UserController@alterarsenhapost')->name("site.alterarsenhapost");

        $route->get('meus-pedidos', 'Auth\UserController@meuspedidos')->name("site.meuspedidos");
        $route->get('meu-pedido/{id}', 'Auth\UserController@meupedido')->name('site.meupedido');

        $route->get('minha-conta', 'Auth\UserController@minhaconta')->name("site.minhaconta");
        $route->post('minha-conta', 'Auth\UserController@minhacontapost')->name('site.minhaconta.post');
        // $route->get('carrinho', 'Auth\UserController@carrinho')->name("site.carrinho");

        $route->get('pagamento', 'PagamentoController@pagamento')->name('site-pagamento');

        $route->get('pagamento-boleto', 'PagamentoBoletoController@index')->name('site-pagamento-boleto');
        $route->post('pagamento-boleto', 'PagamentoBoletoController@post')->name('site-pagamento-boleto-post');

        $route->get('pagamento-cartao', 'PagamentoCartaoController@index')->name('site-pagamento-cartao');
        $route->post('pagamento-cartao', 'PagamentoCartaoController@post')->name('site-pagamento-cartao-post');

        $route->get('pagamento-debito', 'PagamentoDebitoController@index')->name('site-pagamento-debito');
        $route->post('pagamento-debito', 'PagamentoDebitoController@post')->name('site-pagamento-debito-post');

        $route->get('meus-enderecos', 'Auth\UserController@meusenderecos')->name("site.meusenderecos");
        $route->get('meus-enderecos-novo', 'Auth\UserController@meusenderecosnovo')->name("site.meusenderecosnovo");
        $route->get('meus-enderecos-editar/{id?}', 'Auth\UserController@meusenderecoseditar')->name("site.meusenderecoseditar");

        $route->post('meus-enderecos-editar-post/{id?}', 'Auth\UserController@meusenderecoseditarpost')->name("site.meusenderecoseditarpost");
        $route->post('meus-enderecos-post', 'Auth\UserController@meusenderecospost')->name("site.meusenderecospost");
        $route->get('meus-enderecos/remove/{id}', 'Auth\UserController@addressremove')->name("usuario-address-remove");

        $route->get('resposta', 'PagamentoController@resposta')->name('site-resposta');
    });
});
