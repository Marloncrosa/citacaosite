<?php

namespace Modules\Site\Http\Controllers\Auth;

use Carbon\Carbon;
use Modules\Site\Http\Controllers\Controller;
use Modules\Admin\Entities\Clientes;

use Illuminate\Auth\Passwords\TokenRepositoryInterface;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Closure;


use Illuminate\Support\Arr;
use UnexpectedValueException;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Auth\Passwords\DatabaseTokenRepository;
use Illuminate\Support\Facades\Auth;
use DB;
use View;

use Illuminate\Contracts\Hashing\Hasher as HasherContract;

class ResetPasswordController extends Controller
{

    /**
     * Entidade de usuarioas
     *
     * @var \Modules\site\Entities\Usuario
     */
    protected $usuarios ;

    /**
     * The password token repository.
     *
     * @var \Illuminate\Auth\Passwords\TokenRepositoryInterface
     */
    protected $tokens;

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guestSite', [
            'except' => 'logout'
        ]);


        $this -> usuarios = new Clientes();

        $this -> tokens = new DatabaseTokenRepository(
            app('db')-> connection(),
            app('hash'),
            config('auth')['passwords']['site']['table'],
            app('config')['app.key'],
            config('auth')['passwords']['site']['expire']
        );

        // Ativa o log de Query's
        DB::enableQueryLog();

        // Linguagem
        $this->linguagem = 'pt-br';

        app('translator')->setLocale($this->linguagem);
        Carbon::setLocale($this->linguagem);
        setlocale(LC_ALL, Carbon::getLocale());

        View::share('linguagem', $this->linguagem);

        $this->middleware(function ($request, $next) {
            $this->handleS();
            return $next($request);
        });
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('site');
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return PasswordBroker
     */
    protected function broker()
    {
        return Password::broker('site');
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm(Request $request, $token = null)
    {
        return view('site::auth.alterarsenha')->with(
            ['token' => $request->token, 'email' => $request->email]
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(Request $request)
    {
        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        // $response = $this->broker()->reset(
        $response = $this->brokerreset(
            $this->credentials($request),
            function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );


        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
            ? $this->sendResetResponse($response)
            : $this->sendResetFailedResponse($request, $response);
    }


    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }


    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [
            'token.required' => __('validation.required',['attribute' => __("Token")]),
            'email.required' => __('validation.required',['attribute' => __("E-mail")]),
            'email.email' => __('validation.email',['attribute' => __("E-mail")]),
            'password.required' => __('validation.required',['attribute' => __("Senha")]),
            'password.confirmed' => __('validation.confirmed',['attribute' => __("Senha")]),
            'password.min' => __('validation.min.string',['attribute' => __("Senha")])
        ];
    }

    /**
     * Reset the password for the given token.
     *
     * @param  array  $credentials
     * @param  \Closure  $callback
     * @return mixed
     */
    public function brokerreset(array $credentials, Closure $callback)
    {

        // If the responses from the validate method is not a user instance, we will
        // assume that it is a redirect and simply return it from this method and
        // the user is properly redirected having an error message on the post.
        $user = $this->validateReset($credentials);

        if (! $user instanceof CanResetPasswordContract) {
            return $user;
        }

        $password = $credentials['password'];

        // Once the reset has been validated, we'll call the given callback with the
        // new password. This gives the user an opportunity to store the password
        // in their persistent storage. Then we'll delete the token and return.
        $callback($user, $password);

        $user -> password = $password;
        $user -> save();

        $this->tokens->delete($user);

        return Password::PASSWORD_RESET;
    }

    /**
     * Validate a password reset for the given credentials.
     *
     * @param  array  $credentials
     * @return \Illuminate\Contracts\Auth\CanResetPassword
     */
    protected function validateReset(array $credentials)
    {
        if (is_null($user = $this -> usuarios -> getUser($credentials))) {
            return Password::INVALID_USER;
        }

        if (! $this->validateNewPassword($credentials)) {
            return Password::INVALID_PASSWORD;
        }

        if (! $this -> tokens -> exists($user, $credentials['token'])) {
            return Password::INVALID_TOKEN;
        }

        return $user;
    }

    /**
     * Determine if the passwords match for the request.
     *
     * @param  array  $credentials
     * @return bool
     */
    public function validateNewPassword(array $credentials)
    {
        if (isset($this->passwordValidator)) {
            list($password, $confirm) = [
                $credentials['password'],
                $credentials['password_confirmation'],
            ];

            return call_user_func(
                    $this->passwordValidator,

                $credentials
                ) && $password === $confirm;
        }

        return $this->validatePasswordWithDefaults($credentials);
    }

    /**
     * Determine if the passwords are valid for the request.
     *
     * @param  array  $credentials
     * @return bool
     */
    protected function validatePasswordWithDefaults(array $credentials)
    {
        list($password, $confirm) = [
            $credentials['password'],
            $credentials['password_confirmation'],
        ];

        return $password === $confirm && mb_strlen($password) >= 6;
    }

    /**
     * Determine if a token record exists and is valid.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $token
     * @return bool
     */
    public function exists(CanResetPasswordContract $user, $token)
    {
        $conection = $this -> tokens -> getConnection();

        $record = (array) $conection -> table(config('auth')['passwords']['site']['table'])
            -> where(
                'email',
                $user->getEmailForPasswordReset()
            )
            ->first()
        ;

        $haser = $this -> tokens -> getHasher();

        return $record &&
            ! $this->tokenExpired($record['created_at']) &&
            $haser->check($token, $record['token']);
    }


    /**
     * Determine if the token has expired.
     *
     * @param  string  $createdAt
     * @return bool
     */
    protected function tokenExpired($createdAt)
    {
        return Carbon::parse($createdAt)->addSeconds(config('auth')['passwords']['site']['expire'])->isPast();
    }
}
