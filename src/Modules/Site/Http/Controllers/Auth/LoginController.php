<?php

namespace Modules\Site\Http\Controllers\Auth;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\ValidationException;
use Modules\Site\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use View;
use DB;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    use AuthenticatesUsers;

    protected $redirectAfterLogout = '/login';
    protected $redirectTo = '/meus-dados';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guestSite', ['except' => 'logout']);

        // Linguagem
        app('translator')->setLocale('pt-br');

        // Ativa o log de Query's
        DB::enableQueryLog();

        // Linguagem
        $this->linguagem = 'pt-br';

        app('translator')->setLocale($this->linguagem);
        Carbon::setLocale($this->linguagem);
        setlocale(LC_ALL, Carbon::getLocale());

        $this->middleware(function ($request, $next) {
            $this->handleS();
            return $next($request);
        });
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('site');
    }


    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $this->template = 'auth.login';
        return $this->renderizar();
    }


    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate(
            $request,
            [
                'usuario' => 'required|string',
                'password' => 'required|string',
            ],
            [
                'usuario.required' => __('validation.required', ['attribute' => __("Usuário")]),
                'usuario.string' => __('validation.string', ['attribute' => __("Usuário")]),
                'password.required' => __('validation.required', ['attribute' => __("Senha")]),
                'password.string' => __('validation.string', ['attribute' => __("Senha")]),
            ]
        );
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }


    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request)
    {
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);

        return $this->authenticated($request, $this->guard()->user())
            ?: redirect()->intended($this->redirectPath());
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  mixed $user
     * @return mixed
     */
//	protected function authenticated(Request $request, $user)
//	{        // Pega a url que ele pretende ir e valida se tem o idioma
    // $sessionIntend = session('url.intended');
    //
    // $this->redirectTo = (session()->has('url.intended') && !empty(session('url.intended')))? $sessionIntend : '/';
    //
    // session()->forget('url.intended');
//	}

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    // public function redirectPath()
    // {
    //     if (method_exists($this, 'redirectTo')) {
    //         return $this->redirectTo();
    //     }
    //
    //     return property_exists($this, 'redirectTo') ? $this->redirectTo : '/minha-conta';
    // }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        return 'name';
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request)
    {
        return (
        $this->guard()->attempt([
            'name' => $request->usuario,
            'password' => $request->password,
            'active' => true
        ], $request->input('mantenhaconectado', true))
        );
    }


    /**
     * Logout, Clear Session, and Return.
     *
     * @return void
     */
    public function logout(Request $request)
    {
        $errors = $request->session()->get('errors');
        $error = $request->session()->get('error');

        $this->guard()->logout();
        $request->session()->invalidate();
        $request->session()->regenerate();

        $request->flash('errors', $errors);
        $request->flash('error', $error);

        if (!empty($errors) || !empty($error)) {
            return redirect()->route('site.login')->with('errors', $errors)->with('error', $error);
        }

        return redirect()->route('site.login');
    }


    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws ValidationException
     */
//      protected function sendFailedLoginResponse(Request $request)
//      {
//          // Verifica qual tipo de mensagem será enviada
//          $user = Clientes::withoutGlobalScopes()->where("email", $request->email)->first();
//
//          // Verifica se usuário está ativo
//         if (is_null($user) || !isset($user->ativo) || $user->ativo == true) { // Se sim, erro de senha ou e-mail inválido
//             throw ValidationException::withMessages([
//                 'error' => [trans('auth.failed')],
//             ]);
//         } else { // Caso não esteja ativo
//             throw ValidationException::withMessages([
//                 'error' => [__("Usuário inativo, para maiores informações entre em contato com um administrador.")],
//             ]);
//         }
//     }
}
