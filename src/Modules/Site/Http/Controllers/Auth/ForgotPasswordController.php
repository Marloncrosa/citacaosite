<?php

namespace Modules\Site\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Modules\Site\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Modules\Site\Repositories\DatabaseTokenRepository;
use DB;
use Carbon\Carbon;
use View;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * The password token repository.
     *
     * @var \Illuminate\Auth\Passwords\TokenRepositoryInterface
     */
    protected $tokens;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guestSite', [
            'except' => 'logout',
        ]);

        $this->tokens = new DatabaseTokenRepository(app('db')->connection(), app('hash'),
            config('auth')['passwords']['site']['table'], app('config')['app.key'],
            config('auth')['passwords']['site']['expire']);

        // Ativa o log de Query's
        DB::enableQueryLog();

        // Linguagem
        $this->linguagem = 'pt-br';

        app('translator')->setLocale($this->linguagem);
        Carbon::setLocale($this->linguagem);
        setlocale(LC_ALL, Carbon::getLocale());

        View::share('linguagem', $this->linguagem);

        $this->tokens = new DatabaseTokenRepository(app('db')->connection(), app('hash'),
            config('auth')['passwords']['site']['table'], app('config')['app.key'],
            config('auth')['passwords']['site']['expire']);

        $this->middleware(function ($request, $next) {
            $this->handleS();
            return $next($request);
        });
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return PasswordBroker
     */
    protected function broker()
    {
        return Password::broker('site');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm(Request $request)
    {
        return view('site::auth.passwords.email');
    }


    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendResetLinkEmail(Request $request)
    {
        // $this->validateEmail($request);
        $this->validate($request, $this->rules(), $this->validationErrorMessages());

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $response = $this->sendResetLink(['email' => $request->email]);

        return $response == Password::RESET_LINK_SENT ? $this->sendResetLinkResponse($response)
            : $this->sendResetLinkFailedResponse($request, $response);
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'email' => 'required|email',
        ];
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [
            'email.required' => __('validation.required',['attribute' => __("E-mail")]),
            'email.email' => __('validation.email',['attribute' => __("E-mail")]),
        ];
    }


    /**
     * Send a password reset link to a user.
     *
     * @param  array $credentials
     *
     * @return string
     */
    public function sendResetLink(array $credentials)
    {
        // First we will check to see if we found a user at the given credentials and
        // if we did not we will redirect back to this current URI with a piece of
        // "flash" data in the session to indicate to the developers the errors.
        $user = $this->broker()->getUser($credentials);

        if (is_null($user)) {
            return Password::INVALID_USER;
        }

        $token = $this->getRepository()->create($user);

        // Once we have the reset token, we are ready to send the message out to this
        // user with a link to reset their password. We will then redirect back to
        // the current URI having nothing set in the session to indicate errors.
        $user->sendPasswordResetNotificationNew($token, $user);

        return Password::RESET_LINK_SENT;
    }


    /**
     * Get the password reset token repository implementation.
     *
     * @return \Illuminate\Auth\Passwords\TokenRepositoryInterface
     */
    public function getRepository()
    {
        return $this->tokens;
    }
}
