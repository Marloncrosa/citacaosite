<?php

namespace Modules\Site\Http\Controllers\Auth;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Modules\Admin\Entities\Address;
use Modules\Admin\Entities\Cart;
use Modules\Admin\Entities\CartItens;
use Modules\Admin\Entities\Clients;
use Modules\Admin\Entities\Country;
use Modules\Admin\Entities\News;
use Modules\Admin\Entities\NewsCategories;
use Modules\Admin\Entities\OrderStatus;
use Modules\Admin\Entities\Products;
use Modules\Admin\Entities\State;
use Modules\Site\Http\Controllers\Controller;
use Modules\Site\Http\Requests\AlterarSenhaRequest;
use Modules\Site\Http\Requests\CadastroRequest;
use Modules\site\Http\Requests\MeusEnderecosRequest;
use Modules\site\Http\Requests\MinhaContaRequest;

class UserController extends Controller
{
    public function index($pagina = 1, $slugcategoria = null)
    {
        //        $this->view_vars['categorias'] = NewsCategories::where('active', true)->get();
        //
        //        // Seta o paginador
        //        Paginator::currentPageResolver(function () use ($pagina) {
        //            return $pagina;
        //        });
        //
        //        $news = News::where('active',true)
        //            ->whereRaw('date_time <= NOW()')
        //            ->orderBy('date_time', 'desc')
        //            ->orderBy('id', 'desc');
        //
        //        // $this->view_vars['product'] = Products::where('active', true)
        //        //     ->orderByRaw('rand()')
        //        //     ->first();
        //
        //        //        dd( $news->get() );
        //
        //        // if (!is_null($slugcategoria)) {
        //        //     $news = $news->where('categories.slug', $slugcategoria);
        //        // }
        //
        //        $news = $news->paginate(4);
        //
        //        $this->view_vars['slugcategoria'] = $slugcategoria;
        //        $this->view_vars['news'] = $news;
        //
        //        //        $this->view_vars['latest_news'] = News::where('active', true)
        //        //            ->whereRaw('date_time <= NOW()')
        //        //            ->orderBy('date_time', 'desc')
        //        //            ->limit(8)
        //        //            ->get();
        //
        //
        //        $this->template = 'blog.blog';
        return $this->renderizar();
    }

    public function meusenderecos()
    {
        $principal_address = auth('site')->user();

        $this->view_vars['principal_address'] = $principal_address;

        $this->view_vars['other_address'] = Address::where('client_id', auth('site')->user()->id)->get();
        $this->template = 'usuario.meus-enderecos';
        return $this->renderizar();
    }

    public function meusenderecosnovo()
    {
        $this->view_vars['FormModel'] = new Address();
        $this->view_vars['url'] = 'site.meusenderecospost';
        $this->view_vars['pais'] = Country::where('initials', 'BR')->first();
        $this->view_vars['estado'] = State::where('country_id', $this->view_vars['pais']->id)->get();
        //
        $this->template = 'usuario.meus-enderecos-novo';
        return $this->renderizar();
    }

    public function meusenderecoseditar($id)
    {

        $this->view_vars['FormModel'] = $FormModel =  Address::where('id', $id)->first();
        $this->view_vars['url'] = route('site.meusenderecoseditarpost', ['id' => $FormModel]);
        $this->view_vars['pais'] = Country::where('initials', 'BR')->first();
        $this->view_vars['estado'] = State::where('country_id', $this->view_vars['pais']->id)->get();
        //
        $this->template = 'usuario.meus-enderecos-novo';
        return $this->renderizar();
    }

    public function meusenderecospost(MeusEnderecosRequest $request)
    {
        $address = new Address();
        $address->client_id = auth('site')->user()->id;
        // Preenche o model com o post
        $address->fill($request->all());

        // Se salvar o item
        if ($address->save()) {
            return redirect()->route('site.minhaconta')->with('sucesso', $address->name . ', seus dados alterados com sucesso!');
        } else {
            return back()->withErrors($address->erros())->withInput();
        }
    }

    public function meusenderecoseditarpost(MeusEnderecosRequest $request, $id)
    {
        $address = Address::where('id', $id)->first();
        $address->client_id = auth('site')->user()->id;
        // Preenche o model com o post
        $address->fill($request->all());

        // Se salvar o item
        if ($address->save()) {
            return redirect()->route('site.minhaconta')->with('sucesso', $address->name . ', seus dados alterados com sucesso!');
        } else {
            return back()->withErrors($address->erros())->withInput();
        }
    }

    public function addressremove($id)
    {
        //        $product = Products::where('slug', $id)->whereActive(true)->first();
        //        if (is_null($product)) {
        //            return redirect()->back()->with('error', __(':text não encontrado', ['text' => __('Produto')]));
        //        }

        request()->session()->forget("item_{$id}");

        $address = Address::where('id', $id)->first();

        if (!is_null($address)) {
            $address->delete();
            return redirect()->back()->with('success', __(':text removido', ['text' => __('Endereço')]));
        } else {
            return redirect()->back()->with('error', __(':text não encontrado', ['text' => __('Endereço')]));
        }
    }

    public function meuspedidos()
    {
        //
        $pedidos = Cart::orderByDesc('id')
            ->where('client_id', auth('site')->user()->id)
            ->with([
                'payment_method',
                'delivery_address',
                'order_status',
                //                'cart_itens',
            ])
            ->get();
        $this->view_vars['pedidos'] = $pedidos;
        $this->view_vars['status'] = OrderStatus::where('active', true)
            ->get();
        //
        $this->template = 'usuario.meus-pedidos';
        return $this->renderizar();
    }

    public function meupedido($id = null)
    {
        $pedido = Cart::where('id', $id)->first();
        $cart_item = CartItens::where('cart_id', $id)->get();


        $this->view_vars['pedido'] = $pedido;
        $this->view_vars['cart_item'] = $cart_item;

        // dump($pedido);
        // die;

        $this->template .= 'usuario.meu-pedido';
        return $this->renderizar();
    }

    public function minhaconta()
    {
        $this->view_vars['FormModel'] = auth('site')->user();
        $this->view_vars['usuario'] = Clients::where('active', true)->where('id', auth('site')->user()->id)->first();
        $this->view_vars['pais'] = Country::where('initials', 'BR')->first();
        $this->view_vars['estado'] = State::where('country_id', $this->view_vars['pais']->id)->get();
        //
        $this->template = 'usuario.meus-dados';
        return $this->renderizar();
    }

    public function minhacontapost(MinhaContaRequest $request)
    {
        $user = auth('site')->user();

        // Preenche o model com o post
        $user->fill($request->all());

        // Se salvar o item
        if ($user->save()) {
            return redirect()->route('site.minhaconta')->with('sucesso', $user->name . ', seus dados alterados com sucesso!');
        } else {
            return back()->withErrors($user->erros())->withInput();
        }
    }


    public function carrinho()
    {
        $this->view_vars['usuario'] = Clients::where('active', true)->where('id', auth('site')->user()->id)->first();
        //        $this->view_vars['pagename'] = 'site.blog';
        //
        //        $post = News::where('slug', $slug)
        //            ->where('active',true)
        //            ->first();
        //
        //        if (is_null($post)) {
        //            return redirect()->route('site.home', [ 'lang' => $this->linguagem ])->with('error', __('Post not found'));
        //        }
        //
        //        //        $prev_post = News::where('id', '<', $post->id)->ativoelinguagem($this->idioma->id)->orderBy('id', 'desc')->first();
        //        //
        //        //        $next_post = News::where('id', '>', $post->id)->ativoelinguagem($this->idioma->id)->first();
        //
        //        $this->view_vars['post'] = $post;
        //
        //        //        $this->view_vars['prev_post'] = $prev_post;
        //        //        $this->view_vars['next_post'] = $next_post;
        //
        //        $this->view_vars['metatitle'] = $post->title;
        //        $this->view_vars['metaurl'] = route("site.post",[ 'slug' => $slug ]);
        //        $this->view_vars['metadescription'] = (!empty($post->metadescription))? $post->metadescription : $post->synopsis;
        //        if((!empty($post->image_cover) && file_exists(public_path('uploads/'.$post->image_cover)))){
        //            $this->view_vars['imageheader'] = route('thumbfit',[ 770, 480, $post->image_cover, 'ffffff', true]);
        //        }
        //
        //
        $this->template = 'usuario.minha-conta';
        return $this->renderizar();
    }

    public function pagamentos()
    {
        $this->view_vars['usuario'] = Clients::where('active', true)->where('id', auth('site')->user()->id)->first();
        //        $this->view_vars['pagename'] = 'site.blog';
        //
        //        $post = News::where('slug', $slug)
        //            ->where('active',true)
        //            ->first();
        //
        //        if (is_null($post)) {
        //            return redirect()->route('site.home', [ 'lang' => $this->linguagem ])->with('error', __('Post not found'));
        //        }
        //
        //        //        $prev_post = News::where('id', '<', $post->id)->ativoelinguagem($this->idioma->id)->orderBy('id', 'desc')->first();
        //        //
        //        //        $next_post = News::where('id', '>', $post->id)->ativoelinguagem($this->idioma->id)->first();
        //
        //        $this->view_vars['post'] = $post;
        //
        //        //        $this->view_vars['prev_post'] = $prev_post;
        //        //        $this->view_vars['next_post'] = $next_post;
        //
        //        $this->view_vars['metatitle'] = $post->title;
        //        $this->view_vars['metaurl'] = route("site.post",[ 'slug' => $slug ]);
        //        $this->view_vars['metadescription'] = (!empty($post->metadescription))? $post->metadescription : $post->synopsis;
        //        if((!empty($post->image_cover) && file_exists(public_path('uploads/'.$post->image_cover)))){
        //            $this->view_vars['imageheader'] = route('thumbfit',[ 770, 480, $post->image_cover, 'ffffff', true]);
        //        }
        //
        //
        $this->template = 'usuario.pagamentos';
        return $this->renderizar();
    }


    public function pagamentosboletos()
    {
        $this->view_vars['usuario'] = Clients::where('active', true)->where('id', auth('site')->user()->id)->first();
        //        $this->view_vars['pagename'] = 'site.blog';
        //
        //        $post = News::where('slug', $slug)
        //            ->where('active',true)
        //            ->first();
        //
        //        if (is_null($post)) {
        //            return redirect()->route('site.home', [ 'lang' => $this->linguagem ])->with('error', __('Post not found'));
        //        }
        //
        //        //        $prev_post = News::where('id', '<', $post->id)->ativoelinguagem($this->idioma->id)->orderBy('id', 'desc')->first();
        //        //
        //        //        $next_post = News::where('id', '>', $post->id)->ativoelinguagem($this->idioma->id)->first();
        //
        //        $this->view_vars['post'] = $post;
        //
        //        //        $this->view_vars['prev_post'] = $prev_post;
        //        //        $this->view_vars['next_post'] = $next_post;
        //
        //        $this->view_vars['metatitle'] = $post->title;
        //        $this->view_vars['metaurl'] = route("site.post",[ 'slug' => $slug ]);
        //        $this->view_vars['metadescription'] = (!empty($post->metadescription))? $post->metadescription : $post->synopsis;
        //        if((!empty($post->image_cover) && file_exists(public_path('uploads/'.$post->image_cover)))){
        //            $this->view_vars['imageheader'] = route('thumbfit',[ 770, 480, $post->image_cover, 'ffffff', true]);
        //        }
        //
        //
        $this->template = 'usuario.pagamentos-boleto';
        return $this->renderizar();
    }


    public function pagamentoscartao()
    {
        $this->view_vars['usuario'] = Clients::where('active', true)->where('id', auth('site')->user()->id)->first();
        //        $this->view_vars['pagename'] = 'site.blog';
        //
        //        $post = News::where('slug', $slug)
        //            ->where('active',true)
        //            ->first();
        //
        //        if (is_null($post)) {
        //            return redirect()->route('site.home', [ 'lang' => $this->linguagem ])->with('error', __('Post not found'));
        //        }
        //
        //        //        $prev_post = News::where('id', '<', $post->id)->ativoelinguagem($this->idioma->id)->orderBy('id', 'desc')->first();
        //        //
        //        //        $next_post = News::where('id', '>', $post->id)->ativoelinguagem($this->idioma->id)->first();
        //
        //        $this->view_vars['post'] = $post;
        //
        //        //        $this->view_vars['prev_post'] = $prev_post;
        //        //        $this->view_vars['next_post'] = $next_post;
        //
        //        $this->view_vars['metatitle'] = $post->title;
        //        $this->view_vars['metaurl'] = route("site.post",[ 'slug' => $slug ]);
        //        $this->view_vars['metadescription'] = (!empty($post->metadescription))? $post->metadescription : $post->synopsis;
        //        if((!empty($post->image_cover) && file_exists(public_path('uploads/'.$post->image_cover)))){
        //            $this->view_vars['imageheader'] = route('thumbfit',[ 770, 480, $post->image_cover, 'ffffff', true]);
        //        }
        //
        //
        $this->template = 'usuario.pagamentos-cartao';
        return $this->renderizar();
    }

    public function cadastro()
    {
        $this->view_vars['FormModel'] = Clients::class;
        $this->view_vars['state'] = State::where('country_id', 30)->get();
        $this->template = 'auth.cadastro';
        return $this->renderizar();
    }

    public function cadastropost(CadastroRequest $request)
    {
        \DB::enableQueryLog();

        $cliente = new Clients($request->all());

        // Auth em clientes
        $auth = auth('site');

        if ($cliente->save()) {
            $cliente->active = true;
            $cliente->save();

            // Após cadastrar já loga
            $auth->attempt([
                'email' => $this->request->email,
                'password' => $this->request->password,
            ], true);
            return redirect()->route('site.home')->with('sucesso', 'Seja Bem-Vindo, ' . $this->request->name);
        } else {
            return back()->withErrors($cliente->errors())->withInput();
        }
    }

    public function alterarsenha()
    {
        $this->view_vars['model'] = auth('site')->user();
        $this->template .= 'usuario.minhas-senhas';
        return $this->renderizar();
    }

    /**
     * @param AlterarSenhaRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function alterarsenhapost(AlterarSenhaRequest $request)
    {
        $input = $request->all();

        // if (!Hash::check($input['password_old'], Auth::user()->password)) {
        //     return redirect()->route('fornecedor.alterarsenha')->withErrors(['password' => 'Senha atual está incorreta'])->withInput();
        // }

        // Seta a classe em uso
        $cliente = auth('site')->user();
        //        dd($cliente);
        // Preenche o model com o post
        $cliente = $cliente->fill([
            'password' => $request->password
        ]);

        // Se salvar o item
        if ($cliente->save()) {
            return redirect()->route('site.alterarsenha')->with('success', $cliente->name . ', sua senha foi alterada com sucesso!');
        } else {
            return back()->withErrors($cliente->erros())->withInput();
        }

    }
}
