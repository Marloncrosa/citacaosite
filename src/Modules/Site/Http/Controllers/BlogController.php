<?php

namespace Modules\Site\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Modules\Admin\Entities\Banners;
use Modules\Admin\Entities\News;
use Modules\Admin\Entities\NewsCategories;
use Modules\Admin\Entities\Products;

//use Modules\Admin\Entities\Products;

class BlogController extends Controller
{
    public function index($pagina = 1,  $categorieid, $slugcategoria = null)
    {
        $categorias = NewsCategories::where('active', true)->get();
        $this->view_vars['categorias'] = $categorias;

        // Seta o paginador
        Paginator::currentPageResolver(function () use ($pagina) {
            return $pagina;
        });

        $news = News::whereActive(true)
            ->whereRaw('date_time <= NOW()')
            ->orderBy('date_time', 'desc')
            ->orderBy('id', 'desc');

        $listas = NewsCategories::where('active', true);

// dd(request()->news_categorie_id);
        // if (request()->has('news_categorie_id')) {
        //     $listas = $listas->where('news_categorie_id', request()->news_categorie_id);
        //
        // }
        if (!empty($categorieid)) {
            $news = $news->where('news_categorie_id', $categorieid);
        }

       $this->view_vars['produto'] = Products::where('active', true)
           ->orderByRaw('rand()')
           ->first();

        $news = $news->paginate(12);


        $banners = Banners::where('active', true)->get();
        $this->view_vars['banners'] = $banners;


        $this->view_vars['slugcategoria'] = $slugcategoria;
        $this->view_vars['news'] = $news;

        //
        // $categorias = NewsCategories::where('active', true)->get();
        // $this->view_vars['categorias'] = $categorias;
        //

        $this->template = 'blog.blog';
        return $this->renderizar();

    }

    public function post($slug)
    {
        $this->view_vars['pagename'] = 'site.blog';

        $post = News::where('slug', $slug)
            ->whereActive(true)
            ->first();

        $news = News::where('active', true)
            ->where('id', '!=', $post->id)
            ->get();

        if (is_null($post)) {
            return redirect()->route('site-home', ['lang' => $this->linguagem])->with('error', __('Post not found'));
        }

//        $prev_post = News::where('id', '<', $post->id)->ativoelinguagem($this->idioma->id)->orderBy('id', 'desc')->first();
//
//        $next_post = News::where('id', '>', $post->id)->ativoelinguagem($this->idioma->id)->first();

        $this->view_vars['post'] = $post;
        $this->view_vars['news'] = $news;

//        $this->view_vars['prev_post'] = $prev_post;
//        $this->view_vars['next_post'] = $next_post;

        $this->view_vars['metatitle'] = $post->title;
        $this->view_vars['metaurl'] = route("site.post", ['slug' => $slug]);
        $this->view_vars['metadescription'] = (!empty($post->metadescription)) ? $post->metadescription : $post->synopsis;
        if ((!empty($post->image_cover) && file_exists(public_path('uploads/' . $post->image_cover)))) {
            $this->view_vars['imageheader'] = route('thumbfit', [770, 480, $post->image_cover, 'ffffff', true]);
        }

        $this->template = 'blog.post';
        return $this->renderizar();
    }
}
