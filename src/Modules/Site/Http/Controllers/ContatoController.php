<?php

namespace Modules\Site\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use Modules\Admin\Entities\Banners;
use Modules\Admin\Entities\Benefits;
use Modules\Admin\Entities\City;
use Modules\Admin\Entities\Configuration;
use Modules\Admin\Entities\Contact;
use Modules\Admin\Entities\ContactSubjects;
use Modules\Admin\Entities\News;
use Modules\Admin\Entities\Products;
use Modules\Admin\Entities\Breeds;
use Modules\Admin\Entities\State;
use Modules\Admin\Entities\WhoWeAre;
use Modules\Admin\Entities\YourBrand;
use Modules\Site\Http\Requests\ContatoRequest;
use Modules\Site\Http\Requests\InteresseRequest;

class ContatoController extends Controller
{
    public function contato()
    {
        $model = new Contact();

        $this->view_vars['model'] = $model;
        $this->view_vars['subjects'] = ContactSubjects::where('active', true)->get();

        $this->template = 'contato';
        return $this->renderizar();
    }

    public function contatopost(ContatoRequest $request)
    {
        $contact = new Contact($request->all());
        $contact->contact_subjects_id = 1;

        $contact->message = 'Contato enviado pelo site <br>'
            .' Nome: '. $request->name .' <br>'
            .'E-mail: '. $request->email .' <br>'
            .'Celular: '. $request->cellpgone .' <br>'
            .'Mensagem: '. $request->message .' <br>'
        ;


        if ($contact->save()) {
            // Enviar o e-mail
            $assunto = __("Fale Conosco"); // assunto
            $linha = __("Você recebeu um contato enviado pelo site ") . env('APP_SITE_TITLE'); // linha inicial

            // Dados do e-mail
            $emaildata = collect([
                'assunto' => $assunto,
                'saudacao' => __("Olá, ") . env('APP_SITE_TITLE'),
                'introLines' => $linha,
                'dados' => $request
            ]);

            // E-mail para o contato
            $enviou = json_decode(enviaEmail(
                $emaildata,
                true,
                false,
                'site::notifications.faleconosco',
                null,
                false,
                [
                    'sender' => ['name' => $contact->name],
                    // 'to' => ['name' => 'Felipe', 'email' => 'felipe@aireset.com.br'],
                    'cc' => [
                        ['name' => 'Comercial', 'email' => 'comercial2@argen.com.br'],
                        ['name' => 'Delmiro', 'email' => 'delmiro@argen.com.br'],
                    ]
                ]
            ));

            if ($enviou->success) {

                // Dados do e-mail
                $emaildata = collect([
                    'assunto' => $assunto,
                    'saudacao' => __("Olá, ") . $contact->name,
                    'introLines' => [
                        'Você enviou um contato pelo '. env('APP_SITE_TITLE')
                    ],
                    'dados' => $request
                ]);

                // E-mail para o contato
                enviaEmail(
                    $emaildata,
                    false,
                    true,
                    'site::notifications.faleconosco',
                    null,
                    false,
                    [
                        // 'sender' =>[ 'email' => '', 'name' => '' ]
                    ]
                );

                return back()->with('success', $enviou->message)->withInput();
            } else {
                return back()->with('error', 'Mensagem enviada com sucesso mas, ' . $enviou->message)->withInput();
            }

            return back()->with('success', 'Mensagem enviada com sucesso!')->withInput();
        }

        return redirect()->route('site-contato')->with('success', __("Contato enviado! Em breve iremos entrar em contato."));

    }

    public function interessepost(InteresseRequest $request)
    {
        $contact = new Contact($request->all());
        $contact->contact_subjects_id = 2;
        $product = Products::where('id', $request->product_id)->first();
        $state = State::where('id', $request->state_id)->first();
        $city = City::where('id', $request->city_id)->first();

        $contact->message = 'O cliente possui interesse no animal: '.$product->name.'<br>'
            .' Nome: '. $request->name .' <br>'
            .'E-mail: '. $request->email .' <br>'
            .'Celular: '. $request->cellpgone .' <br>'
            .'Estado: '. $state->state .' <br>'
            .'Cidade: '. ucfirst(strtolower($city->city)) .' <br>'
            .'Mensagem: '. $request->message .' <br>'
        ;

        if ($contact->save()) {
            // Enviar o e-mail
            $assunto = __("Interesse"); // assunto
            $linha = [
                __("Você recebeu um contato enviado pelo site ") . env('APP_SITE_TITLE'),
                "O cliente abaixo, possui interesse no animal: ".$product->name,
            ];
            $linha2 = [
            ];

            // Dados do e-mail
            $emaildata = collect([
                'assunto' => $assunto,
                'saudacao' => __("Olá, ") . env('APP_SITE_TITLE'),
                'introLines' => $linha,
                'outroLines' => $linha2,
                'dados' => [
                    'state' => $state->state,
                    'city' => ucfirst(strtolower($city->city)),
                ]+$request->all()
            ]);

            // E-mail para o site
            $enviou = json_decode(enviaEmail(
                $emaildata,
                true,
                false,
                'site::notifications.faleconosco',
                null,
                false,
                [
                    'sender' => ['name' => $contact->name],
                    // 'to' => ['name' => 'Felipe', 'email' => 'felipe@aireset.com.br'],
                    'cc' => [
                        ['name' => 'Comercial', 'email' => 'comercial2@argen.com.br'],
                        ['name' => 'Delmiro', 'email' => 'delmiro@argen.com.br'],
                    ]
                ]
            ));

            if ($enviou->success) {

                // Dados do e-mail
                $emaildata = collect([
                    'assunto' => $assunto,
                    'saudacao' => __("Olá, ") . $contact->name,
                    'introLines' => [
                        'Você enviou um contato pelo site '. env('APP_SITE_TITLE').' de interesse no animal: '.$product->name,
                        'Em breve entraremos em contato, abaixo segue os dados que informou.'
                    ],
                    'dados' => $request
                ]);

                // E-mail para o contato
                enviaEmail(
                    $emaildata,
                    false,
                    true,
                    'site::notifications.faleconosco',
                    null,
                    false,
                    [
                        // 'sender' =>[ 'email' => '', 'name' => '' ]
                    ]
                );

                return back()->with('success', $enviou->message)->withInput();
            } else {
                return back()->with('error', 'Mensagem enviada com sucesso mas, ' . $enviou->message)->withInput();
            }
        }

        return redirect()->route('site-contato')->with('success', __("Contato enviado! Em breve iremos entrar em contato."));

    }
}
