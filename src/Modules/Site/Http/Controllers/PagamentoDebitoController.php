<?php

namespace Modules\Site\Http\Controllers;

use Claudsonm\CepPromise\CepPromise;
use Claudsonm\CepPromise\Exceptions\CepPromiseException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Admin\Entities\Address;
use Modules\Admin\Entities\Aditionals;
use Modules\Admin\Entities\CartItens;
use Modules\Admin\Entities\City;
use Modules\Admin\Entities\Colors;
use Modules\Admin\Entities\Country;
use Modules\Admin\Entities\Coupons;
use Modules\Admin\Entities\Products;
use Darryldecode\Cart\Facades\CartFacade as Cart;
use Modules\Admin\Entities\State;
use PagSeguro;

class PagamentoDebitoController extends PagseguroController
{
    public function index()
    {
        // Verifica se tem carrinho
        if (Cart::isEmpty()) {
            return redirect()->route('site-produtos')->withErrors('Adicione um item ao carrinho.');
        }

        $this->template .= 'debito';

        if (env('APP_ENV') == 'local') {
            $cart = $this->saveCart(1, false);
        } else {
            $cart = $this->saveCart(1);
        }

        return $this->renderizar();
    }

    public function post()
    {
        $this->view_vars['link'] = null;

        $cart = $this->pagseguroInit();
        if(!empty($this->pagseguroCart)){
            $this->pagamentoDebito($cart);
        }

        $this->template .= 'debito-link';
        return $this->renderizar();
    }

    function xml2array($xmlObject, $out = array())
    {
        foreach ((array)$xmlObject as $index => $node)
            $out[$index] = (is_object($node)) ? $this->xml2array($node) : $node;

        return $out;
    }


    public function resposta()
    {
        Cart::clearCartConditions();
        Cart::clear();
        $this->template .= 'resposta';
        return $this->renderizar();
    }

}
