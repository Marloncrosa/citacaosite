<?php namespace Modules\Site\Http\Controllers;

use Carbon\Carbon;
use Darryldecode\Cart\Facades\CartFacade as Cart;
use Illuminate\Support\Facades\Auth;
use Modules\Admin\Entities\Address;
use Modules\Admin\Entities\Configuration;
use Modules\Admin\Entities\Contact;
use Modules\Admin\Entities\Country;
use Modules\Admin\Entities\Institutional;
use Modules\Admin\Entities\ProductsCategories;
use Modules\Admin\Entities\State;
use View;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Request as Urlrequest;

/**
 * Class Controller
 *
 * @package Modules\Sistema\Http\Controllers
 */
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $linguagem;

    /**
     * Configuracao
     *
     * @var
     */
    protected $config;

    /**
     * Request
     *
     * @var \Illuminate\Http\Request
     */
    protected $request;

    /**
     * Routes
     *
     * @var \Illuminate\Http\Route
     */
    protected $rotas;

    /**
     * Action
     *
     * @var Armazena a action atual
     */
    protected $action;

    /**
     * Controller
     *
     * @var Armazena a controller atual
     */
    protected $controller;

    /**
     * Pagina, página atual que o usuario se encontra
     *
     * @var Armazena a controller atual
     */
    protected $pagina;

    /**
     * Array de dados que será passado para o template
     *
     * @var array
     */
    protected $view_vars = [];

    /**
     * Array de dados que será passado para o template
     *
     * @var array
     */
    protected $vars_class = [];

    /**
     * Datatables Html Builder
     * @var Builder
     */
    protected $listBuilder;

    /**
     * Para setar um template dinamico
     * @var Builder
     */
    protected $template;

    /**
     * Qual é o auth/guard que usa
     * @var string
     */
    protected $guard = 'site';

    /**
     * Construtor
     *
     * @param Request $request
     */
    public function __construct(Request $request, Route $rotas)
    {
        #if(\Request::route()->getName() != 'site.sejaumfornecedor' && \Request::route()->getName() != 'site.sejaumfornecedorpost'){
        #    return redirect()->route('site.sejaumfornecedor')->send();
        #}

        // Ativa o log de Query's
        \DB::enableQueryLog();

        // Linguagem
        $this->linguagem = 'pt-br';

        app('translator')->setLocale($this->linguagem);
        Carbon::setLocale($this->linguagem);
        setlocale(LC_ALL, Carbon::getLocale());

        View::share('linguagem', $this->linguagem);

        // config(['app.debug' => true]);

        // Inicia a configuração
        $this->config = new \stdClass;
        $this->config->paginate = 10;

        $this->request = $request;
        $this->rotas = $rotas;

        // Parametros adicionais dinamicos vindos da url
        $totalparam = count(Urlrequest::segments());
        for ($i = 0; $i < $totalparam; $i++) {
            if ($i >= 4) {
                $this->vars_class[Urlrequest::segment($i)][] = Urlrequest::segment($i + 1);
                $i++;
            }
        }

        // Valida páginação
        if (!empty($this->vars_class['pagination'])) {
            $this->config->paginate = $this->vars_class['pagination'][0];
        } else {
            $this->config->paginate = 10;
        }

        /**
         * Pega o nome da action e controller
         */
        list($caminho, $action) = explode('@', Route::current()->getActionName());
        $caminho = explode('\\', $caminho);
        $controller = end($caminho);

        $this->action = $action;
        $this->controller = $controller;
        $this->pagina = str_replace("controller", "", strtolower($this->controller));

        $this->view_vars['action'] = $this->action;
        $this->view_vars['controller'] = $this->controller;
        $this->view_vars['pagina'] = $this->pagina;

        $this->view_vars['modelos'] = ProductsCategories::where('parent_id', 1)->where('active', true)->get();

        $this->view_vars['formato'] = ProductsCategories::where('parent_id', 2)->where('active', true)->get();

        $this->view_vars['acessorios'] = ProductsCategories::where('parent_id', 3)->where('active', true)->get();

        $this->middleware(function ($request, $next) {
            $this->handleS();
            return $next($request);
        });
    }

    /**
     * Manipula antes de executar a action que foi chamada
     */
    protected function handleS()
    {
        $address = null;
        // dump(session('delivery_address'));

        ## Está logado
        if (auth('site')->check()) {
            $user = auth('site')->user();
            $this->view_vars['user'] = $user;

            // if (!session()->has('endereco_entrega')) {
            //     session([
            //         'endereco_entrega' => (object)[
            //             'id' => null,
            //             'postal_code' => auth()->user()->postal_code,
            //             'address' => auth()->user()->address,
            //             'number' => auth()->user()->number,
            //             'address_complement' => auth()->user()->address_complement,
            //             'neighborhood' => auth()->user()->neighborhood,
            //             'city_id' => auth()->user()->city_id,
            //             'city' => (!empty(auth()->user()->city)) ? auth()->user()->city->city : null,
            //             'state_id' => auth()->user()->state_id,
            //             'state' => (!empty(auth()->user()->state)) ? auth()->user()->state->state : null,
            //         ]
            //     ]);
            // }

            // $address = (session()->has('delivery_address')) ? session('delivery_address') : Address::where('client_id', $user->id)->first();
            $address = (session()->has('delivery_address')) ? session('delivery_address') : null;

            if (empty($address)) {
                $address = (object)[
                    'id' => null,
                    'postal_code' => $user->postal_code,
                    'address' => $user->address,
                    'number' => $user->number,
                    'address_complement' => $user->address_complement,
                    'neighborhood' => $user->neighborhood,
                    'city_id' => $user->city_id,
                    'city' => (!empty($user->city)) ? ucfirst(mb_strtolower($user->city->city, 'UTF-8')) : null,
                    'state_id' => $user->state_id,
                    'state' => (!empty($user->state)) ? ucfirst(mb_strtolower($user->state->state, 'UTF-8')) : null,
                    'UF' => (!empty($user->state)) ? $user->state->initials : null,
                ];

                session([
                    'delivery_address' => $address
                ]);
            } elseif (Address::where('client_id', $user->id)->count() > 0 && empty($address)) {
                $address = Address::where('client_id', $user->id)->first();
                $address = (object)[
                    'id' => $address->id,
                    'postal_code' => $address->postal_code,
                    'address' => $address->address,
                    'number' => $address->number,
                    'address_complement' => $address->address_complement,
                    'neighborhood' => $address->neighborhood,
                    'city_id' => $address->city_id,
                    'city' => (!empty($user->city)) ? ucfirst(mb_strtolower($address->city->city, 'UTF-8')) : null,
                    'state_id' => $address->state_id,
                    'state' => (!empty($address->state)) ? ucfirst(mb_strtolower($address->state->state, 'UTF-8')) : null,
                    'UF' => (!empty($address->state)) ? ucfirst(mb_strtolower($address->state->initials, 'UTF-8')) : null,
                ];

                session([
                    'delivery_address' => $address
                ]);
            }
        } else {
            $address = (session()->has('delivery_address')) ? session('delivery_address') : null;
        }


        $this->view_vars['delivery_address'] = $address;

        $total = 0;
        $subtotal = 0;
        $discount = 0;
        $shipping = 0;

        ## Tem carrinho
        if (!Cart::isEmpty()) {
            $this->view_vars['carrinho'] = Cart::getContent();
            $subtotal = Cart::getSubTotal();
            $total += $subtotal;
        }

        # Frete
        if (Cart::getConditionsByType('shipping')->count() > 0) {
            $shippingCart = Cart::getConditionsByType('shipping')->first();
            $shipping = (float)formataNumeroMysql($shippingCart->getValue());
            $total += $shipping;
        }

        # Desconto
        if (Cart::getConditionsByType('coupon')->count() > 0) {
            $coupon = Cart::getConditionsByType('coupon')->first();
            $discount = (float)str_replace(',', '.', $coupon->getValue());
            $total += $discount;
        }

        # Cart
        $this->view_vars['subtotal'] = $subtotal;
        $this->view_vars['discount'] = $discount;
        $this->view_vars['shipping'] = $shipping;
        $this->view_vars['total'] = $total;

        $model = new Contact();
        $this->view_vars['model'] = $model;


        $this->view_vars['footerinstitucionais'] = Institutional::where('active', true)->get();
    }

    /**
     * [AtualizaModelConfig Esta função atualiza as configurações de model a ser usada;
     */
    protected function AtualizaModelConfig()
    {
        // Existe alguma model
        if (isset($this->config->model)) {
            $model = new $this->config->model;

            // Relacionamentos
            $this->config->relacionamentos = isset($model->relacionamentos) && !is_object($model->relacionamentos) ? $model->relacionamentos : '';

            // Colunas
            // $this -> config -> colunas 	= $model -> getFillable();
            $this->config->colunas = $model->atributos;

            // Chave Primária
            $this->config->pk = $model->getKeyName();

            // Caolunas hidden
            $this->config->hidden = $model->getHidden();

            if (isset($model->queryadicionallist)) {
                if (!isset($this->config->queryadicionallist)) {
                    $this->config->queryadicionallist = [];
                }
                $colecao_queryadicional = collect([]);
                $colecao_queryadicional->push($this->config->queryadicionallist);
                $colecao_queryadicional->push($model->queryadicionallist);
                $this->config->queryadicionallist = $colecao_queryadicional->collapse()->toArray();
            }

            // Coluna de ação
            $this->config->tamanho['acao'] = isset($model->acao['tamanho']) ? $model->acao['tamanho'] : '120';
            if (isset($model->acao['tamanho'])) unset($model->acao['tamanho']);
            $this->config->acao = isset($model->acao) ? $model->acao : '';
        }

    }

    /**
     * Imprimi o template
     *
     * @param null $template
     *
     * @return \Illuminate\View\View
     */
    protected function viewshow($template)
    {

        // Exibe a view dinamicamentecom os parametros
        return view('site::' . $template, $this->view_vars);
    }

    /**
     * Função para renderizar view
     * @return [type] [description]
     */
    protected function renderizar()
    {
        $retornof = '';

        $this->view_vars['action'] = $this->action;
        $template = $this->action;

        if (empty($this->template)) {
            $this->template = $template;
        }
        return $this->viewshow($this->template);
    }

    /**
     * Função salvarArquivos
     * Executa upload de todos os campos
     * @param  [model] $modelItem [ Envia a model/item que será feito o upload/atualizado ]
     * @return [type]       [description]
     */
    public function salvarArquivos($modelItem)
    {
        $arrayteste = collect($modelItem->toArray());
        // Pega todos o campos do tipo file
        if (!empty($this->request->allFiles())) {
            foreach ($this->request->allFiles() as $key => $value) {
                // Pega o caminho de upload
                $caminho = 'uploads/' . $modelItem->atributos[$key]['uploadpath'];
                // Verifica se é uma atualização ou inclusão!
                if (!empty($arrayteste)) {
                    // Valida se o campo exite no banco
                    if ($arrayteste->has($key)) {
                        // Valida se o campo está preenchido
                        if (!empty($modelItem->$key) && !is_null($modelItem->$key)) {
                            // Remove o arquivo anterior
                            \File::delete($caminho . $modelItem->$key);

                            // Limpa o campo do banco
                            $modelItem->$key = '';
                            $modelItem->save();
                        }
                        // Valida o campo para ver se existe arquivo
                        if ($this->request->file($key)->isValid()) {
                            // Atribui o file a um arquivo
                            $arquivo = $this->request->file($key);
                            // Pega a extenão
                            $extensao = $arquivo->extension();
                            // Gera o novo nome do arquivo
                            $nome = date('dmYHis') . '_' . str_slug(str_replace("." . $extensao, '', $arquivo->getClientOriginalName())) . '.' . $extensao;
                            // Salva o arquivo novo
                            $arquivo->move($caminho, $nome);

                            // Salva o campo do banco
                            $modelItem->$key = $nome;
                            $modelItem->save();
                        }
                    }
                }
            }
        }
    }

    public function saveCart($payment_method_id, $sendEmail = true)
    {
        #Atualiza os dados do usuario
        $user = auth('site')->user();

        if (request()->has('user')) {
            $user->fill(request()->user);
            $user->save();
        }

        // dd($this);

        # Endereço
        $address = (session()->has('delivery_address')) ? session('delivery_address') : null;
        if (empty($address)) {
            $address = (object)[
                'id' => null,
                'postal_code' => $user->postal_code,
                'address' => $user->address,
                'number' => $user->number,
                'address_complement' => $user->address_complement,
                'neighborhood' => $user->neighborhood,
                'city_id' => $user->city_id,
                'city' => (!empty($user->city)) ? ucfirst(mb_strtolower($user->city->city, 'UTF-8')) : null,
                'state_id' => $user->state_id,
                'state' => (!empty($user->state)) ? ucfirst(mb_strtolower($user->state->state, 'UTF-8')) : null,
                'UF' => (!empty($user->state)) ? $user->state->initials : null,
            ];
        }

        # Cupon id
        $coupon = (session()->has('coupon') ? (object)session('coupon') : null);
        $coupon_id = null;
        if (!empty($this->coupon)) {
            $coupon_id = $this->coupon->id;
        }

        $cart = null;
        if (session()->has('cart')) {
            $cart = (object)session('cart');
            $cart_id = $cart->id;
            $cart = \Modules\Admin\Entities\Cart::where('id', $cart_id)->with('cart_itens')->first();
        }

        if (!empty($cart) && $cart->count() > 0) {
            foreach ($cart->cart_itens as $item) {
                $item->forceDelete();
            }
        } else {
            $cart = \Modules\Admin\Entities\Cart::create([
                'payment_method_id' => $payment_method_id,
                'order_status_id' => 1,
                'client_id' => $user->id,
                'coupon_id' => $coupon_id,
                'delivery_address_id' => (!empty($address)) ? $address->id : null,
                'shipping' => $this->view_vars['shipping'],
                'total' => $this->view_vars['total'],
            ]);
        }
        foreach (Cart::getContent() as $item) {
            $itemSession = session('item_' . $item->id);
            $product = $itemSession['product'];
            $additionals = (isset($itemSession['adicionais'])) ? collect($itemSession['adicionais'])->recursive() : null;
            $color = (isset($itemSession['color'])) ? $itemSession['color'] : null;
            // dd($color);
            $cart->cart_itens()->create([
                'product_categorie_id' => $product->product_categorie_id,
                // 'color_id' => (!is_null($color)) ? $color->id : null,
                'product_id' => $product->id,
                'product' => $product->toArray(),
                'amount' => $item->price,
                'quantity' => $item->quantity,
                'additional' => (!is_null($additionals)) ? $additionals->toArray() : null,
                'color' => (!is_null($color)) ? $color->toArray() : null,
            ]);
        }

        session([
            'cart' => $cart->load('cart_itens')
        ]);

        try {
            if ($cart->wasRecentlyCreated === true && $sendEmail) {
                ###################### ENVIA PARA O USUÁRIO ############################
                $assunto = __("Novo Pedido"); // assunto
                $linha = [
                    __("# Você realizou uma compra, abaixo segue seus dados e os dados da compra.")
                ];
                $linha2 = [];

                // Dados do e-mail
                $emaildata = collect([
                    'assunto' => $assunto,
                    'greeting' => "Olá " . $user->name . ",",
                    'introLines' => $linha,
                    'outroLines' => $linha2,
                    'user' => $user,
                    'cart' => $cart,
                    // 'dados' => $request->all()
                ]);

                // E-mail para o cliente
                enviaEmail(
                    $emaildata,
                    false,
                    true,
                    'site::notifications.novopedido',
                    null,
                    false,
                    [
                        // 'sender' => ['name' => 'Felipe Almeman'],
                        'to' => ['name' => $user->name, 'email' => $user->email],
                        'replyTo' => ['email' => 'alk@wattsscooters.com'],
                    ]
                );

                ###################### ENVIA PARA O SITE ############################
                $assunto = __("Novo Pedido"); // assunto
                $linha = [
                    __("# Foi realizado um novo pedido!")
                ];
                $linha2 = [];

                // Dados do e-mail
                $emaildata = collect([
                    'assunto' => $assunto,
                    'greeting' => "Olá " . env('APP_SITE_TITLE') . ",",
                    'introLines' => $linha,
                    'outroLines' => $linha2,
                    'user' => $user,
                    'cart' => $cart,
                    // 'dados' => $request->all()
                ]);

                // E-mail para o cliente
                enviaEmail(
                    $emaildata,
                    true,
                    false,
                    'site::notifications.novopedidosite',
                    null,
                    false,
                    [
                        // 'sender' => ['name' => 'Felipe Almeman'],
                        'to' => ['name' => env('APP_SITE_TITLE'), 'email' => 'alk@wattsscooters.com'],
                        'cc' => [
                            ['name' => env('APP_SITE_TITLE'), 'email' => 'rg@wattsscooters.com'],
                            ['name' => env('APP_SITE_TITLE'), 'email' => 'maiconcesarweb@gmail.com'],
                        ],
                        'replyTo' => ['email' => $user->email],
                    ]
                );
            }
        } catch (\Exception $e) {
            \Log::error($e);
            \Log::error('Erro no e-mail de novo pedido');
        }

        return $cart;
    }
}
