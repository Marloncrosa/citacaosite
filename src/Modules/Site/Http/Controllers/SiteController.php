<?php

namespace Modules\Site\Http\Controllers;

use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

use Modules\Admin\Entities\Banners;
use Modules\Admin\Entities\City;
use Modules\Admin\Entities\Contact;
use Modules\Admin\Entities\ContactSubjects;
use Modules\Admin\Entities\Country;
use Modules\Admin\Entities\Institutional;
use Modules\Admin\Entities\Logos;
use Modules\Admin\Entities\News;
use Modules\Admin\Entities\Newsletter;
use Modules\Admin\Entities\Portfolio;
use Modules\Admin\Entities\PortfolioTypes;
use Modules\Admin\Entities\Products;
use Modules\Admin\Entities\ProductsCategories;
use Modules\Admin\Entities\Service;
use Modules\Admin\Entities\State;
use Modules\Admin\Entities\Testimony;
use Modules\Site\Http\Requests\ContatoRequest;

class SiteController extends Controller
{
    public function index()
    {

        $banners = Banners::where('active', true)->get();
        $this->view_vars['banners'] = $banners;


        $produtos = Products::where('active', true)->limit(6)->get();


        $this->view_vars['produtos'] = $produtos;

        $depoimentos = Testimony::limit(6)->get();
        $this->view_vars['depoimentos'] = $depoimentos;

        $blog = News::where('active', true)->limit(6)->get();
        $this->view_vars['blog'] = $blog;

        $logos = Logos::where('active', true)->get();
        $this->view_vars['logos'] = $logos;

        $institucionais = Institutional::first();
        $this->view_vars['institucionais'] = $institucionais;


// dd($this->view_vars['institucionais']);
        $servicos = Service::where('active', true)->limit(3)->get();
        $this->view_vars['servicos'] = $servicos;

        // $portfolios = Portfolio::where('active', true)->limit(6)->get();
        // $this->view_vars['portfolios'] = $portfolios;
        //
        // $portifolio_categorias = PortfolioTypes::get();
        // $this->view_vars['portifolio_categorias'] = $portifolio_categorias;
        return $this->renderizar();
    }

    public function contato()
    {
        $this->view_vars['FormModel'] = new Contact();
        $this->view_vars['assuntos'] = ContactSubjects::where('active', true)->get();
        return $this->renderizar();
    }

    public function contatopost(ContatoRequest $request)
    {
        $contato = new Contact($request->all());

        if ($contato->save()) {
            // Enviar o e-mail
            $assunto = __("Contato"); // assunto
            $linha = __("Você recebeu um contato enviado pelo site ") . env('APP_SITE_TITLE'); // linha inicial

            // Dados do e-mail
            $emaildata = collect([
                'assunto' => $assunto,
                'saudacao' => __("Olá, ") . env('APP_SITE_TITLE'),
                'introLines' => $linha,
                'dados' => $request
            ]);

            $emaildata['dados']['setor'] = $contato->assunto->titulo;

            // E-mail para o contato
            $enviou = json_decode(enviaEmail(
                $emaildata,
                true,
                false,
                'site::notifications.faleconosco',
                null,
                false,
                [
                    'sender' => ['name' => $contato->nome]
                ]
            ));

            if ($enviou->success) {
                // Dados do e-mail
                $emaildata = collect([
                    'assunto' => $assunto,
                    'saudacao' => __("Olá, ") . $contato->nome,
                    'introLines' => $linha,
                    'dados' => $request
                ]);

                // E-mail para o contato
                enviaEmail(
                    $emaildata,
                    false,
                    true,
                    'site::notifications.faleconosco',
                    null,
                    false,
                    [
                        // 'sender' =>[ 'email' => '', 'name' => '' ]
                    ]
                );

                return redirect()->route('site.retorno', [
                    'pagina' => 'contato',
                    'tipo' => 'sucesso',
                    'titulo' => 'Sucesso!',
                    'mensagem' => urlencode('Sua solicitação <br> foi enviada com sucesso'),
                ]);
            } else {
                return redirect()->route('site.retorno', [
                    'pagina' => 'contato',
                    'tipo' => 'sucesso',
                    'titulo' => 'Sucesso!',
                    'mensagem' => urlencode('Dados cadastrados com sucesso mas, <br> ' . $enviou->message),
                ]);
            }
        } else {
            return redirect()->route('site.retorno', [
                'pagina' => 'contato',
                'tipo' => 'erro',
                'titulo' => 'Oops!',
                'mensagem' => urlencode('Não conseguimos enviar <br> a sua mensagem!'),
            ]);
        }
    }

    public function retorno($pagina = '', $tipo = 'error', $titulo = null, $mensagem = '')
    {
        $this->view_vars['tipo'] = $tipo;
        $this->view_vars['pagina'] = $pagina;
        $this->view_vars['titulo'] = $titulo;
        $this->view_vars['mensagem'] = (!empty($mensagem)) ? urldecode($mensagem) : '';

        return $this->renderizar();
    }

    public function newsletter($request)
    {
        die('aki');
        $newsletter = new Newsletter($request->all());

        if ($newsletter->save()) {
            return redirect()->route('site.home')->with('sucesso', 'Bem vindo(a) a Newslleter da Muller Diesel!');
        } else {
            return back()->withErrors($newsletter->erros())->withInput();
        }
    }

    public function institucional($slug)
    {

        $this->view_vars['institucional'] = Institutional::where('slug', $slug)->where('active', true)->first();

        if (is_null($this->view_vars['institucional'])) {
            return redirect()->back()->with('error', __('Página não encontrada!'));
        }

        $this->view_vars['institucional_menu'] = Institutional::where('active', true)->where('slug', '!=', str_slug('Quem Somos'))->get();

        $this->template .= 'institucional/institucional';
        return $this->renderizar();
    }
}
