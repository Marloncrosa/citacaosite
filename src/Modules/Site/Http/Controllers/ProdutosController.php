<?php

namespace Modules\Site\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Pagination\Paginator;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Modules\Admin\Entities\News;
use Modules\Admin\Entities\NewsCategories;
use Modules\Admin\Entities\Banners;
use Modules\Admin\Entities\Products;
use Modules\Admin\Entities\ProductsCategories;
use Modules\Admin\Entities\ProductGallery;
use Modules\Site\Http\Requests\InteresseRequest;

//use Modules\Admin\Entities\Products;

class ProdutosController extends Controller
{
    public function index($pagina = 1)
    {
        // Seta o paginador
        Paginator::currentPageResolver(function () use ($pagina) {
            return $pagina;
        });

        $produtos = Products::where('active', true);


        $banners = Banners::where('active', true)->get();
        $this->view_vars['banners'] = $banners;

        if (request()->has('subcategoriaid')) {
            $subcategoriaid = request()->subcategoriaid;
            $produtos = $produtos->whereJsonContains('product_categories', $subcategoriaid);
        }

        if (request()->has('category')) {
            $produtos = $produtos->whereJsonContains('product_categories', (string)request()->category);

        }

        if (request()->has('launch')) {
            $launch = request()->launch;
            $produtos = $produtos->where('launch', $launch);
        }

        if (request()->has('gender')) {
            $gender = request()->gender;
            $produtos = $produtos->where('gender', $gender);
        }

        if (request()->has('busca')) {
            $produtos = $produtos->where(function ($query) {
                $busca = request()->busca;
                $query
                    ->where('title', 'LIKE', "%{$busca}%")
                    ->orWhere('sinopsys', 'LIKE', "%{$busca}%")
                    ->orWhere('description', 'LIKE', "%{$busca}%");
            });
        }

        if (request()->has('value')) {
            $values = explode('-', request()->value);
            if (count($values) <= 1) {
                $produtos = $produtos
                    ->where(function ($query) use ($values) {
                        $query
                            ->where('value', '<=', $values[0])
                            ->orWhere('final_value', '<=', $values[0]);
                    });
            } else {
                if (!empty($values[0])) {
                    $produtos = $produtos
                        ->where(function ($query) use ($values) {
                            $query
                                ->where('value', '>=', $values[0])
                                ->where('value', '<=', $values[1]);
                        })
                        ->orWhere(function ($query) use ($values) {
                            $query
                                ->where('final_value', '>=', $values[0])
                                ->where('final_value', '<=', $values[1]);
                        });
                } else {
                    $produtos = $produtos
                        ->where(function ($query) use ($values) {
                            $query
                                ->where('value', '>=', $values[0]);
                        })
                        ->orWhere(function ($query) use ($values) {
                            $query
                                ->where('final_value', '>=', $values[0]);
                        });
                }
            }
        }

        if (request()->has('de') && !empty(request()->de)) {
            $produtos = $produtos
                ->where(function ($query) {
                    $query
                        ->where('value', '>=', request()->de)
                        ->orWhere('final_value', '>=', request()->de);
                });
            // $produtos = $produtos->where('value', '>=', request()->de);
        }

        if (request()->has('ate') && !empty(request()->ate)) {
            $produtos = $produtos
                ->where(function ($query) {
                    $query
                        ->where('value', '<=', request()->ate)
                        ->orWhere('final_value', '<=', request()->ate);
                });
        }

        if (request()->has('ordem')) {
            switch (request()->ordem) {
                case 'nome-asc':
                    $produtos = $produtos->orderBy('title');
                    break;
                case 'nome-desc':
                    $produtos = $produtos->orderByDesc('title');
                    break;
                case 'menor-valor':
                    $produtos = $produtos->orderBy('final_value');
                    break;
                case 'maior-valor':
                    $produtos = $produtos->orderByDesc('value');
                    break;
                default:
                    $produtos = $produtos->orderByDesc('created_at');
                    break;
            }
        } else {
            // $produtos = $produtos->orderBy(\DB::raw('RAND()'));
            $produtos = $produtos->orderByDesc('value');
        }
        //
        // $marcas = Brands::where('active', true)
        //     ->orderBy('id', 'desc')->get();

        // $produtos = $produtos->get();
        $produtos = $produtos->paginate(24);

        $this->view_vars['produtos'] = $produtos;
        // $this->view_vars['marcas'] = $marcas;
        $this->view_vars['categorias'] = ProductsCategories::where('active', true)->get();
        // $this->view_vars['modelos'] = ProductsCategories::where('parent_id', 1)->where('active', true)->get();
        // $this->view_vars['formato'] = ProductsCategories::where('parent_id', 2)->where('active', true)->get();
        // $this->view_vars['acessorios'] = ProductsCategories::where('parent_id', 3)->where('active', true)->get();

        $this->template = 'produtos.produtos';
        return $this->renderizar();
    }

    public function produto($slug)
    {
        $produto = Products::where('slug', $slug)
            ->with('galeria')
            ->whereActive(true)
            ->first();

        $this->view_vars['pagename'] = 'site-produtos';

        $galeria = ProductGallery::where('active', true)->where('product_id', $produto->id);
        $this->view_vars['galeria'] = $galeria ->get();
        //
        // $colors = Colors::where('product_id', $produto->id)
        //     ->whereActive(true)
        //     ->get();

        $produtos = Products::where('active', true)
            ->where('id', '!=', $produto->id)
            ->orderBy('id', 'desc')->get();
        $this->view_vars['produtos'] = $produtos;


        $banners = Banners::where('active', true)->get();
        $this->view_vars['banners'] = $banners;

        if (is_null($produto)) {
            return redirect()->route('site-home', ['lang' => $this->linguagem])->with('error', __('Post not found'));
        }

        //        $prev_post = News::where('id', '<', $post->id)->ativoelinguagem($this->idioma->id)->orderBy('id', 'desc')->first();
        //
        //        $next_post = News::where('id', '>', $post->id)->ativoelinguagem($this->idioma->id)->first();

        $this->view_vars['produto'] = $produto;
        // $this->view_vars['colors'] = $colors;

        //        $this->view_vars['prev_post'] = $prev_post;
        //        $this->view_vars['next_post'] = $next_post;
        $this->template = 'produtos.produto';
        return $this->renderizar();
    }
    public function lista()
    {
        $categorias = ProductsCategories::where('active', true)->get();
        $this->view_vars['categorias'] = $categorias;

        $listas = Products::where('active', true);
        dd(request()->category);
        if (request()->has('category')) {
            $listas = $listas->whereJsonContains('product_categories', (string)request()->category);

        }
        $listas = $listas->get();
        $this->view_vars['listas'] = $listas;
        //
        // $this->template = "layouts.list-center";
        // return $this->renderizar();
    }

    public function interesse(InteresseRequest $request)
    {
        $product = Products::find($request->product_id)->first();

        $interese = new HasProductInteress($request->all());
        $interese->sended = false;

        if ($interese->save()) {
            // try {

            ###################### ENVIA PARA O CLIENTE ############################
            $assunto = __("Interesse em produto"); // assunto
            $linha = [
                __("Você demonstrou interesse no :produto, assim que disponível iremos lhe informar! :)", [
                    'produto' => $product->title,
                ])
            ];
            $linha2 = [];

            // Dados do e-mail
            $emaildata = collect([
                'assunto' => $assunto,
                'greeting' => "Olá, ",
                'introLines' => $linha,
                'outroLines' => $linha2,
                // 'dados' => $request->all()
            ]);

            enviaEmail(
                $emaildata,
                false,
                true,
                'site::notifications.email',
                null,
                false,
                [
                    // 'sender' => ['name' => 'Felipe Almeman'],
                    'to' => ['email' => $request->email],
                    'replyTo' => ['name' => env('APP_SITE_TITLE'), 'email' => 'alk@wattsscooters.com'],
                ]
            );

            ###################### ENVIA PARA O SITE ############################
            $assunto = __("Interesse em produto"); // assunto
            $linha = [
                __("Houve um interesse no produto :produto pelo e-mail :email", [
                    'produto' => $product->title,
                    'email' => $request->email,
                ])
            ];
            $linha2 = [];

            // Dados do e-mail
            $emaildata = collect([
                'assunto' => $assunto,
                'greeting' => "Olá, ",
                'introLines' => $linha,
                'outroLines' => $linha2,
                // 'dados' => $request->all()
            ]);

            // E-mail para o cliente
            enviaEmail(
                $emaildata,
                true,
                false,
                'site::notifications.email',
                null,
                false,
                [
                    // 'sender' => ['name' => 'Felipe Almeman'],
                    'to' => ['name' => env('APP_SITE_TITLE'), 'email' => 'alk@wattsscooters.com'],
                    'cc' => [
                        ['name' => env('APP_SITE_TITLE'), 'email' => 'rg@wattsscooters.com']
                    ],
                    'replyTo' => ['email' => $request->email],
                ]
            );

            // } catch (\Exception $e) {
            //     \Log::error($e);
            //     \Log::error('Erro ao enviar e-mail de interesse');
            // }
            return redirect()->back()->with('success', __('Agradecemos o seu interesse pelo produto ' . $product->title . '. Te avisaremos quando o produtos estiver disponível!'));
        } else {
            return redirect()->back()->with('error', __('Oooops! Houve algum problema, entre em contato com os administradores do site.'));
        }

    }
}
