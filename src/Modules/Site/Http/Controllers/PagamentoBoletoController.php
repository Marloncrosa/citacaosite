<?php

namespace Modules\Site\Http\Controllers;

use Claudsonm\CepPromise\CepPromise;
use Claudsonm\CepPromise\Exceptions\CepPromiseException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Milon\Barcode\DNS1D;
use Modules\Admin\Entities\Address;
use Modules\Admin\Entities\Aditionals;
use Modules\Admin\Entities\CartItens;
use Modules\Admin\Entities\City;
use Modules\Admin\Entities\Colors;
use Modules\Admin\Entities\Country;
use Modules\Admin\Entities\Coupons;
use Modules\Admin\Entities\Products;
use Darryldecode\Cart\Facades\CartFacade as Cart;
use Modules\Admin\Entities\State;
use PagSeguro;

class PagamentoBoletoController extends PagseguroController
{

    public function index()
    {
        // Verifica se tem carrinho
        if (Cart::isEmpty()) {
            return redirect()->route('site-produtos')->withErrors('Adicione um item ao carrinho.');
        }

        $this->template .= 'boleto';

        $cart = $this->pagseguroInit();
        if(!empty($this->pagseguroCart)){
            $this->pagamentoBoleto($cart);
        }

        return $this->renderizar();
    }
}
