<?php
namespace Modules\Site\Http\Middleware;

use Closure;
use Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = 'site')
    {
        if (!auth($guard)->check()) {
            return redirect()->route('site.logout', [$request->segment(1)])
                ->with('error', __("Por favor logue-se"));
        }

        return $next($request);
    }
}
