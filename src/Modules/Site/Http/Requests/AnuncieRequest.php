<?php

namespace Modules\Site\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnuncieRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome' => 'required',
            'empresa' => 'required',
            'email' => 'required',
            'telefone' => 'required',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'nome.required' => __("validation.required", ['attribute' => 'Nome']),
            'email.required' => __("validation.required", ['attribute' => 'E-mail']),
            'empresa.required' => __("validation.required", ['attribute' => 'Nome da Empresa, Produto ou Négocios']),
            'telefone.required' => __("validation.required", ['attribute' => 'Telefone']),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
