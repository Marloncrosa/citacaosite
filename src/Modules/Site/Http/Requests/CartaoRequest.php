<?php

namespace Modules\Site\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CartaoRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'user.phone' => 'required',
            // 'user.cellphone' => 'required',

            'phone' => 'required',
            'cpf' => 'required',
            'data_nascimento' => 'required',
            'number' => 'required',
            'nome' => 'required',
            'date' => 'required',
            'cvv' => 'required',
            'installments' => 'required',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            // 'user.phone.required' => __("validation.required", ['attribute' => 'Tel. Residencial']),
            // 'user.cellphone.required' => __("validation.required", ['attribute' => 'Tel. Celular']),

            'phone.required' => __("validation.required", ['attribute' => 'Telefone']),
            'cpf.required' => __("validation.required", ['attribute' => 'CPF']),
            'data_nascimento.required' => __("validation.required", ['attribute' => 'Data de Nascimento']),
            'number.required' => __("validation.required", ['attribute' => 'Número do Cartão']),
            'nome.required' => __("validation.required", ['attribute' => 'Nome como esta no cartão']),
            'date.required' => __("validation.required", ['attribute' => 'Vencimento']),
            'cvv.required' => __("validation.required", ['attribute' => 'CVV']),
            'installments.required' => __("validation.required", ['attribute' => 'Parcelas']),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
