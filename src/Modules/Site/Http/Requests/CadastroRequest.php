<?php

namespace Modules\Site\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CadastroRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:clients,email',
            'password' => [
                'required',
               'min:8',
               // 'regex:/^.*(?=.{4,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/',
                'confirmed'
            ],
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __("validation.required", ['attribute' => 'Nome']),
            'email.required' => __("validation.required", ['attribute' => 'E-mail']),
            'email.unique' => __("validation.unique", ['attribute' => 'E-mail']),
            'telefone.required' => __("validation.required", ['attribute' => 'Telefone']),
            'cpf.required' => __("validation.required", ['attribute' => 'CPF']),
            'cpf.unique' => __("validation.unique", ['attribute' => 'CPF']),


            'password.required' => __("validation.required", ['attribute' => 'Senha']),
            'password.min' => __("validation.min", ['attribute' => 'Senha']),
            'password.regex' => 'Informe ao menos um caracter minusculo, um maiusculo, um nmero e um caracter especial.',
            'password.confirmed' => __("validation.confirmed", ['attribute' => 'Senha']),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
