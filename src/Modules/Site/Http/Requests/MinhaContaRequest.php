<?php

namespace Modules\site\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MinhaContaRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'birthdate' => 'required',
            'email' => 'required',
            'document' => 'required',
            'phone' => 'required',
            'cellphone' => 'required',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => __("validation.required", ['attribute' => 'Nome']),
            'birthdate.required' => __("validation.required", ['attribute' => 'Data de Nascimento']),
            'email.required' => __("validation.required", ['attribute' => 'E-Mail']),
            'document.required' => __("validation.required", ['attribute' => 'CPF']),
            'cellphone.required' => __("validation.required", ['attribute' => 'Celular']),
            'phone.required' => __("validation.required", ['attribute' => 'Telefone']),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
