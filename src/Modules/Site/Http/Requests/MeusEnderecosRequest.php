<?php

namespace Modules\site\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MeusEnderecosRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'client_id' => 'required',
            // 'state_id' => 'required',
            // 'city_id' => 'required',
            // 'types_address_id' => 'required',
            'postal_code' => 'required',
            'address' => 'required',
            'number' => 'required',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'client_id.required' => __("validation.required", ['attribute' => 'Usuário']),
            'state_id.required' => __("validation.required", ['attribute' => 'Estado']),
            'city_id.required' => __("validation.required", ['attribute' => 'Cidade']),
            'types_address_id.required' => __("validation.required", ['attribute' => 'Tipo']),
            'postal_code.required' => __("validation.required", ['attribute' => 'Endereço']),
            'number.required' => __("validation.required", ['attribute' => 'Número']),
            'neighborhood.required' => __("validation.required", ['attribute' => 'Bairro']),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
