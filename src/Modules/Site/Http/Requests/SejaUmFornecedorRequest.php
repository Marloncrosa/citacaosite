<?php

namespace Modules\Site\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SejaUmFornecedorRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'corporate_name' => 'required',
            'fantasy_name' => 'required',
            'email' => 'required',
            'document' => 'required',
            'cellphone' => 'required',
            'phone' => 'required',
            'postal_code' => 'required',
            'address' => 'required',
            'number' => 'required',
            // 'address_complement' => 'required',
            'neighborhood' => 'required',
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'corporate_name.required' => __("validation.required", ['attribute' => 'Razão Social']),
            'fantasy_name.required' => __("validation.required", ['attribute' => 'Nome Fantasia']),
            'email.required' => __("validation.required", ['attribute' => 'E-Mail']),
            'document.required' => __("validation.required", ['attribute' => 'CNPJ']),
            'cellphone.required' => __("validation.required", ['attribute' => 'Celular']),
            'phone.required' => __("validation.required", ['attribute' => 'Telefone']),
            'postal_code.required' => __("validation.required", ['attribute' => 'CEP']),
            'address.required' => __("validation.required", ['attribute' => 'Endereço']),
            'number.required' => __("validation.required", ['attribute' => 'Número']),
            'address_complement.required' => __("validation.required", ['attribute' => 'Complemento']),
            'neighborhood.required' => __("validation.required", ['attribute' => 'Bairro']),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
