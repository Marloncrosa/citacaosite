<?php

namespace Modules\Site\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AlterarSenhaRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password' => [
                'required',
//                'min:6',
//                'regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[.!$#%]).*$/',
                'confirmed'
            ]
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'senha.required' => __("validation.required", ['attribute' => 'Senha']),
            'senha.min' => __("validation.min", ['attribute' => 'Senha']),
            'senha.regex' => 'Informe ao menos um caracter minusculo, um maiusculo, um nmero e um caracter especial.',
            'senha.confirmed' => __("validation.confirmed", ['attribute' => 'Senha']),
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
