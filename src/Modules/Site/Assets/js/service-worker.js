// self.__WB_MANIFEST;

importScripts(
    'https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js'
);

if (workbox) {
    console.log(`Yay! Workbox is loaded 🎉`);
} else {
    console.log(`Boo! Workbox didn't load 😬`);
}

workbox.routing.registerRoute(
    new RegExp('/uploads/'),
    new workbox.strategies.StaleWhileRevalidate({
        cacheName: 'uploads',
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 15
            }),
            new workbox.cacheableResponse.Plugin({
                statuses: [200]
            })
        ]
    })
);

workbox.precaching.precacheAndRoute([]);

const networkFirstHandler = new workbox.strategies.NetworkFirst({
    cacheName: 'dynamic',
    plugins: [
        new workbox.expiration.Plugin({
            maxEntries: 10
        }),
        new workbox.cacheableResponse.Plugin({
            statuses: [200]
        })
    ]
});

const FALLBACK_URL = workbox.precaching.getCacheKeyForURL('/offline');
const matcher = ({ event }) => event.request.mode === 'navigate';
const handler = args =>
    networkFirstHandler
        .handle(args)
        .then(response => response || caches.match(FALLBACK_URL))
        .catch(() => caches.match(FALLBACK_URL));

workbox.routing.registerRoute(matcher, handler);
