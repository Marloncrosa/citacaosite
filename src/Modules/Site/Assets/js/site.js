// Sudações
console.log('Desenvolvido com ♥ por aireset.com.br');

require('./pwa');

import jQuery from 'jquery';
window.jQuery = window.jquery = window.$ = jQuery;

window.xs = window.matchMedia("(max-width: 575.98px)");
window.sm = window.matchMedia("(max-width: 767.98px)");
window.md = window.matchMedia("(max-width: 991.98px)");
window.lgup = window.matchMedia("(min-width: 991.98px)");
window.lg = window.matchMedia("(max-width: 1199.98px)");
window.xl = window.matchMedia("(min-width: 1199.98px)");

window.Popper = require('popper.js').default;

// import Foundation from 'foundation-sites';
// window.Foundation = Foundation;

require('./plugins/jquery.easing');
require('./plugins/bootstrap.min');
require('./plugins/viewport');
require('./plugins/equalize.min');
require('./plugins/slick.min');
require('./plugins/isotope.pkgd.min');
require('./plugins/imagesloaded.pkgd.min');
require('./plugins/jquery.magnific-popup.min');
require('./plugins/masonry.pkgd.min');
require('./plugins/jquery.fitvids');

// require('./plugins/owl.carousel.js');
// require('./plugins/jquery.flexslider.js');
// require('./plugins/jquery.maskedinput.js');
// require('./plugins/jquery.fancybox.js');
// require('./plugins/select2.js');

require('select2');
require('select2/dist/js/i18n/pt-BR');

import swaljs from 'sweetalert';
window.swaljs = swaljs;

// Mascaras
import inputmask from 'inputmask';
window.inputmask = inputmask;
require('inputmask/dist/jquery.inputmask.bundle');

require('@fancyapps/fancybox/dist/jquery.fancybox.min');

window.slugify = require('slugify');

require('intl-tel-input/build/js/intlTelInput-jquery.min');
window.utilsIntlTel = require('intl-tel-input/build/js/utils');

require('./plugins/script');

require('./variaveis');
require('./funcoes');
require('./mascaras');
require('./application');
require('./estadocidade');
