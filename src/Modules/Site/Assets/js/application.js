$(function () {
    // Expressao contains in
    $.expr[":"].containsIn = function (el, i, m) {
        var search = m[3];
        if (!search) {
            return false;
        }
        return eval("/" + search + "/i").test($(el).text());
    };

    $.fn.serializeFormJSON = function () {

        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    // Fancybox
    if ($.fn.fancybox) {
        $(".fancy").fancybox();
        $('a[href*=".jpg"],a[href*=".jpeg"],a[href*=".gif"],a[href*=".png"]').fancybox();
        $("a:not(.youtubechannel)[href*=youtu],a:not(.youtubechannel)[href*=vimeo],a:not(.youtubechannel)[href*=vevo]").fancybox({
            autoScale: true,
            transitionIn: "fade",
            transitionOut: "fade",
            titlePosition: "inside",
            type: "iframe",
            openEffect: "elastic",
            openSpeed: 150,
            closeEffect: "elastic",
            closeSpeed: 150,
            swf: {
                wmode: "transparent",
                allowfullscreen: "true",
                allowscriptaccess: "always"
            },
            iframe: {
                scrolling: "auto",
                preload: true
            },
            helpers: {
                media: true,
                comments: {
                    type: "overlay",
                    position: "top",
                    commentsUrl: function (a) {
                        return $(a.element).data("fancybox-comments");
                    },
                    commentsWidth: 330,
                    numberPosts: 5,
                    colorScheme: "light",
                    disableOnEmptyUrl: true
                }
            }
        });
    }

    // Mascaras
    if ($.fn.inputmask) {
        mascaras();
    }

    if ($.fn.select2) {
        $.fn.select2.defaults.set("width", '100%');
        $.fn.select2.defaults.set("debug", true);
        $.fn.select2.defaults.set("language", "pt-BR");
        $.fn.select2.defaults.set("templateResult", function (item) {
            var elemento = $(item.element);

            if (!!elemento.data('help')) {
                var template = $('#' + elemento.data('template'));

                // setTimeout(function(){
                //     $(document).foundation();
                // }, 500);

                return item.text + ' '
                    + '<i class="far fa-question-circle helper swal" style="margin-left: 10px;" data-template="' + elemento.data('template') + '"></i>'
                    ;
            }
            return item.text;
        });
        $.fn.select2.defaults.set("escapeMarkup", function (m) {
            // Do not escape HTML in the select options text
            return m;
        });

        $('select').each(function (e) {
            if ($(this).is('[data-close-on-select]')) {
                $(this).select2({
                    closeOnSelect: false
                });
            } else {
                $(this).select2();
            }
        });

        // $('select[data-close-on-select]').on('select2:closing', function (e) {
        //     $('.select2-results i.helper:not(.swal)').foundation('hide');
        // });

        $('select').on('select2:open', function (e) {
            window.openedSelect2 = $(this);
            window.openedSelect2Value = openedSelect2.val();
        });

        $(document).on('click', '.select2-results i.helper:not(.swal)', function (e) {
            e.preventDefault();
            e.stopPropagation();
        });

        $(document).on('click', '.select2-results .select2-results__option i.helper.swal', function (e) {
            e.preventDefault();
            e.stopPropagation();

            if (!openedSelect2.val()) {
                openedSelect2.val('');
            } else {
                openedSelect2.val(openedSelect2Value);
            }

            openedSelect2.trigger('change.select2');

            swal({
                title: "Informação",
                content: {
                    element: "div",
                    attributes: {
                        innerHTML: $('#' + $(this).data('template')).html()
                    },
                },
                icon: 'success'
            }).then((value) => {
                openedSelect2.select2('open');
            });
        });

        $(document).on('click', '.select2-results .select2-results__option', function (e) {
            e.preventDefault();
            e.stopPropagation();
            openedSelect2.select2('close');
        });


        $('.select2_produtos').select2({
            placeholder: 'Escolha o produto'
        });

        $('.select2_cor').select2({
            placeholder: 'Escolha o produto'
        });
    }

    let floatLabel = ':input.form-control:not(.not-float), .form-group label:not(.not-float)';
    $(document).on('focusin', floatLabel, function () {
        $(this).parent('.column').find('label').addClass('focusfield');
    }).on('focusout', floatLabel, function () {
        input = $(this).parent('.column').find('input');
        if (!input.val() && !input.is(':focus')) {
            $(this).parent('.column').find('label').removeClass('focusfield');
        }
    }).on('mouseenter', floatLabel, function () {
        $(this).parent('.column').find('label').addClass('focusfield');
    }).on('mouseout', floatLabel, function () {
        input = $(this).parent('.column').find('input');
        if (!input.val() && !input.is(':focus')) {
            $(this).parent('.column').find('label').removeClass('focusfield');
        }
    }).on('change', floatLabel, function () {
        input = $(this).parent('.column').find('input');
        if (!!input.val()) {
            $(this).parent('.column').find('label').addClass('focusfield');
        }
    });
    $(floatLabel).trigger('change');

    // $("#sidebar").mCustomScrollbar({
    //     theme: "minimal"
    // });

    $('#dismiss, .overlay').off('click').on('click', function () {
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');
    });
    $('#sidebarCollapse').off('click').on('click', function () {
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
});
