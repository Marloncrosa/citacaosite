$(function(){
    var country = $('select[id*="country"],select[name*="country"],select[class*="country"]');
    var state = $('select[id*="state"],select[name*="state"],select[class*="state"]');
    var city = $('select[id*="city"],select[name*="city"],select[class*="city"]');

    if ($.fn.intlTelInput) {
        var countryData = window.intlTelInputGlobals.getCountryData();
        window.intltelinput = $(".mask-intl-phone");
        $(".mask-intl-phone").intlTelInput({
            utilsScript: window.utilsIntlTel,
            // autoPlaceholder: 'aggressive',
            initialCountry: 'BR',
            preferredCountries: ['BR'],
            formatOnDisplay: true,
        });
        $(".mask-intl-phone").off('countrychange').on("countrychange", function () {
            var elemento = $(this),
                countrySelected = $(this).intlTelInput("getSelectedCountryData"),
                mask = '+' + countrySelected.dialCode + ' ' + elemento.attr('placeholder').replace(/[0-9]/g, 9);

            if (countrySelected.iso2 == 'ar') {
                mask = '+' + countrySelected.dialCode + ' 999 9999-999';
            }

            // elemento.val('').inputmask("remove").inputmask(mask);
            elemento.inputmask("remove").inputmask(mask);
            elemento.trigger('focusout');
            setTimeout(function () {
                elemento.trigger('focusin');
            }, 200);
        });
        // setTimeout(function () {
        //
        // }, 500)
        $(".mask-intl-phone").trigger('countrychange');
    }

    // If exist city select
    if (country.length > 0) {
        var stateValue = '';
        $(document).on('change', 'select[id*="country"],select[name*="country"],select[class*="country"]', function (event) {
            event.preventDefault();
            var element = $(event.target),
                value = element.val(),
                stateValue = element.parents('form').find('select[id*="state"],select[name*="state"],select[class*="state"]').val(),
                stateDataValue = element.parents('form').find('select[id*="state"],select[name*="state"],select[class*="state"]').data('value'),
                requestState;

            if (value == '' && value == undefined) {
                return false;
            }

            if ($.fn.intlTelInput) {

                $(this).parents('form').find(".mask-intl-phone").intlTelInput("setCountry", this.value);
            }

            requestState = $.ajax({
                url: '/webservice/states',
                type: 'POST',
                async: false,
                data: {
                    country_id: value
                }
            });

            requestState.done(function (data) {
                if (isJson(data)) {
                    state.html('');
                    data = JSON.parse(data);
                    $.each(data, function (index, data) {
                        state.append('<option value="' + data.id + '">' + data.state + '</option>');
                    });
                    if (!!stateValue) {
                        state.find('option[value="' + stateValue + '"]').prop('selected', true);
                    }
                    if (!!stateDataValue) {
                        state.find('option[value="' + stateDataValue + '"]').prop('selected', true);
                    }
                } else {
                    state.html('<option value="">' + data + '</option>');
                    // swal(data, null, "error")
                }
            });

        });
    }

    // City, state and countr
    if (city.length > 0) {
        $(document).on('change', 'select[id*="state"],select[name*="state"],select[class*="state"]', function (event) {
            event.preventDefault();
            var element = $(event.target),
                value = element.val(),
                cityValue = element.parents('form').find('select[id*="city"],select[name*="city"],select[class*="city"]').val(),
                cityDataValue = element.parents('form').find('select[id*="city"],select[name*="city"],select[class*="city"]').data('value'),
                requestCity;

            // console.log("############ REQUEST DE CIDADES ##################");
            // console.log(cityValue);
            // console.log(cityDataValue);
            if (value == '' && value == undefined) {
                return false;
            }

            requestCity = $.ajax({
                url: '/webservice/cities',
                type: 'POST',
                async: false,
                data: {
                    state_id: value
                }
            });

            requestCity.done(function (data) {
                if (isJson(data)) {
                    city.html('');
                    data = JSON.parse(data);
                    $.each(data, function (index, data) {
                        city.append('<option value="' + data.id + '">' + data.city + '</option>');
                    });
                    if (!!cityValue) {
                        city.find('option[value="' + cityValue + '"]').prop('selected', true);
                    }
                    if (!!cityDataValue) {
                        city.find('option[value="' + cityDataValue + '"]').prop('selected', true);
                    }
                } else {
                    city.html('<option value="">' + data + '</option>');
                    // swal(data, null, "error")
                }
            });
        });
    }

    country.each(function () {
        if ($(this).val() != '' && $(this).val() != undefined) {
            $(this).trigger('change');
        }
    });

    setTimeout(function () {
        state.each(function () {
            if ($(this).val() != '' && $(this).val() != undefined) {
                $(this).trigger('change');
            }
        });
    }, 500);
});
