if ('serviceWorker' in navigator) {
    // Use the window load event to keep the page load performant
    window.addEventListener('load', () => {
        navigator.serviceWorker
            .register('/modules/site/js/service-worker.js', {
                scope: '../../../'
            })
            /*.register('/modules/admin/js/service-worker.js')*/
            .then(function (registration) {
                console.log(
                    'Service Worker registrado com sucesso com scope: ',
                    registration.scope
                );
            })
            .catch(function (err) {
                console.log('Service Worker falhou em registrar-se: ', err);
            });
    });

    var deferredPrompt;
    window.addEventListener('beforeinstallprompt', function(event) {
        event.preventDefault();
        deferredPrompt = event;
        return false;
    });

    function addToHomeScreen() {
        if (deferredPrompt) {
            deferredPrompt.prompt();
            deferredPrompt.userChoice.then(function (choiceResult) {
                console.log(choiceResult.outcome);
                if (choiceResult.outcome === 'dismissed') {
                    console.log('User cancelled installation');
                } else {
                    console.log('User added to home screen');
                }
            });
            deferredPrompt = null;
        }
    }
}
